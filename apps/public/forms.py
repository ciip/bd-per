import operator
from django import forms
from django.db.models import Q
from pper.models import Contenu

class SearchForm(forms.Form):
    search = forms.CharField(label="")

    def do_search(self, query):
        or_queries = [Q(texte__icontains=bit) for bit in query.split()]
        qs = Contenu.objects.all()
        qs = qs.filter(reduce(operator.or_, or_queries))
        res = set()
        for contenu in qs:
            objs = contenu.usage()
            if objs:
                res = res.union(objs)
        return list(res)


