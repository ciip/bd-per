from django.urls import path, re_path

from pper.static_content import CAPACITES_TRANS_UID
from . import views


urlpatterns = [
    path('', views.index, name='home-public'),
    path('discipline/<slug:disc_slug>/', views.discipline, {'published': False}, name='discipline-public'),
    path('comgen/<slug:dom_abrev>/<slug:slug>/', views.comgen,
        name='commentaires-generaux-public'),
    path('tableau/<int:tableau_uid>/', views.tableau, name='tableau-public'),
    path('tableau/<int:tableau_id>/flat', views.tableau_flat, name='tableau-flat-public'),
    re_path(r'^objectif/(?P<spec_code>[\w&\-]+)/$', views.specification, {'published': False},
        name='course-public'),
    path('content/cap-trans/', views.content_by_uid, {'content_uid': CAPACITES_TRANS_UID},
        name='cap-trans-public'),
    path('content/byuid/<int:content_uid>/', views.content_by_uid, name='content_by_uid-public'),
    path('search/', views.search, name='search-public'),
]
