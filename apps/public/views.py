from collections import OrderedDict, defaultdict

from django.core.paginator import Paginator
from django.db.models import Q
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from pper.models import Contenu, Domain, Discipline, Specification, Tableau, Version
from pper import filters
from pper.static_content import CAPACITES_TRANS_UID
from public.forms import SearchForm
from public.utils import get_spec_neighbours

content_filters = filters.FilterChain([
    filters.HTMLNiveauContentFilter,
    filters.HTMLComposanteContentFilter,
    filters.HTMLLexiqueFilter,
    filters.HTMLImageFilter,
    filters.HTMLPubDomainLinkFilter,
    filters.HTMLPubSpecLinkFilter,
    filters.HTMLPubTitleFilter,
])

def index(request):
    domains = Domain.objects.all()
    context = {
        'domains'           : domains,
        'domains_without_fg': [dom for dom in domains if dom.abrev!='FG'],
        'domain_fg'         : Domain.objects.get(abrev='FG'),
        'spec_cant'         : Discipline.disciplines_cantonales(),
        'cap_trans_url'     : reverse('content_by_uid-public', args=[CAPACITES_TRANS_UID]),
    }
    return render(request, 'public/index.html', context)


def discipline(request, disc_slug, published=True):
    discipline = get_object_or_404(Discipline, slug=disc_slug)
    context = build_disc_context(discipline, published=published)
    context['base'] = "public/base.html"
    return render(request, 'public/discipline.html', context)


class Cycle:
    def __init__(self, s):
        self.s = s
        self.empty = True
    def __repr__(self):
        return "<Cycle %s>" % self.s
    def __str__(self):
        return self.s


def build_disc_context(discipline, published=True):
    """ Create matricial structure :
    [
      ['',    <them>, <them>, ...]
      ['I',   None,   None, ...]
      ['II',  <spec>, <spec>, ...]
      ['III', <spec>, <spec>, ...]
    ]
    """
    links = defaultdict(list)
    #TODO: use the new discipline.build_matrix()
    domain = discipline.domain
    thematiques = list(domain.thematique_set.order_by('order_no'))
    base_qs = Specification.objects.filter(status='published') if published else Specification.objects.all()
    versions = base_qs.filter(disciplines=discipline).order_by('version__weight').values_list('version', flat=True).distinct()
    versions = [Version.objects.get(pk=v) if v is not None else None for v in versions]
    if len(versions) > 1:
        versions = [v for v in versions if v is not None]

    def build_basic_struct():
        th_struct = [ [''], [Cycle('I')], [Cycle('II')], [Cycle('III')] ]
        th_struct[0].extend(thematiques)
        for th in th_struct[0][1:]:
            th.inactive = True
            th.discipline = th.get_disciplines()[0]
        for cycle in (1, 2, 3):
            th_struct[cycle].extend([None] * len(thematiques))
        return th_struct

    vers_structs = OrderedDict()
    for version in versions:
        th_struct = build_basic_struct()

        # Fill structure with specs
        specs = base_qs.filter(
            disciplines=discipline,
        ).filter(Q(version=version) | Q(version__isnull=True)).select_related('objectif')
        if discipline.name == "Économie familiale":
            # This is an exception unmanageable with the current structure (specs linked to 2 disciplines)
            specs = list(specs) + list(Specification.objects.filter(code__in=('CM 35', 'CM 36')))
            domain.visee += "\nAgir en consommateur responsable et assurer la gestion d'un ménage en respectant l'environnement, en connaissant ses besoins physiologiques et nutritionnels et en préservant son capital santé."
        for spec in specs:
            # For example, L 11-12 is linked to two thematiques
            for i, th in enumerate(spec.objectif.thematiques.all().order_by('order_no')):
                th_idx = thematiques.index(th) + 1
                th_struct[spec.objectif.cycle][th_idx] = spec
                if i > 0:
                    # Two adjacents thematics with same spec are merged into one 'cell'
                    th_struct[spec.objectif.cycle][th_idx].colspan = True
                th_struct[0][th_idx].inactive = False
                th_struct[spec.objectif.cycle][0].empty = False

            # Build/check 'links' between objectifs
            for linked_o in spec.get_linked_objectifs():
                links[int(spec.objectif.id)].append(int(linked_o.id))
                for th in linked_o.thematiques.all():
                    try:
                        th_index = thematiques.index(th) + 1
                    except ValueError:
                        # Case where th is in another domain (Mitic/Francais)
                        continue
                    if th_struct[linked_o.cycle][th_index] is None:
                        th_struct[linked_o.cycle][th_index] = {
                            'objectif': linked_o, 'code_display': linked_o.code, 'code_slug': linked_o.code_slug
                        }
                        th_struct[0][th_index].inactive = False
        vers_structs[version] = th_struct

    other_disciplines = list(domain.discipline_set.exclude(pk=discipline.pk).exclude(published=False))
    cant_disciplines = list(Discipline.disciplines_cantonales())
    for d in other_disciplines:
        d.is_cant = bool(d in cant_disciplines)

    context = {
        'domain': domain,
        'com_gen': list(domain.get_comgen_objects()),
        'discipline': discipline,
        'other_discs': other_disciplines,
        'cycles':     (1,2,3),
        'matrixes':   vers_structs,
        'td_width':   (100 - 4) / len(thematiques),
        'links':      dict(links),
    }
    return context

def comgen(request, dom_abrev, slug):
    dom = Domain.objects.get(abrev__iexact=dom_abrev)
    cg = dom.get_comgen_objects().get(slug=slug)
    return tableau_flat(request, cg.id)

def tableau(request, tableau_uid):
    tableau = get_object_or_404(Tableau, uid=tableau_uid)
    context = {
        'tabl_obj'  : tableau,
        'cols'      : range(0, tableau.get_nbcols()),
        'col_width' : 100/tableau.get_nbcols(),
        'tableau'   : tableau.get_celltablestruct(editable=False),
    }
    return render(request, 'public/tableau.html', context)

def tableau_flat(request, tableau_id):
    tableau = get_object_or_404(Tableau, pk=tableau_id)
    context = {
        'tableau'     : tableau,
        'flat_content': tableau.get_flat_content(content_filters.initialize()),
        'base'        : "public/base.html",
    }
    return render(request, 'public/tableau-flat.html', context)


def specification(request, spec_code, published=True):
    """ spec_code is the specification code (not always equal to objectif code) """
    specs = Specification.objects.filter(code=unslug_code(spec_code)).order_by('version__weight')
    if published:
        specs = specs.filter(status='published')
    if not len(specs):
        # e.g. SHS_33G/SHS_33H
        specs = Specification.objects.filter(status='published', objectif__code=unslug_code(spec_code))
        if not len(specs):
            raise Http404
    context = build_spec_context(specs, content_filters)
    context['base'] = "public/base.html"
    return render(request, 'public/specification.html', context)


def build_spec_context(specs, content_filters):
    """ Separate context building from rendering, as it is also used in pper app to create context for getting 
        pure template content to upload to pper """
    # Generally specs contains only one spec, but in some cases (SHS_33 ->G/H)
    #  there are two (one for each different discipline)
    spec = specs[0]
    neighbours = get_spec_neighbours(spec)
    try:
        # FIXME: handle FG special case
        comgen = spec.objectif.domain.get_comgen_objects()[0]
    except (KeyError, IndexError):
        comgen = None
    context = {
        'spec'     : spec,
        'domain'   : spec.objectif.domain,
        'disciplines': spec.disciplines.all(),
        'comgen'   : comgen,
        'neighbours': neighbours,
        'linked_objs': spec.get_linked_objectifs(reverse=True),
        'composantes' : spec.objectif.composante_set.all().order_by('letter'),
        'headers'  : spec.get_headers(),
        'col_width': 100 / spec.get_nbcols(),
        'contents' : [],
    }

    for i, spec in enumerate(specs):
        if i > 0 and spec.fake:
            continue
        filters = content_filters.initialize()
        spec_content = {
            'spec': spec,
            'content' : spec.get_celltablestruct(editable=False, filters=filters),
            'pre_content'  : spec.pre_content and spec.pre_content.render(filters, {'objectif': spec.objectif}) or "",
            'post_content' : spec.post_content and spec.post_content.render(filters, {'objectif': spec.objectif}) or "",
            'pre_tableaux' : [],
            'post_tableaux': [],
        }

        for j, tbl in enumerate(spec.objectif.pre_tableaux(spec.disciplines.all())):
            spec_content['pre_tableaux'].append({
                'title': tbl.title, 'cols': range(0, tbl.get_nbcols()), 'col_width' : 100/tbl.get_nbcols(),
                'init_vis': tbl.init_visible,
                'content':tbl.get_celltablestruct(editable=False, filters=filters)
            })
        for tbl in spec.objectif.tableaux.filter(position__gte=0):
            spec_content['post_tableaux'].append({
                'title': tbl.title, 'cols': range(0, tbl.get_nbcols()), 'col_width' : 100/tbl.get_nbcols(),
                'init_vis': tbl.init_visible,
                'content':tbl.get_celltablestruct(editable=False, filters=filters)
            })
        context['contents'].append(spec_content)
    if len(specs) > 1:
        context['tabs'] = [cont['spec'].version for cont in context['contents'] if cont['spec'].version]
    return context


def content_by_uid(request, content_uid):
    content = get_object_or_404(Contenu, uid=content_uid)
    filters = content_filters.initialize()
    context = {
        'content': content.render(filters, {}),
    }
    return render(request, 'public/content.html', context)


def search(request):
    form = SearchForm(data=request.GET or None)
    result_count, paginator = None, None
    if request.method == "GET" and request.GET.get('search', None):
        if form.is_valid():
            results = form.do_search(request.GET.get('search'))
            paginator = Paginator(results, 15)
            result_count = paginator.count
    # paginate results
    page_num = 1
    context = {
        'search_form': form,
        'total_res'  : result_count,
        'result_list': paginator and paginator.page(page_num).object_list or "",
    }
    return render(request, 'public/search.html', context)
    """context = {
        'search_form': form,
        'qs'         : request.META['QUERY_STRING'], # TODO remove existing p (.replace('&p=',''),
        'total'      : pg.count,
        'results'    : pg.page(nump),
        'editable'   : global_editable(request.user),
    }
    return render(request, 'search.html', context)"""

def unslug_code(code):
    """ Codes are often simplified to be used as slug in urls
        This function does the reverse to get a full code again
    """
    return code.replace("_", " ").replace("ACM", "AC&M")
