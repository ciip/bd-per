from django.urls import path, re_path

from ressource import views as rviews
from . import views


urlpatterns = [
    path('home', views.index, name='home-public'),
    path('web/guest/<slug:disc_slug>/', views.discipline, {'published': True}, name='discipline-public'),
    re_path(r'^web/guest/(?P<spec_code>[\w&\-]+)/$', views.specification, {'published': True}, name='course-public'),
    path('web/guest/capacites-transversales/', views.index, name='cap-trans-public'), # view function not used (dummy)
    path('web/guest/<slug:dom_abrev>/<slug:slug>/', views.index, name='commentaires-generaux-public'),

    path('ressources/groupe/<int:pk>/', rviews.ResourceGroupDisplay.as_view(), name='resource_grp_display'),
]
