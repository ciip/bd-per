# Utility functions put here to not encumber too much pper/models

import re
from pper.models import Specification

code_regex = re.compile(r'(?P<dom>A|CM|FG|MSN|SHS|L|L1|L2|L3|EN) (?P<lev>\d)(?P<num1>\d)(?P<rest>[^ ]*) ?(?P<disc>AV|Mu|AC&M)?')

def get_spec_neighbours(spec):
    def get_or_none(code, ordering):
        if code is None:
            return None
        sps = Specification.objects.select_related('objectif'
            ).filter(disciplines__in=spec.disciplines.all(), objectif__code__regex=code)
        if spec.disciplines.values_list('name', flat=True) == ["Économie familiale"]:
            sps = list(sps) + list(
                Specification.objects.select_related('objectif'
                    ).filter(disciplines__name='Éducation nutritionnelle', objectif__code__regex=code)
            )
        if len(sps):
            # Better sort here because of exceptions
            sp_dict = dict([(sp.code.replace('L ', 'L1 '), sp) for sp in sps])
            code_list = sorted(sp_dict.keys(), reverse = ordering.startswith('-'))
            return sp_dict[code_list[0]]
        else:
            return None
    m = code_regex.match(spec.objectif.code)
    nbrs = {
        'left'  : get_or_none(shift_code(m, 0, -1), '-code'),
        'right' : get_or_none(shift_code(m, 0, 1), 'code'),
        'top'   : get_or_none(shift_code(m, -1, 0), '-code'),
        'bottom': get_or_none(shift_code(m, 1, 0), 'code'),
    }
    if nbrs.values() == [None, None, None, None]:
        return None
    return nbrs

def shift_code(match, level_offset, th_offset):
    res = None
    if not match:
        return None

    dom = match.group('dom')
    if dom in ('L', 'L1'):
        dom = '(L|L1)'
    th_num = int(match.group('num1'))
    if th_num + th_offset > 9 or th_num + th_offset < 1:
        return None
    if th_offset > 0:
        num_expr = '[%d-9]' % (th_num+1,)
    elif th_offset < 0:
        num_expr = '[1-%d]' % (th_num-1,)
    else:
        num_expr = th_num
    try:
        res = "%s %d%s" % (dom, int(match.group('lev'))+level_offset, num_expr)
        if match.group('disc'):
            res = "%s %s" % (res, match.group('disc'))
        res = r"^%s" % res
    except Exception:
        pass
    return res
