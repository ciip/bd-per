import random

import qrcode
import qrcode.image.svg

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import IntegrityError, models, transaction

KEY_CHARS = 'abcdefghjkmnpqrstuvwxyz23456789'
SHORT_DOMAIN = 'ciip-rn.ch'


class ShortLink(models.Model):
    key = models.CharField("Clé du lien court", max_length=5)
    # Generic link to target object
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey()
    # Or arbitrary URL
    url = models.URLField("Cible du lien (si pas d’objet)", blank=True)

    class Meta:
        constraints = [models.UniqueConstraint(fields=['key'], name='key_unique')]

    def __str__(self):
        return "Lien court «%s» vers «%s»" % (self.short_url(), self.url or self.content_object.get_absolute_url())

    @classmethod
    def get_for_object(cls, obj, create=False):
        try:
            return ShortLink.objects.get(object_id=obj.pk, content_type=ContentType.objects.get_for_model(obj))
        except ShortLink.DoesNotExist:
            if create:
                while True:
                    try:
                        with transaction.atomic():
                            if obj.__class__.__name__ == 'Resource':
                                obj.save(index=False)  # Just update the Resource.updated field
                            return ShortLink.objects.create(
                                key=''.join(random.choices(KEY_CHARS, k=4)),
                                object_id=obj.pk,
                                content_type=ContentType.objects.get_for_model(obj)
                            )
                    except IntegrityError:
                        pass
            raise

    def qr_image(self, format='png'):
        factory = qrcode.image.svg.SvgPathImage if format == 'svg' else None
        # Error correction Medium (~15% error), Q is ~25%
        # See https://www.qrcode.com/en/about/version.html
        return qrcode.make(
            self.short_url(with_prefix=True), version=2, image_factory=factory,
            error_correction=qrcode.constants.ERROR_CORRECT_M,
        )

    def short_url(self, with_prefix=False):
        if with_prefix:
            return f"https://{SHORT_DOMAIN}/{self.key}"
        return f"{SHORT_DOMAIN}/{self.key}"

    def long_url(self):
        if self.url:
            return self.url
        try:
            path = self.content_object.get_absolute_url(from_short_link=True)
        except TypeError:
            path = self.content_object.get_absolute_url()
        return f'https://{settings.SITE_DOMAIN}{path}'
