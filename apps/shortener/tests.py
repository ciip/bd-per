from datetime import date, timedelta

from django.test import TestCase
from django.utils.timezone import now

from ressource.models import Resource
from shortener.models import SHORT_DOMAIN, ShortLink


class ShortenerTests(TestCase):
    def test_no_key(self):
        response = self.client.get('/', HTTP_HOST=SHORT_DOMAIN)
        self.assertEqual(response.url, 'https://bdper.plandetudes.ch/ressources/')

    def test_for_resource(self):
        res = Resource.objects.create(
            title="Une ressource", url="https://www.example.org", status='validee',
        )
        Resource.objects.filter(pk=res.pk).update(updated=now()-timedelta(days=3))
        res.refresh_from_db()
        short_url = ShortLink.get_for_object(res, create=True).short_url()
        self.assertTrue(short_url.startswith(SHORT_DOMAIN))
        response = self.client.get('/' + short_url.split('/')[1], follows=True, HTTP_HOST=SHORT_DOMAIN)
        self.assertEqual(response.url, f'https://bdper.plandetudes.ch/ressources/{res.pk}/')
        res.refresh_from_db()
        self.assertEqual(res.updated.date(), date.today())

    def test_for_resource_appr(self):
        res = Resource.objects.create(
            title="Une ressource élève", url="https://www.example.org", status='validee',
            descr_eleve='Descriptif élève', target_gr=['teacher', 'learner'],
        )
        short_url = ShortLink.get_for_object(res, create=True).short_url()
        self.assertTrue(short_url.startswith(SHORT_DOMAIN))
        response = self.client.get('/' + short_url.split('/')[1], follows=True, HTTP_HOST=SHORT_DOMAIN)
        self.assertEqual(response.url, f'https://bdper.plandetudes.ch/ressources/{res.pk}/appr/')

    def test_arbitrary_url(self):
        short = ShortLink.objects.create(key='5678', url='http://www.example.org')
        response = self.client.get(f'/{short.key}', follows=True, HTTP_HOST=SHORT_DOMAIN)
        self.assertEqual(response.url, f'http://www.example.org')

    def test_fallback_case(self):
        short = ShortLink.objects.create(key='ABcd', url='http://www.example.org')
        response = self.client.get(f'/{short.key.upper()}', HTTP_HOST=SHORT_DOMAIN)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, 'http://www.example.org')
