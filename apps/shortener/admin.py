from django import forms
from django.contrib import admin

from .models import ShortLink


class ShortLinkForm(forms.ModelForm):
    class Meta:
        model = ShortLink
        fields = '__all__'

    def clean(self):
        data = super().clean()
        if not data['url'] and (not data['content_type'] or not data['object_id']):
            raise forms.ValidationError("Vous devez indiquer soit une URL, soit un objet")
        if data['url'] and (data['content_type'] or data['object_id']):
            raise forms.ValidationError("Vous ne pouvez pas indiquer à la fois une URL et un objet")
        return data


@admin.register(ShortLink)
class ShortLinkAdmin(admin.ModelAdmin):
    list_display = ['key', 'content_type', 'object_id']
    search_fields = ['key']
    form = ShortLinkForm
