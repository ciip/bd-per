from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('shortener', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shortlink',
            name='url',
            field=models.URLField(blank=True, verbose_name='Cible du lien (si pas d’objet)'),
        ),
        migrations.AlterField(
            model_name='shortlink',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, to='contenttypes.contenttype'),
        ),
        migrations.AlterField(
            model_name='shortlink',
            name='object_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
