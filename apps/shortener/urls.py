from django.urls import path, re_path
from django.views.generic import RedirectView

from .views import redirect

urlpatterns = [
    path('', RedirectView.as_view(url='https://bdper.plandetudes.ch/ressources/')),
    re_path(r'^(?P<key>[a-zA-Z1-9]{4})$', redirect, name='redir'),
]
