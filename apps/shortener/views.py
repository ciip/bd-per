from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

from .models import ShortLink


def redirect(self, key):
    try:
        short = ShortLink.objects.get(key=key)
    except ShortLink.DoesNotExist:
        short = get_object_or_404(ShortLink, key__iexact=key)
    return HttpResponseRedirect(short.long_url())
