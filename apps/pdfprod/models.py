from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, Table, TableStyle, BaseDocTemplate, PageTemplate, Frame, PageBreak
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.rl_config import defaultPageSize
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm
from reportlab.lib.colors import Color, HexColor
from reportlab.lib.enums import TA_CENTER

left_margin = cm * 2.5
right_margin = cm * 2.5
top_margin = cm * 1.5
bottom_margin = cm * 1.5

class PerDocTemplate(BaseDocTemplate):
    def afterFlowable(self, flowable):
        """ Registers TOC entries. toc_text is a custom attribute added to some flowables during document story building. """
        if hasattr(flowable, 'toc_text'):
            self.notify('TOCEntry', (0, flowable.toc_text, self.page))

    def build(self, flowables, filename=None, canvasmaker=canvas.Canvas):
        self._calc()    #in case we changed margins sizes etc

        # cannot call super as BaseDocTemplate is old-style class
        BaseDocTemplate.build(self, flowables, filename, canvasmaker=canvasmaker)

class PDFDocument:

    def __init__(self, io, specs):
        self.doc = PerDocTemplate(io, topMargin=cm*1.7, bottomMargin=cm*1.7)
        basic_frame = Frame(self.doc.leftMargin, self.doc.bottomMargin, self.doc.width, self.doc.height, id='normal')
        pt = PageTemplate(id="basic", frames=basic_frame, onPage=self.header_footer)
        self.doc.addPageTemplates([pt])

        self.specs = specs
        self.page_height = defaultPageSize[1]
        self.page_width  = defaultPageSize[0]

    def header_footer(self, canvas, doc):
        canvas.saveState()
        # Header
        canvas.setFont('Helvetica',10)
        canvas.setStrokeColor(HexColor(0xc20e2c))
        canvas.setFillColor(HexColor(0xc20e2c))
        canvas.drawString(left_margin, self.page_height-top_margin, "PLAN D'ÉTUDES ROMAND")
        canvas.setFont('Helvetica',12)
        canvas.drawRightString(self.page_width-right_margin, self.page_height-top_margin, self.specs[0].objectif.domain.name.upper())
        # Footer
        canvas.setFont('Helvetica',7)
        canvas.drawString(left_margin, bottom_margin, "© CIIP 2009")
        canvas.setFont('Helvetica',12)
        canvas.drawRightString(self.page_width-right_margin, bottom_margin, str(doc.page))
        canvas.restoreState()

    def produce(self, with_toc=False):
        styles = getSampleStyleSheet()
        styleN = styles['Normal']
        styleH = styles['Heading1']
        styleH1 = ParagraphStyle('H1', fontName="Helvetica", fontSize=12, textColor=Color(1,1,1), alignment=TA_CENTER,
                                 spaceBefore=12, spaceAfter=12)
        styleComp = ParagraphStyle('Comp', fontName="Helvetica", fontSize=10)
        ts = TableStyle([('BACKGROUND', (0,0),(3,1), HexColor(0xc20e2c)),
                         ('BACKGROUND', (0,1),(3,1), HexColor(0xe9b7a7)),
                         ('SPAN', (0,0), (3,0)),
                         ('SPAN', (0,1), (3,1)),
                         ('SPAN', (0,2), (1,2)),
                         ('GRID', (0,0), (-1,-1), 1, HexColor(0xce5248)),
                         ('ALIGN', (0,0), (0,1), 'CENTER'),])
        story = []
        if with_toc:
            toc = TableOfContents()
            """toc.levelStyles = [
                ParagraphStyle(fontName='Times-Bold', fontSize=14, name='TOCHeading1',
                               leftIndent=20, firstLineIndent=-20, spaceBefore=5, leading=16),
                ParagraphStyle(fontSize=12, name='TOCHeading2',
                               leftIndent=40, firstLineIndent=-20, spaceBefore=0, leading=12),
            ]"""
            story.append(Paragraph(u"Table des matières", styleH))
            story.append(toc)
            story.append(PageBreak())
        #add some flowables
        for i, spec in enumerate(self.specs):
            if i > 0:
                story.append(PageBreak())
            composantes_pars = [Paragraph(comp.title, styleComp) for comp in spec.objectif.composante_set.all()]
            composantes_pars.insert(0, Paragraph(spec.objectif.title,styleComp))
            data = [[Paragraph(u'%s - cycle %d<br/>%s - thématique' % (spec.discipline.name, spec.objectif.cycle, spec.code), styleH1),'','',''],
                    [composantes_pars,'','',''],
                    [Paragraph('Progression des apprentissages', styleComp),'',Paragraph('Attentes fondamentales', styleComp),Paragraph('Indications pédagogiques', styleComp)],
                   ]
            t = Table(data, (self.page_width-left_margin-right_margin)/4.0)
            t.toc_text = spec.code
            t.setStyle(ts)
            story.append(t)
        self.doc.multiBuild(story)

