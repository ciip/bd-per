from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from pper.models import Specification, Discipline
from pdfprod.models import PDFDocument

def course_pdf(request, course_id):
    course = get_object_or_404(Specification, pk=course_id)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=somefilename.pdf'
    
    doc = PDFDocument(response, [course])
    doc.produce()
    return response

def discipline_pdf(request, disc_id):
    discipline = get_object_or_404(Discipline, pk=disc_id)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=somefilename.pdf'

    doc = PDFDocument(response, discipline.specification_set.all())
    doc.produce(with_toc=True)
    return response

