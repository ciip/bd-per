from django.conf import settings
from django.core import serializers
from django.db import models
from django.db.models.signals import pre_save, post_save, pre_delete

from journal.middleware import get_current_user

OP_CHOICES = (
    ('add', 'addition'),
    ('mod', 'modification'),
    ('del', 'suppression'),
)

class Journal(models.Model):
    """ This model serves as a general journalisation mechanism to log
        modifications to all app's models. """
    
    user  = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    stamp = models.DateTimeField(auto_now_add=True)
    model = models.CharField(max_length=25)
    object_id = models.IntegerField()
    op_type   = models.CharField(max_length=5, choices=OP_CHOICES)
    old_content = models.TextField(null=True, blank=True)
    new_content = models.TextField(null=True, blank=True)
    class Meta:
        db_table = 't_journal'

    MODELS_TO_LOG = []

    def __str__(self):
        return u"%s by %s on %s" % (self.get_op_type_display(), self.user and self.user.username or "nobody", self.stamp)

def register(models):
    Journal.MODELS_TO_LOG.extend(models)

#MODELS_TO_LOG = (Domain, Discipline, Thematique, Objectif, Composante, Contenu, Specification, Cellule, Tableau, Lexique)
def log_modifications(sender, instance, signal, **kwargs):
    if instance.__class__ not in Journal.MODELS_TO_LOG or not instance.pk:
        return
    user = get_current_user()
    if user is None or user.is_anonymous:
        # Typically in tests
        return
    old_instance = instance.__class__.objects.get(pk=instance.pk)
    modified_fields = []
    for field in instance._meta.fields:
        if getattr(old_instance, field.name) != getattr(instance, field.name):
            modified_fields.append(field.name)
    if not modified_fields:
        return
    old_data = serializers.serialize("json", [old_instance], fields=modified_fields)
    new_data = serializers.serialize("json", [instance], fields=modified_fields)
    log = Journal(user = user,
                  model = instance.__class__.__name__,
                  object_id = instance.pk,
                  op_type = 'mod',
                  old_content = old_data,
                  new_content = new_data)
    log.save()

def log_additions(sender, instance, created, **kwargs):
    if instance.__class__ not in Journal.MODELS_TO_LOG or not created:
        return
    user = get_current_user()
    if user is None or user.is_anonymous:
        # Typically in tests
        return
    data = serializers.serialize("json", [instance])
    log = Journal(user = user,
                  model = instance.__class__.__name__,
                  object_id = instance.pk,
                  op_type = 'add',
                  new_content = data)
    log.save()

def log_deletions(sender, instance, **kwargs):
    if instance.__class__ not in Journal.MODELS_TO_LOG:
        return
    user = get_current_user()
    data = serializers.serialize("json", [instance])
    log = Journal(user = user,
                  model = instance.__class__.__name__,
                  object_id = instance.pk,
                  op_type = 'del',
                  old_content = data)
    log.save()

pre_save.connect(log_modifications)
post_save.connect(log_additions)
pre_delete.connect(log_deletions)

