from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Journal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('model', models.CharField(max_length=25)),
                ('object_id', models.IntegerField()),
                ('op_type', models.CharField(max_length=5, choices=[('add', 'addition'), ('mod', 'modification'), ('del', 'suppression')])),
                ('old_content', models.TextField(null=True, blank=True)),
                ('new_content', models.TextField(null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 't_journal',
            },
        ),
    ]
