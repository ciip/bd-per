from django.contrib import admin

from journal.models import Journal

class JournalAdmin(admin.ModelAdmin):
    ordering = ('-stamp',)

admin.site.register(Journal, JournalAdmin)

