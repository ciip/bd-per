# Django settings for dj-pper project.
import os
from django.conf.global_settings import STORAGES

DEBUG = True
DEBUG_TOOLBAR = False
STATIC_SERVE = True

# Project-specific settings
IS_TEST = True
LOCKED = False

BSN_API_URL = 'https://dsb-api.educa.ch'
BSN_USERNAME = 'claude@2xlibre.net'
BSN_PRIVATE_KEY_FILE = ''
BSN_PRIVATE_KEY_PASSWORD = ''

PORTAIL_CIIP_API = {
    'dev': {'URL': 'https://dev-per.ciip.ch/api', 'TOKEN': ''},
    'staging': {'URL': 'https://staging-per.ciip.ch/api', 'TOKEN': ''},
    'prod': {'URL': 'https://per.ciip.ch/api', 'TOKEN': ''},
}

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

ADMINS = (
    ('Claude Paroz', 'claude@2xlibre.net'),
)
SERVER_EMAIL = 'nepasrepondre@ne.ch'

MANAGERS = ADMINS
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'pper',
        'USER': '',
        'PASSWORD': '',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

USE_TZ = True
TIME_ZONE = 'Europe/Zurich'
LANGUAGE_CODE = 'fr-FR'
USE_I18N = True
ALLOWED_HOSTS=['bdper.plandetudes.ch', 'ciip-rn.ch']

# Absolute path to the directory that holds media.
MEDIA_ROOT = os.path.join(BASE_DIR, 'uploads')
MEDIA_URL = '/uploads/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'assets/dist')]
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
# or :
#STATIC_URL = 'http://www.consultation-per.ch/bd-per/static/'

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [os.path.join(BASE_DIR, 'templates'),],
    'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': [
            'django.contrib.auth.context_processors.auth',
            'django.template.context_processors.debug',
            'django.template.context_processors.media',
            'django.template.context_processors.request',
            'django.contrib.messages.context_processors.messages',
            'pper.context_processors.common',
        ],
        'builtins': ['django.templatetags.static'],
    },

}]


MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'api.middleware.JWTAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'journal.middleware.ThreadLocals', 
    'common.middleware.LoginRequiredMiddleware',
]
if DEBUG_TOOLBAR:
    MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

AUTHENTICATION_BACKENDS = [
    'pper.backends.ShibbolethRemoteBackend',
    'pper.backends.TokenUserBackend',
    'django.contrib.auth.backends.ModelBackend',
]
AUTH_USER_MODEL = 'auth.User'

LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/"

ROOT_URLCONF = 'common.urls'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',
    # Third-parties
    'mptt',
    'corsheaders',
    'linkcheck',
    'tinymce',
    # Our apps
    'journal',
    'ressource',
    'pper',
    #'pper_plat',
    'pper_upload',
    #'pdfprod',
    'public',
    'api',
    'shortener',
    'eprocom',
]

INTERNAL_IPS = ['127.0.0.1']

STORAGES = {
    **STORAGES,
    'upload': {
        'BACKEND': 'ressource.storage.OverwriteStorage',
    },
    'upload-s3': {
        'BACKEND': 'ressource.storage.OverwriteStorage',
    },
}

DATA_UPLOAD_MAX_NUMBER_FIELDS = 4000

# Transitional setting (until 6.0)
FORMS_URLFIELD_ASSUME_HTTPS = True

"""
Example of mixed FS/S3 storage:
    'upload-s3': {
        'BACKEND': 'ressource.storage.FallbackStorage',
        'OPTIONS': {
            'ENDPOINT': 'https://sos-ch-gva-2.exo.io',
            'REGION_NAME': 'ch-gva-2',
            'BUCKET': 'ciip-bdper-prod',
            'KEY': '',
            'SECRET': '',
        },
    }
"""
S3_PUBLIC_URL = 'https://{bucket}.sos-ch-gva-2.exo.io'

CORS_ORIGIN_ALLOW_ALL = True

TINYMCE_DEFAULT_CONFIG = {
    'theme': "silver",
    'width': '100%',
    'height': 190,
    'menubar': False,
    'plugins': 'link code paste',
    'paste_as_text': True,
    'target_list': [
        {'title': 'None', 'value': ''}
    ],
    'toolbar': 'bold italic superscript | link unlink | undo redo code',
    'entity_encoding': "raw",
    # When printing PDFs, we need absolute URLs
    'relative_urls': False,
    'convert_urls': False,
}

# Email addresses in this list will receive email when people suggest new resources
RESOURCE_ADMINS = ['CIIP.support-rn@ne.ch']

#LINKCHECK_SITE_DOMAINS = ['bdper.plandetudes.ch']
SITE_DOMAIN = 'bdper.plandetudes.ch'
LINKCHECK_EXTERNAL_RECHECK_INTERVAL = 20160  # 2 weeks

VOD_INFOMANIAK_TOKEN = ''

EPROCOM_TOKEN = ''  # Token pour accéder à l'API EPROCOM

SILENCE_TOKEN = ''  # Token pour accéder à la plate-forme Silence On tourne

if DEBUG_TOOLBAR:
    INSTALLED_APPS.append('debug_toolbar')

from .local_settings import *
