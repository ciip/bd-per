import os
import sys

UPGRADING = False

project_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
apps_dir    = os.path.join(project_dir, "apps")

sys.path.append(apps_dir)

def upgrade_in_progress(environ, start_response):
    upgrade_file = os.path.join(project_dir, 'templates', '503.html')
    if os.path.exists(upgrade_file):
        response_headers = [('Content-type','text/html')]
        response = open(upgrade_file, mode='rb').read()
    else:
        response_headers = [('Content-type','text/plain')]
        response = "Ce site est temporairement en mode maintenance, merci de patienter et de revenir dans quelques minutes.".encode('utf-8')

    if environ['REQUEST_METHOD'] == 'GET':
        status = '200 OK'
    else:
        status = '403 Forbidden'
    start_response(status, response_headers)
    return [response]

if UPGRADING:
    application = upgrade_in_progress
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'common.settings'

    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()

