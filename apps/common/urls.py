from django.contrib import admin
from django.contrib.auth.decorators import permission_required
from django.conf import settings
from django.urls import include, path, re_path, register_converter
from django.urls.converters import IntConverter
from django.views.decorators.cache import never_cache
from django.views.generic import RedirectView, TemplateView
from django.views.static import serve

from ratelimit.decorators import ratelimit

from pper import views
from ressource import views as rviews
from pper_upload import views as upviews
from pdfprod import views as pdfviews

admin.site.index_template= "admin/my_index.html"


class FullIntegerConverter(IntConverter):
    regex = '[-]?[0-9]+'


register_converter(FullIntegerConverter, 'fint')

urlpatterns = [
    path('admin/linkcheck/', include('linkcheck.urls')),
    path('admin/', admin.site.urls),
    path('login/', ratelimit(key='ip', rate='5/3m', method=ratelimit.UNSAFE, block=True)(
        views.LoginView.as_view()
    ), name='login'),
    path('logout/', views.site_logout, name='logout'),
    path('confidentialite/', TemplateView.as_view(template_name='confidentialite.html'), name='confidentialite'),
    path('gestion/', include('pper.urls')),
    path('pub/', include('public.urls')), # namespace='public', app_name='public')),
    path('api/v1/', include('api.urls')),
    path('redir/', include('shortener.urls')),
    # Temporary limiting access to some admins
    path('eprocom/', include('eprocom.urls')),
    path('tinymce/', include('tinymce.urls')),

    path('', RedirectView.as_view(url='/ressources/')),
    # Must be public-accessible
    path('domain/colors.css', views.domain_colors, name='domain_colors'),
]

# Resource URLs
urlpatterns += [
    path('cell/<fint:cell_id>/resource/link/', rviews.ResourceLinkToCellView.as_view(),
        name='resource_link_to_cell'),
    path('resource/new/', rviews.ResourceEditView.as_view(op='new'),
        name='resource_new'),
    path('resource/<int:resource_id>/edit/',
        never_cache(rviews.ResourceEditView.as_view(op='edit')),
        name='resource_edit'),
    path('resource/<int:resource_id>/delete/', rviews.ResourceDeleteView.as_view(),
        name='resource_delete'),

    path('resource_grp/new/', rviews.ResourceGroupEditView.as_view(),
        name='resource_grp_new'),
    path('resource_grp/<int:group_id>/edit/',
        never_cache(rviews.ResourceGroupEditView.as_view()),
        name='resource_grp_edit'),
    path('resource_grp/<int:group_id>/link_resource/',
        rviews.ResourceLinkToGroupView.as_view(), name='resource_link'),
    path('resource_grp/<int:group_id>/unlink_resource/<int:resource_id>/',
        rviews.ResourceUnlinkFromGroupView.as_view(), name='resource_unlink'),
    path('resource_grp/<int:group_id>/unlink_all_resources/',
        rviews.ResourceUnlinkAllFromGroupView.as_view(), name='resource_unlinkall'),
    path('resource_grp/<int:group_id>/delete/', rviews.ResourceGroupDeleteView.as_view(),
        name='resource_grp_delete'),
    path('resource_grp/change_ordering/', rviews.ResourceGroupChangeOrderingView.as_view(),
        name='resource_grp_ordering'),
    # Delete/copy/cut views, object id passed in POST
    path('resource/unlinkcell/', rviews.ResourceUnlinkCellView.as_view(),
        name='resource_unlinkcell'),
    path('resource/copy/', rviews.ResourceCopyView.as_view(), name='resource_copy'),
    path('resource/cut/', rviews.ResourceCutView.as_view(), name='resource_cut'),
    path('resource/paste/<fint:cell_id>/', rviews.ResourcePasteView.as_view(),
        name='resource_paste'), # valide for either resource or resource_grp

    path('resource/subthemes/<int:group_id>/edit/', rviews.SubThemesEdit.as_view(), name='subthemes_edit'),
    re_path(r'^resource_grp/search/(?P<frmt>(json|html))/$', rviews.ResourceGroupSearch.as_view(),
        name='resource_grp_search'),

    # Unprotected, used in resource suggestion form
    re_path(r'^ressources/(?P<group_id>\d+)/search/(?P<frmt>(json|html))/$', rviews.ResourceSearch.as_view(),
        name='resource_search'),

    path('resource/export/', rviews.resource_export, name='resource_export'),

    # WARNING: for public consumption (linked from pper), do not change lightly!
    path('ressources/', rviews.ResourceHomeView.as_view(),
        name='resource_home'),
    path('ressources/nouvelle/', rviews.ResourceSuggestionView.as_view(),
        name='resource_new_public'),
    path('ressources/e-media/', rviews.ResourceHomeView.as_view(emedia=True),
        name='emedia_home'),
    path('ressources/temp/', rviews.ResourceHomeView.as_view(temp=True)),
    path('ressources/selection/', rviews.ResourceSelectedView.as_view(),
        name='resource_selected'),
    path('ressources/<int:pk>/', rviews.ResourceDisplay.as_view(),
        name='resource_display'),
    path('ressources/<int:pk>/appr/', rviews.ResourceDisplay.as_view(version='appr'),
        name='resource-display-appr'),
    path('ressources/<int:pk>/complet/',
        rviews.ResourceDisplay.as_view(template_name='ressource/resource_full.html'),
        name='resource_display_full'),
    path('ressources/<int:pk>/pdf/', rviews.ResourcePDFView.as_view(),
        name='resource_pdf'),
    path('ressources/<int:pk>/telecharger/', rviews.ResourceDownloadView.as_view(),
        name='resource_download'),
    path('ressources/<int:pk>/qrcode/', rviews.ResourceShortQRDownloadView.as_view(),
        name='resource_qr_download'),
    path('ressources/groupe/', rviews.ResourceGroupHome.as_view(), name='resource_grp_home'),
    path('ressources/groupe/<int:group_id>/pdf/', rviews.ResourceGroupPDFView.as_view(),
        name='resourcegroup_pdf'),

    path('ressources/<int:pk>/video/rts/<slug:vkey>/', rviews.AudioVideoView.as_view(player='rts'),
        name='video-rts'),
    path('ressources/<int:pk>/video/imk/', rviews.AudioVideoView.as_view(player='infomaniak'),
        name='video-imk'),
    path('ressources/<int:pk>/video/yt/<slug:vkey>/', rviews.AudioVideoView.as_view(player='youtube'),
        name='video-yt'),
    path('ressources/<int:pk>/video/dm/<slug:vkey>/', rviews.AudioVideoView.as_view(player='dailymotion'),
        name='video-dm'),
    path('ressources/<int:pk>/video/vm/<slug:vkey>/', rviews.AudioVideoView.as_view(player='vimeo'),
        name='video-vm'),
    path('ressources/<int:pk>/audio-local/', rviews.AudioVideoView.as_view(player='audio-local'),
        name='audio-local'),
    path('ressources/<int:pk>/galerie/', rviews.GalleryView.as_view(), name='gallery'),
    path('uploads/ressources/<int:pk>/<path:path>', rviews.ResourceFileView.as_view(), name='download'),

    # duplicated in public/pper_urls.py
    path('ressources/groupe/<int:pk>/', rviews.ResourceGroupDisplay.as_view(),
        name='resource_grp_display'),
    # Protected edition views
    path('ressources/<int:pk>/edit/', rviews.ResourceEditView.as_view(op='edit'),
        name='resource_edit_full'),
    path('ressources/<int:pk>/editurl/', rviews.ResourceEditUrlOnlyView.as_view(),
        name='resource_edit_urlonly'),

    path('ressources/<int:pk>/create_short/', rviews.ResourceCreateShortlinkView.as_view(),
        name='resource_create_shortlink'),

    # Quality
    path('ressources/qualite/', rviews.ResourceQualityListView.as_view(),
        name='resource_quality_list'),
    path('ressources/qualite/<int:pk>/', rviews.ResourceQualityView.as_view(),
        name='resource_quality'),

    path('ressources/stats/', rviews.StatsHomeView.as_view(), name='stats-home'),
    path('ressources/stats/<typ>/<int:year>/', rviews.StatsYearView.as_view(), name='stats-year'),
    path('ressources/stats/ping/', rviews.PingResourceAccessView.as_view(), name='stats-ping-resource'),

    # Cas particulier: resource nécessitant une authentification préalable + un token confidentiel
    path('protege/silence_on_tourne/',
        RedirectView.as_view(url=f'https://ontourne.eduge.ch/token/login/{settings.SILENCE_TOKEN}'),
        name='silence_on_tourne'),

    path('course/<int:course_id>/pper_upload/<str:target>/', upviews.portail_upload_course,
         name='portail_upload'),
]

urlpatterns += [
    path('course/<int:course_id>/pdf', pdfviews.course_pdf, name='course_pdf'),
    path('discipline/<int:disc_id>/pdf', pdfviews.discipline_pdf, name='discipline_pdf'),
]

if settings.DEBUG:
    try:
        import debug_toolbar
    except ImportError:
        pass
    else:
        urlpatterns = [
            path('__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns

if settings.DEBUG:
    urlpatterns += [
        path('uploads/<path:path>', view=serve,
             kwargs={'document_root': settings.MEDIA_ROOT}),
    ]
