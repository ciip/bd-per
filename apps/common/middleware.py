import re

from django.conf import settings
from django.http import HttpResponseForbidden, HttpResponseRedirect

EXEMPT_URLS = [
    re.compile(r'^$'),
    re.compile(r'^favicon.ico$'),
    re.compile(r'^confidentialite/?$'),
    re.compile(settings.LOGIN_URL.lstrip('/')),
    re.compile(r'^login-sso/$'),
    re.compile(r'^ressources/(.*)$'),
    re.compile(r'^uploads/(.*)$'),
    re.compile(r'^api/(.*)$'),
    re.compile(r'^domain/colors.css$'),
]


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.META.get('HTTP_HOST', '') == 'ciip-rn.ch':
            request.path_info = "/redir%s" % request.path_info
        elif not request.user.is_authenticated:
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect("%s?next=%s" % (settings.LOGIN_URL, request.path))
        if (
            re.match(r'^/?gestion/(.*)$', request.path_info)
            or re.match(r'^/?pub/(.*)$', request.path_info)
        ) and not request.user.has_perm('pper.view_contenu'):
            return HttpResponseForbidden("Désolé, vous n'avez pas les droits pour afficher cette page.")
        return self.get_response(request)
