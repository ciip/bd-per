from django.core.management.base import BaseCommand

from eprocom.models import Item, ItemImporter


class Command(BaseCommand):
    def handle(self, *args, **options):
        imp_count = 0
        for item_id in Item.objects.all().values_list('id', flat=True):
            imp = ItemImporter(item_id)
            imp.do_import()
            imp_count += 1
        print(f"{imp_count} items re-imported.")
