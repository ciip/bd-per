from django.contrib import admin
from django.utils.html import strip_tags

from .models import Item, Material, PageBlock, PERLink


class PERLinkInline(admin.StackedInline):
    model = PERLink


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'contenu_start', 'disc', 'type_op_cognitive']
    search_fields = ['titre', 'contenu']
    readonly_fields = ['fts']
    inlines = [PERLinkInline]

    def contenu_start(self, obj):
        return strip_tags(obj.contenu[:200])[:80] + '…'


@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'discipline', 'updated']
    list_filter = ['discipline']


@admin.register(PERLink)
class PERLinkAdmin(admin.ModelAdmin):
    list_display = ['item', 'obj_code', 'chapitre', 'progression']
    list_filter = ['obj_code']
    search_fields = ['chapitre', 'progression']


@admin.register(PageBlock)
class PageBlockAdmin(admin.ModelAdmin):
    pass
