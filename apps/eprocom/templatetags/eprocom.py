from django import template
from django.utils.safestring import SafeString

from eprocom.models import PageBlock

register = template.Library()


@register.simple_tag(takes_context=True)
def editable_block(context, block_slug):
    try:
        bloc = PageBlock.objects.get(slug=block_slug)
    except PageBlock.DoesNotExist:
        return SafeString("<p><em>Erreur, bloc de page non trouvé</em></p>")
    return bloc.render_html(context['user'])


@register.filter('startswith')
def startswith(text, starts):
    if isinstance(text, str):
        return text.startswith(starts)
    return False


@register.filter('endswith')
def endswith(text, starts):
    if isinstance(text, str):
        return text.endswith(starts)
    return False
