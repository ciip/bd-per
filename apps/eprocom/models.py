import base64
from datetime import date, timedelta

from bs4 import BeautifulSoup
from tempfile import NamedTemporaryFile

from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.core.files import File
from django.core.files.storage import default_storage
from django.db import models
from django.db.models import Prefetch, Value
from django.db.models.functions import Concat
from django.templatetags.static import static
from django.urls import reverse
from django.utils import timezone
from django.utils.html import strip_tags
from django.utils.safestring import SafeString

from pper.filters import FilterChain, LexiqueFilter
from pper.models import Contenu, ContenuCellule
from .eprocom_api import EprocomAPI

MAX_DAYS_FOR_NEW_LABEL = 300

CHAPTER_MAP = {
    # Français
    'raconte': {'title': "Le texte qui raconte"},
    'relate': {'title': "Le texte qui relate"},
    'argumente': {'title': "Le texte qui argumente"},
    'transmet': {'title': "Le texte qui transmet des savoirs"},
    'regle': {'title': "Le texte qui règle des comportements"},
    # MSN 21
    #'resolution': "Éléments pour la résolution de problèmes",
    'espace': {'title': "Espace"},
    'figures': {'title': "Figures géométriques planes et solides"},
    'transformations': {'title': "Transformations géométriques"},
    'reperage': {'title': "Repérage dans le plan et dans l'espace"},
    # MSN 22
    'denombrement': {'title': "Dénombrement et extension du domaine numérique"},
    'comparaison': {'title': "Comparaison et représentation de nombres"},
    'ecriture': {'title': "Écriture de nombres"},
    # MSN 23
    'probaddsous': {
        'title': "Problèmes additifs et soustractifs",
        'progression': "Résolution de problèmes additifs et soustractifs (EEE, ECE, ETE, TTT)",
    },
    'probmultdiv': {
        'title': "Problèmes multiplicatifs et divisifs",
        'progression': ("Résolution de problèmes multiplicatifs et divisifs : situations d'itération, "
                        "liées au produit cartésien, de produit de mesures, de proportionnalité"),
    },
    'calculatrice': {'title': "Calculatrice"},
    'multiples': {'title': "Multiples, diviseurs, suites de nombres"},
    'calculs': {'title': "Calculs"},
    'tableaux': {
        'title': "Lecture de tableaux de valeurs et représentations graphiques",
        'progression': "<ul>\r\n<li>élaboration de tableaux de valeurs et lecture de "
                       "représentations graphiques</li>\r\n</ul>",
    },
    # MSN 24
    'mesgrandeurs': {'title': "Mesure de grandeurs"},
    'calcgrandeurs': {'title': "Calcul de grandeurs"},
    'unites': {'title': "Unités de mesure"},
}


class Material(models.Model):
    id = models.IntegerField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField()
    discipline = models.CharField(max_length=10, choices=(('maths', 'Mathémathiques'), ('français', 'Français')))
    longueur = models.CharField(max_length=50, blank=True)
    titre = models.CharField(max_length=250, blank=True)
    description = models.TextField(blank=True)
    genre = models.CharField(max_length=50, blank=True)
    document = models.FileField(upload_to='eprocom')
    lien_notice = models.URLField("Notice", blank=True)

    def __str__(self):
        return self.title()

    def title(self):
        if self.titre:
            return self.titre
        return self.document.name

    def is_new(self):
        return self.created.date() > date.today() - timedelta(days=MAX_DAYS_FOR_NEW_LABEL)


class Item(models.Model):
    DIFFICULT_CHOICES = (
        (-1, "Facile"),
        (0, "Intermédiaire"),
        (1, "Difficile"),
    )
    id = models.IntegerField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField()
    titre = models.CharField(max_length=250, blank=True)
    contenu = models.TextField(blank=True)
    variante = models.CharField("Variante", max_length=5, blank=True)
    lien_notice = models.URLField("Notice", blank=True)
    mat_dispo = models.TextField("Matériel disponible", blank=True)
    corrige = models.TextField("Corrigé", blank=True)
    # Français
    genre_textuel = models.CharField(max_length=100, blank=True)
    format_question = models.CharField(max_length=100, blank=True)
    format_question_diff = models.SmallIntegerField(choices=DIFFICULT_CHOICES, blank=True, null=True)
    oper_diff = models.CharField("Difficulté de l’opération", max_length=50, blank=True)
    assoc_mat = models.TextField("Matériel associé", blank=True)
    type_op_cognitive = models.CharField(max_length=50, blank=True)
    # Maths
    categ_tache = models.CharField("Catégorisation de la tâches", max_length=50, blank=True)
    descr_tache = models.TextField(blank=True)
    actions_ops = models.TextField("Actions et opérations", blank=True)
    didac_vars = models.TextField("Variables didactiques possibles", blank=True)
    success_fact = models.TextField("Critères et indications de réussite", blank=True)
    fts = SearchVectorField(blank=True)

    materiels = models.ManyToManyField(Material, blank=True)

    def __str__(self):
        return f'Item {self.id} - {self.titre or ""}'

    def is_new(self):
        return self.created.date() > date.today() - timedelta(days=MAX_DAYS_FOR_NEW_LABEL)

    @property
    def disc(self):
        return 'francais' if self.type_op_cognitive else 'maths'

    def title(self):
        if self.titre:
            return self.titre
        # lxml is buggy with wsgi
        soup = BeautifulSoup(self.contenu, features="html5lib")
        try:
            return list(soup.p.stripped_strings)[0]
        except (AttributeError, IndexError):
            return strip_tags(self.contenu)[:60] + '…'

    def populate_fts(self):
        # Populate full text search field
        soup = BeautifulSoup(self.contenu, features="html5lib")
        stripped = ' '.join(soup.stripped_strings)
        self.fts = SearchVector(Concat('titre', Value(' '), Value(stripped)), config='french')
        self.save()

    def oper_diff_display(self):
        # lower, fix missing ô/é, remove final parens
        text = self.oper_diff.lower().replace('tot', 'tôt').replace('mediaire', 'médiaire')
        if text.find(' (') > 0:
            text = text[:text.find(' (')]
        return text

    def progressions(self):
        return self.perlink_set.exclude(progression='').order_by('pk')


class PERLink(models.Model):
    """
    Lien dénormalisé avec le PER. Devrait pouvoir être recréé lors de mises à jour par l'API.
    """
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    obj_code = models.CharField(max_length=10)
    chapitre = models.CharField(max_length=200, db_index=True, blank=True)
    progression = models.TextField(blank=True)
    cellule_pk = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return f'Lien PER pour {self.item} vers {self.obj_code} {self.chapitre}'

    @property
    def pper_url(self):
        return 'https://www.plandetudes.ch/web/guest/' + self.obj_code.replace(' ', '_')

    def parent_epro_url(self):
        """Renvoie un tuple ('Titre', <url>) renvoyant au menu de gauche correspondant."""
        code = self.obj_code.lower().replace(' ', '')
        disc = 'maths' if 'msn' in code else 'francais'
        for key, content in CHAPTER_MAP.items():
            if (
                self.progression and self.progression == content.get('progression') or
                self.chapitre and self.chapitre == content['title']
            ):
                return (
                    f'{self.obj_code}: {content["title"]}',
                    reverse('epro-page', args=[f'{disc}-8/{code}-{key}'])
                )

    @classmethod
    def from_api(cls, item, struct):
        # TODO: Create one PERLink for each 'chapters' item in struct[?]
        pass


class PageBlock(models.Model):
    slug = models.SlugField(max_length=50)
    content = models.TextField()

    def __str__(self):
        return f'Bloc «{self.slug}»'

    def render_html(self, user):
        edit_div = ''
        if user.has_perm('eprocom.change_pageblock'):
            edit_div = (
                '<div class="edit_div" hx-boost="true" hx-target="closest .epro-bloc">'
                '    <a href="{url}"><img class="edit_img" src="{img}"></a>'
                '</div>'
            ).format(
                url=reverse('epro-blockedit', args=[self.pk]),
                img=static('admin/img/icon-changelink.svg')
            )
        return SafeString('<div class="epro-bloc">' + self.content + edit_div + '</div>')


class CleanLexiqueFilter(LexiqueFilter):
    def __call__(self, content, context):
        return self.re_link_lex.sub(r"\2", content)


class ItemImporter:
    content_filters = FilterChain([CleanLexiqueFilter])

    def __init__(self, item_id):
        try:
            self.item = Item.objects.get(id=item_id)
        except Item.DoesNotExist:
            self.item = Item(id=item_id)
        self.api = EprocomAPI()

    def do_import(self):
        item = self.item
        details = self.api.item_details(item.id)
        item.updated = timezone.now()
        item.titre = details['title'] or ''
        item.contenu = details['content']
        item.variante = details['variant'] or ''
        item.mat_dispo = details['availableMaterial'] or ''
        item.corrige = details['solution'] or ''
        item.format_question = details['questioningFormat'] or ''
        item.format_question_diff = details['envelopDiffQuestionFormat']
        item.type_op_cognitive = details['contentDiffOperationType'] or ''
        item.oper_diff = details['contentDifficultyName'] or ''
        item.categ_tache = details['taskCategorization'] or ''
        item.descr_tache = details['generalTaskDescription'] or ''
        item.actions_ops = details['actionsAndOperations'] or ''
        item.didac_vars = details['possibleDidacticVariables'] or ''
        item.assoc_mat = details['associatedMaterial'] or ''
        item.success_fact = details['successFactors'] or ''
        for mat in details['francaisSimpleMaterialEntities']:
            if mat['rnNoticeUrl']:
                item.lien_notice = mat['rnNoticeUrl']
                break
        item.full_clean()
        item.save()
        item.populate_fts()

        def import_material(mat, disc):
            try:
                material = Material.objects.get(id=mat['id'])
                is_new = False
            except Material.DoesNotExist:
                material = Material(id=mat['id'])
                is_new = True
            material.updated = timezone.now()
            material.discipline = disc
            material.longueur = mat.get('lengthInPagesAndWords', '')
            material.titre = mat.get('title', '')
            material.description = mat['description'] or ''
            material.genre = mat.get('genreGrouping', '')
            material.lien_notice = mat.get('rnNoticeUrl', '')
            if material.lien_notice and disc == 'maths':
                item.lien_notice = material.lien_notice
                item.save()
            material.save()
            for doc in mat['documents']:
                if not doc['isMainDocumentOfMaterial']:
                    continue  # Temporarily ignoring
                if is_new or not material.document:
                    with NamedTemporaryFile() as tmp_file:
                        tmp_file.write(base64.b64decode(doc['content']))
                        tmp_file.flush()
                        file_name = default_storage.get_available_name(doc['name'])
                        material.document = File(tmp_file, name=file_name)
                        material.save()
                else:
                    fh = material.document.open(mode='wb')
                    fh.write(base64.b64decode(doc['content']))
            if material not in item.materiels.all():
                item.materiels.add(material)

        for mat in details['francaisAdvancedMaterialEntities']:
            import_material(mat, 'français')
        for mat in details['mathMaterialEntities']:
            import_material(mat, 'maths')

        self.import_per(item)

    def import_per(self, item):
        filters = self.content_filters.initialize()
        per_data = self.api.item_per(self.item.id)
        item.perlink_set.all().delete()
        for per_item in per_data:
            obj_code = per_item['learningObjective']['code']
            # Maths: exclude too generic chapter
            chapters = [
                chap for chap in per_item['chapters']
                if chap['description'] != 'Éléments pour la résolution de problèmes' or len(per_item['chapters']) < 2
            ]
            for chap in chapters:
                PERLink.objects.create(
                    item=item,
                    obj_code=obj_code,
                    chapitre=chap['description'],
                    progression='',
                )
                if chap.get('textualGenre'):
                    item.genre_textuel = chap['textualGenre']
                    item.save(update_fields=['genre_textuel'])
            progressions = Contenu.objects.filter(
                pk__in=[prog['id'] for prog in per_item['progressions']]
            ).prefetch_related(
                Prefetch(
                    'contenucellule_set',
                    queryset=ContenuCellule.objects.filter(
                        cellule__specification__code=obj_code
                    ).select_related('cellule')
                )
            )
            # idx is a trick to sort progs belonging to the same cell (as Contenus are unsortable)
            progressions_with_cells = sorted(
                (prog.contenucellule_set.all().first().cellule, idx, prog) for idx, prog in enumerate(progressions)
            )
            for cell, _, prog in progressions_with_cells:
                PERLink.objects.create(
                    item=item,
                    obj_code=obj_code,
                    chapitre='',
                    progression=filters.filter_content(prog.texte, {}),
                    cellule_pk=cell.pk,
                )
            if not chapters and not progressions:  # e.g. MSN 25
                PERLink.objects.create(
                    item=item,
                    obj_code=obj_code,
                    chapitre='',
                    progression=''
                )
