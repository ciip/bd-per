from pathlib import Path

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.templatetags.static import static
from django.urls import reverse
from django.utils.html import strip_tags
from django.views.generic import DeleteView, DetailView, TemplateView, UpdateView, View

from .eprocom_api import EprocomAPI
from .forms import BlockEditForm, SearchForm
from .models import CHAPTER_MAP, Item, ItemImporter, Material, PageBlock


class Page:
    """Simulate paginator page."""
    def __init__(self, page_number):
        self.number = page_number
        api = EprocomAPI()
        self.response = api.all_items(page=page_number - 1)

    def has_next(self):
        return self.number < self.num_pages()

    def has_previous(self):
        return self.number > 1

    def next_page_number(self):
        return self.number + 1

    def previous_page_number(self):
        return self.number - 1

    def num_pages(self):
        return self.response['totalPages']

    def items(self):
        existing_items = dict(Item.objects.values_list('id', 'updated'))
        for line in self.response['content']:
            item = {
                'id': line['id'],
                'discipline': line['discipline']['name'],
                'cycle': line['cycle'],
                'objectif': line['mainLearningObjectiveCode'],
                'titre': line['title'],
                'updated': existing_items.get(line['id'], None),
            }
            if not item['titre']:
                item['titre'] = strip_tags(line['content'])[:60] + '…'
            yield item


class AdminItemListView(PermissionRequiredMixin, TemplateView):
    permission_required = 'eprocom.add_item'
    template_name = 'eprocom/admin.html'

    def get_context_data(self, **kwargs):
        page_number = int(self.request.GET.get('p', 1))
        return {
            **super().get_context_data(**kwargs),
            'page': Page(page_number),
        }


class AdminItemImportView(PermissionRequiredMixin, View):
    permission_required = 'eprocom.add_item'

    def post(self, request, *args, **kwargs):
        imp = ItemImporter(self.kwargs['item_id'])
        imp.do_import()

        messages.success(request, "L’item avec l’id %s a été importé avec succès" % self.kwargs['item_id'])
        return HttpResponseRedirect(reverse('epro-admin-home') + '?p=%s' % request.POST.get('p', 1))


class AdminItemJSONView(View):
    part = None

    def get(self, request, *args, **kwargs):
        item = get_object_or_404(Item, pk=self.kwargs['item_id'])
        api = EprocomAPI()
        if self.part == 'details':
            response = api.item_details(item.pk)
        elif self.part == 'per':
            response = api.item_per(item.pk)
        return JsonResponse(response, safe=False)


class AdminItemDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'eprocom.delete_item'
    model = Item
    pk_url_kwarg = 'item_id'

    def get_success_url(self):
        return self.request.headers.get('Referer', reverse('epro-admin-home'))


PAGES = {
    # Français
    'francais-8/eclairages_comprehension': {
        'page_title': 'Compréhension de l’écrit',
        'block_name': 'fra8-comprehension',
    },
    'francais-8/eclairages_choixquestions': {
        'page_title': 'Évaluer la compréhension de l’écrit',
        'block_name': 'fra8-choixquestions',
    },
    'francais-8/eclairages_diffecrits': {
        'page_title': 'Évaluer la compréhension de l’écrit – Difficulté des textes',
        'block_name': 'fra8-diffecrits',
    },
    'francais-8/eclairages_difficulte': {
        'page_title': 'Évaluer la compréhension de l’écrit – Difficulté des questions',
        'block_name': 'fra8-difficultes',
    },
    'francais-8/eclairages_questions': {
        'page_title': 'Format des questions',
        'block_name': 'fra8-format-questions',
    },
    'francais-8/eclairages_production': {
        'page_title': 'Production de l’écrit',
        'block_name': 'fra8-production',
    },
    'francais-8/eclairages_evalproduction': {
        'page_title': 'Évaluer la production de l’écrit',
        'block_name': 'fra8-evalproduction',
    },
    # Maths
    'maths-8/eclairages_categ': {
        'page_title': 'Catégories de tâches',
        'block_name': 'maths8-categ-taches',
    },
    'maths-8/eclairages_resolution': {
        'page_title': 'Catégories de tâches – Résolution de problèmes',
        'block_name': 'maths8-commentaires-gen',
    },
    'maths-8/eclairages_variables': {
        'page_title': 'Catégories de tâches – Variables didactiques',
        'block_name': 'maths8-variables-didac',
    },
    'maths-8/eclairages_guide': {
        'page_title': 'Catégories de tâches – Choix des tâches',
        'block_name': 'maths8-choix-taches',
    },
    'maths-8/eclairages_questions': {
        'page_title': 'Format des questions',
        'block_name': 'maths8-format-questions',
    },
}

OBJECTIF_MAP = {
    'msn21': "Espace",
    'msn22': "Nombres",
    'msn23': "Opérations",
    'msn24': "Grandeurs et mesures",
    'msn25': "Modélisation",
    'l121': "Compréhension de l’écrit",
    'l122': "Production de l’écrit",
    'l123': "Compréhension de l’écrit",
    'l124': "Production de l’écrit",
    'l125': "Accès à la littérature",
    'l126': "Fonctionnement de la langue",
    'l127': "Approches interlinguistiques",
    'l128': "Écriture et instruments de la communication",
}


class GenericPageView(TemplateView):
    def get_template_names(self):
        return [f"eprocom/{self.kwargs['page_slug']}.html", f'eprocom/{self.disc}-8/generic.html']

    def get_context_data(self, **kwargs):
        OPER_TYPE_MAP = {
            'repérage': 'Identifier/repérer',
            'inférence de niveau 1': 'Inférer niveau simple',
            'inférence de niveau 2': 'Inférer niveau complexe',
            'interprétation': 'Interpréter',
        }
        dom_map = {
            'francais': 'L',
            'maths': 'MSN',
        }
        possible_levels = ['8']

        context = super().get_context_data(**kwargs)
        parts = self.kwargs['page_slug'].split('/')
        if parts[0].count('-') != 1:
            raise Http404
        self.disc, level = parts[0].split('-')
        if self.disc not in dom_map or level not in possible_levels:
            raise Http404
        materials = []
        itemlist = []
        code, chapter = '', ''
        if len(parts) > 1 and '-' in parts[1]:
            code, chapter = parts[1].split('-')  # Ex: 'l121', 'raconte'
            if self.disc == 'francais' and code == 'l121':
                materials = get_material_for(code, chapter)
                for mat in materials:
                    # Group items by oper types
                    mat.oper_cognitives = []
                    mat_items = mat.item_set.all().order_by('pk')
                    mat.has_variantes = False
                    for item in mat_items:
                        if item.variante:
                            mat.has_variantes = True
                            item.autres_variantes = [
                                i for i in mat_items
                                if i != item and i.variante and i.variante[:-1] == item.variante[:-1]
                            ]
                    for oper in OPER_TYPE_MAP:
                        mat.oper_cognitives.append({
                            'title': OPER_TYPE_MAP[oper],
                            'itemlist': [
                                item for item in mat_items if item.type_op_cognitive.strip() == oper
                            ],
                        })
                context['block_name'] = f'{code}-{chapter}-top'
            elif self.disc == 'francais' and code == 'l122':
                chap = CHAPTER_MAP.get(chapter)
                context['block_name'] = f'{code}-{chapter}-top'
                itemlist = Item.objects.filter(perlink__obj_code="L1 22", perlink__chapitre=chap['title']).order_by('pk')
            else:
                chap = CHAPTER_MAP.get(chapter)
                if not chap:  # e.g. MSN 25
                    itemlist = Item.objects.filter(perlink__obj_code=code_to_display(code))
                elif 'progression' in chap:
                    itemlist = Item.objects.filter(perlink__progression=chap['progression']).order_by('titre', 'pk')
                else:
                    itemlist = Item.objects.filter(perlink__chapitre=chap['title']).order_by('titre', 'pk')
        elif len(parts) > 1:
            code = parts[1]

        chapter_title = "Éclairages" if 'eclairages' in code else "Ressources évaluatives"
        if self.disc == 'maths':
            search_form = SearchForm()
        else:
            search_form = None
        context.update({
            'domain': dom_map.get(self.disc, ''),
            'menu_template': f'eprocom/menu_{self.disc}_{level}.html',
            'chapter_title': chapter_title,
            'code': code,
            'materials': materials,  # pour 'francais'
            'itemlist': itemlist,  # pour 'maths'
            'search_form': search_form,
        })
        context.update(PAGES.get(self.kwargs['page_slug'], {}))
        if not context.get('page_title') and code.startswith(('msn', 'l1')):
            context['page_title'] = code_to_display(code) + ' – ' + (
                'MODÉLISATION' if chapter == 'modelisation' else CHAPTER_MAP.get(chapter, {}).get('title', '')
            )
        return context


class MathsSearchView(TemplateView):
    template_name = 'eprocom/maths-8/generic.html'

    def get(self, request, *args, **kwargs):
        if not request.GET:
            return HttpResponseRedirect(reverse('epro-page', args=['maths-8']))
        self.search_form = SearchForm(self.request.GET)
        if not self.search_form.is_valid():
            return HttpResponseRedirect(reverse('epro-page', args=['maths-8']))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        itemlist = list(self.search_form.search().prefetch_related('perlink_set'))
        for item in itemlist:
            item.liens_objectifs = sorted(
                item for item in set(lk.parent_epro_url() for lk in item.perlink_set.all()) if item
            )
        return {
            **super().get_context_data(**kwargs),
            'domain': 'MSN',
            'menu_template': 'eprocom/menu_maths_8.html',
            'chapter_title': 'Résultats de la recherche',
            'search_form': self.search_form,
            'itemlist': itemlist,
            'empty_message': "Aucun résultat ne correspond à votre recherche.",
        }


class ItemDetailView(DetailView):
    model = Item
    template_name = 'eprocom/item-detail.html'


class BlockDetailView(DetailView):
    model = PageBlock

    def get(self, request, *args, **kwargs):
        return HttpResponse(self.get_object().render_html(request.user))


class BlockEditView(UpdateView):
    model = PageBlock
    form_class = BlockEditForm
    template_name = 'eprocom/block_edit.html'

    def form_valid(self, form):
        obj = form.save()
        return HttpResponse(obj.render_html(self.request.user))


class EditorImageList(View):
    """Image list for the TinyMCE editor"""
    def get(self, request, *args, **kwargs):
        images = []
        for image in (Path(__file__).parent / 'static' / 'img').iterdir():
            images.append(image)
        return JsonResponse([
            {'title': img.stem, 'value': Path(static('img/' + img.name)).name} for img in images
        ], safe=False)


def get_material_for(code, chapter):
    return Material.objects.filter(
        item__perlink__obj_code=code_to_display(code),
        item__perlink__chapitre=CHAPTER_MAP[chapter]['title'],
    ).distinct().order_by('pk')


def code_to_display(code):
    return code.upper().replace('MSN', 'MSN ').replace('L1', 'L1 ')
