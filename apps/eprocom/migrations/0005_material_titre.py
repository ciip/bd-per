from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0004_item_titre'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='titre',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
