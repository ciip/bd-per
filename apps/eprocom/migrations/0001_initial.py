from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('updated', models.DateTimeField()),
                ('discipline', models.CharField(choices=[('maths', 'Mathémathiques'), ('français', 'Français')], max_length=10)),
                ('longueur', models.CharField(blank=True, max_length=50)),
                ('description', models.TextField(blank=True)),
                ('genre', models.CharField(blank=True, max_length=50)),
                ('document', models.FileField(upload_to='eprocom')),
            ],
        ),
        migrations.CreateModel(
            name='PageBlock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField()),
                ('content', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('updated', models.DateTimeField()),
                ('contenu', models.TextField(blank=True)),
                ('mat_dispo', models.TextField(blank=True, verbose_name='Matériel disponible')),
                ('corrige', models.TextField(blank=True, verbose_name='Corrigé')),
                ('didac_vars', models.TextField(blank=True, verbose_name='Variables didactiques possibles')),
                ('format_question', models.CharField(blank=True, max_length=50)),
                ('type_op_cognitive', models.CharField(blank=True, max_length=50)),
                ('categ_tache', models.CharField(blank=True, max_length=50, verbose_name='Catégorisation de la tâches')),
                ('descr_tache', models.TextField(blank=True)),
                ('actions_ops', models.TextField(blank=True, verbose_name='Actions et opérations')),
                ('materiels', models.ManyToManyField(blank=True, to='eprocom.Material')),
            ],
        ),
        migrations.CreateModel(
            name='PERLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('obj_code', models.CharField(max_length=10)),
                ('chapitre', models.CharField(blank=True, db_index=True, max_length=200)),
                ('progression', models.TextField(blank=True)),
                ('item', models.ForeignKey(on_delete=models.deletion.CASCADE, to='eprocom.item')),
            ],
        ),
    ]
