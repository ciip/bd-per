from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0011_item_genre_textuel'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='assoc_mat',
            field=models.TextField(blank=True, verbose_name='Matériel associé'),
        ),
    ]
