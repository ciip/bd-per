from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0007_alter_item_format_question'),
    ]

    operations = [
        migrations.AddField(
            model_name='perlink',
            name='cellule_pk',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
