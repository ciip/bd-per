from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='format_question_diff',
            field=models.SmallIntegerField(blank=True, choices=[(-1, 'Facile'), (0, 'Intermédiaire'), (1, 'Difficile')], null=True),
        ),
        migrations.AddField(
            model_name='item',
            name='oper_diff',
            field=models.CharField(blank=True, max_length=50, verbose_name='Difficulté de l’opération'),
        ),
    ]
