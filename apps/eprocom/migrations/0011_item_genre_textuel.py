from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0010_item_fts'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='genre_textuel',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
