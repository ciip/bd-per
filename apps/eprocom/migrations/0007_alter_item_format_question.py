from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0006_notice_variante_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='format_question',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
