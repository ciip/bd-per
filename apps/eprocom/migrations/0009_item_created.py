from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0008_perlink_cellule_pk'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default='2021-09-01'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='material',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default='2021-09-01'),
            preserve_default=False,
        ),
    ]
