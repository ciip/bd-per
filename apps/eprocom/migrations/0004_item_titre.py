from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0003_item_success_fact'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='titre',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
