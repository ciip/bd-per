from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0002_item_diffs'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='success_fact',
            field=models.TextField(blank=True, verbose_name='Critères et indications de réussite'),
        ),
    ]
