import django.contrib.postgres.search
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0009_item_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='fts',
            field=django.contrib.postgres.search.SearchVectorField(blank=True),
        ),
    ]
