from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eprocom', '0005_material_titre'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='lien_notice',
            field=models.URLField(blank=True, verbose_name='Notice'),
        ),
        migrations.AddField(
            model_name='item',
            name='variante',
            field=models.CharField(blank=True, max_length=5, verbose_name='Variante'),
        ),
        migrations.AddField(
            model_name='material',
            name='lien_notice',
            field=models.URLField(blank=True, verbose_name='Notice'),
        ),
    ]
