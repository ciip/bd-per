import requests

from django.conf import settings


class EprocomAPI:
    list_url = 'https://eprocom.ciip.ch/api/items/read-only'
    detail_url = 'https://eprocom.ciip.ch/api/items/read-only/{id}'
    per_url = 'https://eprocom.ciip.ch/api/items/read-only/{id}/thematic-axes'

    def __init__(self):
        self.session = requests.Session()

    def all_items(self, page=0, size=10):
        resp = self.session.get(
            self.list_url,
            params={'page': page, 'size': size, 'token': settings.EPROCOM_TOKEN},
        )
        return resp.json()

    def item_details(self, item_id):
        resp1 = self.session.get(
            self.detail_url.format(id=item_id),
            params={'token': settings.EPROCOM_TOKEN},
        )
        return resp1.json()

    def item_per(self, item_id):
        resp2 = self.session.get(
            self.per_url.format(id=item_id),
            params={'token': settings.EPROCOM_TOKEN},
        )
        return resp2.json()
