import json
from pathlib import Path
from unittest import mock

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import TestCase, override_settings
from django.urls import reverse

from pper.tests.tests import TempMediaRootMixin
from eprocom.models import Item, Material

MODEL = 'django.contrib.auth.backends.ModelBackend'
FRANCAIS_ITEM_ID = 83
MATHS_ITEM_ID = 59


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


class MockSession(mock.MagicMock):
    def get(self, url, **kwargs):
        mapping = {
            'https://eprocom.ciip.ch/api/items/read-only/83': 'details_fra_83.json',
            'https://eprocom.ciip.ch/api/items/read-only/83/thematic-axes': 'per_fra_83.json',
            'https://eprocom.ciip.ch/api/items/read-only/59': 'details_maths_59.json',
            'https://eprocom.ciip.ch/api/items/read-only/59/thematic-axes': 'per_maths_59.json',
        }
        if url not in mapping:
            return MockResponse(None, 404)
        with (Path(__file__).parent / mapping[url]).open('r') as fh:
            return MockResponse(json.loads(fh.read()), 200)


class EprocomTests(TempMediaRootMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = get_user_model().objects.create_user('john', 'doe@example.org', 'johnpw')
        cls.user.user_permissions.add(Permission.objects.get_by_natural_key('view_item', 'eprocom', 'item'))
        cls.user.user_permissions.add(Permission.objects.get_by_natural_key('add_item', 'eprocom', 'item'))

    @classmethod
    def tearDownClass(cls):
        """ Remove caching of user in local thread """
        super().tearDownClass()
        from journal.middleware import _thread_locals
        if hasattr(_thread_locals, 'user'):
            delattr(_thread_locals, 'user')

    @mock.patch('requests.Session', new=MockSession())
    def test_import_item_francais(self):
        self.client.force_login(self.user, backend=MODEL)
        self.client.post(reverse('epro-admin-import', args=[FRANCAIS_ITEM_ID]), data={})
        self.assertTrue(Item.objects.filter(id=FRANCAIS_ITEM_ID).exists())
        self.assertEqual(Material.objects.count(), 1)
        # Second time for potential updates
        self.client.post(reverse('epro-admin-import', args=[FRANCAIS_ITEM_ID]), data={})
        self.assertTrue(Item.objects.filter(id=FRANCAIS_ITEM_ID).exists())
        self.assertEqual(Material.objects.count(), 1)

    @mock.patch('requests.Session', new=MockSession())
    def test_import_item_maths(self):
        self.client.force_login(self.user, backend=MODEL)
        self.client.post(reverse('epro-admin-import', args=[MATHS_ITEM_ID]), data={})
        self.assertTrue(Item.objects.filter(id=MATHS_ITEM_ID).exists())
        self.assertEqual(Material.objects.count(), 1)
