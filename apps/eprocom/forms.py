from django import forms
from django.db.models import Q

from .models import Item, PageBlock


class BlockEditForm(forms.ModelForm):
    class Meta:
        model = PageBlock
        fields = ['content']
        labels = {'content': ''}


class SearchForm(forms.Form):
    q = forms.CharField(widget=forms.TextInput(attrs={'placeholder': "Rechercher…"}), required=True)

    def search(self):
        # Exclusion français avec type_op_cognitive vide
        return Item.objects.filter(
            type_op_cognitive="", fts=self.cleaned_data['q']
        )
