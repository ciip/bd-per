from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('', TemplateView.as_view(template_name='eprocom/index.html')),
    path('admin/', views.AdminItemListView.as_view(), name='epro-admin-home'),
    path('admin/import/<int:item_id>/', views.AdminItemImportView.as_view(), name='epro-admin-import'),
    path(
        'admin/<int:item_id>/json-details/', views.AdminItemJSONView.as_view(part='details'),
        name='epro-admin-jsondetails'
    ),
    path('admin/<int:item_id>/json-per/', views.AdminItemJSONView.as_view(part='per'), name='epro-admin-jsonper'),
    path('admin/<int:item_id>/delete/', views.AdminItemDeleteView.as_view(), name='epro-admin-delete'),
    path('bloc/<int:pk>/', views.BlockDetailView.as_view(), name='epro-block'),
    path('bloc/<int:pk>/edit/', views.BlockEditView.as_view(), name='epro-blockedit'),
    path('item/<int:pk>/detail/', views.ItemDetailView.as_view(), name='epro-item-detail'),
    path('image_list/', views.EditorImageList.as_view(), name='epro-image-list'),
    path('maths8/recherche/', views.MathsSearchView.as_view(), name='epro-maths-search'),
    path('<path:page_slug>/', views.GenericPageView.as_view(), name='epro-page'),
]
