from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models

from pper import filters

content_filters = filters.FilterChain([
    filters.HTMLNiveauContentFilter,
    filters.HTMLComposanteContentFilter,
    filters.HTMLLexiqueFilter,
    filters.HTMLImageFilter,
    filters.HTMLPubDomainLinkFilter,
    filters.HTMLPubSpecLinkFilter,
    filters.HTMLPubTitleFilter,
])


class UploadedEntity(models.Model):
    # Generic relation to BD-PER object
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id    = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    # id of journal content in Liferay
    pperid_valid = models.IntegerField(blank=True, null=True)
    pperid_prod  = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return str(self.content_object)

    def get_pperid(self, is_validation):
        if is_validation:
            return self.pperid_valid
        else:
            return self.pperid_prod

    def set_pperid(self, is_validation, new_id):
        if is_validation:
            self.pperid_valid = new_id
        else:
            self.pperid_prod = new_id
        self.save()

