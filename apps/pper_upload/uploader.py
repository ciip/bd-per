from urllib.error import HTTPError, URLError

import requests

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.staticfiles import finders
from django.template import loader
from django.urls import set_urlconf, get_script_prefix, set_script_prefix

from pper.models import Domain
from pper_upload.models import UploadedEntity


class PPERUpload:
    def __init__(self, target):
        self.target = target

    def __enter__(self):
        self.old_prefix = get_script_prefix()
        set_urlconf("public.pper_urls")
        # Prevent /bdper/ prefix on consultation-per.ch
        set_script_prefix(u'/')
        return self

    def __exit__(self, type, value, tb):
        set_urlconf(None)
        set_script_prefix(self.old_prefix)

    def push_content_portail(self, spec_id, title, content):
        """Push content on the new portail.ciip.ch site."""
        base_url = settings.PORTAIL_CIIP_API[self.target]['URL']
        token = settings.PORTAIL_CIIP_API[self.target]['TOKEN']
        url = f"{base_url}/bdper/content/{spec_id}?token={token}"
        response = requests.put(url, json={'title': title, 'content': content}, timeout=30)
        if response.status_code not in (201, 204):
            raise Exception(
                f"Pushing to portail CIIP resulted in a response with status code {response.status_code}"
            )
        return response

    def upload_object(self, obj, context, template, title_col):
        """ Render the object obj and push resulting content on PPER
            Return ('error', 'new' or 'updated', complement) """
        context['base'] = "public/base_pper.html"
        color_t = loader.get_template('domain_colors.css')
        color_content = color_t.render({'domains': Domain.objects.all()})
        context['css'] = color_content
        with open(finders.find('css/public.css'), 'r') as fh:
            context['css'] += fh.read().replace('../img', '%simg' % settings.STATIC_URL)

        t = loader.get_template(template)
        content = t.render(context)
        try:
            self.push_content_portail(obj.pk, getattr(obj, title_col), content)
        except (HTTPError, URLError, Exception) as e:
            msg = "Unable to upload the content. Error is: %s" % str(e)
            return ("error", msg)
        return ("updated", None)
