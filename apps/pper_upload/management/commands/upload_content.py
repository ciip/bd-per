from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from pper_upload.uploader import PPERUpload
from pper_upload.models import content_filters
from pper.models import Specification, Tableau, Discipline
from public.views import build_disc_context, build_spec_context


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--target', dest='target',
            help="specify the category of content to upload ('specs', 'disciplines', 'com_gen' or discipline slug)"
        )
        parser.add_argument(
            '--spec', dest='spec', help="specify a specific course to upload"
        )
        parser.add_argument(
            '--site', dest='site', help="specify the site to upload to (dev, staging or prod)"
        )

    def handle(self, *args, **options):
        """ This command upload a page content onto a remote site """
        target = options.get('target')
        spec_code = options.get('spec')
        site = options.get('site')
        if not site or site not in {'dev', 'staging', 'prod'}:
            raise CommandError(
                "Please specify a site to upload to with the --site argument (dev, staging or prod).\n"
            )
        if not (spec_code or target):
            raise CommandError("Please specify either --spec or --target.\n")

        # Filtering what type of objects to upload
        specs = []
        discs = []
        com_gens = []
        if spec_code:
            specs = Specification.objects.published().filter(code__startswith=spec_code)
            if not specs:
                return "Unable to find a specification with code '%s'" % spec_code
        elif target == 'specs':
            specs = Specification.objects.published()
        elif target == 'disciplines':
            discs = Discipline.objects.filter(published=True)
        elif target == 'com_gen':
            com_gens = Tableau.objects.filter(type_t='com_gen')
        else:
            # Try to get discpline as if target == slug
            discs = Discipline.objects.filter(slug=target)

        with PPERUpload(site) as uploader:
            for spec in specs:
                print("uploading content for %s" % spec.code)
                specs_lst = spec.get_grouped_specs(status='published')
                result, up_obj = uploader.upload_object(
                    specs_lst[0], build_spec_context(specs_lst, content_filters),
                    "public/specification.html", 'code'
                )

            for disc in discs:
                print("uploading content for %s" % disc.slug)
                result, up_obj = uploader.upload_object(disc, build_disc_context(disc), "public/discipline.html", 'slug')

            for tableau in com_gens:
                print("uploading content for %s" % tableau.slug)
                context = {
                    'tableau'     : tableau,
                    'flat_content': tableau.get_flat_content(content_filters.initialize()),
                }
                result, up_obj = uploader.upload_object(tableau, context, 'public/tableau-flat.html', 'title')
