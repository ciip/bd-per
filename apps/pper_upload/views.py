from django.conf import settings
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404

from pper.models import Specification
from pper.utils import global_editable
from pper_upload.uploader import PPERUpload
from pper_upload.models import content_filters
from public.views import build_spec_context


def portail_upload_course(request, course_id, target=None):
    """ Upload content to PPER platform """
    if settings.IS_TEST:
        return HttpResponse("Cette fonction n'est pas disponible sur le site de test")
    course = get_object_or_404(Specification, pk=course_id)
    editable = global_editable(request.user) and course.can_edit(request.user)
    if not editable:
        return HttpResponseForbidden("Insufficient permissions")

    with PPERUpload(target) as uploader:
        courses = course.get_grouped_specs()
        context = build_spec_context(courses, content_filters)
        status, compl = uploader.upload_object(courses[0], context, "public/specification.html", 'code')
    if status in ("new", "updated"):
        return HttpResponse(f"{course.code} a été mis a jour sur le portail PER ({target})")
    return HttpResponse(f"Une erreur s'est produite pendant le transfert vers la PPER ({compl})")
