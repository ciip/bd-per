from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0006_set_fk_on_delete'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tableau',
            name='cycle',
            field=models.CommaSeparatedIntegerField(default='', max_length=10, null=True, blank=True),
        ),
        migrations.RunSQL(sql="UPDATE t_tableau SET cycle=NULL WHERE cycle=''"),

        migrations.AlterField(
            model_name='tableau',
            name='cycle',
            field=models.SmallIntegerField(blank=True, null=True),
        ),
    ]
