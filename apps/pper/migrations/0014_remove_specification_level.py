from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0013_remove_contenucellule_sensibil'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='specification',
            options={
                'ordering': ('code',),
                'permissions': (('insert_resources', 'Can insert resources into specifications'),),
                },
        ),
        migrations.RemoveField(
            model_name='specification',
            name='level',
        ),
    ]
