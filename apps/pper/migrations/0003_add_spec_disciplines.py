from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0002_tableau_init_visible'),
    ]

    operations = [
        migrations.AddField(
            model_name='specification',
            name='disciplines',
            field=models.ManyToManyField(to='pper.Discipline', blank=True),
        ),
    ]
