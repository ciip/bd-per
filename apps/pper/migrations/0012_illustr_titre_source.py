from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0011_illustration'),
    ]

    operations = [
        migrations.AddField(
            model_name='illustration',
            name='source',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='illustration',
            name='titre',
            field=models.CharField(default='', max_length=250),
            preserve_default=False,
        ),
    ]
