from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0014_remove_specification_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='discipline',
            name='lom_id',
            field=models.CharField(blank=True, max_length=40),
        ),
        migrations.AddField(
            model_name='domain',
            name='lom_id',
            field=models.CharField(blank=True, max_length=40),
        ),
    ]
