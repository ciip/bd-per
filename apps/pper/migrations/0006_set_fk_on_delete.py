from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0005_remove_specification_discipline'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discipline',
            name='domain',
            field=models.ForeignKey(to='pper.Domain', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='lexique',
            name='parent',
            field=models.ForeignKey(related_name='children', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='pper.Lexique', null=True),
        ),
        migrations.AlterField(
            model_name='objectif',
            name='domain',
            field=models.ForeignKey(to='pper.Domain', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='objectif',
            name='objectif2',
            field=models.ForeignKey(related_name='linked1', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Objectif li\xe9', blank=True, to='pper.Objectif', null=True),
        ),
        migrations.AlterField(
            model_name='objectif',
            name='objectif3',
            field=models.ForeignKey(related_name='linked2', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Objectif li\xe9 (2e)', blank=True, to='pper.Objectif', null=True),
        ),
        migrations.AlterField(
            model_name='specification',
            name='post_content',
            field=models.ForeignKey(related_name='spec_post_content', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='pper.Contenu', null=True),
        ),
        migrations.AlterField(
            model_name='specification',
            name='pre_content',
            field=models.ForeignKey(related_name='spec_pre_content', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='pper.Contenu', null=True),
        ),
        migrations.AlterField(
            model_name='specification',
            name='version',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='pper.Version', null=True),
        ),
        migrations.AlterField(
            model_name='tableau',
            name='content_type',
            field=models.ForeignKey(to='contenttypes.ContentType', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='tableau',
            name='post_content',
            field=models.ForeignKey(related_name='tabl_post_content', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='pper.Contenu', null=True),
        ),
        migrations.AlterField(
            model_name='tableau',
            name='pre_content',
            field=models.ForeignKey(related_name='tabl_pre_content', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='pper.Contenu', null=True),
        ),
        migrations.AlterField(
            model_name='thematique',
            name='domain',
            field=models.ForeignKey(to='pper.Domain', on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
