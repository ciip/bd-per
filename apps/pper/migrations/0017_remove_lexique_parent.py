from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0016_discipline_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lexique',
            name='parent',
        ),
    ]
