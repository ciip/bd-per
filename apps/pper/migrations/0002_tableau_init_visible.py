from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tableau',
            name='init_visible',
            field=models.BooleanField(default=False, help_text='Tell if this table is initially visible or hidden'),
            preserve_default=True,
        ),
    ]
