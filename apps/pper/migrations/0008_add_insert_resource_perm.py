from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0007_tableau_cycle_to_int'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='specification',
            options={'ordering': ('level', 'code'), 'permissions': (('insert_resources', 'Can insert resources into specifications'),)},
        ),
    ]
