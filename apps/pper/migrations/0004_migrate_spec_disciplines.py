from django.db import models, migrations


def migrate_disc_to_discs(apps, schema_editor):
    Specification = apps.get_model("pper", "Specification")
    for spec in Specification.objects.all():
        spec.disciplines.add(spec.discipline)


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0003_add_spec_disciplines'),
    ]

    operations = [migrations.RunPython(migrate_disc_to_discs)]
