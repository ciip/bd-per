from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0012_illustr_titre_source'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contenucellule',
            name='sensibil',
        ),
    ]
