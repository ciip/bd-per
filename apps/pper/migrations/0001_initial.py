import os

from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cellule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField()),
                ('position', models.IntegerField()),
                ('scycle1', models.BooleanField(default=False)),
                ('scycle2', models.BooleanField(default=False)),
                ('scycle3', models.BooleanField(default=False)),
                ('attentes', models.BooleanField(default=False)),
                ('indications', models.BooleanField(default=False)),
                ('type_c', models.IntegerField(choices=[(-1, 'titre 1'), (-2, 'titre 2'), (-3, 'titre 3'), (-4, 'titre 2 (pleine largeur)'), (1, 'progression'), (2, 'attente'), (3, 'indication'), (5, 'pleine largeur')])),
                ('type_bord', models.CharField(default='all', max_length=8, choices=[('all', 'All borders'), ('noup', 'No top border'), ('nobot', 'No bottom border'), ('noupbot', 'No up, no bottom border')])),
                ('anchor', models.SlugField(blank=True, max_length=100)),
            ],
            options={
                'db_table': 't_cellules',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Composante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('title', models.TextField()),
                ('letter', models.CharField(max_length=1, blank=True)),
            ],
            options={
                'db_table': 't_composantes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contenu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('texte', models.TextField()),
            ],
            options={
                'db_table': 't_contenus',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContenuCellule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('position', models.IntegerField(default=0, null=True, blank=True)),
                ('level', models.CommaSeparatedIntegerField(max_length=10, null=True, blank=True)),
                ('sensibil', models.BooleanField(default=False)),
                ('cellule', models.ForeignKey(to='pper.Cellule', on_delete=models.CASCADE)),
                ('composantes', models.ManyToManyField(to='pper.Composante', blank=True)),
                ('contenu', models.ForeignKey(to='pper.Contenu', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 't_cellule_contenus',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Discipline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('name', models.CharField(max_length=80)),
                ('slug', models.SlugField(default='', max_length=80)),
                ('position', models.IntegerField(help_text='Position is used in paper-version to order specs')),
                ('published', models.BooleanField(default=True, help_text='Indicate if this discipline is officially published or in the works')),
            ],
            options={
                'ordering': ('domain', 'position'),
                'db_table': 't_disciplines',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('name', models.TextField()),
                ('abrev', models.CharField(max_length=4)),
                ('visee', models.TextField()),
                ('position', models.IntegerField()),
            ],
            options={
                'ordering': ('position',),
                'db_table': 't_domaines',
                'verbose_name': 'Domaine',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(unique=True, max_length=150)),
                ('png', models.FilePathField(max_length=150, path=os.path.join(settings.STATIC_ROOT, 'resources/public/images'), null=True, match='\\.png$', blank=True)),
                ('eps', models.FilePathField(max_length=150, path=os.path.join(settings.STATIC_ROOT, 'resources/public/images'), null=True, match='\\.eps$', blank=True)),
                ('char', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
                'db_table': 't_image',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lexique',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('term', models.CharField(max_length=100)),
                ('definition', models.TextField()),
                ('comgen', models.BooleanField(default=False)),
                ('disciplines', models.ManyToManyField(to='pper.Discipline', db_table='t_lexique_discipline', blank=True)),
                ('domain', models.ForeignKey(to='pper.Domain', on_delete=models.CASCADE)),
                ('parent', models.ForeignKey(related_name='children', blank=True, to='pper.Lexique', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 't_lexique',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Objectif',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('title', models.TextField()),
                ('cycle', models.IntegerField()),
                ('code', models.CharField(max_length=10)),
                ('cantons', models.CharField(default='', help_text='Liste de cantons s\xe9par\xe9s par des virgules (VD,FR), vide pour tous les cantons', max_length=30, blank=True)),
                ('domain', models.ForeignKey(to='pper.Domain', on_delete=models.PROTECT)),
                ('objectif2', models.ForeignKey(related_name='linked1', verbose_name='Objectif li\xe9', blank=True, to='pper.Objectif', null=True, on_delete=models.SET_NULL)),
                ('objectif3', models.ForeignKey(related_name='linked2', verbose_name='Objectif li\xe9 (2e)', blank=True, to='pper.Objectif', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'ordering': ('code',),
                'db_table': 't_objectifs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Specification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('position', models.IntegerField(default=0)),
                ('code', models.CharField(max_length=10)),
                ('level', models.IntegerField(default=None, null=True, blank=True)),
                ('fake', models.BooleanField(default=False)),
                ('status', models.CharField(default='draft', max_length=20, choices=[('draft', 'en pr\xe9paration'), ('published', 'publi\xe9'), ('archived', 'archiv\xe9')])),
                ('locked', models.BooleanField(default=False)),
                ('printable', models.URLField(null=True, blank=True)),
                ('discipline', models.ForeignKey(to='pper.Discipline', on_delete=models.CASCADE)),
                ('objectif', models.ForeignKey(to='pper.Objectif', on_delete=models.CASCADE)),
                ('post_content', models.ForeignKey(related_name='spec_post_content', blank=True, to='pper.Contenu', null=True, on_delete=models.SET_NULL)),
                ('pre_content', models.ForeignKey(related_name='spec_pre_content', blank=True, to='pper.Contenu', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'ordering': ('level', 'code'),
                'db_table': 't_specifications',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tableau',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('position', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('slug', models.SlugField(max_length=20, null=True, blank=True)),
                ('type_t', models.CharField(default='tableau', max_length=10, choices=[('tableau', 'Tableau'), ('com_gen', 'Commentaire g\xe9n\xe9ral de domaine'), ('texte_gen', 'Texte g\xe9n\xe9ral')])),
                ('cycle', models.CommaSeparatedIntegerField(default='', max_length=10, blank=True)),
                ('nb_cols', models.IntegerField(default=1)),
                ('locked', models.BooleanField(default=False)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.PROTECT)),
                ('post_content', models.ForeignKey(related_name='tabl_post_content', blank=True, to='pper.Contenu', null=True, on_delete=models.SET_NULL)),
                ('pre_content', models.ForeignKey(related_name='tabl_pre_content', blank=True, to='pper.Contenu', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'ordering': ('position',),
                'db_table': 't_tableau',
                'verbose_name_plural': 'Tableaux',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thematique',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(blank=True)),
                ('name', models.TextField()),
                ('order_no', models.IntegerField()),
                ('domain', models.ForeignKey(to='pper.Domain', on_delete=models.PROTECT)),
            ],
            options={
                'db_table': 't_thematiques',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UniqueId',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 't_uniqueid',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('complement', models.TextField()),
                ('weight', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 't_version',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='specification',
            name='version',
            field=models.ForeignKey(blank=True, to='pper.Version', null=True, on_delete=models.SET_NULL),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='specification',
            unique_together=set([('code', 'version')]),
        ),
        migrations.AddField(
            model_name='objectif',
            name='thematiques',
            field=models.ManyToManyField(related_name='objectifs', db_table='t_objectif_thematique', to='pper.Thematique'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='discipline',
            name='domain',
            field=models.ForeignKey(to='pper.Domain', on_delete=models.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='contenucellule',
            unique_together=set([('cellule', 'contenu')]),
        ),
        migrations.AddField(
            model_name='composante',
            name='objectif',
            field=models.ForeignKey(to='pper.Objectif', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cellule',
            name='contenus',
            field=models.ManyToManyField(to='pper.Contenu', through='pper.ContenuCellule'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cellule',
            name='specification',
            field=models.ForeignKey(blank=True, to='pper.Specification', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cellule',
            name='tableau',
            field=models.ForeignKey(blank=True, to='pper.Tableau', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
