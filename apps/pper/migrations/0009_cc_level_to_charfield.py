import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0008_add_insert_resource_perm'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contenucellule',
            name='level',
            field=models.CharField(blank=True, max_length=10, null=True, validators=[django.core.validators.validate_comma_separated_integer_list]),
        ),
    ]
