from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0015_discipline_lom_id_domain_lom_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='discipline',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]
