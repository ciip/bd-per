from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '0004_migrate_spec_disciplines'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='specification',
            name='discipline',
        ),
    ]
