from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as DjangoGroupAdmin, UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import Group, User
from django.forms import ModelForm, TextInput
from django.utils.html import format_html

from pper.models import (
    Cellule, Composante, Contenu, ContenuCellule, Discipline, Domain,
    Illustration, Image, Lexique, Specification, Tableau, Objectif,
    Thematique, UniqueId, Version
)


def persons(self):
    return ', '.join(['%s' % x.username for x in self.user_set.all().order_by('username')])

class GroupAdmin(DjangoGroupAdmin):
    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ('permissions',)
    list_display = ['name', persons]
    list_display_links = ['name']


class UserAdmin(DjangoUserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'last_login')


class DomainForm(ModelForm):
    class Meta:
        model = Domain
        fields = '__all__'
        widgets = {
            'col_dark': TextInput(attrs={'type': 'color'}),
            'col_medium': TextInput(attrs={'type': 'color'}),
            'col_light': TextInput(attrs={'type': 'color'}),
        }


class DomainAdmin(admin.ModelAdmin):
    form = DomainForm
    list_display = ('name', 'abrev', 'position', 'colors')
    #raw_id_fields = ('com_gen',)

    def colors(self, obj):
        return format_html(
            '<span style="font-size: 40px; color: {dark};">■</span> ({dark})'
            '<span style="font-size: 40px; color: {medium};">■</span> ({medium})'
            '<span style="font-size: 40px; color: {light};">■</span> ({light})',
            dark=obj.col_dark, medium=obj.col_medium, light=obj.col_light
        )


class DisciplineAdmin(admin.ModelAdmin):
    list_display = ('name', 'domain', 'position', 'published')
    list_filter = ('domain',)
    prepopulated_fields = {"slug": ("name",)}

class ThematiqueAdmin(admin.ModelAdmin):
    list_display = ('name', 'domain', 'order_no')
    list_filter = ('domain',)

class ComposanteInline(admin.TabularInline):
    model = Composante
    fields = ('uid', 'title', 'letter')
    extra = 1

class ObjectifAdmin(admin.ModelAdmin):
    search_fields = ('code', 'title')
    list_filter = ('domain',)
    inlines = (ComposanteInline,)

class ComposanteAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display  = ('title', 'obj_code', 'letter')
    def obj_code(self, obj):
        return obj.objectif.code

class SpecificationAdmin(admin.ModelAdmin):
    search_fields = ('code',)
    ordering = ('code',)
    list_display  = ('__str__', 'version', 'status', 'fake', 'position')
    list_filter = ('objectif__domain', 'fake', 'status')
    raw_id_fields = ('pre_content', 'post_content')

class TableauAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('__str__', 'position')
    list_filter = ('type_t',)
    raw_id_fields = ('pre_content', 'post_content')

class ContenuCelluleInline(admin.TabularInline):
    model = ContenuCellule
    fields = ["cellule", "contenu", "position", "level"]
    raw_id_fields = ["cellule", "contenu"]
    extra = 1

class ContenuAdmin(admin.ModelAdmin):
    list_display = ('uid','__str__')
    search_fields = ('uid','texte')
    inlines = (ContenuCelluleInline,)

class CelluleAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'anchor')
    list_filter = ('type_c',)
    search_fields = ('uid',)
    inlines = (ContenuCelluleInline,)


class LexiqueAdmin(admin.ModelAdmin):
    search_fields = ('term', 'definition')
    list_filter = ('domain',)
    list_display = ('domain', '__str__')
    ordering = ('domain', 'term')


class IllustrationAdmin(admin.ModelAdmin):
    raw_id_fields = ['cellule']
    list_display = ['titre', 'published']
    search_fields = ['titre', 'texte']


class ImageAdmin(admin.ModelAdmin):
    search_fields = ('source', 'char')
    ordering = ('source',)
    list_display = ('source', 'char')
    list_editable = ('char',)

class VersionAdmin(admin.ModelAdmin):
    list_display = ('name', 'complement', 'weight')


admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Cellule, CelluleAdmin)
admin.site.register(Composante, ComposanteAdmin)
admin.site.register(Contenu, ContenuAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(Discipline, DisciplineAdmin)
admin.site.register(Illustration, IllustrationAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Lexique, LexiqueAdmin)
admin.site.register(Objectif, ObjectifAdmin)
admin.site.register(Specification, SpecificationAdmin)
admin.site.register(Tableau, TableauAdmin)
admin.site.register(Thematique, ThematiqueAdmin)
admin.site.register(Version, VersionAdmin)
admin.site.register(UniqueId)
