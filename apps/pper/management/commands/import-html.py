import os, re, libxml2, string
from django.core.management.base import BaseCommand

from pper.models import Specification, Objectif, Contenu, Cellule, ContenuCellule

# Constantes (cellules.type_c)
TYPE_TITRE1 = -1
TYPE_TITRE2 = -2 # "titre2" ne s'etend que dans la partie progression
TYPE_TITRE3 = -3
TYPE_ELEMENT = 1
TYPE_ATTENTE = 2
TYPE_INDICATION = 3
TYPE_LIEN = 5

source_encoding = "latin-1"

class Command(BaseCommand):

    def handle(self, *args, **options):
        if len(args) != 2:
            print("Usage: import-html html_path specification_code")
            return
        
        path = args[0]
        try:
            f = open(path)
        except:
            print("Unable to find file '%s'" % path)
            return
        s = f.read()
        # Try and process the page.
        parse_options = libxml2.HTML_PARSE_RECOVER + \
		        libxml2.HTML_PARSE_NOERROR + \
		        libxml2.HTML_PARSE_NOWARNING
        doc = libxml2.htmlReadDoc(s,'',None,parse_options)

        try:
            spec = Specification.objects.get(code=args[1])
        except Specification.DoesNotExist:
            print("Unable to find a specification with code '%s'" % args[1])
            return
        
        tables = doc.xpathEval('//table')
        t_specification = None; t_entetes = None
        spec_columns = 0
        # Recherche le tableau des specifications
        try:
            t_specification = doc.xpathEval('//table[@id="progression_attentes"]')[0]
        except:
            print("Aucun tableau de progression trouvé sur la page %s." % path)
            return

        # Analyser le tableau des progressions
        colonnes = {1:1, 2:1, 3:1, 4:2, 5:3}
        re_link = re.compile(r'^MM_showHideLayers\(\'Txt_comp_(?P<letter>\w)(?P<objnum>\d*)\'')
        re_link2 = re.compile(r'^MM_showHideLayers\(\'(Txt_comp_\w\d*)\'')
        rowspans = [0, 0, 0, 0, 0]
        for noligne, tr in enumerate(t_specification.xpathEval('.//tr')):
            if tr.content.strip() == "":
                continue
            ligne = []
            xpos = 1
            for nocellule, td in enumerate(tr.xpathEval('.//td')):
                # Ignore nested tables
                if td.nodePath().count('table') > 1 or xpos > len(colonnes):
                    continue

                cellule = {};
                while rowspans[xpos-1] > 0:
                    xpos += 1
                try:
                    cellule['colspan'] = int(td.prop('colspan'))
                except:
                    cellule['colspan'] = 1

                """ Détection de titre, parfois selon la classe, parfois selon la couleur
                    En maths, div class='CycleActif' (+ colspan = 2)
                    En français, td class="titre_colonne", colspan=4 -> grand titre, colspan = 2 -> petit titre
                    SHS : td bgcolor="#A9FDAF"
                """
                if td.children.prop('class') == "CycleActif" or td.children.prop('class') == "titre_colonne" or td.prop('class') == "titre_colonne":
                    if cellule['colspan'] <= spec_columns:
                        cellule['type'] = TYPE_TITRE2
                    else:
                        cellule['type'] = TYPE_TITRE1
                    cellule['contenu'] = getGoodContent(td)
                
                # Détection des cellules de type "Liens : ..." """
                elif td.prop('bgcolor') == "#CCCCCC" and td.content[:4] == "Lien":
                    cellule['type'] = TYPE_LIEN
                    cellule['contenu'] = getGoodContent(td)
                else:
                    try:
                        cellule['type'] = colonnes[xpos] # "element" ou "attente" ou "indication"
                    except:
                        pdb.set_trace()
                    cellule['contenu'] = getGoodContent(td)
                    # Récupérer les liens "Composantes" dans les cellules "normales"
                    divs = td.xpathEval('.//div')
                    for div in divs:
                        if not div.content or not div.prop('onmouseover'):
                            continue
                        rech = re_link.match(div.prop('onmouseover')) # MM_showHideLayers('Txt_comp_D'
                        if rech:
                            comp_div_name = re_link2.match(div.prop('onmouseover')).groups()[0]
                            comp_div_text = str(doc.xpathEval('//div[@id="%s"]' % comp_div_name)[0].content, 'utf-8')
                            if rech.group('objnum') == '':
                                obj_code = spec.objectif.getNum() # ex: "MSN35"
                                target_obj = spec.objectif
                            else:
                                # Le lien est vers un objectif secondaire
                                if spec.objectif2_id:
                                    obj2 = Objectif.get(pk=spec.objectif2_id)
                                    obj_code = obj2.getNum()
                                    target_obj = obj2
                                else:
                                    print("Lien vers objectif secondaire, mais pas d'objectif trouvé.")

                            letter = None
                            for comp in target_obj.composante_set.all():
                                if comp.title == comp_div_text:
                                    letter = comp.letter
                                    break
                            if not letter:
                                # Essentiellement pour MSN 32
                                print("Warning, no letter found for comp '%s' (%s)" % (comp_div_text, spec.url))
                                continue
                            # Remplacer le texte du lien dans le contenu par le code Wiki
                            splitted = cellule['contenu'].rpartition(str(div.content, 'utf-8'))
                            link_code = u"<ulink url='comp:%s#%s'>%s</ulink>" % (obj_code, letter, str(div.content, 'utf-8'))
                            cellule['contenu'] = splitted[0] + link_code + splitted[2]
                        else:
                            print("Impossible de déterminer le lien pour le div '%s'" % str(div))

                scycle1 = False; scycle2 = False; scycle3 = False
                if cellule['type'] == TYPE_ELEMENT or cellule['type'] == TYPE_TITRE2:
                    if xpos == 1:
                        scycle1 = True
                    if xpos == 2 or (xpos < 2 and xpos+cellule['colspan']-1 >=2):
                        scycle2 = True
                    if xpos == 3 or (xpos < 3 and xpos+cellule['colspan']-1 >=3):
                        scycle3 = True
                try:
                    cellule['rowspan'] = int(td.prop('rowspan'))
                except:
                    cellule['rowspan'] = 1

                # TODO fractionner le contenu pour les attentes pédagogiques
                contenu = []
                if cellule['type'] == TYPE_ATTENTE and len(cellule['contenu']) > 0:
                    ct = cellule['contenu'].replace("<p>", "").replace("</p>", "")
                    liste_contenus = ct.split('&#8226;')
                    if liste_contenus[0].strip() == '':
                        liste_contenus = liste_contenus[1:] # Enlever premier élément vide
                    liste_contenus = map(strip_first_char, liste_contenus)
                    #pdb.set_trace()
                else:
                    liste_contenus = [cellule['contenu']]
                
                for cont in liste_contenus:
                    if len(cont.strip())>0:
                        # Chercher si contenu identique existant (excepté cellules vides)
                        q = Contenu.objects.filter(texte=cont)
                        if len(q.all()) > 0:
                            contenu.append(q[0]) # contenu existant
                        else:
                            c = Contenu(texte=cont)
                            c.save()
                            contenu.append(c)
                
                # Simuler le rowspan en ajoutant des cellules fictives dans chaque ligne concernée
                for row in range(0, cellule['rowspan']):
                    cel = Cellule(specification=spec, type_c=cellule['type'], position=noligne + row, scycle1=scycle1, scycle2=scycle2, scycle3=scycle3)
                    cel.save()
                    for c in contenu:
                        cc = ContenuCellule(cellule=cel, contenu=c)
                        cc.save()
                    
                rowspans[xpos-1] = cellule['rowspan']
                xpos += cellule['colspan']

            # Décrémenter rowspans avant de passer à la ligne suivante
            for idx, r in enumerate(rowspans):
                if r > 0:
                    rowspans[idx] -= 1

        doc.freeDoc()

def getGoodContent(node):
    """ This recursive function choose if node markup should be preserved or discarded
        Returns unciode string """
    content = ""
    if node.name in ('text','strong', 'br', 'em', 'a'):
        # "Final" nodes
        content = str(node.serialize(), source_encoding)
        if node.name == "a":
            if not node.content:
                content = ''
            else:
                href = node.prop('href')
                if href.find("ArtsObjectifs") != -1:
                    content = "<ulink url='dom:A'>%s</ulink>" % str(node.content, 'utf-8')
                elif href.find("CMPresentation") != -1:
                    content = "<ulink url='dom:CM'>%s</ulink>" % str(node.content, 'utf-8')
                elif href.find("FGPresentation") != -1:
                    content = "<ulink url='dom:FG'>%s</ulink>" % str(node.content, 'utf-8')
                elif href.find("SHSPresentation") != -1:
                    content = "<ulink url='dom:SHS'>%s</ulink>" % str(node.content, 'utf-8')
                # Exceptions: lien absolu, lien vers ancre, lien vers dossier Documents
                elif href[:4].lower() != "http" and href[0] != "#" and href[:9] != "Documents" :
                    try:
                        spec_code = normalizeCode(href.split(".")[-2].split("/")[-1]) # Get A_34 from '../A34.html'
                    except:
                        pdb.set_trace()
                    content = "<ulink url='spec:%s'>%s</ulink>" % (spec_code, str(node.content, 'utf-8'))
    else:
        # Recurse through child nodes and concatenate content
        child = node.children
        while child is not None:
            content += " %s" % getGoodContent(child)
            child = child.next
        if node.name == "p":
            content = "<p>%s</p>" % content
    content = content.strip(string.whitespace + u" ")
    return content

def normalizeCode(code):
    """ Normaliser le code à "CODEDOMAINE_NoOBJ" """
    if code[:5] == 'LNG2-':
        code = 'L2_%s' % code[5:]
    elif code[:3] == 'LNG':
        code = "L_" + code[3:]
    elif code[:3] in ['MSN','SHS']:
        code = code[:3] + '_' + code[3:]
    elif code[:2] in ['CM','FG']:
        code = code[:2] + '_' + code[2:]
    elif code[:1] == "A":
        code = "A_" + code[1:]
    else:
        print("Unable to normalize code '%s'" % code)
        pdb.set_trace()
    return code

def strip_first_char(ustring):
    if ustring[0] in (u"\xa0", u"\t"):
        return ustring[1:].strip() # Enlever premier caractère
    else:
        return ustring.strip()
