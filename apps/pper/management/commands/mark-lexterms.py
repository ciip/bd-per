import logging
import re

from django.core.management.base import BaseCommand

from pper.models import Domain, Cellule, Lexique


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--domains', dest='domains',
            help="specify a comma-separated list of domain abrevs to output (default to all)"
        )
        parser.add_argument("--unmark", action="store_true", dest="unmark")

    def handle(self, *args, **options):
        """ This command is used to mark lexique terms in all contents """

        unmark = options.get('unmark')
        domains = []
        if options.get('domains'):
            doms = [o.strip() for o in options['domains'].split(',')]
            doms = [Domain.objects.get(abrev__iexact=d) for d in doms]
        else:
            doms = Domain.objects.all()
        for dom in doms:
            spec_cells = Cellule.objects.filter(specification__objectif__domain=dom)
            dom_tabl_ids = [t.id for t in dom.get_tableaux()]
            tabl_cells = Cellule.objects.filter(tableau__id__in=dom_tabl_ids)
            if unmark:
                Lexique.unmark(spec_cells)
                Lexique.unmark(tabl_cells)
            else:
                # Mark global domain terms
                terms = Lexique.objects.filter(domain=dom)
                terms = [t for t in terms if t.disciplines.count() == 0]
                Lexique.mark(terms, spec_cells)
                Lexique.mark(terms, tabl_cells)
                for disc in dom.discipline_set.all():
                    # Mark discipline specific terms
                    terms = Lexique.objects.filter(domain=dom, disciplines=disc)
                    spec_cells = Cellule.objects.filter(specification__disciplines=disc)
                    spec_tabl_ids = [t.id for t in dom.get_tableaux() if t.discipline == disc]
                    tabl_cells = Cellule.objects.filter(tableau__id__in=spec_tabl_ids)
                    Lexique.mark(terms, spec_cells)
                    Lexique.mark(terms, tabl_cells)
