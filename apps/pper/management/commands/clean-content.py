from django.core.management.base import BaseCommand
from pper.management.commands.searchreplace import search_replace

from pper.models import Contenu

replacements = (
    ("^A ",       "À ", True),
    (". A ",      ". À ", False),
    ("Ech",       "Éch", False),
    ("Ecosystème","Écosystème", False),
    ("Ecoute",    "Écoute", False),
    ("Ecri",      "Écri", False),
    ("Ecre",      "Écre", False),
    ("Edit",      "Édit", False),
    ("Educ",      "Éduc", False),
    ("Elabor",    "Élabor", False),
    ("Elément",   "Élément", False),
    ("Enonc",     "Énonc", False),
    ("Equilibr",  "Équilibr", False),
    ("Etablissement", "Établissement", False),
    ("Etat",      "État", False),
    ("Etendue",   "Étendue", False),
    ("Ethique",   "Éthique", False),
    ("Etre",      "Être", False),
    ("Etud",      "Étud", False),
    ("Evalu",     "Évalu", False),
    ("Eveil",     "Éveil", False),
    ("Eviter",    "Éviter", False),
    ("Evo",       "Évo", False),
    
    ("...",          "…", False),
    ("oe",           "œ", False),
    ("</em>[ \t]*<em>", "", True),
    ("<em> </em>",   " ", False),
    ("<ul>\r\n</ul>", "", False),
    ("</em>…",       "…</em>", False),
    (", …",          ",…", False),
    #("[  ][  ]",    " "),
    ("</a> - ",      "</a> – ", False),
    ("CT - ",        "CT – ", False),
    ("> ([^<]*)</a>", ">\1</a>", True), # espace en premier dans un lien
    (" </a>",        "</a>", False), # espace en dernier dans un lien
    ("<a href=[^>]*></a>", "", True), # Lien vide
    (" ,",           ", ", False), # Espace avant une virgule
    ("([^=])\"([^ >][^\"]*)\"", "\1«\2»", True), # Remplacer guillemets droits par guillements français
)

# Things to fix manually
to_fix_manually = ("../", "<p>\r\n<p>")

class Command(BaseCommand):
    def handle(self, *args, **options):
        for repl in replacements:
            print("Recherche de %s:\n" % repl[0])
            search_replace(repl[0], repl[1], grep=repl[2])
            res = raw_input("Continuer le remplacement du mot suivant ? ('o' pour accepter) ")
            if res != 'o':
                break
        print("Fin des remplacements")

        for substr in to_fix_manually:
            conts = Contenu.objects.filter(texte__contains=substr)
            for cont in conts:
                print("Contenu (uid=%s) à corriger manuellement, contient '%s'" % (cont.uid, substr))

