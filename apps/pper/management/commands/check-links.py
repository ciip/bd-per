import re
from django.core.management.base import BaseCommand

from pper.models import Contenu, Specification, Domain

class Command(BaseCommand):

    def handle(self, *args, **options):
        """ This command is used to check all links in various content """
        cont_with_links = Contenu.objects.filter(texte__contains="<a href=")
        for cont in cont_with_links:
            links = re.findall(r"""<a href=['"]([^"']*)['"]>""", cont.texte)
            for link in links:
                m = re.match(r"(https?|spec|dom|niv|lex)://([-\w&;\.]*)", link)
                if not m:
                    print(u"(Contenu UID: %s) La syntaxe d'un lien n'est pas correcte (%s)." % (cont.uid, link))
                    continue
                if m.groups()[0] in ('http', 'https'):
                    print(u"(Contenu UID: %s) Lien Web: %s" % (cont.uid, m.groups()[1]))
                elif m.groups()[0] == 'spec':
                    if m.groups()[1] in ('CT_APPR', 'CT_COLL', 'CT_COMM', 'CT_CREA', 'CT_REFL'):
                        continue
                    code = m.groups()[1].replace("_", " ")
                    try:
                        spec = Specification.objects.get(code=code)
                    except Specification.DoesNotExist:
                        print(u"(Contenu UID: %s) Le lien spec://%s n'est pas correct" % (cont.uid, m.groups()[1]))
                elif m.groups()[0] == 'dom':
                    code = m.groups()[1]
                    try:
                        dom = Domain.objects.get(abrev=code)
                    except Domain.DoesNotExist:
                        print(u"(Contenu UID: %s) Le lien dom://%s n'est pas correct" % (cont.uid, m.groups()[1]))
                elif m.groups()[0] == 'niv':
                    pass

