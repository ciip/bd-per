from django.core.management.base import BaseCommand
from django.db.models import Count

from pper.models import Contenu

class Command(BaseCommand):

    def handle(self, *args, **options):
        """ This command is used to delete content that is no more referenced anywhere """
        content_to_purge = []
        no_cell_content = Contenu.objects.annotate(num_cells=Count('contenucellule')).filter(num_cells__exact=0)
        for content in no_cell_content:
            if (content.spec_pre_content.count() + content.spec_post_content.count() + content.tabl_pre_content.count() + content.tabl_post_content.count()) > 0:
                continue
            # UID between 10000 and 10030 are reserved for special content
            if content.uid >= 10000 and content.uid <= 10030:
                continue
            content_to_purge.append(content.id)
        print(u"%d éléments de contenu à supprimer" % len(content_to_purge))
        
        confirm = raw_input(u"Voulez-vous vraiment supprimer ces contenus ? (o pour accepter)")
        if confirm == "o":
            Contenu.objects.filter(id__in=content_to_purge).delete()
            print(u"Contenu supprimé")
        else:
            print(u"Aucun contenu supprimé")
