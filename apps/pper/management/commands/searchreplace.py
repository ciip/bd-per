import logging
import re

from itertools import chain
from django.core.management.base import BaseCommand

from pper.models import Contenu, Specification, Tableau, Objectif
from pper.static_content import COM_GEN_UIDS

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
COLOR_SEQ = "\033[1;%dm"
CONTENT_TO_SKIP = [10020, 10021, 10022, 10023, 10024, 10025] # Commentaire généraux de domaine


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--grep', action="store_true", dest='grep',
            help="perform a regex search in the database"
        )
        parser.add_argument(
            '--cycle', dest='cycle',
            help="specify a comma-separated list of cycle numbers to search in (default to all)"
        )
        parser.add_argument(
            '--titles', action="store_true", dest='in_titles',
            help="search only in title contents"
        )

    def handle(self, *args, **options):
            
        if len(args) != 2:
            print("Usage: search-replace str_to_search replace_str")
            return
        to_search = str(args[0], 'utf-8').replace("\\n", "\n").replace("\\r", "\r")
        to_replace = str(args[1], 'utf-8')
        if options.get('cycle'):
            cycles = [int(c) for c in options.get('cycle').split(",")]
        else:
            cycles = [1, 2, 3]
        print(search_replace(to_search, to_replace, cycles,
                             grep=options.get('grep'), in_titles=options.get('in_titles')))

def filter_cycles(ct):
    is_target = False
    # UID between 10000 and 10030 are reserved for special content
    if ct.uid >= 10000 and ct.uid <= 10030:
        return False
    for sp in chain(ct.spec_post_content.all(), ct.spec_pre_content.all()):
        is_target = sp.objectif.cycle in cycles
        if is_target:
            return True
    for tb in chain(ct.tabl_post_content.all(), ct.tabl_pre_content.all()):
        if isinstance(tb.per_object, Objectif):
            is_target = tb.per_object.cycle in cycles
            if is_target:
                return True
    for cc in ct.contenucellule_set.all():
        container = cc.cellule.get_super()
        if isinstance(container, Specification):
            is_target = container.objectif.cycle in cycles
        elif isinstance(container, Tableau) and isinstance(container.per_object, Objectif):
            is_target = container.per_object.cycle in cycles
        else:
            logging.warn("Unable to determine the cycle property of object %s" % container)
        if is_target:
            return True
    #if not is_target:
    #    used_in  = ", ".join([str(obj) for obj in ct.usage()])
    #    logging.info("Content used in %s does not match cycles %s" % (used_in, cycles))
    return is_target

def search_replace(to_search, to_replace, cycles=[1,2,3], grep=False, in_titles=False):
        if grep:
            #mysql_to_search = to_search.replace("\w", "[[:alpha:]]")  # MySQL wouldn't support \w
            conts = Contenu.objects.filter(texte__regex=to_search)
        else:
            conts = Contenu.objects.filter(texte__contains=to_search)
        if in_titles:
            conts = conts.filter(cellule__type_c__lt=0)
        if len(cycles) < 3:
            conts = filter(filter_cycles, conts)
        print("%d items found." % len(conts))

        ask = True
        replaced = 0
        for contenu in conts:
            if contenu.uid in COM_GEN_UIDS:
                print(u"Contenu %s sauté" % contenu.uid)
                continue
            old_cont = contenu.texte
            used_in_str  = ", ".join([str(obj) for obj in contenu.usage()])
            if grep:
                new_cont = re.sub(to_search, to_replace, contenu.texte, re.UNICODE)
                old_cont_colored = re.sub("("+to_search+")", r"%s\1%s" % (COLOR_SEQ % RED, COLOR_SEQ % BLACK), old_cont, re.UNICODE)
                new_cont_colored = re.sub(to_search, "%s%s%s" % (COLOR_SEQ % RED, to_replace, COLOR_SEQ % BLACK), old_cont, re.UNICODE)
            else:
                new_cont = contenu.texte.replace(to_search, to_replace)
                old_cont_colored = old_cont.replace(to_search, '%s%s%s' % (COLOR_SEQ % RED, to_search, COLOR_SEQ % BLACK))
                new_cont_colored = new_cont.replace(to_replace, '%s%s%s' % (COLOR_SEQ % RED, to_replace, COLOR_SEQ % BLACK))
            if ask:
                print(u"Remplacer (uid %d, %s):\n%s\n par\n%s" % (
                    contenu.uid, used_in_str, old_cont_colored, new_cont_colored
                ))
                res = raw_input(u"Taper 'o' pour accepter ('q' pour quitter, 'tout' pour tout remplacer): ")
                if res == u"o" or res == u"tout":
                    contenu.texte = new_cont
                    contenu.save()
                    replaced += 1
                    print(u"Contenu modifié et enregistré")
                    if res == u"tout":
                        ask = False
                elif res == "q":
                    break
                else:
                    print(u"Modification non effectuée.")
            else:
                contenu.texte = new_cont
                contenu.save()
                replaced += 1
        return u"%s remplacements effectués" % replaced

