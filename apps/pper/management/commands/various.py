from django.core.management.base import BaseCommand
from pper import models

class Command(BaseCommand):

    def handle(self, *args, **options):
         """ Managmenet script to do various bulk updates """
         self.migrate_composantes()
    
    def various(self):
         objs = models.Objectif.objects.all()
         for obj in objs:
             if not obj.specification_set.all():
                 print("(%s) %s" % (obj.code, obj.title))
             #print("%s: %s" % (obj.code, ",".join([sp.code for sp in obj.specification_set.all()])))
             """new_code = obj.code.replace("_"," ")
             if new_code != obj.code:
                 old_code = obj.code
                 obj.code = new_code
                 obj.save()
                 print("Code '%s' changed to '%s'" % (old_code, new_code))
             """


    def create_spec_for_objs(self):
        content_txt = u"Cet objectif n'est pas travaillé pour lui-même. Il s'active dans les autres axes seulement."
        non_developped_obj = {
            'MSN 25': (u'Mathématiques', u'Sciences de la Nature'),
            'MSN 35': (u'Mathématiques', u'Sciences de la Nature'),
            'FG 11': (u'Identité', ),
            'FG 21': (u'Identité', ),
            'FG 31': (u'Identité', ),
            'SHS 23': (u'Histoire', u'Géographie'),
            'SHS 33': (u'Histoire', u'Géographie'),
            'L 37': (u'Français', ),
            'L 38': (u'Français', )
        }
        for obj_code, obj_discs in non_developped_obj.items():
            obj = models.Objectif.objects.get(code=obj_code)
            for disc_name in obj_discs:
                disc = models.Discipline.objects.get(name=disc_name)
                # new spec
                if len(obj_discs) > 1:
                    spec_code = "%s%s" % (obj.code, disc_name[0])
                else:
                    spec_code = obj.code
                spec = models.Specification(uid=models.UniqueId.getNewID(), objectif = obj,
                                     discipline=disc, code=spec_code, rendered=False)
                spec.save()
                print("Created specification '%s' for discipline '%s'" % (obj.code, disc_name))
                # new content
                content = models.Contenu(texte=content_txt)
                content.save()
                # new cell
                cell = models.Cellule(uid=models.UniqueId.getNewID(), specification = spec, position=0,
                               scycle1=True, scycle2=True, scycle3=True,
                               attentes=False, indications=False, type_c=models.TYPE_ELEMENT)
                cell.save()
                # link cell and content
                cc = models.ContenuCellule(cellule=cell, contenu=content, position=0)
                cc.save()

    def migrate_composantes(self):
        import re
        from pper.models import Contenu, Composante
        conts = Contenu.objects.filter(texte__contains="comp://")
        re_comp = r"""<a href=['"]comp://([^#]*)#([A-I])['"]>[^<]*</a>"""
        for cont in conts:
            m = re.findall(re_comp, cont.texte)
            if not m:
                print("Warning: link not found in Contenu id %d" % cont.id)
            else:
                comps = []
                for found in m:
                    obj_code = found[0].replace('_', ' ')
                    obj_letter = found[1]
                    try:
                        comps.append(Composante.objects.get(objectif__code=obj_code, letter=obj_letter))
                    except Composante.DoesNotExist:
                        print("Warning: code '%s' with letter '%s' not found in Contenu id %d" % (obj_code, obj_letter, cont.id))
                        break
                new_cont = re.sub(re_comp, "", cont.texte).strip()
                if new_cont[-5:] == "&amp;":
                    new_cont = new_cont[:-5].strip()
                if new_cont[-1] == "&":
                    new_cont = new_cont[:-1].strip()
                print("Replace:\n'%s'\nby\n'%s'" % (cont.texte, new_cont))
                rep = raw_input("Enter o to accept: ")
                if rep == "o":
                    for cc in cont.contenucellule_set.all():
                        cc.composantes = comps
                    cont.texte = new_cont
                    cont.save()
                else:
                    break

