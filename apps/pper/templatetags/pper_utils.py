from django import template
from django.utils.html import strip_tags as django_strip_tags

register = template.Library()


@register.filter
def strip_tags(txt):
    return django_strip_tags(txt)
