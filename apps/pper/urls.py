from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('recherche/', views.search, name='search'),
    path('images/', views.images, name='images'),
    path('domain/<int:dom_id>/', views.domain, name='domain'),
    # Lexique
    path('domain/<int:dom_id>/lexique/', views.lexique, name='lexique'),
    path('domain/<int:dom_id>/lexique/<int:lex_id>/edit/', views.lexique_edit, name='lexique_edit'),
    path('lexique/delete/', views.lexique_delete, name='lexique_delete'),

    path('domain/spcant/', views.spec_cantonales, name='spec_cantonales'),
    path('discipline/<int:pk>/', views.DisciplineView.as_view(), name='discipline'),
    path('tableau/<int:tableau_id>/', views.tableau, name='tableau'),
    path('tableau/<int:tableau_id>/set_visible/', views.tableau_set_visible, name='tableau_set_visible'),
    path('course/<int:course_id>/', views.course, name='course'),
    path('course/<int:course_id>/composantes/', views.composantes, name='composantes'),

    path('course/<slug:obj_type>/<int:obj_id>/update-table/', views.CourseTableBaseView.as_view(),
        name='course_table_data'),
    path('course/<slug:obj_type>/<int:obj_id>/cell/<fint:cell_id>/', views.course_cell_content,
        name='course_cell_content'),
    path('course/<slug:obj_type>/<int:obj_id>/insert-row/', views.CourseTableInsertRow.as_view(),
        name='course_insert_row'),
    path('course/<slug:obj_type>/<int:obj_id>/delete-row/', views.CourseTableDeleteRow.as_view(),
        name='course_delete_row'),
    path('course/<slug:obj_type>/<int:obj_id>/moveup-row/', views.CourseTableMoveupRow.as_view(),
        name='course_moveup_row'),
    path('course/<slug:obj_type>/<int:obj_id>/grow-down/', views.CourseTableGrowdownRow.as_view(),
        name='course_grow_down'),
    path('course/<slug:obj_type>/<int:obj_id>/shrink-up/', views.CourseTableShrinkupRow.as_view(),
        name='course_shrink_up'),
    path('course/<slug:obj_type>/<int:obj_id>/grow-right/', views.CourseTableGrowrightRow.as_view(),
        name='course_grow_right'),
    path('course/<slug:obj_type>/<int:obj_id>/shrink-left/', views.CourseTableShrinkleftRow.as_view(),
        name='course_shrink_left'),
    path('course/<slug:obj_type>/<int:obj_id>/content-delete/', views.CourseTableDeleteContent.as_view(),
        name='content_delete'),
    path('course/<slug:obj_type>/<int:obj_id>/tip-delete/', views.CourseTableDeleteTip.as_view(),
        name='tip_delete'),

    path('content/<int:content_id>/', views.content),
    path('content/byuid/<int:content_uid>/', views.content_by_uid, name='content_by_uid'),
    path('cell/<fint:cell_id>/', views.cell_content, name='cell_content'),
    path('cell/<fint:cell_id>/content/new/', views.content_new, name='content_new'),
    path('cell/<fint:cell_id>/content/<int:content_id>/edit/', views.content_edit,
        name='content_edit'),
    path('cell/<fint:cell_id>/tip/new/', views.tip_new, name='tip_new'),
    path('cell/<fint:cell_id>/tip/<int:tip_id>/edit/', views.tip_edit,
        name='tip_edit'),
    path('upload_image/', views.upload_image, name='upload_image'),
    path('cell/setborder/', views.CourseTableSetBorder.as_view(), name='set_border'), # cell_id / mode passed in POST
    path('objectif/<int:obj_id>/', views.objectif, name='objectif'),
    path('objectif/<int:obj_id>/composante/<int:comp_id>/edit/', views.composante_edit,
        name='composante_edit'),
    path('composante/delete/', views.composante_delete, name='composante_delete'), # id passed in POST

    path('attentes/', views.attentes),
    path('attentes/discipline/<int:uid>/', views.attentes_discipline, name='attentes_discipline'),
    path('attentes/cycle/<int:no>/', views.attentes_cycle, name='attentes_cycle'),

    path('titres1/', views.titres),
    path('liens/', views.liens),
    path('ordreimp/', views.print_order, name='print_order'),
    path('export2xml/', views.export2xml, name='export2xml'),
]
