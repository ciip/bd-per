var edited_cell_id = null;
var csrftoken = Cookies.get('csrftoken');

function update_table_body() {
    $.post(table_data_url, {csrfmiddlewaretoken: csrftoken}, update_table_data);
    return false;
}

function update_table_data(data) {
    $("#course-body").html( data );
    $("#course-body").ready(addClickHandlers);
    $(".edit_column").show();
    $(".edit_widgets").show();
}

function update_composantes() {
    $("#composantes").load(composantes_url, "",
        function (responseText, textStatus, XMLHttpRequest) {
          $(".edit_widgets").show();
        });
}

function update_prepostcontent() {
    $("#cell-pre_content").load(pre_content_url, "",
        function (responseText, textStatus, XMLHttpRequest) {
          $(".edit_widgets", "#cell-pre_content").show();
        });
    $("#cell-post_content").load(post_content_url, "",
        function (responseText, textStatus, XMLHttpRequest) {
          $(".edit_widgets", "#cell-post_content").show();
        });
}

function update_cell(cell_id, load_url) {
    if (cell_id <= 0) {
        update_prepostcontent();
        return false;
    }
    $("#cell-"+cell_id).load(load_url, "",
        function (responseText, textStatus, XMLHttpRequest) {
        $(".edit_widgets", "#cell-"+cell_id).show();
        }
    );
}

function add_row(link, key) {
    link.parentNode.innerHTML = "<img src='"+ ajaxLoaderImg + "'>";
    $.post(add_row_url, {csrfmiddlewaretoken: csrftoken, after: key},
      update_table_data
    );
    return false; 
}

function del_row(link, key) {
    if (!confirm("Voulez-vous vraiment supprimer cette ligne ?"))
      return false;
    var parent = link.parentNode;
    var oldContent = parent.innerHTML;
    parent.innerHTML = "<img src='"+ ajaxLoaderImg + "'>";
    $.post(del_row_url, {csrfmiddlewaretoken: csrftoken, row_num: key},
      update_table_data
    ).fail(function() {
      parent.innerHTML = oldContent;
      alert("Impossible de supprimer cette ligne. Si des ressources sont liées, supprimez d'abord ces liens.");
    });
    return false; 
}

function moveup_row(link, key) {
    if (!confirm("Déplacer cette ligne au-dessus ?"))
      return false;
    link.parentNode.innerHTML = "<img src='"+ ajaxLoaderImg + "'>";
    $.post(moveup_row_url, {csrfmiddlewaretoken: csrftoken, row_num: key},
      update_table_data
    );
    return false;
}

function grow_down(ids) {
    $.post(grow_down_url, {csrfmiddlewaretoken: csrftoken, cell_ids: ids},
      update_table_data
    );
    return false;
}

function shrink_up(id) {
    $.post(shrink_up_url, {csrfmiddlewaretoken: csrftoken, cell_id: id},
      update_table_data
    );
    return false;
}

function grow_right(id) {
    $.post(grow_right_url, {csrfmiddlewaretoken: csrftoken, cell_id: id},
      update_table_data
    );
    return false;
}

function shrink_left(id) {
    $.post(shrink_left_url, {csrfmiddlewaretoken: csrftoken, cell_id: id},
      update_table_data
    );
    return false;
}

function tinyNiveaux(editor) {
    function insertNiv(value) {
        editor.execCommand('mceInsertLink', false, {href : "niv://" + value, 'class' : "niveau"});
    }
    editor.ui.registry.addMenuButton('niveaux', {
        text: 'Niveaux',
        fetch: function(callback) {
            var items = [
                {type: 'menuitem', text:'s', onAction: () => insertNiv('s')},
                {type: 'menuitem', text:'1', onAction: () => insertNiv('1')},
                {type: 'menuitem', text:'1s', onAction: () => insertNiv('1s')},
                {type: 'menuitem', text:'1-2', onAction: () => insertNiv('1-2')},
                {type: 'menuitem', text:'1s-2', onAction: () => insertNiv('1s-2')},
                {type: 'menuitem', text:'1s-2-3', onAction: () => insertNiv('1s-2-3')},
                {type: 'menuitem', text:'2', onAction: () => insertNiv('2')},
                {type: 'menuitem', text:'2s', onAction: () => insertNiv('2s')},
                {type: 'menuitem', text:'2-3', onAction: () => insertNiv('2-3')},
                {type: 'menuitem', text:'2s-3', onAction: () => insertNiv('2s-3')},
                {type: 'menuitem', text:'2s-3s', onAction: () => insertNiv('2s-3s')},
                {type: 'menuitem', text:'3', onAction: () => insertNiv('3')},
                {type: 'menuitem', text:'3s', onAction: () => insertNiv('3s')}
            ];
            callback(items);
        }
    });
}

function loadContentInModal(text, richEdit=false, afterSuccess=null) {
    const modal = document.querySelector('#modal');
    modal.querySelector('.modal-content').innerHTML = text;
    modal.classList.remove('hidden');
    if (modal.querySelectorAll('textarea.wysiwyg').length) {
        tinymce.remove();
        tinyOptions = {
            selector: ".wysiwyg",  // Only textareas with class='wysiwyg'
            theme: "silver",
            menubar: false,
            content_css: tinyCSS,  // defined in edition_headers.html
            entity_encoding: "raw"
        }
        if (richEdit) {
            tinyOptions = Object.assign({}, tinyOptions, {
                convert_fonts_to_spans: false,
                plugins: "lists link code table",
                toolbar: ["bold italic superscript subscript bullist numlist | link unlink formatselect | undo redo code",
                          "table"],
                block_formats: "Paragraphe=p;Section=div;Titre1=h1;Titre2=h2;Titre3=h3",
                valid_elements: "p,br,strong/b,em/i[class],span[class],font[color],sup,sub,ul,li,img[src|alt],a[href|name|target],"
                                +"table[border|class|style|width|cellspacing|cellpadding],tr,th[class|width|colspan],td[class|width|colspan|rowspan],h1[class],h2[class],h3[class],div[class|style],ol[class|style|type]"
            });
        } else if (modal.querySelectorAll('textarea.tip').length) {
            tinyOptions = Object.assign({}, tinyOptions, {
                plugins: "link code image hr",
                toolbar: ["bold italic hr | link unlink | image | undo redo code"],
                image_uploadtab: true,
                images_upload_url: images_upload_url,
                relative_urls: false,
                valid_elements: "p,br,hr,strong/b,em/i[class],img[src|alt],a[href|name|target|class]"
            });
        } else {
            tinyOptions = Object.assign({}, tinyOptions, {
                setup: tinyNiveaux,
                plugins: "lists link code",
                toolbar: "niveaux bold italic superscript subscript bullist | link unlink | undo redo code",
                valid_elements: "p,br,strong/b,em/i[class],sup,sub,ul,li,img[src|alt],a[href|name|target|class]" +
                ",math,mfrac,msqrt,mrow,mn,mi,mo,msub,msup"
            });
        }
        tinyMCE.init(tinyOptions);
    }
    modal.querySelector('form').addEventListener('submit', ev => {
        ev.preventDefault();
        const data = new FormData(ev.target);
        fetch(ev.target.action, {
            method: 'POST',
            headers: {
                'X-CSRFToken': Cookies.get("csrftoken")
            },
            body: data
        }).then(resp => {
            const contentType = resp.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                return resp.json().then(data => afterSuccess(data));
            } else {
                // Redisplay with errs?
                return resp.text().then(text => loadContentInModal(text, richEdit, afterSuccess));
            }
            console.log(resp);
        });
    });
    modal.querySelectorAll('.modal-close').forEach(item => {
        item.addEventListener("click", (ev) => {ev.preventDefault(); modal.classList.add('hidden');});
    });
    init_search_boxes();
}

function edit_content(link, richEdit=false) {
    fetch(link).then(resp => resp.text()).then(
        text => loadContentInModal(text, richEdit=richEdit, afterSuccess=(data) => {
            modal.classList.add('hidden');
            if (data.refreshPage) window.location.reload();
            else if (data.refreshTable) update_table_body();
            else if (data.composante) update_composantes();
            else update_cell(data.cell_id, data.cell_url);
        })
    );
    return false;
}

function delete_content(link, cell_id, content_id) {
    if (!confirm("Voulez-vous vraiment supprimer ce contenu ?"))
      return false;
    link.parentNode.innerHTML = "<img src='"+ ajaxLoaderImg + "'>";
    $.post(link.href, {csrfmiddlewaretoken: csrftoken, cell_id: cell_id, content_id: content_id},
      update_table_data
    );
    update_prepostcontent();
    return false;
}

function delete_composante(link, comp_id) {
    if (!confirm("Voulez-vous vraiment supprimer cette composante de l'objectif "+obj_code+" ?"))
      return false;
    $.post(delete_composante_url, {csrfmiddlewaretoken: csrftoken, comp_id: comp_id},
      update_composantes
    );
    return false;
}

function set_border(link, cell_id, mode) {
    link.parentNode.innerHTML = "<img src='"+ ajaxLoaderImg + "'>";
    $.post(set_border_url, {csrfmiddlewaretoken: csrftoken, cell_id: cell_id, mode: mode},
      update_table_data
    );
    return false;
}

function delete_lexique(link, item_id) {
    if (!confirm("Voulez-vous vraiment supprimer cette entrée du lexique ?"))
        return false;
    $.post(link.href, {item_id: item_id}, window.location.reload);
    return false;
}

function addClickHandlers() {
}

$(document).ready(function(){
    //addClickHandlers();
    $("#edit_button").click(function(){
        $(".edit_column").toggle();
        $(".edit_widgets").toggle();
        if ($(this).attr("value") == "Activer l'édition") {
            $(this).attr("value", "Désactiver l'édition");
            $(".resource_grp").sortable({
                axis: 'y',
                handle: 'img.inline',
                update: function(event, ui) {
                    var post_url = ui.item.parent().data('orderurl');
                    var data = $.makeArray(ui.item.parent().children().map(function() {
                        return $(this).data('grpid');
                    }));
                    $.post(post_url, {csrfmiddlewaretoken: csrftoken, 'grp_ids': data});
                }
            });
            addClickHandlers();
        } else {
            $(this).attr("value", "Activer l'édition");
            $(".resource_grp").sortable("destroy");
        }
    });

    // Buttons to push content on portail-ciip.ch
    const devBtn = document.querySelector("#pper_button_dev");
    const stagingBtn = document.querySelector("#pper_button_staging");
    const prodBtn = document.querySelector("#pper_button_prod");
    [devBtn, stagingBtn, prodBtn].forEach(btn => {
        if (btn) {
            btn.addEventListener('click', (ev) => {
                document.body.style.cursor = 'wait';
                $.post(btn.dataset.url, {csrfmiddlewaretoken: csrftoken}, (data) => {
                    alert(data);
                    document.body.style.cursor = 'default';
                });
            });
        }
    });

    $("input#id_init_visible").change(function(){
        var form = $(this).parent();
        $.post(form.attr('action'), form.serialize());
    });

    $(".pop_editor").click(function(ev) { ev.preventDefault(); edit_content(this); });

    $('.t_progression').on('click', 'a.resource_copy, a.resource_cut', function(ev) {
        ev.preventDefault();
        var cut = $(this).hasClass('resource_cut');
        $.post(this.href, {csrfmiddlewaretoken: csrftoken, obj_id: $(this).parent().data('resid')},
            function(data) {
                if (cut)
                    alert("Cette ressource a été coupée, mais elle ne sera réellement déplacée qu'au moment du collage.");
                else
                    alert("Cette ressource a été copiée.");
                $("a.paste_icon").show();
            }
        );
    });

    $('.t_progression').on('click', 'a.paste_icon', function(ev) {
        ev.preventDefault();
        $.post(this.href, {csrfmiddlewaretoken: csrftoken}, function(data) {
            $("a.paste_icon").hide();
            if (data.status == 'OK') {
                update_cell(data.old_cell_id, data.old_reload_url);
                update_cell(data.new_cell_id, data.new_reload_url);
            } else alert(data.message);
        }, 'json');
    });

    $('.t_progression').on('click', 'a.resource_delete', function(ev) {
        if (!confirm("Voulez-vous vraiment supprimer le lien vers cette ressource ?"))
            return false;
        ev.preventDefault();
        $.post(this.href, {csrfmiddlewaretoken: csrftoken, obj_id: $(this).parent().data('resid')},
            function(data) {
                update_cell(data.cell_id, data.reload_url);
            }, 'json'
        );
    });
});

