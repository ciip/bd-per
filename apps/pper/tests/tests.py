import shutil
import tempfile
from pathlib import Path

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission, User
from django.test import TestCase, override_settings
from django.urls import reverse, set_urlconf

from linkcheck.listeners import disable_listeners

from pper import filters
from pper import models
from pper.static_content import (
    CAPACITES_TRANS_UID, DECLARATION_CIIP_UID, PRES_GENERALE_UID,
)


class TempMediaRootMixin:
    @classmethod
    def setUpClass(cls):
        cls._temp_media = tempfile.mkdtemp()
        cls._overridden_settings = cls._overridden_settings or {}
        cls._overridden_settings['MEDIA_ROOT'] = cls._temp_media
        Path.mkdir(Path(cls._temp_media, 'qrcodes'), exist_ok=True)
        Path.mkdir(Path(cls._temp_media, 'resource_thumbs'), exist_ok=True)
        Path.mkdir(Path(cls._temp_media, 'ressources'), exist_ok=True)
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls._temp_media)
        super().tearDownClass()


@override_settings(PASSWORD_HASHERS=(
    'django.contrib.auth.hashers.MD5PasswordHasher',
))
class BDPERTests(TempMediaRootMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Disable linkcheck listeners
        cls.enterClassContext(disable_listeners())

    @classmethod
    def setUpTestData(cls):
        """ Create basic bd-per structure """
        super().setUpTestData()
        User = get_user_model()
        cls.user = User.objects.create_user('john', 'doe@example.org', 'johnpw')
        cls.user.user_permissions.add(Permission.objects.get(codename='view_contenu'))

        cls.domain = models.Domain.objects.create(
            name="Langues", abrev="L", visee="Visée", position=1, col_dark='#D9B222', col_light='#FBF0CD'
        )
        cls.thematique = models.Thematique.objects.create(
            domain=cls.domain, name="Thématique 1", order_no=1
        )
        cls.objectif = models.Objectif.objects.create(title="Objectif 1", cycle=1, code="L 11", domain=cls.domain)
        cls.objectif.thematiques.add(cls.thematique)
        cls.composante = models.Composante.objects.create(letter='A', title="Composante 1", objectif=cls.objectif)
        cls.discipline = models.Discipline.objects.create(
            name="Français", slug="francais", lom_id="french school language", domain=cls.domain, position=1
        )
        cls.spec = models.Specification.objects.create(objectif=cls.objectif, code="L 11", status='published')
        cls.spec.disciplines.add(cls.discipline)
        cls.spec.insert_row(after=0)
        cls.lex1 = models.Lexique.objects.create(domain=cls.domain, term="ostinato", definition="Définition du mot ostinato")
        # Create static and mandatory content
        c1 = models.Contenu.objects.create(uid=DECLARATION_CIIP_UID, texte=u"Déclaration de la CIIP")
        c2 = models.Contenu.objects.create(uid=PRES_GENERALE_UID, texte=u"Présentation générale")
        c3 = models.Contenu.objects.create(uid=CAPACITES_TRANS_UID, texte=u"Capacités transversales")

    def tearDown(self):
        """ Remove caching of user in local thread """
        super().tearDown()
        from journal.middleware import _thread_locals
        if hasattr(_thread_locals, 'user'):
            delattr(_thread_locals, 'user')

    @classmethod
    def create_domain_chain(cls, domain_name, discipline_name, obj_cycle=1):
        abrev = domain_name[0:1]
        dom = models.Domain.objects.create(name=domain_name, abrev=abrev, visee=u"Visée", position=2)
        thematique = models.Thematique.objects.create(
            domain=dom, name=u"Thématique %s" % domain_name, order_no=1)
        objectif = models.Objectif.objects.create(
            title="Objectif %s" % domain_name, cycle=obj_cycle, code="%s 11" % abrev, domain=dom)
        discipline = models.Discipline.objects.create(
            name=discipline_name, slug=discipline_name.lower(), domain=dom, position=1)
        spec = models.Specification.objects.create(
            objectif=objectif, code="%s 11" % abrev, status='published')
        spec.disciplines.add(discipline)
        spec.insert_row(after=0)


class DisplayTests(BDPERTests):
    def test_cell_display(self):
        cell = self.spec.cellule_set.all()[0]
        c1 = models.Contenu.objects.create(texte="<p>Un paragraphe</p>")
        cell.add_content(c1, None, None)

        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('course_cell_content', args=['spec', self.spec.pk, cell.pk]))
        self.assertContains(response, "Un paragraphe")

    def test_pre_content_display(self):
        self.spec.pre_content = models.Contenu.objects.create(texte="<p>Un paragraphe</p>")
        self.spec.save()
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('course_cell_content', args=['spec', self.spec.pk, -1]))
        self.assertContains(response, "Un paragraphe")


class GeneralTests(BDPERTests):
    def test_filter_spec_pusblished(self):
        """ Test the published Specification manager method """
        disc = models.Discipline.objects.create(name=u"Français", domain=self.domain, position=2, published=False)
        spec = models.Specification.objects.create(objectif=self.objectif, code="L 14")
        spec.disciplines.add(disc)
        self.assertEqual(models.Specification.objects.all().count(), 2)
        self.assertEqual(models.Specification.objects.published().count(), 1)


class EditionTests(BDPERTests):
    def test_content_new(self):
        self.client.login(username='john', password='johnpw')
        cell = models.Cellule.objects.all()[0]
        response = self.client.get(reverse('content_new', args=[cell.pk]))
        self.assertContains(response, '<input type="submit" value="Créer contenu">', html=True)
        response = self.client.post(
            reverse('content_new', args=[cell.pk]), data={
                'texte': "Du nouveau texte",
                'type_c': cell.type_c,
        })
        cell.refresh_from_db()
        self.assertEqual(cell.get_content(), "Du nouveau texte")

    def test_content_edit(self):
        self.client.login(username='john', password='johnpw')
        cell = models.Cellule.objects.all()[0]
        url = reverse('content_edit', args=[cell.id, '999999'])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class FilterTests(BDPERTests):
    def test_niveau_filters(self):
        content1 = u"<p>Content without niveau indication</p>"
        content2 = u"""<a href="niv://2" class="niveau">Première partie</a>, <a href="niv://3" class="niveau">deuxième partie</a>"""
        self.assertEqual(filters.HTMLNiveauContentFilter()(content1, {}), content1)
        self.assertEqual(filters.HTMLNiveauContentFilter()(content2, {}),
            u"Première partie <span class=\"ref_niveau\">(Niv 2)</span>, deuxième partie <span class=\"ref_niveau\">(Niv 3)</span>")

    def test_lexique_filters(self):
        content1 = """<p>Ceci est un <a href="lex://ostinato">ostinato</a></p>"""
        self.assertEqual(filters.HTMLLexiqueFilter()(content1, {'domain': self.domain}),
            u"""<p>Ceci est un <span class="lex_term" title="Définition du mot ostinato">ostinato</span></p>""")
        content2 = """<p>Ceci est un <a href="lex://introuvable">ostinato</a></p>"""
        self.assertEqual(filters.HTMLLexiqueFilter()(content2, {'domain': self.domain}),
            u"""<p>Ceci est un <span class="lex_mark_notfound" title="Erreur: définition introuvable">*</span>ostinato</p>""")
        self.assertEqual(filters.XMLLexiqueFilter()(content1, {'domain': self.domain}),
            u"""<p>Ceci est un <span style="border-bottom:1px dotted gray;" title="Définition du mot ostinato">ostinato</span></p>""")

    def test_image_filters(self):
        content1 = ""
        pass #FIXME

    def test_speclinks_filters(self):
        content1 = '<p>Un texte avec un <a href="spec://CT_APPR">lien</a>, et même <a href="spec://L_11">un aûtre</a></p>'
        self.assertEqual(
            filters.SpecLinkFilter()(content1, {}),
            '<p>Un texte avec un <a href="/gestion/content/byuid/%s/#APPR" class="linkCT">lien</a>, et même <a href="/gestion/course/%d/" class="linkL">un aûtre</a></p>' % (CAPACITES_TRANS_UID, self.spec.pk)
        )
        self.assertEqual(
            filters.HTMLPubSpecLinkFilter()(content1, {}),
            '<p>Un texte avec un <a href="/pub/content/cap-trans/#APPR" class="linkCT">lien</a>, et même <a href="/pub/objectif/L_11/" class="linkL">un aûtre</a></p>'
        )
        try:
            set_urlconf("public.pper_urls")
            self.assertEqual(
                filters.HTMLPubSpecLinkFilter()(content1, {}),
                '<p>Un texte avec un <a href="/web/guest/capacites-transversales/#APPR" class="linkCT">lien</a>, et même <a href="/web/guest/L_11/" class="linkL">un aûtre</a></p>'
            )
        finally:
            set_urlconf(None)

    def test_speclinks_filter_with_anchor(self):
        content1 = '<p>Un texte avec un <a href="spec://L_11#304">lien</a></p>'
        self.assertEqual(
            filters.HTMLPubSpecLinkFilter()(content1, {}),
            '<p>Un texte avec un <a href="/pub/objectif/L_11/#304" class="linkL">lien</a></p>'
        )
