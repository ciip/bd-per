from django.contrib.auth.backends import ModelBackend, RemoteUserBackend
from django.utils import timezone

from api.models import Token

header_map = {
    'Shib-InetOrgPerson-mail': 'email',
    'Shib-Person-surname': 'last_name',
    'Shib-InetOrgPerson-givenName': 'first_name',
    'mail': 'email',  # GE, VD
    'displayName': 'last_name',  # GE, contains full name
    # FR
    'DisplayName': 'last_name',
    'EMail': 'email',
    'FirstName': 'first_name',
    'LastName': 'last_name',
}

GE_AUTHORIZED_GROUPS = ['EEL.EP.ETAT', 'EEL.CO.ETAT', 'EEL.PO.ETAT', 'EEL.DIP.ETAT']


class ShibbolethRemoteBackend(RemoteUserBackend):
    def authenticate(self, request, remote_user):
        if 'geneveid.ch' in remote_user:
            # Check status in memberOf attribute
            if not any(gr in request.META.get('memberOf') for gr in GE_AUTHORIZED_GROUPS):
                return None
        if 'studentfr.ch' in remote_user:
            # Students are not allowed to connect for now.
            return None
        if 'edu.vs.ch' in remote_user and 'Enseignant' not in request.META.get('userroleflat', ''):
            return None
        if 'pupil' in request.META.get('EdulogPersonRole', ''):
            return None

        user = super().authenticate(request, remote_user)
        if user:
            # Update user properties from shibboleth headers
            changed = False
            for header, field in header_map.items():
                req_value = request.META.get(header)
                if req_value is None:
                    continue
                if field == 'first_name':
                    req_value = req_value[:30]  # Prevent field overflow
                if getattr(user, field) != req_value:
                    setattr(user, field, req_value)
                    changed = True
            if changed:
                user.save()
        return user


class TokenUserBackend(ModelBackend):
    """Backend to authenticate based on key-based tokens (BSN-like)."""
    def authenticate(self, request, **kwargs):
        token_key = request.headers.get('X-Token-Key') if request else None
        if not token_key:
            return None
        try:
            token = Token.objects.get(
                sid=token_key, issued_at__gte=timezone.now() - Token.valid_duration
            )
        except Token.DoesNotExist:
            return None
        return token.user
