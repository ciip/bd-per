import re
from itertools import chain

from django import forms

from pper.models import (
    Contenu, Cellule, ContenuCellule, Composante, Illustration, Lexique,
    Discipline, Image, Tableau
)
from pper.models import (
    TYPE_ELEMENT, TYPE_TITRE1, TYPE_TITRE2, TYPE_TITRE2_ETENDU, TYPE_LIEN,
    TYPE_INDICATION
)


class ContentForm(forms.ModelForm):
    LEVEL_CHOICES = (
        ('', 'Tous'),
        ('s', 's'),
        ('1s', '1s'),
        ('1', '1'),
        ('1s,2', '1s,2'),
        ('1,2', '1,2'),
        ('1s,2,3', '1s,2,3'),
        ('2s', '2s'),
        ('2', '2'),
        ('2s,3s', '2s,3s'),
        ('2s,3', '2s,3'),
        ('2,3', '2,3'),
        ('3s', '3s'),        
        ('3', '3'),        
    )
    class Meta:
        model = Contenu
        fields = ('texte',)

    def __init__(self, cell, *args, **kwargs):
        """ cell may be none in certain context """
        self.cell = cell
        super().__init__(*args, **kwargs)
        if self.cell:
            # Determine if cell can be converted in title 1 or other wide cell type
            can_extend = False
            if cell.type_c in (TYPE_ELEMENT, TYPE_TITRE2) and cell.scycle1:
                can_extend = True
                # Test if all other cells in row are empty
                other_cells_on_line = Cellule.objects.filter(specification=cell.specification, tableau=cell.tableau, position=cell.position, scycle1=False)
                for c in other_cells_on_line:
                    if not c.is_empty():
                       can_extend = False
                       break
        
            type_choices = [(cell.type_c,cell.get_type_c_display())]
            if can_extend:
                type_choices.append((TYPE_TITRE1, 'titre 1'))
                type_choices.append((TYPE_TITRE2_ETENDU, 'titre 2 (pleine largeur)'))
            if self.cell.type_c == TYPE_ELEMENT:
                type_choices.append((TYPE_TITRE2, 'titre 2'))
                type_choices.append((TYPE_LIEN, 'pleine largeur'))
            if cell.type_c == TYPE_TITRE1:
                type_choices.append((TYPE_TITRE2, 'titre 2'))
            elif cell.type_c == TYPE_INDICATION and cell.specification.objectif.domain.abrev == "FG":
                type_choices.append((TYPE_TITRE2, 'titre 2'))
            if cell.type_c in (TYPE_TITRE1, TYPE_TITRE2, TYPE_TITRE2_ETENDU, TYPE_LIEN):
                type_choices.append((TYPE_ELEMENT, 'progression'))
            self.fields['type_c'] = forms.ChoiceField(label="Type:", choices=type_choices, required=True, initial=self.cell.type_c)
        if not self.cell or self.cell.type_c != TYPE_TITRE1:
            self.fields['texte'].widget.attrs.update({'class':'wysiwyg'})
            self.fields['texte'].widget.attrs.update({'cols': 60, 'rows': 10})
            self.fields['texte'].required = False  # Avoid TinyMCE issue
        self.has_comps = False
        if getattr(self.cell, 'specification', None):
            # Special handling for specification cells
            if self.instance.pk:
                try:
                    contenucellule = self.instance.contenucellule_set.get(cellule=self.cell)
                except ContenuCellule.DoesNotExist:
                    contenucellule = None
            else:
                contenucellule = None
            if self.cell.specification.objectif.cycle == 3:
                initial_level = getattr(contenucellule, 'level', '')
                self.fields['level'] = forms.ChoiceField(label="Niveau:", choices=self.LEVEL_CHOICES, required=False, initial=initial_level)
            self.comp_fields = []; self.bound_comp_fields = [];
            if self.cell.specification.objectif.domain.abrev in ('MSN', 'SHS'):
                # Add checkboxes for composantes
                self.has_comps = True
                for idx, obj in enumerate(
                    chain([self.cell.specification.objectif], self.cell.specification.get_linked_objectifs())):
                    self.comp_fields.append([])
                    self.bound_comp_fields.append([obj.code,[]])
                    for comp in obj.composante_set.all():
                        initial_state = False
                        try:
                            contenucellule.composantes.get(id=comp.id)
                            initial_state = True
                        except:
                            pass
                        self.fields['comp_%s' % comp.id] = forms.BooleanField(
                            label="%s %s" % (comp.get_letter(self.cell.specification.objectif), comp.title),
                            required=False, initial=initial_state)
                        self.comp_fields[idx].append('comp_%s' % comp.id)
                        self.bound_comp_fields[idx][1].append(self['comp_%s' % comp.id])

    def save(self, new_content, *args, **kwargs):
        """ Return True if currently edited table should be refreshed """
        super(ContentForm, self).save(*args, **kwargs)
        changed = False
        if self.cell:
            level_value = self.cleaned_data.get('level', '')
            if new_content:
                # Link new content to cell
                self.cell.add_content(content=self.instance, position=None, level=level_value)
            contenucellule = self.instance.contenucellule_set.get(cellule=self.cell)
            if not new_content and 'level' in self.fields:
                contenucellule.set_level(level_value)
                changed = True
            # Check if type_c select has changed
            if 'type_c' in self.fields:
                type_value = int(self.cleaned_data.get('type_c'))
                if self.cell and self.cell.type_c != type_value:
                    self.cell.change_type(int(type_value))
                    changed = True
            if self.has_comps:
                comp_list = [cf for cf in chain(*self.comp_fields) if self.cleaned_data.get(cf)]
                comp_objs = [Composante.objects.get(pk=comp.split('_')[1]) for comp in comp_list]
                contenucellule.set_composantes(comp_objs)
                changed = True
        return changed

    def clean_texte(self):
        texte = self.cleaned_data['texte']
        links = re.findall(r"""<a href=['"]([^"']*)['"]>""", texte)
        for link in links:
            m = re.match(r"(https?|spec|dom|niv|lex)://([\w&;]*)", link) or re.match(r"(#)(.*)", link)
            if not m:
                raise forms.ValidationError(u"La syntaxe d'un lien n'est pas correcte, impossible d'enregistrer.")
            else:
                if '&amp;' in m.groups()[1]:
                    texte = texte.replace(m.groups()[1], m.groups()[1].replace('&amp;','&'))
        check_image_exists(texte)
        return texte


class TipForm(forms.ModelForm):
    class Meta:
        model = Illustration
        exclude = ['cellule']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['texte'].required = False  # Avoid TinyMCE issue
        self.fields['texte'].widget.attrs.update({'class':'wysiwyg tip'})


class ComposanteForm(forms.ModelForm):
    class Meta:
        model = Composante
        fields = ('title',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'cols': 60, 'rows': 8})


class LexiqueForm(forms.ModelForm):
    class Meta:
        model = Lexique
        exclude = ('domain',)

    def __init__(self, dom, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.domain = dom
        self.fields['definition'].widget.attrs.update({'class':'wysiwyg', 'cols': 60, 'rows': 8})
        self.fields['definition'].required = False  # Avoid TinyMCE issue
        self.fields['disciplines'].queryset = Discipline.objects.filter(domain=dom)

    def save(self, *args, **kwargs):
        self.instance.domain = self.domain
        super().save(*args, **kwargs)

    def clean_definition(self):
        texte = self.cleaned_data['definition']
        check_image_exists(texte)
        return texte


class SearchForm(forms.Form):
    searched_text = forms.CharField(label=u"Texte à rechercher")
    case_sensitiv = forms.BooleanField(label=u"Tenir compte de la casse", required=False)

    def search(self):
        searched_content = self.cleaned_data['searched_text']
        if self.cleaned_data['case_sensitiv']:
            return Contenu.objects.filter(texte__contains=searched_content)
        else:
            return Contenu.objects.filter(texte__icontains=searched_content)


class VisibleForm(forms.ModelForm):
    init_visible = forms.BooleanField(required=False, label="Tableau initialement visible (soufflet ouvert).")

    class Meta:
        model = Tableau
        fields = ('init_visible',)


# ******** Utility functions ************
def check_image_exists(txt):
    """ Find expressions like [[\sqrt{2}]] in text and create corresponding image if not existing """
    latex = re.findall(r"""\[\[([^\]]*)\]\]""", txt)
    for eq in latex:
        eq_obj = None
        try:
            eq_obj = Image.objects.get(source=eq)
            eq_obj.get_png_src() # Test if image is really created
        except Image.DoesNotExist:
            if not eq_obj:
                eq_obj = Image(source=eq)
            eq_obj.save()
