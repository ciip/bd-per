from django.contrib.postgres.fields import ArrayField
from django.forms import CheckboxSelectMultiple, TypedMultipleChoiceField


class ChoiceArrayField(ArrayField):
    """
    From https://blogs.gnome.org/danni/2016/03/08/multiple-choice-using-djangos-postgres-arrayfield/
    A field that allows us to store an array of choices.

    Uses Django's postgres ArrayField and a MultipleChoiceField for its formfield.
    See also https://code.djangoproject.com/ticket/27704
    """
    widget = CheckboxSelectMultiple

    def formfield(self, **kwargs):
        defaults = {
            'form_class': TypedMultipleChoiceField,
            'coerce': self.base_field.to_python,
            'choices': self.base_field.choices,
            'widget': self.widget,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)
