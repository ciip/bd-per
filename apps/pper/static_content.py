DECLARATION_CIIP_UID = 10010
PRES_GENERALE_UID = 10011
CAPACITES_TRANS_UID = 10012
COM_GEN_UIDS = [10020, 10021, 10022, 10023, 10024, 10025]

CAPACITES_TRANS = [
    {'code': 'COLL', 'title': "La Collaboration", 'uid': 10001},
    {'code': 'COMM', 'title': "La Communication", 'uid': 10002},
    {'code': 'APPR', 'title': "Les Stratégies d’apprentissage", 'uid': 10003},
    {'code': 'CREA', 'title': "La Pensée créatrice", 'uid': 10004},
    {'code': 'REFL', 'title': "La Démarche réflexive", 'uid': 10005},
]

# See Specification.sub_header() method for header customization by spec/domain
STATIC_CONTENT = {
    # No ID if content only used for paper output
    'c1':                 {'id': 100001, 'txt': "1er cycle"},
    'c2':                 {'id': 100002, 'txt': "2e cycle"},
    'c3':                 {'id': 100003, 'txt': "3e cycle"},
    'niv1':               {'id': 100006, 'txt': "Niveau 1"},
    'niv2':               {'id': 100007, 'txt': "Niveau 2"},
    'niv3':               {'id': 100008, 'txt': "Niveau 3"},

    'prog':               {'id': 100009, 'txt': "Progression des apprentissages"},
    'prog_fg':            {'id': 100010, 'txt': "Apprentissages à favoriser"},
    'c1_1':               {'id': 100011, 'txt': "1<sup>re</sup> – 2<sup>e</sup> années"},
    'c1_2':               {'id': 100012, 'txt': "3<sup>e</sup> – 4<sup>e</sup> années"},
    'c2_1':               {'id': 100013, 'txt': "5<sup>e</sup> – 6<sup>e</sup> années"},
    'c2_2':               {'id': 100014, 'txt': "7<sup>e</sup> – 8<sup>e</sup> années"},
    'c3_1':               {'id': 100015, 'txt': "9<sup>e</sup> année"},
    'c3_2':               {'id': 100016, 'txt': "10<sup>e</sup> année"},
    'c3_3':               {'id': 100017, 'txt': "11<sup>e</sup> année"},
    'c3_1_lat':           {'id': 100018, 'txt': "1<sup>re</sup> partie du cycle"},
    'c3_2_lat':           {'id': 100019, 'txt': "2<sup>e</sup> partie du cycle"},
    'c3_3_lat':           {'id': 100020, 'txt': "Prolongement"},

    'att' :               {'id': 100022, 'txt': "Attentes fondamentales"},
    'att_1':              {'id': 100023, 'txt': "Au cours, mais au plus tard à la fin du cycle, l'élève…"},
    'indic':              {'id': 100024, 'txt': "Indications pédagogiques"},
    'indic_1':            {'id': 100025, 'txt': "Ressources, indices, obstacles. Notes personnelles"},
    'premier_cycle':      {'id': None, 'txt': "PREMIER CYCLE"},
    'deuxieme_cycle':     {'id': None, 'txt': "DEUXIÈME CYCLE"},
    'troisieme_cycle':    {'id': None, 'txt': "TROISIÈME CYCLE"},
    'empty':              {'id': 100040, 'txt': ""},
    'pre_content_title':  {'id': 100041, 'txt': "Note préalable"},
    'post_content_title': {'id': 100042, 'txt': "Informations complémentaires"},
}
