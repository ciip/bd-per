import logging
import re
from functools import lru_cache

from django.urls import reverse
from django.utils.html import strip_tags
from pper.static_content import CAPACITES_TRANS_UID


class FilterChain(list):
    """ This is a list of filters to apply successively to some text content """
    def initialize(self):
        # Transform classes into instances
        return FilterChain([f() for f in self])

    def filter_content(self, content, context):
        if 'domain' not in context and 'objectif' in context:
            context['domain'] = context['objectif'].domain
        for f in self:
            content = f(content, context)
        return content


class CellFilter:
    """ Cell filter get a cell object, obtain its content and render as text """
    def __call__(self, cell):
        cell_content = cell.get_content_list()
        return "".join([c[1] for c in cell_content])

#class ResourceCellFilter
#class XMLResourceCellFilter

class ContentFilter:
    """ Content filter get text content, apply any logic and render it (still text) """
    def __call__(self, content, context):
        return content

    @classmethod
    def append_content(cls, txt_content, new_content):
        """ Intelligent append (add at least one space) and insert into <ul>, if needed """
        if txt_content.startswith("<ul>") and txt_content.endswith("</li>\r\n</ul>") and txt_content.count("<li>") == 1:
            # Special case when there is only a <ul> with a single <li>, append comp_str just before the ending </li>
            txt_content = "%s %s</li>\r\n</ul>" % (txt_content[:-12], new_content)
        else:
            txt_content = "%s %s" % (txt_content, new_content)
        return txt_content


# Niveau filters
class NiveauContentFilter(ContentFilter):
    re_link_niv  = re.compile(r"<a href=['\"]niv://([^'\"]*)['\"][^>]*>([^<]*)</a>")

class HTMLNiveauContentFilter(NiveauContentFilter):
    def __call__(self, content, context):
        content = self.re_link_niv.sub(r'\2 <span class="ref_niveau">(Niv \1)</span>', content)
        if 'contenucellule' in context and context['contenucellule'].level:
            mark = u"Niv %s" % context['contenucellule'].level.replace(',','-')
            content = self.append_content(content, '<span class="ref_niveau">(%s)</span>' % mark)
        return content

class PublicNiveauContentFilter(NiveauContentFilter):
    def __call__(self, content, context):
        content = self.re_link_niv.sub(r'\2 <span class="ref_niveau">Niv \1</span>', content)
        if 'contenucellule' in context and context['contenucellule'].level:
            mark = u"Niv %s" % contenucellule.level.replace(',','-')
            content = self.append_content(content, '<span class="ref_niveau">%s</span>' % mark)
        return content

class RawNiveauContentFilter(NiveauContentFilter):
    def __call__(self, content, context):
        def replace_link_niv(match):
            niv, link_cont = match.groups()
            return u"%s <NIV>Niv. %s</NIV>" % (link_cont, niv.replace(u'-',u' | '))
        #content = self.re_link_niv.sub(r'\2 <NIV>Niv. \1</NIV>', content)
        content = self.re_link_niv.sub(replace_link_niv, content)
        if 'contenucellule' in context and context['contenucellule'].level:
            content = u"%s <NIV>Niv. %s</NIV>" % (content, contenucellule.level.replace(',',' | '))
        return content


# Composante filters
class ComposanteContentFilter(ContentFilter):
    def __call__(self, content, context):
        if 'contenucellule' in context and context['contenucellule'].composantes:
            comp_list = [(comp.get_letter(context['objectif']), comp) for comp in context['contenucellule'].composantes.all()]
            if comp_list:
                comp_list.sort()
                content = self.render_comp_list(content, comp_list)
        return content

    def render_comp_list(self, content, comp_list):
        comp_str = " ".join(["%s" % letter for letter, comp in comp_list])
        return "%s <COMP>%s</COMP>" % (content, comp_str)

class HTMLComposanteContentFilter(ComposanteContentFilter):
    def render_comp_list(self, content, comp_list):
        comp_str = ", ".join(['<span class="ref_composante" title="%s: %s">%s</span>' % (comp.objectif.code, comp.title, letter) for letter, comp in comp_list])
        return self.append_content(content, "(%s)" % comp_str)

class PublicComposanteContentFilter(ComposanteContentFilter):
    def render_comp_list(self, content, comp_list):
        comp_str = " ".join(['<span class="ref_composante" title="%s: %s">%s</span>' % (comp.objectif.code, comp.title, letter) for letter, comp in comp_list])
        return self.append_content(content, comp_str)
                    
class XMLComposanteContentFilter(ComposanteContentFilter):
    def render_comp_list(self, content, comp_list):
        comp_str = ", ".join(["""<a href="comp://%(obj_code)s#%(letter)s">%(disp_letter)s</a>""" % {
            'obj_code': comp.objectif.code, 'letter': comp.letter, 'disp_letter': letter
            } for letter, comp in comp_list])
        return self.append_content(content, "(%s)" % comp_str)


# Lexique filters
@lru_cache(maxsize=100)
def find_lex_objects(lex_term, domain):
    from pper.models import Lexique
    query = Lexique.objects.filter(term=lex_term.replace('_',' '))
    if domain:
        query = query.filter(domain=domain)
    return query


class LexiqueFilter(ContentFilter):
    # single quotes may happen in lex content
    re_link_lex = re.compile(r"<a href=[\"]lex://([^\"]*)[\"][^>]*>([^<]*)</a>")

    def __call__(self, content, context):
        # Remove href and replace it by a star (*).
        return self.re_link_lex.sub(r"*\2", content)


class XMLLexiqueFilter(LexiqueFilter):
    def __call__(self, content, context):
        def replace_link_lex(match):
            lex, lex_cont = match.groups()
            terms = find_lex_objects(lex, context['domain'])
            if len(terms) == 1:
                return '<span style="border-bottom:1px dotted gray;" title="%s">%s</span>' % (
                    strip_tags(terms[0].definition), lex_cont)
            else:
                return lex_cont
        if 'domain' not in context and 'objectif' in context:
            context['domain'] = context['objectif'].domain
        content = self.re_link_lex.sub(replace_link_lex, content)
        return content


class HTMLLexiqueFilter(LexiqueFilter):
    def __call__(self, content, context):
        def replace_link_lex(match):
            lex, lex_cont = match.groups()
            try:
                terms = find_lex_objects(lex, context.get('domain'))
            except KeyError:
                terms = []
            if len(terms) == 1:
                txt = '<span class="lex_term" title="%s">%s</span>' % (strip_tags(terms[0].definition), lex_cont)
                #txt = "<span class='lex_mark' title='%s'>*</span>%s" % (strip_tags(terms[0].definition), txt)
            elif len(terms) == 0:
                txt = '<span class="lex_mark_notfound" title="Erreur: définition introuvable">*</span>%s' % lex_cont
            else:
                txt = '<span class="lex_mark_notfound" title="Erreur: plusieurs termes trouvés">*</span>%s' % lex_cont
            return txt
        content = self.re_link_lex.sub(replace_link_lex, content)
        return content


# Image filters
class ImageContentFilter:
    img_regex = r"""\[\[([^\]]*)\]\]"""


class HTMLImageFilter(ImageContentFilter):
    def __init__(self, b64_embed=True):
        self.b64_embed = b64_embed

    def __call__(self, content, context):
        from pper.models import Image
        images = re.findall(self.img_regex, content)
        for im in images:
            try:
                img = Image.objects.get(source=im)
                style = ""
                if "frac" in im:
                    style = "style=\"vertical-align:middle;\""
                content = content.replace(
                    f"[[{im}]]", f'<img {style} src="{img.get_png_src(embed=self.b64_embed)}">'
                )
            except Image.DoesNotExist:
                content = content.replace(im, f'<font color="#FF0000">image «{im}» absente</font>')
        return content


class XmlImageFilter(ImageContentFilter):
    def __call__(self, content, context):
        from pper.models import Image
        images = re.findall(self.img_regex, content)
        for im in images:
            try:
                img = Image.objects.get(source=im)
                content = content.replace(
                    "[[%s]]" % im,
                    """<img src="resources/public/images/%s" alt="image" />""" % (img.get_png_filename(),)
                )
            except Image.DoesNotExist:
                logging.warning("Image '%s' does not exist" % im)
        return content


class DomainLinkFilter:
    re_link_dom = re.compile(r"<a href=['\"]dom://([^'\"]*)['\"]>([^<]*)</a>")

    @staticmethod
    def replace_link_dom(match):
        from pper.models import Domain
        code, cont = match.groups()
        try:
            dom = Domain.objects.get(abrev=code)
        except Domain.DoesNotExist:
            return u'''<font color="red">no domain found for code '%s'</font>''' % code
        return u'<a href="%s">%s</a>' % (reverse('domain', args=[dom.id]), cont)

    def __call__(self, content, context):
        content = self.re_link_dom.sub(self.replace_link_dom, content)
        return content

class HTMLPubDomainLinkFilter(DomainLinkFilter):
    def __call__(self, content, context):
        # No possible link to domain in current layout
        content = self.re_link_dom.sub(r"\1", content)
        return content


# Specification links filters
class SpecLinkFilter:
    app_name = 'pper'
    re_link_spec_ct   = re.compile(r"<a href=['\"]spec://CT([^'\"]*)['\"]>([^<]*)</a>")
    re_link_spec_noct = re.compile(r"<a href=['\"]spec://([^'\"#]*)(#\d*)?['\"]>([^<]*)</a>")
    # reverse here or at init level causes a circular dependency problem (deported in __call__)
    _cap_trans_url = None

    @classmethod
    def cap_trans_url(cls):
        if cls._cap_trans_url is None:
            cls._cap_trans_url = reverse('content_by_uid', args=[CAPACITES_TRANS_UID])
        return cls._cap_trans_url

    @classmethod
    def replace_link_spec_ct(cls, match):
        code, link_cont = match.groups()
        if not '_' in code:
            return u'<font color="red">Missing "_" in CT link</font>'
        else:
            ct_code = code.split("_")[1]
            return u"<a href=\"%s#%s\" class=\"linkCT\">%s</a>" % (
                cls.cap_trans_url(), ct_code, link_cont)

    @classmethod
    def replace_link_spec_noct(cls, match):
        code, anchor, link_cont = match.groups()
        from pper.models import Specification
        code = code.replace("_", " ")
        try:
            spec = Specification.objects.select_related('objectif', 'objectif__domain').filter(code=code)[0]
        except IndexError:
            link_str = f'<font color="red">no spec found for code «{code}»</font>'
        else:
            link_str = '<a href="%s%s" class="link%s">%s</a>' % (
                cls.reverse_spec_url(spec), anchor or '',
                spec.objectif.domain.abrev, link_cont,
            )
        return link_str

    @classmethod
    def reverse_spec_url(cls, spec):
        return reverse('course', args=[spec.id])
    
    def __call__(self, content, context):
        # Transform links
        if self.cap_trans_url is None:
            self.__class__.cap_trans_url = reverse('content_by_uid', args=[CAPACITES_TRANS_UID])
        content = self.re_link_spec_ct.sub(self.replace_link_spec_ct, content)
        content = self.re_link_spec_noct.sub(self.replace_link_spec_noct, content)
        return content


class HTMLPubSpecLinkFilter(SpecLinkFilter):
    app_name = 'public'
    @classmethod
    def cap_trans_url(cls):
        return reverse('cap-trans-public')

    @classmethod
    def reverse_spec_url(cls, spec):
        return reverse('course-public', args=[spec.code_slug()])

class XmlSpecLinkFilter(SpecLinkFilter):
    # FIXME: after identical output as previous system works
    #def __init__(self):
    #    XmlPubSpecLinkFilter.cap_trans_url = '/web/guest/capacites-transversales'
    pass # FIXME: see serialize.py


class HTMLPubTitleFilter(ContentFilter):
    """ Add a permalink to all titles """
    def __init__(self):
        super().__init__()
        self.seen_titles = set()

    def __call__(self, content, context):
        try:
            cell = context['contenucellule'].cellule
        except KeyError:
            pass
        else:
            if cell.is_title():
                anchor_txt = cell.get_anchor()
                content += ' <a class="headerlink" href="#%s" title="Lien permanent vers ce titre">¶</a>' % anchor_txt
                if anchor_txt in self.seen_titles:
                    logging.warning("The anchor '%s' has already been used" % anchor_txt)
                    # Deduplicate the anchor (field has max_length=100)
                    idx = 1
                    while anchor_txt in self.seen_titles:
                        anchor_txt = anchor_txt[:98] + f'-{idx}'
                        idx += 1
                    cell.anchor = anchor_txt
                    cell.save(update_fields=['anchor'])
                self.seen_titles.add(anchor_txt)
        return content


class TxtFilter:
    def __call__(self, content, context):
        content = strip_tags(content.replace('<li>', u'• '))
        content = re.sub(r"""\[\[[^\]]*\]\]""", "(image)", content)
        return content
