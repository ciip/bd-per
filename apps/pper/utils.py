import os
import re
import logging
from subprocess import Popen, PIPE

from django.conf import settings

STATUS_OK = 0

def run_shell_command(cmd, env=None, input_data=None, raise_on_error=False):
    if settings.DEBUG: logging.info(cmd)
    
    stdin = None
    if input_data:
        stdin = PIPE
    if env:
        os.environ.update(env)
        env = os.environ
    pipe = Popen(cmd, shell=True, env=env, stdin=stdin, stdout=PIPE, stderr=PIPE)
    if input_data:
        pipe.stdin.write(input_data)
    (output, errout) = pipe.communicate()
    status = pipe.returncode
    if settings.DEBUG:
        logging.info(output)
        if errout: logging.error(output + errout)
    if raise_on_error and status != STATUS_OK:
        raise OSError(status, errout)
    
    return (status, output, errout)

def ellipsize(string, max_length):
    if len(string) <= max_length:
        return string
    substr = string[:max_length]
    substr = substr.rsplit(None, 1)[0]
    return "%s..." % substr


def global_editable(user):
    return user.is_superuser or (
        not settings.LOCKED and user.has_perm('pper.change_specification')
    )
