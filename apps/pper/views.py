from collections import OrderedDict
from urllib.parse import quote

from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView as AuthLoginView
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.core.paginator import Paginator
from django.db import IntegrityError
from django.http import (
    HttpResponse, HttpResponseRedirect, HttpResponseForbidden,
    HttpResponseNotFound, HttpResponseServerError, JsonResponse,
)
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView, View

from pper import models
from pper.forms import (
    ContentForm, ComposanteForm, LexiqueForm, SearchForm, TipForm, VisibleForm
)
from pper.static_content import CAPACITES_TRANS_UID
from pper.utils import global_editable


class PerViewMixin(View):
    def get_context_data(self, **kwargs):
        context = super(PerViewMixin, self).get_context_data(**kwargs)
        context['domains'] = models.Domain.objects.all()
        return context


class IndexView(PerViewMixin, TemplateView):
    template_name='index.html'


class LoginView(AuthLoginView):
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # There are many forms, avoid focusing on this one.
        del context['form'].fields['username'].widget.attrs['autofocus']
        context['target_url'] = f'https://{settings.SITE_DOMAIN}'
        if self.request.GET.get('next'):
            context['target_url'] += quote(self.request.GET.get('next'))
        return context


def site_logout(request):
    referer = request.META.get('HTTP_REFERER', reverse("resource_home"))
    if request.method == 'POST':
        if 'logout' in request.POST and request.POST['logout']:
            logout(request)
    return HttpResponseRedirect(referer)


class DisciplineView(PerViewMixin, DetailView):
    model = models.Discipline
    context_object_name = "discipline"
    template_name = 'discipline.html'


def search(request):
    form = SearchForm(data=request.GET or None)
    results = []
    nump = request.GET.get('p', 1)
    if request.method == "GET" and request.GET.get('searched_text', None):
        if form.is_valid():
            results = form.search()
    pg = Paginator(results, 15)
    context = {
        'search_form': form,
        'qs'         : request.META['QUERY_STRING'], # TODO remove existing p (.replace('&p=',''),
        'total'      : pg.count,
        'results'    : pg.page(nump),
        'editable'   : global_editable(request.user),
    }
    return render(request, 'search.html', context)


def images(request):
    context = {
        'imgs': models.Image.objects.all().order_by('source'),
    }
    return render(request, 'images.html', context)


def domain_colors(request):
    return render(request, 'domain_colors.css', {'domains': models.Domain.objects.all()}, content_type='text/css')


def domain(request, dom_id):
    domain = get_object_or_404(models.Domain, pk=dom_id)
    thematiques = models.Thematique.objects.filter(domain=domain).order_by('order_no')
    thematiques = [th for th in thematiques if not th.is_canton_specific()]
    
    # Build objectifs structure
    objectifs = [{'cycle': '1', 'thematiques': OrderedDict([(th.id, []) for th in thematiques]),},
                 {'cycle': '2', 'thematiques': OrderedDict([(th.id, []) for th in thematiques]),},
                 {'cycle': '3', 'thematiques': OrderedDict([(th.id, []) for th in thematiques]),},
    ]
    for obj in models.Objectif.objects.filter(domain=domain, cantons__exact=''):
        thematique_id = obj.thematiques.all()[0].id
        if objectifs[obj.cycle-1]['thematiques'][thematique_id] and obj.title == objectifs[obj.cycle-1]['thematiques'][thematique_id][0].title:
            obj.suppress_title = True
        else:
            obj.suppress_title = False
        objectifs[obj.cycle-1]['thematiques'][thematique_id].append(obj)
    textes_gen = [] # Contains Tableau objects
    for cg in domain.com_gen.all():
        textes_gen.append(cg)
    for disc in domain.discipline_set.all():
        textes_gen.extend(disc.tableaux.all())
    context = { 'domains' : models.Domain.objects.all(),
                'domain': domain,
                'textes_gen': textes_gen,
                'objectifs': objectifs,
                'thematiques': thematiques,
                'cycles': (1,2,3),
                'visees': domain.visee.split("\n"), }
    return render(request, 'domain.html', context)

def spec_cantonales(request):
    """ Spécificités cantonales is a sort of 'special' domain """
    thematiques = [th for th in models.Thematique.objects.all() if th.is_canton_specific()]
    # Build objectifs structure
    objectifs = [{'cycle': '1', 'thematiques': OrderedDict([(th.id, []) for th in thematiques]),},
                 {'cycle': '2', 'thematiques': OrderedDict([(th.id, []) for th in thematiques]),},
                 {'cycle': '3', 'thematiques': OrderedDict([(th.id, []) for th in thematiques]),},
    ]
    for obj in models.Objectif.objects.exclude(cantons__exact=''):
        thematique_id = obj.thematiques.all()[0].id
        objectifs[obj.cycle-1]['thematiques'][thematique_id].append(obj)
    context = { 'domains' : models.Domain.objects.all(),
                'domain': {'name': u"Spécificités cantonales"},
                'objectifs': objectifs,
                'thematiques': thematiques,
                'cycles': (1,2,3),
                'visees': [], }
    return render(request, 'domain.html', context)

def tableau(request, tableau_id):
    tableau = get_object_or_404(models.Tableau, pk=tableau_id)
    editable = global_editable(request.user) and tableau.can_edit(request.user)
    context = { 
        'domains'   : models.Domain.objects.all(),
        'tabl_obj'  : tableau,
        'td_width'  : 100/tableau.get_nbcols(),
        'tableau'   : tableau.get_celltablestruct(editable=editable),
        'editable'  : editable,
        'editable_res': editable or request.user.has_perm('pper.insert_resources'),
        'clipboard_set': 'clipboard' in request.session,
        'visible_form': VisibleForm(instance=tableau),
        'rich_editing': tableau.get_type() in ('texte_gen', 'com_gen'),
    }
    return render(request, 'tableau.html', context)

def tableau_set_visible(request, tableau_id):
    if request.method != 'POST':
        return HttpResponseForbidden("Method not allowed")
    tableau = get_object_or_404(models.Tableau, pk=tableau_id)
    form = VisibleForm(request.POST, instance=tableau)
    if form.is_valid():
        form.save()
    return HttpResponse('')

def course(request, course_id):
    course = get_object_or_404(models.Specification, pk=course_id)
    spec_columns = course.get_subcols()
    editable = global_editable(request.user) and course.can_edit(request.user)
    filters = models.TableauBase.default_content_filters
    context = { 
        'domains'   : models.Domain.objects.all(),
        'course'    : course,
        'objectifs_lies' : course.get_linked_objectifs(),
        'pre_tableaux' : course.objectif.tableaux.filter(position__lt=0),
        'post_tableaux': course.objectif.tableaux.filter(position__gte=0),
        'composantes' : course.objectif.composante_set.all().order_by('letter'),
        'pre_content' : course.pre_content and course.pre_content.render(filters, {'objectif': course.objectif}) or "",
        'post_content' : course.post_content and course.post_content.render(filters, {'objectif': course.objectif}) or "",
        'sub_headers' : course.sub_headers(),
        'static_txt'  : models.STATIC_CONTENT,
        'spec_columns': spec_columns,
        'col_width' : int(760 / (spec_columns + 2)),
        'tableau': course.get_celltablestruct(editable=editable, filters=filters),
        'editable'  : editable,
        'editable_res': editable or request.user.has_perm('pper.insert_resources'),
        'clipboard_set': 'clipboard' in request.session,
    }
    return render(request, 'course.html', context)


def course_cell_content(request, obj_type, obj_id, cell_id):
    """ Get the content of one cell only """
    course = get_object_or_404(get_obj_class(obj_type), pk=obj_id)
    filters = models.TableauBase.default_content_filters
    if cell_id > 0:
        cell = course.cellule_set.get(id=cell_id)
    elif cell_id == -1 and getattr(course, "pre_content", None):
        c = course.pre_content
        cell = {
            'id': cell_id, 'is_attente': False,
            'formatted_content': [{'id':c.id, 'texte':c.render(filters, {'objectif': course.objectif})}]
        }
    elif cell_id == -2 and getattr(course, "post_content", None):
        c = course.post_content
        cell = {
            'id': cell_id, 'is_attente': False,
            'formatted_content': [{'id':c.id, 'texte':c.render(filters, {'objectif': course.objectif})}]
        }
    else:
        return HttpResponse("")
    context = {
        'cellule': {'cell': cell},
        'editable': global_editable(request.user) and course.can_edit(request.user),
        'rich_editing': cell is None or isinstance(cell, dict) or (cell and cell.get_super().get_type() in ('texte_gen', 'com_gen')),
    }
    return render(request, 'course-cell-content.html', context)


def cell_content(request, cell_id):
    """ Same as course_cell_content, but simpler """
    cell = get_object_or_404(models.Cellule, pk=cell_id)
    context = {
        'cellule'   : {'cell': cell},
        'editable'  : global_editable(request.user) and cell.get_super().can_edit(request.user),
    }
    return render(request, 'course-cell-content.html', context)


class CourseTableBaseView(TemplateView):
    """ Return only table body data (ajax update) """
    template_name = 'course-table-body.html'

    def post(self, request, *args, **kwargs):
        self.course = self.get_course(request, **kwargs)
        try:
            self.apply_operation()
        except IntegrityError as err:
            return HttpResponseServerError("Unable to apply operation (%s)" % err)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_course(self, request, **kwargs):
        return get_object_or_404(get_obj_class(kwargs['obj_type']), pk=kwargs['obj_id'])

    def apply_operation(self):
        pass

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        editable = global_editable(self.request.user) and self.course.can_edit(self.request.user)
        context.update({
            'tableau': self.course.get_celltablestruct(),
            'editable': editable,
            'editable_res': editable or self.request.user.has_perm('pper.insert_resources'),
            'clipboard_set': 'clipboard' in self.request.session,
        })
        return context


class CourseTableInsertRow(CourseTableBaseView):
    def apply_operation(self):
        self.course.insert_row(int(self.request.POST.get('after')))

class CourseTableDeleteRow(CourseTableBaseView):
    def apply_operation(self):
        self.course.delete_row(int(self.request.POST.get('row_num')))

class CourseTableMoveupRow(CourseTableBaseView):
    def apply_operation(self):
        self.course.moveup_row(int(self.request.POST.get('row_num')))

class CourseTableGrowdownRow(CourseTableBaseView):
    def apply_operation(self):
        cell_id, cell_free = self.request.POST.get('cell_ids').split("-")
        self.course.grow_down(int(cell_id), int(cell_free))

class CourseTableShrinkupRow(CourseTableBaseView):
    def apply_operation(self):
        self.course.shrink_up(int(self.request.POST.get('cell_id')))

class CourseTableGrowrightRow(CourseTableBaseView):
    def apply_operation(self):
        cell_id = int(self.request.POST.get('cell_id'))
        self.course.grow_right(cell_id)

class CourseTableShrinkleftRow(CourseTableBaseView):
    def apply_operation(self):
        cell_id = int(self.request.POST.get('cell_id'))
        self.course.shrink_left(cell_id)


def content(request, content_id):
    content = get_object_or_404(models.Contenu, pk=content_id)
    return HttpResponse(content.render())

def content_by_uid(request, content_uid):
    content = get_object_or_404(models.Contenu, uid=content_uid)
    context = {
        'domains'   : models.Domain.objects.all(),
        'content'   : content,
        'editable'  : global_editable(request.user) and content.can_edit(request.user),
    }
    return render(request, 'content.html', context)

@login_required
def content_new(request, cell_id):
    # Trick: -1 signifies new content
    return content_edit(request, cell_id, -1)


@login_required
def content_edit(request, cell_id, content_id):
    if content_id < 0:
        content = None
        used_in_specs = []
    else:
        content = get_object_or_404(models.Contenu, pk=content_id)
        # FIXME use of content.usage() ?
        used_in_specs = set([cell.specification for cell in content.cellule_set.all()])
    cell = None
    if cell_id > 0:
        cell = models.Cellule.objects.get(pk=cell_id)
        obj_type = isinstance(cell.get_super(), models.Tableau) and 'tableau' or 'spec'
    else:
        obj_type = None
    form = ContentForm(cell, data=request.POST or None, instance=content)
    if request.method == "POST":
        if form.is_valid():
            refresh_table = form.save(int(content_id) < 0)
            obj_id = cell and cell.get_super().id or 0
            return JsonResponse({
                'cell_id': cell_id,
                'cell_url': reverse('course_cell_content', args=[obj_type, obj_id, cell_id]),
                'refreshPage': cell_id == 0,
                'refreshTable': refresh_table,
            })
    context = {
        'form': form,
        'used_in_specs': len(used_in_specs) > 1 and used_in_specs,
        'new_content': content_id < 0,
    }
    return render(request, 'content_edit.html', context)


@login_required
def tip_new(request, cell_id):
    # Trick: -1 signifies new content
    return tip_edit(request, cell_id, -1)


@login_required
def tip_edit(request, cell_id, tip_id):
    if tip_id < 0:
        tip = None
        used_in_specs = []
    else:
        tip = get_object_or_404(models.Illustration, pk=tip_id)
    cell = get_object_or_404(models.Cellule, pk=cell_id)
    form = TipForm(data=request.POST or None, instance=tip)

    if request.method == "POST":
        if form.is_valid():
            form.instance.cellule = cell
            form.save()
            obj_type = 'tableau' if isinstance(cell.get_super(), models.Tableau) else 'spec'
            return JsonResponse({
                'cell_id': cell_id,
                'cell_url': reverse('course_cell_content', args=[obj_type, cell.get_super().id, cell_id]),
            })

    context = {
        'form': form,
        'new_content': tip_id < 0,
    }
    return render(request, 'tip_edit.html', context)


# CSRF exempting because a) the view is limited to auth users, b) the POST is done by TinyMCE itself
@csrf_exempt
def upload_image(request):
    if not request.user.has_perm('pper.change_specification'):
        raise PermissionDenied()
    img = request.FILES['file']
    file_name = default_storage.save(f'illustr/{img.name}', img)
    return JsonResponse({'location': default_storage.url(file_name)})


class CourseTableDeleteContent(CourseTableBaseView):
    def apply_operation(self):
        cell_id = int(self.request.POST.get('cell_id'))
        content_id = int(self.request.POST.get('content_id'))
        if cell_id > 0:
            content = get_object_or_404(models.ContenuCellule, cellule__pk=cell_id, contenu__pk=content_id)
            content.delete()
        elif cell_id == -1:
            self.course.pre_content = None
            self.course.save()
        elif cell_id == -2:
            self.course.post_content = None
            self.course.save()


class CourseTableDeleteTip(CourseTableBaseView):
    def apply_operation(self):
        cell_id = int(self.request.POST.get('cell_id'))
        tip_id = int(self.request.POST.get('content_id'))
        get_object_or_404(models.Illustration, cellule__pk=cell_id, pk=tip_id).delete()


class CourseTableSetBorder(CourseTableBaseView):
    def get_course(self, request, **kwargs):
        cell_id = int(request.POST.get('cell_id'))
        self.cell = get_object_or_404(models.Cellule, pk=int(cell_id))
        return self.cell.get_super()

    def apply_operation(self):
        mode = self.request.POST.get('mode')
        self.cell.set_border(mode)


def objectif(request, obj_id):
    objectif = get_object_or_404(models.Objectif, pk=obj_id)
    context = {
        'objectif'    : objectif,
        'composantes' : objectif.composante_set.all().order_by('letter'),
        'domains'   : models.Domain.objects.all(),
    }
    return render(request, 'objectif.html', context)

def composantes(request, course_id):
    course = get_object_or_404(models.Specification, pk=course_id)
    context = {
        'objectif'    : course.objectif,
        'composantes' : course.objectif.composante_set.all().order_by('letter'),
        'editable'    : global_editable(request.user) and course.can_edit(request.user),
    }
    return render(request, 'course-comp-list.html', context)


@login_required
def composante_edit(request, obj_id, comp_id):
    if int(comp_id) > 0:
        composante = get_object_or_404(models.Composante, pk=int(comp_id))
    else:
        composante = None
    form = ComposanteForm(request.POST or None, instance=composante)
    if request.method == "POST":
        if form.is_valid():
            if not form.instance.objectif_id:
                form.instance.objectif = models.Objectif.objects.get(pk=obj_id)
            form.save()
            return JsonResponse({
                'composante': True,
            })

    context = {
        'composante': composante,
        'form': form,
    }
    return render(request, 'composante_edit.html', context)


@login_required
def composante_delete(request):
    if request.method == 'POST':
        comp_id = int(request.POST.get('comp_id'))
        composante = get_object_or_404(models.Composante, pk=comp_id)
        composante.delete()
        return HttpResponse("OK")
    return HttpResponseForbidden("Method not allowed")

def titres(request):
    titres = models.Contenu.objects.filter(cellule__type_c__lt=0).order_by('texte')
    context = {
        'titres':   titres,
        'editable': global_editable(request.user),
    }
    return render(request, 'titres.html', context)

#? @cache_page(60 * 30)
def liens(request):
    from pper.utils import re_link_spec
    contenus = models.Contenu.objects.filter(texte__contains="spec://")
    liens = []
    ct_url = reverse('content_by_uid', args=[CAPACITES_TRANS_UID])
    for contenu in contenus:
        res = re_link_spec.findall(contenu.texte)
        for link_dest,link_text in res:
            link_text = link_text.replace("&amp;", "&")
            if link_dest.startswith("CT"):
                dest = u"<a href='%s#%s'>%s</a>" % (ct_url, link_dest.split("_")[1], link_text)
            else:
                code = link_dest.replace("_", " ")
                try:
                    spec = models.Specification.objects.select_related('objectif').get(code=code)
                    dest = u"""<a href="%s">%s</a> – %s""" % (spec.get_absolute_url(), spec.objectif.code, spec.objectif.title)
                except models.Specification.DoesNotExist:
                    dest = u"<font color='red'>no spec found for code '%s'</font>" % code
            liens.append({'txt':link_text, 'dst':dest, 'orig': contenu.usage()})
    context = {
        'liens':   liens,
    }
    return render(request, 'liens.html', context)

def print_order(request):
    context = {'specs': models.Specification.printing_order()}
    return render(request, 'print_order.html', context)


def lexique(request, dom_id):
    items = models.Lexique.objects.filter(domain__id=int(dom_id)).order_by('term')
    filters = models.TableauBase.default_content_filters
    context = {
        'domains' : models.Domain.objects.all(),
        'domain'  : get_object_or_404(models.Domain, pk=int(dom_id)),
        'items'   : [{
            'id':   i.id,
            'disciplines': i.get_disciplines(),
            'term': i.term, 
            'definition': i.get_definition(filters)
            } for i in items],
        'editable': global_editable(request.user),
    }
    return render(request, 'lexique.html', context)


@login_required
def lexique_edit(request, dom_id, lex_id):
    domain = get_object_or_404(models.Domain, pk=int(dom_id))
    lex_id = int(lex_id)
    if lex_id > 0:
        item = get_object_or_404(models.Lexique, pk=int(lex_id))
    else:
        item = None
    edit_form = LexiqueForm(domain, request.POST or None, instance=item)
    if request.method == 'POST':
        if edit_form.is_valid():
            edit_form.save()
            return JsonResponse({
                'refreshPage': True,
            })
    return render(request, 'lexique_edit.html', {'form': edit_form})


@login_required
def lexique_delete(request):
    if request.method == 'POST':
        item_id = int(request.POST.get('item_id'))
        item = get_object_or_404(models.Lexique, pk=item_id)
        item.delete()
        return HttpResponse("OK")
    return HttpResponseForbidden("Method not allowed")

def convert_encoding(obj, attr_list):
    for attr in attr_list:
        text = getattr(obj, attr)
        try:
            newtext = text.encode('latin1').decode('utf8')
        except UnicodeDecodeError:
            continue        
        except UnicodeEncodeError:
            text = text.replace("\xe2".decode('latin-1'), "")
            text = text.replace(u"\u20ac\u2122", "xxx")
            text = text.replace(u'\u20ac\xa6', 'yyy')
            text = text.replace(u'\xc5\u201c','zzz')
            text = text.replace(u'\u20ac\u201c', 'vvv')
            text = text.replace(u'\u20ac\xa2', 'kkk')
            text = text.replace(u'\xc3\u2030', 'jjj')
            # → √ π ≤
            #text.find(u"\u20ac\u2122")
            #if text.find("\xe2\u20ac\u2122") != -1:
            #    text.replace("\xe2\u20ac\u2122", "¶")
            try:
                newtext = text.encode('latin1').decode('utf8')
            except:
                import pdb; pdb.set_trace()
                continue
            newtext = newtext.replace("xxx", u"’")
            newtext = newtext.replace("yyy", u"...")
            newtext = newtext.replace("zzz", u"œ")
            newtext = newtext.replace("vvv", u"–")
            newtext = newtext.replace("kkk", u"•")
            newtext = newtext.replace("jjj", u"É")
                    
        if newtext != text:
            setattr(obj, attr, newtext)
            obj.save()

def attentes(request):
    context = {
        'domains' : models.Domain.objects.all(),
    }
    return render(request, 'attentes/attentes.html', context)

def attentes_discipline(request, uid):
    disc = get_object_or_404(models.Discipline, uid=uid)
    context = {
        'domains': models.Domain.objects.all(),
        'domain' : disc.domain,
        'disc'   : disc,
        'attentes'  : get_attentes(models.Contenu.objects.filter(cellule__specification__discipline=disc, cellule__type_c=models.TYPE_ATTENTE).order_by('cellule__specification__objectif__cycle'), {'domain': disc.domain}),
    }
    return render(request, 'attentes/attentes_discipline.html', context)

def attentes_cycle(request, no):
    if no not in ("1","2","3"):
        return HttpResponseNotFound("Le cycle %s n'existe pas" % no)
    context = {
        'domains': models.Domain.objects.all(),
        'cycle'  : no,
        'attentes'  : get_attentes(models.Contenu.objects.filter(cellule__specification__objectif__cycle=int(no), cellule__type_c=models.TYPE_ATTENTE).order_by('cellule__specification__objectif__cycle'), {}),
    }
    return render(request, 'attentes/attentes_cycle.html', context)


def get_attentes(query, context):
    attentes = []
    cont_ids = set()
    filters = models.TableauBase.default_content_filters
    for contenu in query:
        cont = contenu.render(filters, context)
        if cont and contenu.id not in cont_ids:
            attentes.append(cont)
            cont_ids.add(contenu.id)
    return attentes


def export2xml(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("You are not allowed to use this function")
    import os
    from datetime import datetime
    from django.core.servers.basehttp import FileWrapper
    from pper.management.commands import export2xml
    export_cmd = export2xml.Command()
    export_cmd.handle(target='bedag')
    filename = "pper_migration%s.zip" % datetime.now().strftime("%Y%m%d")
    export_path = os.path.join(settings.STATIC_ROOT, "export", filename)
    if os.access(export_path, os.R_OK):
        wrapper = FileWrapper(file(export_path))
        response = HttpResponse(wrapper, content_type='application/zip')
        response['Content-Disposition'] = 'filename=%s' % filename
        response['Content-Length'] = os.path.getsize(export_path)
        return response
    else:
        return HttpResponseNotFound("Unable to generate XML export of BD-PER")

def get_obj_class(obj_name):
    return {'spec': models.Specification, 'tableau': models.Tableau}.get(obj_name)
