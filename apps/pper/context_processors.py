from django.conf import settings

from pper.static_content import (
    DECLARATION_CIIP_UID, PRES_GENERALE_UID, CAPACITES_TRANS_UID,
)

def common(request):
    return {
        'is_test': settings.IS_TEST,
        'DECLARATION_CIIP_UID': DECLARATION_CIIP_UID,
        'PRES_GENERALE_UID': PRES_GENERALE_UID,
        'CAPACITES_TRANS_UID': CAPACITES_TRANS_UID,
    }
