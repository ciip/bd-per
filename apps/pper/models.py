import copy
import re, os
import logging
from base64 import b64encode
from itertools import chain
from functools import partial, total_ordering
from pathlib import Path

from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_comma_separated_integer_list
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from django.urls import reverse

from pper.static_content import STATIC_CONTENT
from pper.utils import run_shell_command, ellipsize
from pper import filters
from ressource.utils import ResourceMixin

# Constantes (cellules.type_c)
TYPE_TITRE1 = -1
TYPE_TITRE2 = -2 # "titre2" ne s'étend que dans la partie progression
TYPE_TITRE2_ETENDU = -4 # titre2 sur toute la largeur
TYPE_TITRE3 = -3
TYPE_ELEMENT = 1
TYPE_ATTENTE = 2
TYPE_INDICATION = 3
TYPE_LIEN = 5

# Mapping entre types BD-PER et types XSD-Bedag
TYPE_TABLE = {
    TYPE_TITRE1: 'title1',
    TYPE_TITRE2: 'title2',
    TYPE_TITRE2_ETENDU: 'title2',
    TYPE_TITRE3: 'title3',
    TYPE_ELEMENT: 'cell',
    TYPE_ATTENTE: 'waiting',
    TYPE_INDICATION: 'indication',
    TYPE_LIEN: 'cell',
}

CYCLES = [
    {
        'no': 1,
        'txt': "1<sup>er</sup> cycle",
        'sous-cycles': [
            {
                'txt': "1<sup>re</sup> – 2<sup>e</sup> années",
                'annees': [1, 2],
            },
            {
                'txt': "3<sup>e</sup> – 4<sup>e</sup> années",
                'annees': [3, 4],
            },
        ],
    },
    {
        'no': 2,
        'txt': "2<sup>e</sup> cycle",
        'sous-cycles': [
            {
                'txt': "5<sup>e</sup> – 6<sup>e</sup> années",
                'annees': [5, 6],
            },
            {
                'txt': "7<sup>e</sup> – 8<sup>e</sup> années",
                'annees': [7, 8],
            },
        ],
    },
    {
        'no': 3,
        'txt': "3<sup>e</sup> cycle",
        'sous-cycles': [
            {
                'txt': "9<sup>e</sup> année",
                'annees': [9],
            },
            {
                'txt': "10<sup>e</sup> année",
                'annees': [10],
            },
            {
                'txt': "11<sup>e</sup> année",
                'annees': [11],
            },
        ],
    },
]

THEMATIQUE_EXCEPTIONS = {
    'MSN 32': "Nombres et opérations",
    'MSN 33': "Fonctions et algèbre",
}

#logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('bd-per')


class MyOrderedDict(dict):
    """A dict where items() return items sorted by key (insertion is not ordered)."""
    def items(self):
        for k in sorted(self.keys()):
            yield k, self[k]


class Version(models.Model):
    name = models.CharField(max_length=100)
    complement = models.TextField()
    weight = models.IntegerField(default=0)

    class Meta:
        db_table = 't_version'

    def __str__(self):
        return self.name


@total_ordering
class Domain(models.Model):
    uid = models.IntegerField(blank=True)
    lom_id = models.CharField(max_length=40, blank=True)
    name = models.TextField()
    abrev = models.CharField(max_length=4)
    # Several visees separated by new lines
    visee = models.TextField()
    position = models.IntegerField()
    # Color scheme
    col_dark = models.CharField(max_length=9, blank=True)
    col_medium = models.CharField(max_length=9, blank=True)
    col_light = models.CharField(max_length=9, blank=True)
    
    com_gen = GenericRelation('Tableau')

    class Meta:
        db_table = 't_domaines'
        ordering = ('position',)
        verbose_name = "Domaine"

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.pk)

    def __eq__(self, other):
        return isinstance(other, Domain) and self.name == other.name

    def __lt__(self, other):
        return self.name < str(other)

    def get_absolute_url(self):
        return reverse('domain', args=[str(self.id)])

    def get_domain(self):
        return self

    def disciplines_romandes(self, only_published=False):
        """ Exclude canton specific disciplines from discipline_set """
        discs = self.discipline_set.filter(specification__objectif__cantons="").distinct()
        if only_published:
            discs = discs.exclude(published=False)
        return discs

    def get_visees(self):
        """ Return visees, HTML formatted """
        return "<h3>Visées prioritaires</h3><ul>%s</ul>" % "".join(["<li><strong>%s</strong></li>" % v for v in self.visee.split("\n")])

    def get_comgen_objects(self):
        return self.com_gen.filter(type_t='com_gen')

    def get_comgen(self, content_filters, with_visees=True, cycle=None):
        """ Returns flat text of com gen , cycle can be 1, 2 or 3 """
        cg_txt = ""
        visees_txt = with_visees and self.get_visees() or ""
        if not cycle:
            cg = self.com_gen.filter(type_t='com_gen')[0]
            cg_txt = "<h1>%s</h1>" % cg.title + visees_txt + cg.get_flat_content(content_filters)
        else:
            for cg in self.com_gen.all().order_by('type_t'): # 'com_gen' before 'texte_gen'
                if not cg.cycle or str(cycle) in cg.cycle:
                    if cg.type_t == 'com_gen':
                        cg_txt = "<h1>%s</h1>" % cg.title + visees_txt + cg.get_flat_content(content_filters)
                    else:
                        cg_txt += cg.get_flat_content(content_filters)
        return cg_txt

    def get_tableaux(self):
        """ Return tableaux objects related to the domain objectives """
        return chain(*[obj.tableaux.all() for obj in self.objectif_set.all()])


class Discipline(models.Model):
    uid = models.IntegerField(blank=True)
    name = models.CharField(max_length=80)
    slug = models.SlugField(max_length=80, default="")
    description = models.TextField(blank=True)
    lom_id = models.CharField(max_length=40, blank=True)
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)
    position = models.IntegerField(help_text="Position is used in paper-version to order specs")
    published = models.BooleanField(default=True,
        help_text="Indicate if this discipline is officially published or in the works")

    tableaux = GenericRelation('Tableau')

    class Meta:
        db_table = 't_disciplines'
        ordering = ('domain', 'position')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('discipline', args=[str(self.id)])

    @property
    def pper_url(self):
        url = reverse('discipline-public', urlconf='public.pper_urls', args=[self.slug])
        return f'https://www.plandetudes.ch{url}'

    def get_domain(self):
        return self.domain

    @classmethod
    def disciplines_cantonales(cls, only_published=False):
        discs = cls.objects.exclude(specification__objectif__cantons="").distinct()
        if only_published:
            discs = discs.exclude(published=False)
        return discs

    def get_tableaux_by_cycle(self, cycle):
        """ Return a list of tableaux linked to this discipline, filtered by cycle """
        return filter(partial(Tableau.filter_by_cycle, cycle=cycle), self.tableaux.all())

    def is_canton_specific(self):
         res = Specification.objects.filter(
            models.Q(discipline=self),
            models.Q(objectif__cantons__isnull=True) | models.Q(objectif__cantons="")
         ).count() == 0
         return res

    def cantons(self):
        """
        Renvoie la liste des cantons concernés par la discipline, vide si pour
        tous les cantons.
        """
        cantons = set()
        for can in Objectif.objects.filter(
            specification__disciplines=self
        ).values_list('cantons', flat=True).distinct():
            cantons.update([c for c in can.split(',') if c])
        return sorted(cantons)

    def build_matrix(self):
        """ Returns the discipline matrix (cycles x thematiques):
        [ ('<them>', [<spec_c1>, <spec_c2>, <spec_c3>]),
          ('<them>', [None,      None,      <spec_c3>]),
          ...
        ]
        """
        structure = []
        thematiques = list(self.domain.thematique_set.order_by('order_no'))
        for th in thematiques:
            structure.append((th,
                [th.get_specification(1, self), th.get_specification(2, self), th.get_specification(3, self)]))
        return structure


CANTON_CHOICES = (
    ('AG', 'AG'),
    ('AI', 'AI'),
    ('AR', 'AR'),
    ('BE', 'BE'),
    ('BL', 'BL'),
    ('BS', 'BS'),
    ('FR', 'FR'),
    ('GE', 'GE'),
    ('GL', 'GL'),
    ('GR', 'GR'),
    ('JU', 'JU'),
    ('LU', 'LU'),
    ('NE', 'NE'),
    ('NW', 'NW'),
    ('OW', 'OW'),
    ('SG', 'SG'),
    ('SH', 'SH'),
    ('SO', 'SO'),
    ('SZ', 'SZ'),
    ('TG', 'TG'),
    ('TI', 'TI'),
    ('UR', 'UR'),
    ('VD', 'VD'),
    ('VS', 'VS'),
    ('ZG', 'ZG'),
    ('ZH', 'ZH'),
)
#class Canton(models.Model):
#    discipline = models.ForeignKey('Discipline')
#    canton = models.CharField(max_length="2", options=CANTON_CHOICES)

class Thematique(models.Model):
    uid = models.IntegerField(blank=True)
    name = models.TextField()
    order_no = models.IntegerField()
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)

    exceptions = {
        u'3-Économie familiale-Sens et besoins physiologiques': 'CM 35',
        u'3-Économie familiale-Équilibre alimentaire': 'CM 36',
    }
    class Meta:
        db_table = 't_thematiques'

    def __str__(self):
        return "%s (%s)" % (self.name, self.domain.abrev)

    def is_canton_specific(self):
        """ Return True if *all* objectifs are canton specific """
        for obj in self.objectifs.all():
            if not obj.cantons:
                return False
        return True

    def get_specification(self, cycle, discipline):
        """ Return an objectif (or None) that matches cycle and discipline """
        res = Specification.objects.filter(disciplines=discipline,
            objectif__cycle=cycle, objectif__in=self.objectifs.all())
        if res:
            return res[0]
        else:
            key = '%s-%s-%s' % (cycle, discipline.name, self.name)
            if key in self.exceptions:
                return Specification.objects.get(code=self.exceptions[key])
            return None

    def get_disciplines(self):
        """ Return discipline(s) that are concerned by this thematique """
        disc_set = set()
        for spec in Specification.objects.filter(objectif__in=self.objectifs.all()):
            disc_set.update(spec.disciplines.all())
        return list(disc_set)


class Objectif(models.Model):
    uid = models.IntegerField(blank=True)
    title = models.TextField()
    cycle = models.IntegerField()
    code = models.CharField(max_length=10) # ex: MSN16-17
    # Objectif lié (secondaire)
    objectif2 = models.ForeignKey('self', related_name='linked1', blank=True, null=True,
        verbose_name="Objectif lié", on_delete=models.SET_NULL)
    # Objectif lié (tertiaire) (MSN32, un seul cas ?)
    objectif3 = models.ForeignKey('self', related_name='linked2', blank=True, null=True,
        verbose_name="Objectif lié (2e)", on_delete=models.SET_NULL)
    domain   = models.ForeignKey(Domain, on_delete=models.PROTECT)
    cantons  = models.CharField(max_length=30, blank=True, default='',
                                help_text="Liste de cantons séparés par des virgules (VD,FR), vide pour tous les cantons")
    thematiques = models.ManyToManyField(Thematique, db_table='t_objectif_thematique',
        related_name='objectifs')
    tableaux = GenericRelation('Tableau')

    # cache for is_fake
    _fake = {}

    class Meta:
        db_table = 't_objectifs'
        ordering = ('code',)

    def __str__(self):
        return "%s: %s" % (self.code, self.title)

    def __lt__(self, other):
        return self.code < other.code

    def get_absolute_url(self):
        return reverse('objectif', args=[str(self.id)])

    def code_slug(self):
        """ Return code without spaces """
        return self.code.replace(" ", "_").replace("&", "")

    @property
    def linked(self):
        return [
            getattr(self, name) for name in ('objectif2', 'objectif3')
            if getattr(self, name + '_id') is not None
        ]

    def get_linked_objectifs(self, reverse=False):
        """ Return a list of linked objectifs, if any """
        objs = self.linked
        if reverse:
            objs.extend([o for o in self.linked1.all() if o not in objs])
            objs.extend([o for o in self.linked2.all() if o not in objs])
        return objs

    @property
    def composantes(self):
        return self.composante_set.order_by('letter')

    @property
    def extended_code(self):
        """ Return code with linked objectif code(s) """
        if not self.objectif2:
            return self.code
        ext_code = self.code
        dom2, rem2 = self.objectif2.code.split(' ', 1)
        if self.code.startswith(dom2):
            # Don't repeat domain abrev
            ext_code += " – %s" % rem2
        else:
            ext_code += " – %s" % self.objectif2.code
        if not self.objectif3:
            return ext_code
        dom3, rem3 = self.objectif3.code.split(' ', 1)
        if self.code.startswith(dom3):
            # Don't repeat domain abrev
            ext_code += " – %s" % rem3
        else:
            ext_code += " – %s" % self.objectif3.code
        return ext_code

    def get_thematique_name(self):
        return THEMATIQUE_EXCEPTIONS.get(self.code, self.thematiques.first().name)

    def is_fake(self):
        """ fake if no progression for this objectif """
        if self.code not in self._fake:
            self._fake[self.code] = self.specification_set.filter(fake=False).count() == 0
        return self._fake[self.code]

    def pre_tableaux(self, disc_list):
        """ get pre-tableaux from domain/discipline or self """
        return Tableau.objects.filter(
            Q(cycle=self.cycle) | Q(cycle__isnull=True)
        ).filter(
            Q(content_type__model='objectif', object_id=self.id, type_t='tableau', position__lt=0) |
            Q(content_type__model='domain', object_id=self.domain.id, type_t='texte_gen') |
            Q(content_type__model='discipline',
              object_id__in=([d.id for d in disc_list]),
              type_t='texte_gen')
        )


class Composante(models.Model):
    uid = models.IntegerField(blank=True)
    title = models.TextField()
    letter = models.CharField(max_length=1, blank=True) # position in comp list (A, B, C, ...)
    objectif = models.ForeignKey(Objectif, on_delete=models.CASCADE)
    
    class Meta:
        db_table = 't_composantes'
    def __str__(self):
        return "(%s) %s" % (self.letter, self.title)

    def save(self, *args, **kwargs):
        if not self.letter:
            letter_list = [comp.letter for comp in self.objectif.composante_set.all().order_by('letter')]
            if letter_list:
                last_letter = letter_list[-1]
                self.letter = chr(ord(last_letter)+1)
            else:
                self.letter = 'A'
        super(Composante, self).save(*args, **kwargs)

    def get_letter(self, context_obj=None):
        """ Return the 'letter', can change depending on the rendering context (main objectif, linked 1, linked 2...) """
        if self.objectif.is_fake():
            return self.letter
        elif context_obj is None or self.objectif == context_obj:
            return {'A':'1', 'B':'2', 'C':'3', 'D':'4', 'E':'5', 'F':'6', 'G':'7', 'H':'8', 'I':'9'}.get(self.letter)
        elif self.objectif.id == context_obj.objectif2_id:
            return self.letter
        elif self.objectif.id == context_obj.objectif3_id:
            return self.letter.lower()
        return self.letter

class Contenu(models.Model):
    uid = models.IntegerField(blank=True, null=False)
    texte = models.TextField()

    class Meta:
        db_table = 't_contenus'

    def __str__(self):
        short_text = " ".join(self.texte.split(" ")[:12])
        if len(short_text) < len(self.texte):
            short_text += "..." 
        return short_text
    
    def can_edit(self, user):
        return user.has_perm('pper.change_contenu')

    def save(self, *args, **kwargs):
        # If texte content is only <p>text without other p inside</p>, remove the <p></p>
        txt = self.texte.replace("\r\n", " ")
        if txt[:3] == "<p>" and txt[-4:] == "</p>" and txt[3:-4].find('<p>') == -1:
            self.texte = txt[3:-4]
        super(Contenu, self).save(*args, **kwargs)
    
    def usage(self):
        """ Return a list of context (Specification or Tableau) where the content is used in """
        used_in = set([cell.get_super() for cell in self.cellule_set.all()])
        if not used_in:
            # Try to find in pre/post content
            for use in ('spec_post_content', 'spec_pre_content', 'tabl_post_content', 'tabl_pre_content'):
                for obj in getattr(self, use).all():
                    used_in.add(obj)
        return used_in

    def render(self, filters, context):
        """ Apply content filters to self.texte """
        return filters.filter_content(self.texte, context)


class TableauBase(models.Model):
    uid      = models.IntegerField(blank=True)
    position = models.IntegerField(default=0)

    default_content_filters = filters.FilterChain([
        filters.HTMLNiveauContentFilter(),
        filters.HTMLComposanteContentFilter(),
        filters.HTMLLexiqueFilter(),
        filters.HTMLImageFilter(),
        filters.DomainLinkFilter(),
        filters.SpecLinkFilter(),
    ])

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        self.cell_dict = {}  # Cached cell lists, loaded by preload_content to save queries
        super().__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        """ Add automatically an empty row for newly created objects """
        is_new = self.pk is None
        super().save(*args, **kwargs)
        if is_new:
            self.insert_row(0)

    def can_edit(self, user):
        return user.is_superuser or (
            not self.locked and user.has_perm('pper.change_specification')
        )

    def get_title_levels(self):
        """ Return a list of level titles : [-1, -2, ...] """
        return filter(lambda x: x < 0, self.cellule_set.distinct().values_list('type_c', flat=True))

    def get_nbrows(self):
        res = self.cellule_set.aggregate(models.Max('position'))
        return res['position__max'] or 0

    def get_type(self):
        """ May return: 'specification', 'tableau', 'texte_gen' """
        raise NotImplementedError
    def get_objectif(self):
        raise NotImplementedError
    def get_nbcols(self):
        raise NotImplementedError
    def col_map(self):
        raise NotImplementedError
    def preload_content(self, content_filters):
        raise NotImplementedError
    def get_typec_for_idx(self, idx):
        raise NotImplementedError
    def get_typec_for_colname(self, idx):
        raise NotImplementedError

    def get_celltablestruct(self, editable=True, set_rowspan=True, filters=None):
        """ Mettre les cellules dans un tableau à 2 dimensions (= liste de tuples)
        [ (0, [1:{'cell': cell1, 'colspan':2, 'rowspan':1, 'style':css}, 3:{'cell': cell2, 'colspan':1, 'rowspan':1, 'style':css}, ...]),
          (1, [1:{'cell': cell3, 'colspan':1, 'rowspan':1, 'style':css}, ...]) ]
        """
        table = MyOrderedDict()
        subcols = self.get_subcols()
        if not self.cell_dict:
            self.preload_content(filters or self.default_content_filters)
        domain_abrev = self.get_domain() and self.get_domain().abrev or ''
        for cellule in self.cell_dict.values():
            position_y = cellule.position
            colspan = cellule.get_colspan()
            css_class = cellule.get_css_class(domain_abrev)

            if not position_y in table:
                table[position_y] = MyOrderedDict()
            table[position_y][cellule.position_x] = {
                'cell':cellule, 'colspan':colspan, 'rowspan':0, 'style':css_class,
                'can_grow_down': False, 'can_shrink_up': False,
                'can_grow_right': False, 'can_shrink_left': False, 'can_title1': False
            }
        if set_rowspan:
            # Déterminer rowspan quand le tableau est complet
            for key, line in table.items():
                for posx, cell_info in line.items():
                    cell_info['rowspan'], end_cell_id = cell_info['cell'].get_rowspan_in_table(table, key, posx)
                    if not editable:
                        continue
                    # Check if cell can grow down (augment rowspan)
                    cell_idx = key+cell_info['rowspan']
                    if cell_info['colspan'] < 2 and (cell_idx) in table and posx in table[cell_idx] and table[cell_idx][posx]['cell'].is_empty() and not cell_info['cell'].is_empty():
                        # FIXME: Currently empty cells cannot grow down (due to implementation, content should be equal, but empty cells are never equals)
                        cell_info['can_grow_down'] = table[cell_idx][posx]['cell'].id
                    if cell_info['rowspan'] > 1:
                        cell_info['can_shrink_up'] = end_cell_id
                    # Check if cell can grow right (augment colspan)
                    if cell_info['cell'].type_c in (TYPE_ELEMENT, TYPE_TITRE2):
                        colspan = cell_info['colspan']
                        if posx < subcols and posx+colspan <= subcols and posx+colspan in table[key] and table[key][posx+colspan]['cell'].is_empty():
                            cell_info['can_grow_right'] = table[key][posx+colspan]['cell'].id
                        if cell_info['colspan'] > 1:
                            cell_info['can_shrink_left'] = True
        line_list = []
        for key in sorted(table.keys()):
            line_list.append((key, table[key]))
        return line_list

    def delete_row(self, row_num):
        """ Delete a specific row """
        after_cells = self.cellule_set.filter(position__gt=row_num)
        self.cellule_set.filter(position=row_num).delete()
        for cell in after_cells:
            cell.position = cell.position - 1
            cell.save()

    def moveup_row(self, row_num):
        """ Move the current row over the previous """
        this_row_cells = self.cellule_set.filter(position=row_num)
        prev_row_cells = self.cellule_set.filter(position=row_num-1)
        if len(prev_row_cells):
            this_row_cells.update(position=-1)
            prev_row_cells.update(position=row_num)
            self.cellule_set.filter(position=-1).update(position=row_num-1)

    def grow_right(self, cell_id):
        """ Extend colspan right way """
        cell = Cellule.objects.get(pk=cell_id)
        rowspan = cell.get_rowspan()
        # Delete right cell(s)
        cells_to_delete = []
        for idx in range(rowspan):
            cell_to_delete = cell.get_cell_right(position_offset=idx)
            if not cell_to_delete.is_empty():
                raise Exception ("Unable to delete a non empty cell")
            new_col_idx = cell_to_delete.position_x
            cells_to_delete.append(cell_to_delete)
        for c in cells_to_delete:
            c.delete()
        # Setting scycle must happen after to not catch it in the deletion process
        cell.set_colspan(new_col_idx, True) # Also take care of other rowspan cells

    def grow_down(self, cell_id, cell_free):
        """ Extend the rowspan of a cell """
        cell = Cellule.objects.get(pk=cell_id)
        cell_free = Cellule.objects.get(pk=cell_free)
        cell_free.copy_content_of(cell)

    def shrink_up(self, cell_id):
        """ Reduce the rowspan of a cell """
        cell = Cellule.objects.get(pk=cell_id)
        cell.empty()


class SpecManager(models.Manager):
    def published(self):
        return self.get_queryset().filter(status='published')

STATUS_CHOICES = (
    ('draft', 'en préparation'),
    ('published', 'publié'),
    ('archived', 'archivé'),
)

class Specification(TableauBase):
    objectif     = models.ForeignKey(Objectif, on_delete=models.CASCADE)
    code         = models.CharField(max_length=10)
    pre_content  = models.ForeignKey(Contenu, null=True, blank=True, related_name='spec_pre_content',
        on_delete=models.SET_NULL)
    post_content = models.ForeignKey(Contenu, null=True, blank=True, related_name='spec_post_content',
        on_delete=models.SET_NULL)
    # fake is used for specs which have no progression table by itself (linked from other specs)
    # or "multi-disciplines" specs (SHSx3/Lx7)
    fake         = models.BooleanField(default=False)
    version      = models.ForeignKey(Version, null=True, blank=True, on_delete=models.SET_NULL)
    status       = models.CharField(max_length=20, choices=STATUS_CHOICES, default='draft')
    # locked is used to prevent spec editing in Web UI
    locked       = models.BooleanField(default=False)
    printable    = models.URLField(blank=True, null=True)
    disciplines = models.ManyToManyField(Discipline, blank=True)

    objects = SpecManager()

    class Meta:
        db_table = 't_specifications'
        ordering = ('code',)
        unique_together = (('code', 'version'),)
        permissions = (
            ("insert_resources", "Can insert resources into specifications"),
        )

    def __str__(self):
        if self.code != self.objectif.code:
            code = "%s/%s" % (self.code, self.objectif.code)
        else:
            code = self.code
        return "%s (%s)" % (code, ', '.join([d.name for d in self.disciplines.all()]))

    @property
    def pper_url(self):
        return 'https://www.plandetudes.ch%s' % reverse(
            'course-public', urlconf='public.pper_urls', args=[self.code_slug()])

    def get_absolute_url(self):
        return reverse('course', args=[self.id])

    def get_grouped_specs(self, status=None):
        """ Get self + similar specs. Several can happen:
            - fakes with different disciplines (SHS13G, SHS13H)
            - versions of same specification (L3 32, L3 32 (2013))
        """
        specs = self.objectif.specification_set.all()
        if status is not None:
            specs = specs.filter(status=status)
        if self.fake:
            return list(specs.order_by('fake', 'version__weight', 'code'))
        else:
            # Return self, or a list of versions of self
            return list(specs.filter(code__startswith=self.code).order_by('fake', 'version__weight'))

    def is_pushable(self):
        """
        In corner cases (MSN ?5/SHS ?3), only one of two specs are indeed published.
        """
        return not (self.fake and self != self.get_grouped_specs()[0])

    @classmethod
    def printing_order(cls):
        """ Iterator that returns Specifications by printing order """
        # Specificités cantonales is at the end of their respective domain due to discipline positionning
        for cycle in (1, 2, 3):
            for domain in Domain.objects.all().order_by('position'):
                for disc in domain.discipline_set.all().order_by('position'):
                    for spec in Specification.objects.filter(disciplines=disc, objectif__cycle=cycle).order_by('position', 'code'):
                        yield spec

    def get_type(self):
        return "specification"

    def get_domain(self):
        return self.objectif.domain

    def code_display(self):
        # S/G/H suffixes are used to differentiate when an objective has 2 specs for
        # different disciplines (MSN15S/SHS13G).
        if self.fake and self.code[-1] in ['S', 'G', 'H', 'D', 'E']:
            return self.code[:-1]
        return self.code

    def code_slug(self):
        """ Return code without spaces """
        return self.code_display().replace(" ", "_").replace("&", "")

    def title(self):
        return "%s %s" % (self.objectif.code, self.objectif.title)

    def get_objectif(self):
        return self.objectif

    def get_linked_objectifs(self, reverse=False):
        return self.objectif.get_linked_objectifs(reverse=reverse)

    def get_nbcols(self):
        return self.get_subcols() + 1 + (self.has_attentes() and 1 or 0)

    def sub_headers(self):
        """ Return sub_headers """
        subh = CYCLES[self.objectif.cycle - 1]['sous-cycles']
        if self.code == "Latin":
            subh = copy.deepcopy(subh)
            subh[0]['txt'] = STATIC_CONTENT['c3_1_lat']['txt']
            subh[1]['txt'] = STATIC_CONTENT['c3_2_lat']['txt']
            subh[2]['txt'] = STATIC_CONTENT['c3_3_lat']['txt']
        return subh

    def get_headers(self):
        """ Return headers as a structure :
            {'ligne1': [{'label': 'colspan':}, {}, ...],
             'ligne2': [{'label': 'colspan':}, {}, ...],
            }
        """
        head_dict = {
            'ligne1': [{'label':STATIC_CONTENT['prog']['txt'], 'colspan': self.get_subcols()}],
            'ligne2': []
        }
        # Ligne 1
        if self.has_attentes():
            head_dict['ligne1'].append({'label':STATIC_CONTENT['att']['txt'], 'colspan':1})
        head_dict['ligne1'].append({'label':STATIC_CONTENT['indic']['txt'], 'colspan':1})
        if self.objectif.domain.abrev == "FG":
            head_dict['ligne1'][0]['label'] = STATIC_CONTENT['prog_fg']['txt']
        # Ligne 2
        for head in self.sub_headers():
            head_dict['ligne2'].append({'label': head['txt'], 'colspan':1})
        if self.has_attentes():
            head_dict['ligne2'].append({'label':STATIC_CONTENT['att_1']['txt'], 'colspan':1})
        head_dict['ligne2'].append({'label':STATIC_CONTENT['indic_1']['txt'], 'colspan':1})
        return head_dict
 
    def preload_content(self, content_filters):
        contenus = ContenuCellule.objects.select_related('contenu', 'cellule', 'cellule__specification'
            ).prefetch_related('composantes'
            ).filter(cellule__specification=self).order_by('position')

        self.cell_dict = dict([(c.id, c) for c in self.cellule_set.all().prefetch_related('illustration_set')])
        for cont in contenus:
            self.cell_dict[cont.cellule.id].set_content(
                cont.pk,
                cont.contenu.id,
                cont.contenu.render(content_filters, {'contenucellule': cont, 'objectif': self.objectif}),
            )
        # empty cells:
        for cell in self.cell_dict.values():
            if cell._cached_content is None:
                cell._cached_content = []
    
    def has_attentes(self):
        """
        `Attentes fondamentales` are absent from FG objectives, except for MITIC.
        """
        return (self.objectif.domain.abrev != "FG" or
                list(self.disciplines.values_list('name', flat=True)) == ["MITIC"])
    
    def get_subcols(self):
        """ Returns the number of columns in the Progression part """
        spec_columns = 2
        if self.objectif.cycle == 3:
            spec_columns = 3
        return spec_columns

    def col_map(self):
        cmap = ['scycle1', 'scycle2']
        if self.objectif.cycle == 3:
            cmap.append('scycle3')
        if self.has_attentes():
            cmap.append('attentes')
        cmap.append('indications')
        return cmap

    def get_typec_for_idx(self, idx):
        return [TYPE_ELEMENT,TYPE_ELEMENT,TYPE_ELEMENT,TYPE_ATTENTE,TYPE_INDICATION][idx]

    def get_typec_for_colname(self, name):
        return {
            'scycle1': TYPE_ELEMENT, 'scycle2': TYPE_ELEMENT, 'scycle3': TYPE_ELEMENT,
            'attentes': TYPE_ATTENTE, 'indications':TYPE_INDICATION
        }.get(name)
    
    # Editing functions
    def insert_row(self, after):
        """ Insert a new row in a specification table """
        # Repousser toutes les cellules d'une ligne, update() serait plus rapide, mais ne passe
        # pas le signal pre_save
        after_cells = self.cellule_set.filter(position__gt=after)
        for cell in after_cells:
            cell.position = cell.position + 1
            cell.save()
        # créer nouvelles cellules
        just_higher_cells = dict([(c.position_x, c) for c in self.cellule_set.filter(position=after)])
        just_lower_cells = dict([(c.position_x, c) for c in self.cellule_set.filter(position=after+2)])
        subcols = list(range(self.get_subcols()))
        if self.has_attentes():
            subcols.append(3)
        subcols.append(4) # [0,1,2,3,4] ou [0,1,3,4]
        skip_count = 0
        for idx in subcols:
            if skip_count > 0:
                skip_count -= 1
                continue
            c = Cellule(specification = self, position = after+1)
            c.scycle1     = idx == 0
            c.scycle2     = idx == 1
            c.scycle3     = idx == 2
            c.attentes    = idx == 3
            c.indications = idx == 4
            c.type_c = self.get_typec_for_idx(idx)
            c.save()
            # Si contenu cellule avant == contenu cellule après ET contenu non vide,
            # mettre même contenu dans la cellule (simulation rowspan)
            posx = c.position_x
            if posx in just_higher_cells and posx in just_lower_cells and \
                       just_higher_cells[posx].isEqual(just_lower_cells[posx]):
                c.copy_content_of(just_higher_cells[posx])
            skip_count = c.get_colspan() -1

    def shrink_left(self, cell_id):
        """ Reduce the colspan of a cell """
        cell = Cellule.objects.get(pk=cell_id)
        rowspan = cell.get_rowspan()
        if cell.scycle3:
            cell.set_scycle3(False)
            for idx in range(rowspan):
                new_cell = Cellule(specification=cell.specification, position=cell.position + idx,
                                   scycle1=False, scycle2=False, scycle3=True, type_c=TYPE_ELEMENT)
                new_cell.save()
        elif cell.scycle2:
            cell.set_scycle2(False)
            for idx in range(rowspan):
                new_cell = Cellule(specification=cell.specification, position=cell.position + idx,
                                   scycle1=False, scycle2=True, scycle3=False, type_c=TYPE_ELEMENT)
                new_cell.save()
        cell.save()


TYPE_T_CHOICES = (
    ('tableau',   "Tableau"),
    ('com_gen',   "Commentaire général de domaine"), # Visee affichée
    ('texte_gen', "Texte général"),
)

class Tableau(TableauBase):
    title    = models.CharField(max_length=255, blank=True, null=True)
    slug     = models.SlugField(max_length=20, blank=True, null=True)
    type_t   = models.CharField(max_length=10, choices=TYPE_T_CHOICES, default='tableau')
    cycle    = models.SmallIntegerField(null=True, blank=True)
    nb_cols  = models.IntegerField(default=1)
    pre_content  = models.ForeignKey(Contenu, null=True, blank=True, related_name='tabl_pre_content',
        on_delete=models.SET_NULL)
    post_content = models.ForeignKey(Contenu, null=True, blank=True, related_name='tabl_post_content',
        on_delete=models.SET_NULL)
    init_visible = models.BooleanField(default=False, help_text="Tell if this table is initially visible or hidden")
    locked       = models.BooleanField(default=False)
    # Generic relation to any pper model
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id    = models.PositiveIntegerField()
    per_object   = GenericForeignKey()

    class Meta:
        db_table = 't_tableau'
        ordering = ('position',)
        verbose_name_plural = "Tableaux"

    def __str__(self):
        return "%s (tableau pour '%s')" % (self.title, self.per_object)

    def get_absolute_url(self):
        return reverse('tableau', args=[self.id])
    
    def filter_by_cycle(self, cycle):
        """ cycle (int) : 1, 2 or 3 """
        if not self.cycle:
            return True
        else:
            return cycle == self.cycle

    @property
    def disciplines(self):
        if isinstance(self.per_object, Objectif):
            return self.per_object.specification_set.all()[0].disciplines

    def get_type(self):
        return self.type_t

    def get_domain(self):
        return hasattr(self.per_object, 'domain') and self.per_object.domain or (isinstance(self.per_object, Domain) and self.per_object) or None

    def get_objectif(self):
        return isinstance(self.per_object, Objectif) and self.per_object or None

    def get_nbcols(self):
        return self.nb_cols

    def get_subcols(self):
        return self.nb_cols

    def col_map(self):
        """ Return the column names of the tableau """
        return ['scycle1', 'scycle2', 'scycle3', 'attentes', 'indications'][:self.get_nbcols()]

    def get_typec_for_idx(self, idx):
        return TYPE_ELEMENT

    def get_typec_for_colname(self, name):
        return TYPE_ELEMENT

    def preload_content(self, content_filters):
        contenus = ContenuCellule.objects.select_related('contenu', 'cellule', 'cellule__tableau'
            ).filter(cellule__tableau=self).order_by('position')

        for cell in self.cellule_set.all():
            self.cell_dict[cell.id] = cell
        objectif = self.get_objectif()
        dom = self.get_domain()
        for cont in contenus:
            self.cell_dict[cont.cellule.id].set_content(
                cont.pk,
                cont.contenu.id,
                cont.contenu.render(content_filters, {'contenucellule': cont, 'objectif': objectif, 'domain': dom}),
            )
        # empty cells:
        for cell in self.cell_dict.values():
            if cell._cached_content is None:
                cell._cached_content = []

    def get_flat_content(self, content_filters):
        """ Return the tableau content as linear flat content (used for general texts)
            Only valid for content on a single column """
        contenus = ContenuCellule.objects.select_related('contenu', 'cellule', 'cellule__tableau'
            ).filter(cellule__tableau=self).order_by('cellule__position', 'position')
        txt = ""
        dom = self.get_domain()
        for cont in contenus:
            txt += "\n%s" % cont.contenu.render(content_filters, {'domain': dom})
        return txt

    def insert_row(self, after):
        """ Insert a new row in a specification table """
        # Repousser toutes les cellules d'une ligne, update() serait plus rapide, mais ne passe
        # pas le signal pre_save
        after_cells = self.cellule_set.filter(position__gt=after)
        for cell in after_cells:
            cell.position = cell.position + 1
            cell.save()
        # créer nouvelles cellules
        just_before_cells = dict([(c.position_x, c) for c in self.cellule_set.filter(position=after)])
        just_after_cells = dict([(c.position_x, c) for c in self.cellule_set.filter(position=after+2)])
        for idx in range(self.nb_cols): # [0,1,2,...]
            c = Cellule(tableau = self, position = after+1)
            c.scycle1     = idx == 0
            c.scycle2     = idx == 1
            c.scycle3     = idx == 2
            c.attentes    = idx == 3
            c.indications = idx == 4
            c.type_c = self.get_typec_for_idx(idx)
            c.save()
            # Si contenu cellule avant == contenu cellule après ET contenu non vide,
            # mettre même contenu dans la cellule (simulation rowspan)
            posx = c.position_x
            if posx in just_before_cells and posx in just_after_cells and \
                       just_before_cells[posx].isEqual(just_after_cells[posx]):
                c.copy_content_of(just_before_cells[posx])

    def shrink_left(self, cell_id):
        """ Reduce the colspan of a cell """
        cell = Cellule.objects.get(pk=cell_id)
        rowspan = cell.get_rowspan()
        line = [cell.scycle1, cell.scycle2, cell.scycle3, cell.attentes, cell.indications]
        line.reverse()
        # find the first True value (from the end, hence the reverse) and others to False [0 1 1 0 0] -> [0 0 1 0 0]
        true_found = False
        for i, b in enumerate(line):
            if not true_found and b: 
                true_found = True
                col_idx_to_shrink = 5 - i
            elif b:
                line[i] = 0
        line.reverse()
        cell.set_colspan(col_idx_to_shrink, False)
        for idx in range(rowspan):
            new_cell = Cellule(tableau=self, position=cell.position + idx,
                               scycle1=line[0], scycle2=line[1], scycle3=line[2], attentes=line[3], indications=line[4],
                               type_c=TYPE_ELEMENT)
            new_cell.save()


TYPE_CELL = (
    (TYPE_TITRE1, 'titre 1', 'titre'),
    (TYPE_TITRE2, 'titre 2', 'titre'),
    (TYPE_TITRE3, 'titre 3', 'titre'),
    (TYPE_TITRE2_ETENDU, 'titre 2 (pleine largeur)', 'titre'),
    (TYPE_ELEMENT, 'progression', 'progression'),
    (TYPE_ATTENTE, 'attente', 'attente'),
    (TYPE_INDICATION, 'indication', 'indication'),
    (TYPE_LIEN, 'pleine largeur', 'liens'),
)
TYPE_C_CHOICES = tuple([(tp[:2]) for tp in TYPE_CELL])

TYPE_B_CHOICES = (
    ('all',    'All borders'),
    ('noup',   'No top border'),
    ('nobot',  'No bottom border'),
    ('noupbot','No up, no bottom border'),
)

class Cellule(ResourceMixin, models.Model):
    uid = models.IntegerField()
    specification = models.ForeignKey(Specification, blank=True, null=True, on_delete=models.CASCADE)
    tableau = models.ForeignKey(Tableau, blank=True, null=True, on_delete=models.CASCADE)
    contenus = models.ManyToManyField(Contenu, through="ContenuCellule")

    position = models.IntegerField()
    # five fields for five columns...
    scycle1 = models.BooleanField(default=False)
    scycle2 = models.BooleanField(default=False)
    scycle3 = models.BooleanField(default=False)
    attentes = models.BooleanField(default=False)
    indications = models.BooleanField(default=False)
    type_c    = models.IntegerField(choices=TYPE_C_CHOICES)
    type_bord = models.CharField(max_length=8, choices=TYPE_B_CHOICES, default='all')
    anchor = models.SlugField(max_length=100, blank=True)

    class Meta:
        db_table = 't_cellules'
    
    def __str__(self):
        return "%s (x:%s;y:%s)" % (self.get_type_c_display(), self.position_x, self.position)
    
    def __init__(self, *args, **kwargs):
        self._cached_content = None
        models.Model.__init__(self, *args, **kwargs)
    
    def __lt__(self, other):
        """ Sort by line, then by position on line """
        if self.position == other.position:
            return self.position_x < other.position_x
        return self.position < other.position

    def delete(self):
        # TODO: Delete content if this was the only reference
        super(Cellule, self).delete()

    def isEqual(self, cell):
        """ Return true if cell has same content as self """
        if cell.is_empty() or self.is_empty():
            return False # Des cellules vides ne sont pas considérées identiques
        else:
            return (self.get_content() == cell.get_content())

    def is_empty(self):
        return self.get_content().strip() == ""

    def get_anchor(self):
        return self.anchor or self.pk

    def get_super(self):
        """ Return the 'parent' object, either Tableau or Specification """
        return self.specification or self.tableau

    def get_cellule_set(self):
        if self.specification:
            cellule_set = Cellule.objects.filter(specification=self.specification)
        else:
            cellule_set = Cellule.objects.filter(tableau=self.tableau)
        return cellule_set

    def get_other_cells_in_line(self, offset_y=0):
        """ Return a list with other cells on same line, sorted from left to right """
        cells = list(Cellule.objects.filter(specification=self.specification, tableau=self.tableau, position=self.position+offset_y).exclude(id=self.id))
        cells.sort()
        return cells
    
    def get_under(self, position=1):
        """ Return the cell *exactly* under self in a spec table, or None """
        cellule_set = self.get_cellule_set()
        try:
            return cellule_set.get(position=self.position+position,
                               scycle1=self.scycle1, scycle2=self.scycle2, scycle3=self.scycle3,
                               type_c=self.type_c)
        except Cellule.DoesNotExist:
            return None

    def get_cell_right(self, position_offset=0):
        """ Return the cell just to the right of this """
        after_cells_on_line = [c for c in self.get_other_cells_in_line(position_offset)
                               if c.position_x > self.position_x]
        if after_cells_on_line:
            return after_cells_on_line[0]
        else:
            return None        

    def formatted_content(self, filters=None):
        """ Return a dict : {'id': <content id>, 'texte': <rendered text content>} """
        if self._cached_content is not None:
            for c in self._cached_content:
                yield {'id': c[0], 'texte': c[1]}
        else:
            if filters is None:
                filters = self.get_super().default_content_filters
            objectif = self.get_super().get_objectif()
            domain = None
            if not objectif:
                domain = self.get_super().get_domain()
            for cc in self.contenucellule_set.all().order_by('position'):
                yield {
                    'id':cc.contenu.id,
                    'texte':cc.contenu.render(filters, {'contenucellule': cc, 'objectif': objectif, 'domain': domain})
                }
    
    def get_content_list(self, filters=None):
        if self._cached_content is None:
            # Cache content
            self._cached_content = []
            if filters is None:
                filters = self.get_super().default_content_filters
            objectif = self.get_super().get_objectif()
            for cc in self.contenucellule_set.all().order_by('position'):
                self._cached_content.append(
                    (cc.contenu.id,
                     cc.contenu.render(filters, {'contenucellule': cc, 'objectif': objectif}),
                     cc.pk)
                )
        return self._cached_content

    def get_content(self, filters=None):
        return "".join([c for id, c, _ in self.get_content_list(filters)])
    
    def set_content(self, cc_id, cont_id, content):
        if self._cached_content is not None:
            self._cached_content.append((cont_id, content, cc_id))
        else:
            self._cached_content = [(cont_id, content, cc_id)]
    
    def add_content(self, content, position, level):
        """ Link new content to cell """
        if not position:
            try:
                position = self.contenus.aggregate(max_pos=models.Max('contenucellule__position'))['max_pos'] + 1
            except TypeError:
                position = 0
        # Also add content in cells under if identical to self
        cell_under = self.get_under()
        if cell_under and self.isEqual(cell_under):
            cell_under.add_content(content, position, level)
        new_cont = ContenuCellule(cellule=self, contenu=content, position=position, level=level)
        new_cont.save()
        self._cached_content = None
        return new_cont

    def delete_content(self, content):
        """ Delete content link, not content itself """
        for cc in self.contenucellule_set.all():
            if cc.contenu == content:
                cc.delete()
        self._cached_content = None

    def set_border(self, mode):
        self.type_bord = mode
        self.save()

    def set_colspan(self, pos_x, boolean):
        """ Increase or decrease (depending of boolean) the colspan to column pos_x (1-based) """
        rowspan = self.get_rowspan()
        col_name = self.get_super().col_map()[pos_x - 1]
        cells = [self]
        for idx in range(1, rowspan):
            cells.append(self.get_under(idx))
        for cell in cells:
            setattr(cell, col_name, boolean)
            cell.save()       

    def set_scycle2(self, boolean):
        """ Set cycle2 property for this cell and equal cells under...""" 
        ## should be deprecated by set_colspan
        rowspan = self.get_rowspan()
        for idx in range(1, rowspan):
            cell = self.get_under(idx)
            cell.scycle2 = boolean
            cell.save()
        self.scycle2 = boolean
        self.save()

    def set_scycle3(self, boolean):
        """ Set cycle3 property for this cell and equal cells under...""" 
        ## should be deprecated by set_colspan
        rowspan = self.get_rowspan()
        for idx in range(1, rowspan):
            cell = self.get_under(idx)
            cell.scycle3 = boolean
            cell.save()
        self.scycle3 = boolean
        self.save()

    def empty(self):
        """ Empty a cell """
        ContenuCellule.objects.filter(cellule=self).delete()
        self._cached_content = None
    
    def is_progression(self):
        return self.type_c == TYPE_ELEMENT

    def is_attente(self):
        return self.type_c == TYPE_ATTENTE

    def is_indication(self):
        return self.type_c == TYPE_INDICATION

    def is_title(self):
        return self.type_c in (TYPE_TITRE1, TYPE_TITRE2_ETENDU, TYPE_TITRE2)

    def is_liens(self):
        return self.type_c == TYPE_LIEN

    def copy_content_of(self, cell):
        # Copy content
        for cc in cell.contenucellule_set.all():
            new_cc = ContenuCellule(cellule=self, contenu=cc.contenu, position=cc.position, level=cc.level)
            new_cc.save()
            for comp_link in cc.composantes.all():
                new_cc.composantes.add(comp_link)
        # Copy position x
        self.scycle1 = cell.scycle1
        self.scycle2 = cell.scycle2
        self.scycle3 = cell.scycle3
        self.attentes = cell.attentes
        self.indications = cell.indications
        self.save()
    
    def change_type(self, new_type):
        cellule_set = self.get_cellule_set()
        if new_type in (TYPE_TITRE1, TYPE_TITRE2_ETENDU):
            # Delete all other empty cells on same row
            other_cells_on_line = cellule_set.filter(position=self.position, scycle1=False)
            for c in other_cells_on_line:
                if not c.is_empty():
                   raise Exception ("Cannot set cell as title, other non empty cells in same row")
                else:
                    cell_pos_col = c.position_x
                    c.delete()
                    self.set_colspan(cell_pos_col, True)
        if new_type == TYPE_LIEN:
            # Expand until next cell is not empty
            after_cells_on_line = [c for c in self.get_other_cells_in_line()
                                   if c.position_x > self.position_x]
            for cell in after_cells_on_line:
                if cell.is_empty():
                    cell_pos_col = cell.position_x
                    cell.delete()
                    self.set_colspan(cell_pos_col, True)
                else:
                    break
        if new_type == TYPE_TITRE2:
            # Delete cells (of type ELEMENT) after this one
            after_cells_on_line = [c for c in self.get_other_cells_in_line()
                                   if c.position_x > self.position_x]
            for cell in after_cells_on_line:
                if cell.is_empty() and cell.type_c == TYPE_ELEMENT:
                    cell_pos_col = cell.position_x
                    cell.delete()
                    self.set_colspan(cell_pos_col, True)
                else:
                    break
        if new_type == TYPE_ELEMENT:
            # Reset to colspan of 1 (first cell)
            true_found = False
            for col_name in self.get_super().col_map():
                if true_found and getattr(self, col_name):
                    setattr(self, col_name, False)
                elif getattr(self, col_name):
                    true_found = True
        if not (self.scycle1 or self.scycle2 or self.scycle3 or self.attentes or self.indications):
            # Ensure at least one col is activated
            self.scycle1 = True
                
        self.type_c = new_type
        self.save()
        self.refill_line()

    def refill_line(self):
        """ Recreate missing cells on line """
        all_cells_in_line = self.get_cellule_set().filter(position=self.position)
        line = [0] * self.get_super().get_nbcols() # [0 0 0] (FG c1/c2) to [0 0 0 0 0]
        col_map = self.get_super().col_map() # same as line but with colnames ['scycle1','scycle2','attentes',...]
        for cell in all_cells_in_line.all():
            for col_idx, col_name in enumerate(col_map):
                if getattr(cell, col_name):
                    line[col_idx] = 1
        for idx, val in enumerate(line):
            if not val:
                new_cell = Cellule(specification=self.specification, tableau=self.tableau,
                                   position=self.position, type_c=self.get_super().get_typec_for_colname(col_map[idx]))
                setattr(new_cell, col_map[idx], True)
                new_cell.save()
                                   
    @property
    def position_x(self):
        """ Déterminer la position X selon le type de cellule """
        if self.type_c in (TYPE_TITRE1, TYPE_TITRE2_ETENDU):
            pos_x = 1
        elif self.type_c in (TYPE_ELEMENT,TYPE_LIEN, TYPE_TITRE2, TYPE_TITRE3):
            pos_x = self.scycle1 and 1 or (self.scycle2 and 2 or (self.scycle3 and 3 or(self.attentes and 4 or 5)))
        elif self.type_c == TYPE_ATTENTE:
            nb_cols = self.get_super().get_nbcols()
            pos_x = nb_cols - 1
        elif self.type_c == TYPE_INDICATION:
            nb_cols = self.get_super().get_nbcols()
            pos_x = nb_cols
        return pos_x

    def get_colspan(self):
        if self.type_c in (TYPE_ELEMENT, TYPE_LIEN, TYPE_TITRE2) or isinstance(self.get_super(), Tableau):
            colspan = int(self.scycle1) + int(self.scycle2) + int(self.scycle3) + int(self.attentes or 0) + int(self.indications or 0) or 1
        elif self.type_c in (TYPE_ATTENTE, TYPE_INDICATION):
            colspan = 1
        elif self.type_c in (TYPE_TITRE1, TYPE_TITRE2_ETENDU):
            # the line spans the entire row
            colspan = self.get_super().get_nbcols()
        return colspan or 1

    def get_rowspan_in_table(self, celltable, indexintable, posx):
        """ This method has a side effect to delete identical cells under this one in the celltable structure """
        rowspan = 1
        end_cell_id = 0
        while indexintable+rowspan in celltable:
            if posx in celltable[indexintable+rowspan] and celltable[indexintable+rowspan][posx]['cell'].isEqual(self):
                end_cell_id = celltable[indexintable+rowspan][posx]['cell'].id
                del celltable[indexintable+rowspan][posx]
                rowspan += 1
            else:
                break
        return rowspan, end_cell_id

    def get_rowspan(self):
        rowspan = 1
        while True:
            cell_under = self.get_under(rowspan)
            if not cell_under or not self.isEqual(cell_under):
                break
            rowspan += 1
        return rowspan
            
    def get_css_class(self, dom_abrev, specific_only=False):
        """ If specific_only, return only "progressionMSN", else return "progression progressionMSN" """
        if self.type_c == TYPE_ELEMENT:
            t_class = "progression%s" % dom_abrev
            if not specific_only: t_class = "progression %s" % t_class
        elif self.type_c == TYPE_ATTENTE:
            t_class = "attente%s" % dom_abrev
            if not specific_only: t_class = "attente %s" % t_class
        elif self.type_c == TYPE_INDICATION:
            t_class = "indication%s" % dom_abrev
            if not specific_only: t_class = "indication %s" % t_class
        elif self.type_c == TYPE_TITRE1:
            t_class = "titre1%s" % dom_abrev
            if not specific_only: t_class = "titre %s" % t_class
        elif self.type_c in (TYPE_TITRE2, TYPE_TITRE2_ETENDU):
            t_class = "titre2%s" % dom_abrev
            if not specific_only: t_class = "titre %s" % t_class
        elif self.type_c == TYPE_LIEN:
            t_class = "liens"
        if self.type_bord != 'all':
            t_class += " %s" % self.type_bord
        return t_class


class ContenuCellule(models.Model):
    cellule = models.ForeignKey(Cellule, on_delete=models.CASCADE)
    contenu = models.ForeignKey(Contenu, on_delete=models.CASCADE)
    position = models.IntegerField(default=0, null=True, blank=True)
    level = models.CharField(max_length=10, null=True, blank=True,
        validators=[validate_comma_separated_integer_list])
    composantes = models.ManyToManyField(Composante, blank=True)

    class Meta:
        db_table = 't_cellule_contenus'
        unique_together = ('cellule', 'contenu')

    def __str__(self):
        return "%s (pos %d)" % (self.id, self.position)

    def set_level(self, level):
        self.level = level
        self.save()
        # Set also the level for identical (merged) cells
        cell_under = self.cellule.get_under()
        if cell_under and self.cellule.isEqual(cell_under):
            cell_under.contenucellule_set.get(contenu=self.contenu).set_level(level)

    def set_composantes(self, comps):
        self.composantes.set(comps)  # Clears existing content
        cell_under = self.cellule.get_under()
        if cell_under and self.cellule.isEqual(cell_under):
            cell_under.contenucellule_set.get(contenu=self.contenu).set_composantes(comps)

    def delete(self):
        cell_under = self.cellule.get_under()
        if cell_under and self.cellule.isEqual(cell_under):
            # Also delete same content in cell under this one, if identical (with recursive effect)
            cell_under.delete_content(self.contenu)
        if self.cellule.type_c == TYPE_ELEMENT and self.cellule.get_colspan() > 1:
            for i in range(1, self.cellule.get_colspan()):
                self.cellule.get_super().shrink_left(self.cellule.id)
        super().delete()


class Illustration(models.Model):
    cellule = models.ForeignKey(Cellule, on_delete=models.PROTECT)
    titre = models.CharField(max_length=250)
    texte = models.TextField()
    source = models.TextField(blank=True)
    published = models.BooleanField("Publié ?", default=True)

    def __str__(self):
        return f"Illustration pour cellule {self.cellule}"

    @property
    def texte_public(self):
        return self.texte.replace('src="/uploads/', 'src="https://bdper.plandetudes.ch/uploads/')


class Lexique(models.Model):
    domain      = models.ForeignKey(Domain, on_delete=models.CASCADE)
    term        = models.CharField(max_length=100)
    definition  = models.TextField()
    comgen      = models.BooleanField(default=False)
    disciplines = models.ManyToManyField(Discipline, db_table='t_lexique_discipline', blank=True)

    class Meta:
        db_table = 't_lexique'

    def __str__(self):
        return "%s: %s" % (self.term, ellipsize(self.definition, 50))

    def get_disciplines(self):
        """ Return a string with comma-separated discipline names or the empty string """ 
        disc_list = [d.name for d in self.disciplines.all()]
        if self.comgen:
            disc_list.append("CG")
        string = ", ".join(disc_list)
        return string and "(%s)" % string or ""

    def get_definition(self, filters):
        return filters.filter_content(self.definition, {'domain': self.domain})

    @classmethod
    def mark(cls, terms, cells, confirm=False):
        """ Mark all terms in all cells contents """
        BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
        COLOR_SEQ = "\033[1;%dm"
        for term in terms:
            clean_term = term.term.strip()
            term_no_space = clean_term.replace(' ','_')
            for cell in cells:
                for content in cell.contenus.all():
                    new_texte = cls.mark_content(clean_term, content.texte)
                    if new_texte != content.texte:
                        if confirm:
                            to_replace = "<a href=\"lex://%s\">" % term_no_space
                            new_texte_colored = new_texte.replace(to_replace, '%s%s%s' % (COLOR_SEQ % RED, to_replace, COLOR_SEQ % BLACK))
                            print("\nMarquer %s (uid %d)?\n%s" % (
                                clean_term, content.uid, new_texte_colored
                            ))
                            res = input("Taper 'o' pour accepter ('q' pour quitter): ")
                            if res == "q":
                                return
                            elif res != "o":
                                continue
                        content.texte = new_texte
                        content.save()
                        logging.info("Term %s has been marked in content uid %d" % (clean_term, content.uid))

    @classmethod
    def mark_content(cls, term, text):
        term_no_space = term.replace(' ','_')
        if f"lex://{term_no_space}" in text:
            # Don't overmark
            return text
        return re.sub(r'(?iu)(%ss?)' % term, f"<a href=\"lex://{term_no_space}\">\g<1></a>", text)

    @classmethod
    def unmark(cls, cells):
        for cell in cells:
            for content in cell.contenus.all():
                new_texte = re.sub('(?u)<a href="lex://[^"]*">([^<]*)</a>', "\g<1>", content.texte)
                if new_texte != content.texte:
                    content.texte = new_texte
                    content.save()


class Image(models.Model):
    # FIXME: png and eps files are currently 'pseudo' static files. They should
    # probably be moved to MEDIA_ROOT instead, but beware that exported contents
    # still have the /static/... hardcoded path.
    source = models.CharField(max_length=150, unique=True)
    png = models.FilePathField(
        path=os.path.join(settings.STATIC_ROOT, 'resources/public/images'),
        match="\.png$", max_length=150, blank=True, null=True)
    eps = models.FilePathField(
        path=os.path.join(settings.STATIC_ROOT, 'resources/public/images'),
        match="\.eps$", max_length=150, blank=True, null=True)
    char = models.CharField(max_length=1, null=True, blank=True)

    class Meta:
        db_table = 't_image'

    def __str__(self):
        return self.source

    def save(self, *args, **kwargs):
        def clean(txt):
            cleaned = "".join([l for l in txt if l in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890+-_^"])
            return cleaned.replace("+","plus").replace("^","pow")
        # Create png/eps images corresponding to source
        dvif = None
        img_path = os.path.join(settings.STATIC_ROOT, 'resources/public/images/')
        if not self.png:
            dvif = self._get_dvi()
            png_fname = "%s.png" % clean(self.source)
            stat, out, err = run_shell_command('dvipng -T tight -bg transparent -o %s%s %s' % (img_path, png_fname, dvif))
            if stat != 0:
                logger.error(err)
            else:
                self.png = os.path.join(img_path, png_fname)
        if not self.eps:
            if not dvif:
                dvif = self._get_dvi()
            eps_fname = "%s.eps" % clean(self.source)
            stat, out, err = run_shell_command('dvips -E -o %s%s %s' % (img_path, eps_fname, dvif))
            if stat != 0:
                logger.error(err)
            else:
                self.eps = os.path.join(img_path, eps_fname)
        if self.char:
            # Manage uniqueness at Django level because MySQL uses case-insensitive unique constraints
            exist = Image.objects.filter(char__exact=self.char)
            for im in exist:
                if im.char == self.char and im.pk != self.pk:
                    raise ValidationError("There is already an image with char '%s' in the database" % self.char)
        else:
            # Do not allow empty string
            self.char = None

        super(Image, self).save(*args, **kwargs)

    def _get_dvi(self):
        from tempfile import NamedTemporaryFile, gettempdir
        
        tex_file = NamedTemporaryFile(suffix='.tex')
        tex_preamble = br'''
\nonstopmode
\documentclass{article} 
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{bm}
\newcommand{\T}{\text{T}}                % Transpose
\pagestyle{empty} 
\begin{document}'''
        tex_file.write(b"%s\n$\mathsf{\displaystyle %s}$}\n\end{document}" % (tex_preamble, self.source.encode('utf-8')))
        tex_file.flush()
        stat, out, err = run_shell_command('latex -output-directory %s %s' % (gettempdir(), tex_file.name))
        return "%s.dvi" % tex_file.name[:-4]

    def get_png_src(self, embed=True):
        """Return path or base64 data suitable for src in <img src="">."""
        if not self.png:
            raise Image.DoesNotExist
        if embed:
            try:
                file_str = b64encode(Path(self.png).read_bytes()).decode()
            except FileNotFoundError:
                pass
            else:
                return f"data:image/png; base64,{file_str}"
        # See FIXME note at class start.
        url = os.path.join(settings.STATIC_URL, "resources/public/images", self.png.split('/')[-1])
        return f"https://{settings.SITE_DOMAIN}{url}"

    def get_png_filename(self):
        if not self.png:
            raise Image.DoesNotExist
        return self.png.split('/')[-1]

    def get_eps_url(self):
        if not self.eps:
            raise Image.DoesNotExist
        # See FIXME note at class start.
        return os.path.join(settings.STATIC_URL, "resources/public/images", self.eps.split('/')[-1])

    def get_eps_filename(self):
        if not self.eps:
            raise Image.DoesNotExist
        return self.eps.split('/')[-1]

class UniqueId(models.Model):
    """ This model is used to generate a unique id between all different
        models of the application.
        The automated id field is the only field of the model
        
        UID reserved for database element are 10100 - 99999
        UID reserved for sub-elements 101000 - 999990 (*10)
        100001-100050 are reserved for static content (headers, etc.) """
    #id = Column(Integer, primary_key=True)
    UID_BASE = 10100
    next_free_uid = 2000000

    @classmethod
    def getNewID(cls):
        uid = UniqueId()
        uid.save()
        assert(int(uid.id) > 0)
        return uid.id + cls.UID_BASE

    @classmethod
    def get_free_id(cls):
        """ This gives a non-stable free id, based on memory value """
        cls.next_free_uid  +=1
        return cls.next_free_uid

    class Meta:
        db_table = 't_uniqueid'
    
    def __str__(self):
        return str(self.id + self.UID_BASE)

def fill_uid(sender, instance, **kwargs):
    if hasattr(instance, 'uid') and not instance.uid:
        instance.uid = UniqueId.getNewID()

pre_save.connect(fill_uid)

from journal.models import register
register([Domain, Discipline, Thematique, Objectif, Composante, Contenu, Specification, Cellule, Tableau, Lexique])

