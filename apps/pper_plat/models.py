import re
import logging
from django.db import models
from pper import filters
from pper_plat import filters as plat_filters
from pper.models import Domain, Objectif, Contenu, Specification, Lexique
from pper.models import STATIC_CONTENT, TYPE_TITRE1, TYPE_TITRE2, TYPE_TITRE3, TYPE_LIEN, TYPE_ATTENTE
from pper.utils import ellipsize
from pper_plat.utils import get_domain_tag

content_filters = filters.FilterChain([
    filters.ComposanteContentFilter(),
    plat_filters.HotsLexiqueFilter(),
    plat_filters.HotsImageFilter(),
    plat_filters.HotsSpecLinkFilter(),
    plat_filters.HotsFinalFilter(),
])


def reset_content():
    """ Delete all denormalized content """
    DomainPlat.objects.all().delete()
    CyclePlat.objects.all().delete()
    DisciplinePlat.objects.all().delete()
    SpecificationPlat.objects.all().delete()
    TableauPlat.objects.all().delete()
    Tableau1Plat.objects.all().delete()
    Tableau2Plat.objects.all().delete()
    Tableau3Plat.objects.all().delete()
    Tableau4Plat.objects.all().delete()
    LexiquePlat.objects.all().delete()

def denormalize():
    reset_content()
    # Denormalize Domains in DomainPlat
    for domain in Domain.objects.all().order_by('position'):
        dp = DomainPlat(id=domain.id, nom=domain.name,
                        abrev=domain.abrev, visee=domain.visee)
        dp.save()
        visee = u"<h1><%(dom)s>Visées prioritaires</%(dom)s></h1>%(visee)s" % { 'visee': domain.visee, 'dom': dp.get_tag() }
        # Denormalize Cycles/Disciplines
        for cycle_num in (1,2,3):
            cp = CyclePlat(domain=dp, domain_name=domain.name, 
                           domain_name_st="<%(dom)s>%(nom)s</%(dom)s>" % {'nom': domain.name, 'dom': dp.get_tag()},
                           visee=visee,
                           nom=get_cycle_txt(cycle_num), numero=str(cycle_num),
                           comgen=domain.get_comgen(content_filters, with_visees=False, cycle=cycle_num))
            cp.save()
            for disc in domain.discipline_set.all().order_by('position'):
                discp = DisciplinePlat(cycle=cp, cycle_name=get_cycle_txt(cycle_num), domain_name=domain.name, visee=visee, discipline=disc.name)
                txts = disc.get_tableaux_by_cycle(cycle_num)
                discp.texte_intro = txts and txts[0].get_flat_content(content_filters) or None
                discp.save()
        # Denormalize Lexiques
        for lex in Lexique.objects.filter(domain=domain).order_by('term'):
            lp = LexiquePlat(domain=dp, term=lex.term,
                definition=content_filters.filter_content(lex.definition, {'domain': domain}))
            if domain.abrev == "CM" and u"Éducation nutritionnelle" in lex.get_disciplines():
                # Exception for "CM" (separated lexique for EN in cycle 3)
                lp.discipline =  u"Éducation nutritionnelle"
            if domain.abrev == "L" and u"Latin" in lex.get_disciplines():
                lp.discipline =  u"Latin"
            lp.save()
    # Denormalize Specifications
    numbering = 1
    rendered_specs = []
    for spec in Specification.printing_order():
        domainp = DomainPlat.objects.get(id=spec.objectif.domain.pk)
        disciplinp = DisciplinePlat.objects.get(discipline=spec.discipline.name, cycle__numero=str(spec.objectif.cycle))
        if spec.id in rendered_specs:
            continue
        sp = SpecificationPlat(domain=domainp, position=numbering, non_decline=spec.fake, disciplin=disciplinp)
        sp.populate(spec)
        sp.save()
        tableaux  = spec.objectif.tableaux.order_by('position')
        positions = [-1, 1, 2, 3] # 4 possible table positions
        for tableau in tableaux:
            sp.create_tableau_tabl(tableau)
            positions.remove(tableau.position)
        for pos in positions:
            # Create empty tableaux
            sp.create_tableau_tabl(None, pos)
        if -1 not in positions:
            sp.pre_table = True
            sp.save()
        if 1 not in positions or 2 not in positions or 3 not in positions:
            sp.post_table = True
            sp.save()
        sp.create_tableau_spec()
        numbering += 1
        rendered_specs.append(spec.id)

def get_cycle_txt(cycle):
    return {
        1: STATIC_CONTENT['premier_cycle']['txt'],
        2: STATIC_CONTENT['deuxieme_cycle']['txt'],
        3: STATIC_CONTENT['troisieme_cycle']['txt'],
    }.get(cycle)

def format_cell_content(cell):
    """ Format a cell content of cell """
    content_list = cell.get_content_list()
    if cell.type_c == TYPE_ATTENTE:
        content = "".join([u"<p>…\t%s</p>" % c for id, c in content_list])
    elif cell.type_c != TYPE_ATTENTE and len(content_list) > 1:
        # separate multiple content in paragraphs
        content = "".join([u"<p>%s</p>" % c for id, c in content_list])
    else:
        content = "".join([c for id, c in content_list])
    return content


class DomainPlat(models.Model):
    # no auto-increment, because id should be the same as main Domain table
    id     = models.IntegerField(primary_key=True)
    nom   = models.TextField()
    abrev  = models.CharField(max_length=4)
    visee  = models.TextField()
    gabarit = models.CharField(max_length=1, default='B')

    class Meta:
        db_table = 't_domaines_plat'

    def __str__(self):
        return self.nom

    def get_tag(self):
        """ Return the code corresponding to a domain specific tag, like <FG></FG> """
        return get_domain_tag(self.abrev)

class CyclePlat(models.Model):
    domain      = models.ForeignKey(DomainPlat)
    domain_name = models.TextField()
    domain_name_st = models.TextField()
    visee       = models.TextField()
    nom         = models.TextField()
    numero      = models.CharField(max_length=1)
    comgen      = models.TextField()
    gabarit     = models.CharField(max_length=1, default='A')
    texte_intro = models.TextField(blank=True, null=True)
    class Meta:
        db_table = 't_cycles_plat'
    def __str__(self):
        return "%s (cycle %s)" % (self.domain.nom, self.numero)

class DisciplinePlat(models.Model):
    cycle       = models.ForeignKey(CyclePlat)
    cycle_name  = models.TextField()
    domain_name = models.TextField()
    visee       = models.TextField()
    discipline  = models.TextField()
    gabarit     = models.CharField(max_length=1, default='B')
    texte_intro = models.TextField(blank=True, null=True)
    class Meta:
        db_table = 't_disciplines_plat'
    def __str__(self):
        return "%s (cycle %s)" % (self.discipline, self.cycle.numero)

class SpecificationPlat(models.Model):
    # no auto-increment, because id should be the same as main Domain table
    id           = models.IntegerField(primary_key=True)
    domain       = models.ForeignKey(DomainPlat)
    position     = models.IntegerField()
    non_decline  = models.BooleanField(default=False)
    gabarit      = models.CharField(max_length=1, default='C')
    code         = models.TextField() # MSN 12
    code_num     = models.TextField()
    code_complet = models.TextField() # MSN 12 - 13
    cycle        = models.TextField() # PREMIER CYCLE
    thematique   = models.TextField()
    thematique_st= models.TextField()
    objectif     = models.TextField()
    discipline   = models.TextField()
    disciplin    = models.ForeignKey(DisciplinePlat)
    composantes  = models.TextField()
    titre_lie    = models.TextField(blank=True, null=True)
    code_lie     = models.TextField(blank=True, null=True)
    composanteslie = models.TextField(blank=True, null=True)
    titre_lie2    = models.TextField(blank=True, null=True)
    code_lie2     = models.TextField(blank=True, null=True)
    composanteslie2 = models.TextField(blank=True, null=True)
    pre_content  = models.TextField(blank=True, null=True)
    post_content = models.TextField(blank=True, null=True)
    pre_table    = models.BooleanField(default=False)
    post_table   = models.BooleanField(default=False)
    col1_1       = models.TextField(blank=True, null=True)
    col2_1       = models.TextField(blank=True, null=True)
    col3_1       = models.TextField(blank=True, null=True)
    col5_1       = models.TextField(blank=True, null=True)
    col6_1       = models.TextField(blank=True, null=True)
    col1_2       = models.TextField(blank=True, null=True)
    col2_2       = models.TextField(blank=True, null=True)
    col3_2       = models.TextField(blank=True, null=True)
    col5_2       = models.TextField(blank=True, null=True)
    col6_2       = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 't_specifications_plat'
        ordering = ('position',)

    def __str__(self):
        return "%s %s" % (self.code, self.objectif)

    def populate(self, spec):
        self.spec = spec
        self.id = self.spec.id
        objectif_lie1 = None; objectif_lie2 = None
        if self.spec.objectif.objectif2:
            objectif_lie1 = self.spec.objectif.objectif2
        if self.spec.objectif.objectif3:
            objectif_lie2 = self.spec.objectif.objectif3
        self.code = self.spec.code
        splitted_code = self.spec.objectif.code.split()
        self.code_num = len(splitted_code) > 1 and splitted_code[1] or splitted_code[0]
        self.code_complet = self.spec.code
        if objectif_lie1:
            # MSN 21 -> MSN 21 - 25
            self.code_complet = "%s - %s" % (self.code_complet,  objectif_lie1.code.split()[-1])
        if objectif_lie2:
            # MSN 21 -> MSN 32 - 33 - 35
            self.code_complet = "%s - %s" % (self.code_complet,  objectif_lie2.code.split()[-1])
        self.cycle = get_cycle_txt(self.spec.objectif.cycle)
        self.thematique = self.spec.objectif.thematiques.all()[0].name
        self.thematique_st = "<%(dom)s>%(content)s</%(dom)s>" % {'dom': self.domain.get_tag(), 'content': self.spec.objectif.thematiques.all()[0].name}
        self.objectif = u"%s…" % self.spec.objectif.title
        self.discipline = self.spec.discipline.name
        self.composantes = u"".join([u"<p><COMP>%s</COMP>\t… %s</p>" % (cp.get_letter(), cp.title) for cp in self.spec.objectif.composante_set.all().order_by('letter')])
        if objectif_lie1:
             self.code_lie  = objectif_lie1.code
             self.titre_lie = objectif_lie1.title
             self.composanteslie = u"".join([u"<p><COMP>%s</COMP>\t… %s</p>" % (cp.get_letter(context_obj=self.spec.objectif), cp.title) for cp in objectif_lie1.composante_set.all().order_by('letter')])
        if objectif_lie2:
             self.code_lie  = objectif_lie2.code
             self.titre_lie = objectif_lie2.title
             self.composanteslie2 = u"".join([u"<p><COMP>%s</COMP>\t… %s</p>" % (cp.get_letter(context_obj=self.spec.objectif), cp.title) for cp in objectif_lie2.composante_set.all().order_by('letter')])
        if self.spec.pre_content:
             self.pre_content = self.spec.pre_content.texte
        if self.spec.post_content:
             self.post_content = self.spec.post_content.texte

    def create_tableau_spec(self):
        struct = self.spec.get_celltablestruct(editable=False, set_rowspan=False)
        progr_cols = self.spec.get_subcols()
        nb_cols = progr_cols + 2
        def is_attente_col(idx):
            return idx == progr_cols+1 and self.spec.has_attentes()
        def is_indication_col(idx):
            return idx == progr_cols+2 or (idx == progr_cols+1 and not self.spec.has_attentes())

        sub_headers = self.spec.sub_headers()
        # First header line entete1
        if self.domain.abrev == "FG":
            contenu1 = STATIC_CONTENT['prog_fg']['txt']
        else:
            contenu1 = STATIC_CONTENT['prog']['txt']
        for i in range(progr_cols):
            setattr(self, "col%d_1" % (i+1), u"%s" % contenu1)
        if self.spec.has_attentes():
            self.col5_1 = STATIC_CONTENT['att']['txt']
        self.col6_1 = STATIC_CONTENT['indic']['txt']

        # Second header line entete2
        sub_columns = sub_headers[self.spec.objectif.cycle - 1]
        for i, contenu2 in enumerate(sub_columns):
            setattr(self, "col%d_2" % (i+1), contenu2['txt'])
        if self.spec.has_attentes():
            self.col5_2 = STATIC_CONTENT['att_1']['txt'] # Au cours, mais au plus tard ...
        self.col6_2 = STATIC_CONTENT['indic_1']['txt'] # Ressources, indices, ...
        self.save()

        # If there is only one title level, pass all titles as <H2>
        one_title_level = len(self.spec.get_title_levels()) < 2
        next_is_FG_special = False # Special style for cell following "Objectifs particuliers visés" in FG
        prev_att_content = prev_ind_content = ""
        count_att_simil_content = count_ind_simil_content = 0
        for lineno, line in struct:
            tb = TableauPlat(spec=self)
            tb.noligne = lineno + 2
            # We are on a subtitle line (but there may be other content also...)
            subtitle_context = line[1]['cell'].is_title() and not line[1]['cell'].type_c == TYPE_TITRE1
            for posx, cellinfo in line.items():
                cell_content = format_cell_content(cellinfo['cell'])
                orig_cell_content = cell_content
                if cellinfo['cell'].type_c == TYPE_TITRE1 and not one_title_level:
                    cell_content = "<h1><%(dom)s>%(content)s</%(dom)s></h1>" % {'content': cell_content, 'dom': self.domain.get_tag()}
                elif (one_title_level and cellinfo['cell'].is_title()) or cellinfo['cell'].type_c == TYPE_TITRE2:
                    cell_content = "<h2>%s</h2>" % cell_content
                elif cellinfo['cell'].type_c == TYPE_TITRE3:
                    cell_content = "<h3>%s</h3>" % cell_content

                for span_idx in range(0, cellinfo['colspan']):
                    style = ""
                    if is_attente_col(posx + span_idx) or is_indication_col(posx + span_idx):
                        # Common stuff to attente/indications columns
                        if span_idx > 0 and (subtitle_context or cellinfo['cell'].type_c in (TYPE_TITRE2, TYPE_LIEN)):
                            # Do not extend content on last 2 columns
                            cell_content = u"\t" * (lineno % 2 + 1) # alternate \t\t and \t
                        if subtitle_context and cellinfo['cell'].is_empty():
                            # set the title style of the first cell on the line
                            style = line[1]['style'].split()[-1]
                            cell_content = u"\t" * (lineno % 2 + 1)

                        if is_attente_col(posx + span_idx):
                            col_idx = 5
                            if cell_content == prev_att_content:
                                count_att_simil_content += 1
                                if count_att_simil_content >= 8:
                                    cell_content = u"\t" * (lineno % 2 + 1)
                                    style = "sans_sup"
                            else:
                                count_att_simil_content = 0
                            prev_att_content = orig_cell_content
                        elif is_indication_col(posx + span_idx):
                            col_idx = 6
                            if cell_content == prev_ind_content:
                                count_ind_simil_content += 1
                                if count_ind_simil_content >= 8:
                                    cell_content = u"\t" * (lineno % 2 + 1)
                                    style = "sans_sup"
                            else:
                                count_ind_simil_content = 0
                            prev_ind_content = orig_cell_content
                    else:
                        col_idx = posx + span_idx
                    setattr(tb, "col%d" % (col_idx), cell_content)
                    if next_is_FG_special and col_idx < 5:
                        style = "fondFG"
                        next_is_FG_special = False
                    else:
                        style = style or cellinfo['style'].split(" ")[-1]
                    if style.startswith("titre1") and one_title_level:
                        style = style.replace("titre1", "titre2")
                    if style == "titre2FG" and self.discipline == "MITIC":
                        style = "titre2MITIC"
                    if not cellinfo['cell'].is_title():
                        if cellinfo['cell'].type_bord == 'nobot':
                            style = "sans_inf"
                    setattr(tb, "style%d" % (col_idx), style)
                if cellinfo['cell'].type_c == TYPE_TITRE2 and self.domain.abrev == "FG" and self.discipline != "MITIC":
                    next_is_FG_special = True

            if tb.col2 == tb.col5 and tb.col2 != "":
                # Content that traverse table (link, title, ...)
                tb.col3 = tb.col2; tb.col4 = tb.col2
            tb.save()

    def create_tableau_tabl(self, tbl, position=0):    
        if not tbl:
            # Create empty tableau
            Table_Class = ContenuPlat.get_table_class_from_position(position)
            t_id = self.id * 1000 + position
            csp = Table_Class(id=t_id, spec=self, noligne=0, position=0)
            csp.save()
            return
        struct = tbl.get_celltablestruct(editable=False, set_rowspan=False)
        Table_Class = ContenuPlat.get_table_class_from_position(tbl.position)
        # If there is only one title level, pass all titles as <H2>
        one_title_level = len(self.spec.get_title_levels()) < 2
        for lineno, line in struct:
            t_id = self.id * 100 + lineno
            csp = Table_Class(id=t_id, spec=self, noligne=lineno, position=tbl.position)
            for posx, cellinfo in line.items():
                cell_content = format_cell_content(cellinfo['cell'])
                if cellinfo['cell'].type_c == TYPE_TITRE1 and not one_title_level:
                    cell_content = "<h1><%(dom)s>%(content)s</%(dom)s></h1>" % {'content': cell_content, 'dom': self.domain.get_tag()}
                elif (one_title_level and cellinfo['cell'].is_title()) or cellinfo['cell'].type_c == TYPE_TITRE2:
                    cell_content = "<h2>%s</h2>" % cell_content
                elif cellinfo['cell'].type_c == TYPE_TITRE3:
                    cell_content = "<h3>%s</h3>" % cell_content
                for span_idx in range(0, cellinfo['colspan']):
                    col_idx = posx + span_idx
                    setattr(csp, "col%d" % (col_idx), cell_content)
                    style = cellinfo['style'].split(" ")[-1]
                    if style.startswith("titre1") and one_title_level:
                        style = style.replace("titre1", "titre2")
                    if not cellinfo['cell'].is_title():
                        if cellinfo['cell'].type_bord == 'nobot':
                            style = "sans_inf"
                    setattr(csp, "style%d" % (col_idx), style)
            csp.save()

class TableauPlat(models.Model):
    spec    = models.ForeignKey(SpecificationPlat)
    noligne = models.IntegerField()
    # col1 - col3, progressions
    col1    = models.TextField()
    style1  = models.CharField(max_length=20)
    col2    = models.TextField()
    style2  = models.CharField(max_length=20)
    col3    = models.TextField()
    style3  = models.CharField(max_length=20)
    # col4, central margin
    col4    = models.TextField()
    # col5 - col6, attentes/indications
    col5    = models.TextField()
    style5  = models.CharField(max_length=20)
    col6    = models.TextField()
    style6  = models.CharField(max_length=20)

    class Meta:
        db_table = 't_tableau_plat'
        ordering = ('spec__code', 'noligne')

    def __str__(self):
        return "%s, ligne %d" % (self.spec.code, self.noligne)

class ContenuPlat(models.Model):
    #contenu = models.ForeignKey(ContenuPlat)
    id       = models.IntegerField(primary_key=True)
    spec     = models.ForeignKey(SpecificationPlat)
    position = models.IntegerField()
    noligne = models.IntegerField()
    col1    = models.TextField()
    style1  = models.CharField(max_length=20)
    col2    = models.TextField()
    style2  = models.CharField(max_length=20)
    col3    = models.TextField()
    style3  = models.CharField(max_length=20)
    col4    = models.TextField()
    style4  = models.CharField(max_length=20)
    col5    = models.TextField()
    style5  = models.CharField(max_length=20)
    col6    = models.TextField()
    style6  = models.CharField(max_length=20)

    class Meta:
        abstract = True
        ordering = ('spec', 'position', 'noligne')

    @classmethod
    def get_table_class_from_position(cls, pos):
        if pos < 0:
            return Tableau1Plat
        elif pos == 1:
            return Tableau2Plat
        elif pos == 2:
            return Tableau3Plat
        elif pos == 3:
            return Tableau4Plat
        else:
            raise Exception("Unable to choose a table for position %s" % str(pos))

    def __str__(self):
        return "%s (%d), ligne %d" % (self.spec.code, self.position, self.noligne)

# Table before spec
class Tableau1Plat(ContenuPlat):
    class Meta:
        db_table = 't_tableau1_plat'
# Tables after spec
class Tableau2Plat(ContenuPlat):
    class Meta:
        db_table = 't_tableau2_plat'
class Tableau3Plat(ContenuPlat):
    class Meta:
        db_table = 't_tableau3_plat'
class Tableau4Plat(ContenuPlat):
    class Meta:
        db_table = 't_tableau4_plat'

class LexiquePlat(models.Model):
    domain      = models.ForeignKey(DomainPlat)
    discipline  = models.CharField(max_length=80, blank=True, null=True)
    term        = models.CharField(max_length=100)
    definition  = models.TextField()
    class Meta:
        db_table = 't_lexique_plat'

    def __str__(self):
        return "%s: %s" % (self.term, ellipsize(self.definition, 50))

