import logging
import re

from pper import filters
from pper_plat.utils import get_domain_tag

class HotsLexiqueFilter(filters.LexiqueFilter):
    def __call__(self, content, context):
        return self.re_link_lex.sub(r"<LEX>\2</LEX>", content)

class HotsImageFilter(filters.ImageContentFilter):
    def __call__(self, content, context):
        from pper.models import Image
        images = re.findall(self.img_regex, content)
        for im in images:
            try:
                img = Image.objects.get(source=im)
                content = content.replace("[[%s]]" % im, "[[%s]]" % img.char)
            except Image.DoesNotExist:
                logging.warn("Image '%s' does not exist" % im)
        return content

class HotsSpecLinkFilter(filters.SpecLinkFilter):
    def __call__(self, content, context):
        content = self.re_link_spec_ct.sub(u"<CT>\2</CT>", content)
        content = self.re_link_spec_noct.sub(u"<%(tag)s>\2</%(tag)s>" % {'tag': get_domain_tag(context['domain'].abrev)}, content)
        return content

class HotsFinalFilter:
    """ Various replacements specific to hots (pper-plat) app """
    def __call__(self, content, context):
        # Suppress all remaining links
        content = re.sub(r"<a [^>]*>([^<]*)</a>", r"\1", content)
        # Get rid of newlines
        content = content.replace('\n', '')
        # Replace <ul>
        content = content.replace('<ul>', u'<p>')
        content = content.replace('<li>', u'<p>•\t')
        content = content.replace('</li>', u'<br/>')
        content = content.replace('</ul>', u'</p>')
        # Replace <h1> <h2> <h3> by <T1><MSN> <...
        content = re.sub(r"<[hH]([12])>", r"<T\1><%s>" % get_domain_tag(context['domain'].abrev), content)
        content = re.sub(r"</[hH]([12])>", r"</%s></T\1>" % get_domain_tag(context['domain'].abrev), content)
        content = re.sub(r"<[hH]3>", r"<T3>", content)
        content = re.sub(r"</[hH]3>", r"</T3>", content)
        if 'contenucellule' in context and context['contenucellule'].cellule.is_title():
            # Remove LEX tags from titles
            content = re.sub(r"<LEX>([^<]*)</LEX>", r"\1", content)
        return content
