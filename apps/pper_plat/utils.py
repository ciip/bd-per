def get_domain_tag(dom_abrev):
    return {
            'L2': 'L',
            'L3': 'L',
            'A': 'ART'
    }.get(dom_abrev, dom_abrev)
