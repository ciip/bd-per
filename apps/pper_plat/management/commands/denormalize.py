from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.core.management.commands import syncdb
from django.db import connection
from pper_plat.models import denormalize

class Command(BaseCommand):

    def handle(self, *args, **options):
        # Drop tables
        cursor = connection.cursor()
        for table in ('t_domaines_plat', 't_disciplines_plat', 't_cycles_plat', 't_specifications_plat', 't_tableau_plat', 't_lexique_plat'):
            cursor.execute("drop table %s" % table) 
        # Resyncdb
        call_command('syncdb')
        denormalize()
        return "PPER tables have been denormalized."
