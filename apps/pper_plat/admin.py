from django.contrib import admin
from pper_plat import models

class LexiquePlatAdmin(admin.ModelAdmin):
    search_fields = ('term', 'definition')
    list_filter = ('domain',)
    list_display = ('domain','discipline','__str__')
    ordering = ('term',)

class SpecificationPlatAdmin(admin.ModelAdmin):
    search_fields = ('code', 'objectif')
    list_display = ('__str__', 'position')

class TableauPlatAdmin(admin.ModelAdmin):
    search_fields = ('spec__code',)
    list_display = ('__str__', 'col1', 'style1', 'col2', 'style2', 'col3', 'style3', 'col4', 'col5', 'style5', 'col6', 'style6')

class TableauXAdmin(admin.ModelAdmin):
    search_fields = ('spec__code',)
    list_display = ('__str__', 'col1', 'style1', 'col2', 'style2', 'col3', 'style3', 'col4', 'style4', 'col5', 'style5', 'col6', 'style6')

admin.site.register(models.DomainPlat)
admin.site.register(models.CyclePlat)
admin.site.register(models.DisciplinePlat)
admin.site.register(models.LexiquePlat, LexiquePlatAdmin)
admin.site.register(models.SpecificationPlat, SpecificationPlatAdmin)
admin.site.register(models.TableauPlat, TableauPlatAdmin)
admin.site.register(models.Tableau1Plat, TableauXAdmin)
admin.site.register(models.Tableau2Plat, TableauXAdmin)
admin.site.register(models.Tableau3Plat, TableauXAdmin)
admin.site.register(models.Tableau4Plat, TableauXAdmin)
