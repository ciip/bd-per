from django.urls import path

from .views import auth as auth_views
from .views import per as views
from .views import resources as rviews


urlpatterns = [
    path('', views.APIOverview.as_view()),

    # routes Auth pour BSN-like API
    path('auth', auth_views.GetTokenWithKeysView.as_view(), name='auth_token'),
    # routes Auth pour portail.ciip.ch
    path('get_sid', auth_views.GetSIDView.as_view()),
    path('afterlogin/<sid>', auth_views.AfterLoginView.as_view(), name='afterlogin'),
    path('get_token/<sid>', auth_views.GetTokenView.as_view(), name='get_token'),

    # routes PER
    path('ct', views.CapaciteList.as_view(), name='api_ct'),
    path('domaines', views.DomaineList.as_view(), name='api_domaines'),
    path('domaines/<int:id>', views.DomaineDetails.as_view(), name='api_domaine'),
    path('domaines/<int:id>/objectifs', views.ObjectifListByDomaine.as_view(),
        name='api_objectifs_par_domaine'),
    path('domaines/<int:id>/lexique', views.LexiqueDetails.as_view(), name='api_lexique'),
    path('domaines/<int:id>/com/<int:id_com>', views.DomaineComGen.as_view(), name='api_domaine_com_gen'),
    path('disciplines', views.DisciplineList.as_view(), name='api_disciplines'),
    path('disciplines/<int:id>', views.DisciplineDetails.as_view(), name='api_discipline'),
    path('disciplines/<int:id>/objectifs', views.ObjectifListByDiscipline.as_view(),
        name='api_objectifs_par_discipline'),
    path('thematiques', views.ThematiqueList.as_view(), name='api_thematiques'),
    path('thematiques/<int:id>', views.ThematiqueDetails.as_view(), name='api_thematique'),
    path('objectifs', views.ObjectifList.as_view(), name='api_objectifs'),
    path('objectifs/<int:id>', views.ObjectifDetails.as_view(), name='api_objectif'),
    path('objectifs/<int:id>/tableau/', views.ObjectifTable.as_view(), name='api_objectif_table'),
    path('progressions/<int:id>', views.ProgressionDetails.as_view(), name='api_progression'),
    path('attentes/<int:id>', views.AttenteDetails.as_view(), name='api_attente'),
    path('indications/<int:id>', views.IndicationDetails.as_view(), name='api_indication'),
    # With those APIs, the granular item is able to give more context about the objectif
    # in which it is inserted.
    path('objectifs/<int:pid>/progressions/<int:id>',
        views.ProgressionDetails.as_view(), name='api_progression_rich'),
    path('objectifs/<int:pid>/attentes/<int:id>',
        views.AttenteDetails.as_view(), name='api_attente_rich'),
    path('objectifs/<int:pid>/indications/<int:id>',
        views.IndicationDetails.as_view(), name='api_indication_rich'),

    path('ressources', rviews.ResourcesSearch.as_view(), name='api_ressource_search'),
    path('ressources/ids', rviews.ResourcesAllIDs.as_view(), name='api_ressource_ids'),
    path('ressources/e-media/', rviews.ResourcesEMedia.as_view(), name='api_e-media'),
    path('ressources/e-media/ids/', rviews.ResourcesEMedia.as_view(ids_only=True),
        name='api_e-media_ids'),
    path('ressources/ontologies/', rviews.ResourceOntologies.as_view(), name='api_ontologies'),
    path('ressources/<int:id>', rviews.ResourceDetails.as_view(), name='api_ressource'),
    path('ressources/catalogues', rviews.ResourceGroups.as_view(), name='api_catalogues'),
    path('ressources/catalogues/ids', rviews.ResourceGroupsAllIDs.as_view(), name='api_catalogues_ids'),
    path('ressources/catalogue/<int:id>', rviews.ResourceGroupDetails.as_view(), name='api_catalogue'),

    # BSN-like to create/search resource
    path('description', rviews.ResourceCreateView.as_view(), name='api_ressource_create'),
    path('description/<uuid:uuid>', rviews.ResourceLOMDetailsView.as_view(), name='api_lom_ressource'),
    path('search', rviews.ResourcesLOMSearchView.as_view(), name='api_lom_search'),
]
