from django.contrib.auth import get_user_model
from django.core.signing import BadSignature
from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin

from api.models import signer


class JWTAuthenticationMiddleware:
    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        token = self.get_auth_jwt(request)
        if token:
            try:
                payload = signer.decode_token(token)
            except BadSignature:
                return JsonResponse({'result': 'Error', 'error': 'JWT token invalid or expired.'})
            except Exception:
                return JsonResponse({'result': 'Error', 'error': 'Unable to decode the JWT token.'})
            User = get_user_model()
            try:
                user = User.objects.get(pk=payload['_id'], is_active=True)
            except User.DoesNotExist:
                user = None
            if user:
                # User is valid.  Set request.user
                request.user = user
        return self.get_response(request)

    def get_auth_jwt(self, request):
        auth_header = request.META.get('HTTP_AUTHORIZATION', '').strip()
        if auth_header and auth_header.startswith('Bearer'):
            try:
                token = auth_header.split()[1]
            except IndexError:
                return None
            return token
        return None
