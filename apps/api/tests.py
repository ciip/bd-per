import base64
import json
import os
from datetime import date, datetime
from pathlib import Path
from unittest.mock import patch

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core import mail
from django.core.files import File
from django.core.serializers.json import DjangoJSONEncoder
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import get_current_timezone, make_aware, now

from pper.models import (
    Cellule, Contenu, ContenuCellule, Discipline, Objectif, Specification,
    TYPE_TITRE2
)
from pper.tests.tests import BDPERTests
from ressource.models import (
    Resource, ResourceFile, ResourceGroup, ResourceGroupLink, ResourcePerLink,
    ResourceRelation, SubTheme
)
from ressource.tests.tests import ResourceTestsBase
from shortener.models import ShortLink
from api.models import Token, signer

json_encoder = DjangoJSONEncoder()
MODEL = 'django.contrib.auth.backends.ModelBackend'


class AuthTests(ResourceTestsBase):
    def test_get_sid_and_token(self):
        user = get_user_model().objects.create_user('jim0', 'doe@example.org', 'jimpw')
        user.username = 'jim'
        user.pk = 100  # Fixed id as it is used in token which should be testable.
        user.save()
        response = self.client.get('/api/v1/get_sid')
        resp = response.json()
        self.assertEqual(resp['loginURL'], f'http://testserver/login/?sid={resp["sid"]}')
        self.assertEqual(resp['targetURL'], f'http://testserver/api/v1/afterlogin/{resp["sid"]}')
        # Reaching targetURL as anonymous produces a JSON error
        response = self.client.get(resp['targetURL'])
        self.assertEqual(response.json()['result'], 'Error')
        # Simulate the login phase
        self.client.force_login(user, backend=MODEL)
        response = self.client.get(reverse('afterlogin', args=[resp['sid']]))
        self.assertEqual(response.content, b'<script>window.close()</script>')
        token = Token.objects.get(sid=resp['sid'])
        self.assertEqual(token.user, user)

        # Getting the token
        # Fixed issued_at so we can test the token string
        token.issued_at = make_aware(datetime(2020, 3, 31, 14, 24, 30))
        token.save()
        response = self.client.get(f"/api/v1/get_token/{resp['sid']}")
        #print(response.json()['jwt'])
        self.assertEqual(
            response.json()['jwt'],
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJiZHBlci5wbGFuZGV0d'
            'WRlcy5jaCIsImlhdCI6MTU4NTY1NzQ3MCwiZXhwIjoxNTg2MjYyMjcwLCJfaWQiOjE'
            'wMCwiZW1haWwiOiJkb2VAZXhhbXBsZS5vcmciLCJyb2xlIjoidXNlciJ9.bOcoV2ed'
            'NNQQ0xQqwRXV4p1r9w-dU2RWeFbcvjC2ZJ0'
        )
        # Check token is decodable
        payload = signer.decode_token(response.json()['jwt'])
        self.assertEqual(payload['iss'], 'bdper.plandetudes.ch')
        self.assertEqual(payload['email'], 'doe@example.org')
        self.assertEqual(payload['role'], 'user')

    def test_auth_by_jwt(self):
        non_validated = Resource.objects.create(status='atraiter')
        response = self.client.get(reverse('api_ressource', args=[non_validated.pk]))
        self.assertEqual(response.status_code, 403)
        # Malformed auth
        response = self.client.get(reverse('api_ressource', args=[non_validated.pk]), HTTP_AUTHORIZATION='Bearer')
        self.assertEqual(response.status_code, 403)
        # Bad JWT
        response = self.client.get(reverse('api_ressource', args=[non_validated.pk]), HTTP_AUTHORIZATION='Bearer XXX')
        self.assertEqual(response.json()['error'], 'Unable to decode the JWT token.')
        # Now with a JWT
        today = date.today()
        jwt = Token(
            sid='some_string',
            issued_at=make_aware(datetime(today.year, today.month, today.day, 14, 24, 30)),
            user=self.user
        ).make_jwt()
        response = self.client.get(reverse('api_ressource', args=[non_validated.pk]), HTTP_AUTHORIZATION=f'Bearer {jwt}')
        self.assertEqual(response.status_code, 200)


class PPERApiTests(BDPERTests):
    def _fill_data(self):
        self.spec.pre_content = Contenu.objects.create(uid=9989, texte="Pré-contenu")
        # =====================================================
        #  Prog 1   |  Prog 2  |           |  Indication [rg] |
        # ======================  Attente  ====================
        #        Prog 3 [r]    |  Attente2 |                  |
        # =====================================================
        #           |   Titre  |           |                  |
        # =====================================================
        self.spec.insert_row(after=1)
        cell_struct = self.spec.get_celltablestruct()
        self.spec.grow_right(cell_struct[1][1][1]['cell'].pk)
        cell_struct[0][1][3]['cell'].add_content(
            Contenu.objects.create(uid=9990, texte="Attente"), 0, None)
        cell_struct[0][1][3]['cell'].add_content(
            Contenu.objects.create(uid=9991, texte="Attente2"), 0, None)
        self.spec.grow_down(cell_struct[0][1][3]['cell'].pk, cell_struct[1][1][3]['cell'].pk)
        cell_struct[1][1][1]['cell'].add_content(
            Contenu.objects.create(uid=9992, texte="Prog 3"), 0, None)
        cell_struct[0][1][1]['cell'].add_content(
            Contenu.objects.create(uid=9993, texte="Prog 1"), 0, None)
        cell_struct[0][1][2]['cell'].add_content(
            Contenu.objects.create(uid=9994, texte="Prog 2"), 0, None)
        cell_struct[0][1][4]['cell'].add_content(
            Contenu.objects.create(uid=9995, texte="Indication"), 0, None)
        cell_struct[2][1][2]['cell'].add_content(
            Contenu.objects.create(uid=9996, texte="Titre"), 0, None)
        cell_struct[2][1][2]['cell'].change_type(TYPE_TITRE2)
        # Attach some Resource/ResourceGroup
        self.resource = Resource.objects.create(status='validee', title='Ressource 1')
        ResourcePerLink.objects.create(resource=self.resource, content_object=cell_struct[1][1][1]['cell'])
        self.resource.index()
        self.resource_group = ResourceGroup.objects.create(title='Catalogue 1', grp_status='valide')
        ResourcePerLink.objects.create(catalog=self.resource_group, content_object=cell_struct[0][1][4]['cell'])

    def test_domaines(self):
        response = self.client.get(reverse('api_domaines'))
        self.assertContains(response, self.domain.name)
        self.assertContains(response, json.dumps(self.discipline.name))
        response = self.client.get(reverse('api_domaine', args=[self.domain.pk]))
        self.assertContains(response, '"id": %d' % self.domain.pk)
        self.assertContains(response, self.domain.name)

    def test_disciplines(self):
        Discipline.objects.create(name="Allemand", slug="allemand", domain=self.domain, position=2)
        response = self.client.get(reverse('api_disciplines'))
        self.assertContains(response, json.dumps(self.discipline.name))
        self.assertContains(response, '"nom": "Allemand"')

    def test_discipline(self):
        response = self.client.get(reverse('api_discipline', args=[self.discipline.pk]))
        self.assertContains(response, json.dumps(self.discipline.name))

    def test_thematiques(self):
        response = self.client.get(reverse('api_thematiques'))
        self.assertContains(response, json.dumps(self.thematique.name))
        self.assertContains(response, reverse('api_thematique', args=[self.thematique.pk]))

        response = self.client.get(reverse('api_thematique', args=[self.thematique.pk]))
        self.assertContains(response, json.dumps(self.thematique.name))
        self.assertContains(response, '"code": "%s"' % self.spec.code)
        self.assertContains(response, reverse('api_objectif', args=[self.spec.pk]))

    def test_objectif(self):
        self._fill_data()
        obj2 = Objectif.objects.create(title="Objectif lié", cycle=1, code="L 33", domain=self.domain)
        spec2 = Specification.objects.create(objectif=obj2, code="L 33", status='published')
        self.objectif.objectif2 = obj2
        self.objectif.save()
        response = self.client.get(reverse('api_objectif', args=[self.spec.pk]))
        self.assertContains(response, self.objectif.code)
        self.assertContains(response, json.dumps(self.thematique.name))
        self.assertContains(response, json.dumps(self.discipline.name))
        self.assertContains(response, self.composante.title)
        result = response.json()
        self.assertEqual(result['cycle'], self.objectif.cycle)
        self.assertEqual(result['titres'],
            [{'lignes': [3], 'contenus': ['Titre'], 'annees': [3, 4], 'niveau': 2}])
        self.assertEqual(result['objectifs_lies'], [
            {'id': spec2.pk, 'code': 'L 33', 'href': f'http://testserver/api/v1/objectifs/{spec2.pk}'}
        ])
        # Resources
        self.assertEqual(result['indications'][0]['ressources'],
            [{'titre': 'Catalogue 1', 'id': self.resource_group.pk,
              'href': 'http://testserver/api/v1/ressources/catalogue/%d' % self.resource_group.pk,
              'type_princ': 'catalogue',
              'updated': json_encoder.default(self.resource_group.updated)}]
        )
        self.assertEqual(result['progressions'][0]['items'][1]['ressources'],
            [{'titre': 'Ressource 1', 'id': self.resource.pk,
              'href': 'http://testserver/api/v1/ressources/%d' % self.resource.pk,
              'type_princ': 'ressource',
              'type': 'med',
              'updated': json_encoder.default(self.resource.updated)}])

    def test_objectifs_by_domaine(self):
        self._fill_data()
        response = self.client.get(reverse('api_objectifs_par_domaine', args=[self.domain.pk]))
        self.assertEqual(response.json()[0]['nom'], 'Objectif 1')

    def test_objectif_as_table(self):
        self._fill_data()
        response = self.client.get(reverse('api_objectif_table', args=[self.spec.pk]))
        self.assertContains(response, self.objectif.code)
        result = response.json()
        self.assertEqual(
            result['tableau'][0][0],
            {'colspan': 2, 'rowspan': 1, 'type': 'en-tête',
             'contenus': {'texte': 'Progression des apprentissages'}}
        )
        ccont = ContenuCellule.objects.get(contenu__texte="Prog 3")
        self.assertEqual(
            result['tableau'][3][0],
            {'id': ccont.cellule_id, 'colspan': 2, 'rowspan': 1,
             'type': 'progression', 'bordure': 'all',
             'contenus': [{'id': ccont.contenu.pk, 'id_cc': ccont.pk, 'texte': 'Prog 3'}],
             'ressources': [{
                'href': 'http://testserver/api/v1/ressources/%d' % self.resource.pk,
                'id': self.resource.pk,
                'titre': 'Ressource 1',
                'type': 'med',
                'type_princ': 'ressource',
                'updated': json_encoder.default(self.resource.updated)}],
             'illustrations': [],
            }
        )
        ccont1 = ContenuCellule.objects.filter(contenu__texte="Attente").first()
        ccont2 = ContenuCellule.objects.filter(contenu__texte="Attente2").first()
        self.assertEqual(
            result['tableau'][2][2],
            {'id': ccont1.cellule_id, 'colspan': 1, 'rowspan': 2,
             'type': 'attente', 'bordure': 'all',
             'contenus': [{'id': ccont1.contenu.pk, 'id_cc': ccont1.pk, 'texte': 'Attente'},
                          {'id': ccont2.contenu.pk, 'id_cc': ccont2.pk, 'texte': 'Attente2'}],
             'illustrations': [],
            }
        )
        ccont = ContenuCellule.objects.get(contenu__texte="Indication")
        self.assertEqual(
            result['tableau'][2][3],
            {'id': ccont.cellule_id, 'colspan': 1, 'rowspan': 1,
             'type': 'indication', 'bordure': 'all',
             'contenus': [{'id': ccont.contenu.pk, 'id_cc': ccont.pk, 'texte': 'Indication'}],
             'ressources': [{
                'href': 'http://testserver/api/v1/ressources/catalogue/%d' % self.resource_group.pk,
                'id': self.resource_group.pk,
                'titre': 'Catalogue 1',
                'type_princ': 'catalogue',
                'updated': json_encoder.default(self.resource_group.updated)}],
             'illustrations': [],
            }
        )
        ccont = ContenuCellule.objects.get(contenu__texte="Titre")
        self.assertEqual(
            result['tableau'][4][1],
            {'id': ccont.cellule_id, 'colspan': 1, 'rowspan': 1,
             'type': 'titre', 'bordure': 'all',
             'contenus': [{'id': ccont.contenu.pk, 'id_cc': ccont.pk, 'texte': 'Titre'}],
             'niveau': 2,
             'illustrations': [],
            }
        )

    def test_contenus(self):
        self._fill_data()

        response = self.client.get(
            reverse('api_progression_rich', args=[self.spec.pk, Contenu.objects.get(texte='Prog 3').pk])
        )
        result = response.json()
        self.assertEqual(result['id'], Contenu.objects.get(texte="Prog 3").pk)
        self.assertEqual(result['lignes'], [2])
        self.assertEqual(result['texte'], "Prog 3")
        self.assertEqual(result['objectif']['code'], self.spec.code)
        self.assertEqual(len(result['attentes']), 2)
        self.assertEqual(len(result['indications']), 0)

        response = self.client.get(
            reverse('api_progression', args=[Contenu.objects.get(texte='Prog 3').pk])
        )
        self.assertEqual(response.json()['texte'], "Prog 3")

        response = self.client.get(
            reverse('api_attente_rich', args=[self.spec.pk, Contenu.objects.get(texte='Attente').pk])
        )
        result = response.json()
        self.assertEqual(result['id'], Contenu.objects.get(texte="Attente").pk)
        self.assertEqual(result['lignes'], [1, 2])
        self.assertEqual(result['texte'], "Attente")
        self.assertEqual(result['objectif']['code'], self.spec.code)
        self.assertEqual(len(result['progressions']), 3)

        response = self.client.get(
            reverse('api_attente', args=[Contenu.objects.get(texte='Attente').pk])
        )
        self.assertEqual(response.json()['texte'], "Attente")

        response = self.client.get(
            reverse('api_indication_rich', args=[self.spec.pk, Contenu.objects.get(texte='Indication').pk])
        )
        result = response.json()
        self.assertEqual(result['id'], Contenu.objects.get(texte="Indication").pk)
        self.assertEqual(result['lignes'], [1])
        self.assertEqual(result['texte'], "Indication")
        self.assertEqual(result['objectif']['code'], self.spec.code)
        self.assertEqual(len(result['progressions']), 2)

        response = self.client.get(
            reverse('api_indication', args=[Contenu.objects.get(texte='Indication').pk])
        )
        self.assertEqual(response.json()['texte'], "Indication")


class ResourceApiBase(BDPERTests):
    @classmethod
    def _create_resource(cls, rich=False, index=True, **kwargs):
        if rich:
            params = dict(
                status='validee', title='Ressource à voir',
                url="http://www.ciip.ch",
                description="<p>Ligne1<br>Ligne2</p>",
                type_doc=['audio','video'],
                languages=['de'],
                level=['cycle 1', '1st and 2nd year'],
                thumb_url='http://www.example.org/thumb.png',
                mitic_asp=['sources', 'prod_media'],
            )
        else:
            params = {'title': 'Ressource', 'status': 'validee'}
        params.update(kwargs)
        resource = Resource.objects.create(**params)
        if rich:
            cell1 = cls.spec.cellule_set.filter(scycle1=True)[0]
            cc1 = cell1.add_content(Contenu.objects.first(), None, None)
            # Create one link to a spec, one to a discipline and one to a content cell
            ResourcePerLink.objects.create(
                resource=resource, content_object=cls.spec,
            )
            ResourcePerLink.objects.create(
                resource=resource, content_object=cls.discipline,
            )
            ResourcePerLink.objects.create(
                resource=resource, content_object=cc1,
            )
            if index:
                resource.index()
        return resource


class ResourceApiTests(ResourceApiBase):
    def test_resource_api(self):
        resource = self._create_resource(rich=True, index=False)
        disc2 = Discipline.objects.create(
            name="Allemand", slug="allemand", lom_id="deutsch", domain=self.domain, position=2
        )
        ResourcePerLink.objects.create(resource=resource, content_object=disc2)
        resource.index()
        rr = ResourceRelation.objects.create(
            resource=resource, rtype='has_version', url='http://www.example.org/1'
        )
        response = self.client.get(reverse('api_ressource', args=[resource.pk]))
        result = response.json()
        self.assertEqual(result['ID'], resource.pk)
        self.assertEqual(result['Titre'], 'Ressource à voir')
        self.assertEqual(result['URL'], 'http://www.ciip.ch')
        self.assertEqual(result['Langues de la ressource'], 'allemand')
        self.assertEqual(result['Description générale'], '<p>Ligne1<br>Ligne2</p>')
        self.assertEqual(result['Type documentaire'], 'document sonore, document vidéo')
        self.assertEqual(result['Niveau scolaire'], 'Cycle 1, Cycle 1 - 1re-2e')
        self.assertEqual(result['Vignette'], 'http://www.example.org/thumb.png')
        self.assertEqual(result['Coûts'], "Non")
        # mitic_asp 'prod_media' value is automatically adding the prod-scol emedia_categ value
        self.assertEqual(result['Catégorie e-media'], 'Production scolaire')
        cell_id = self.spec.cellule_set.filter(scycle1=True)[0].pk
        self.assertEqual(result['Liens PER'], [
            {'titre': 'L 11', 'URL': 'https://www.plandetudes.ch/web/guest/L_11/',
             'objectif': f'http://testserver/api/v1/objectifs/{self.spec.pk}', 'catalogue': None},
            {'titre': 'L 11', 'URL': f'https://www.plandetudes.ch/web/guest/L_11/#{cell_id}',
             'objectif': f'http://testserver/api/v1/objectifs/{self.spec.pk}', 'catalogue': None},
            {'titre': 'Allemand', 'URL': 'https://www.plandetudes.ch/web/guest/allemand/',
             'objectif': None, 'catalogue': None},
        ])
        self.assertEqual(result["Plan d'études"][0]['element'], 'Discipline')
        self.assertEqual(result["Plan d'études"][1]['element'], 'Discipline')
        self.assertEqual(result["Plan d'études"][2]['element'], 'Objectif')
        self.assertEqual(result["Plan d'études"][3]['element'], "Contenu d'objectif")
        self.assertEqual(result["Aspects MITIC"], 'Sources de l’info, Production de réalisations médiatiques, Éducation aux médias')
        self.assertEqual(result["Relations"], [{
            'ID': rr.pk, "Type de relation": 'has_version', "URL": 'http://www.example.org/1',
            'Description': '', 'Autre ressource': None,
        }])
        # Format = raw parameter
        response = self.client.get(reverse('api_ressource', args=[resource.pk]) + '?format=raw')
        result = response.json()
        self.assertEqual(result['Langues de la ressource'], ['de'])
        self.assertEqual(result['Type documentaire'], ['audio', 'video'])
        self.assertEqual(result['Niveau scolaire'], ['cycle 1', '1st and 2nd year'])
        self.assertEqual(result['Catégorie e-media'], ['prod-scol'])
        self.assertEqual(result["Aspects MITIC"], ['sources', 'prod_media', 'Éducation aux médias', 'Production de réalisations médiatiques'])
        self.assertIs(result['Coûts'], False)
        # Format = emedia parameter
        response = self.client.get(reverse('api_ressource', args=[resource.pk]) + '?format=emedia')
        result = response.json()
        self.assertEqual(result['Niveau scolaire'], ['cycle_1', '1st_and_2nd_year'])

        # Check when thumb is defined instead of thumb_url
        ressource_test_dir = os.path.join(
            os.path.dirname(os.path.dirname(__file__)), 'ressource', 'tests'
        )
        with open(os.path.join(ressource_test_dir, 'thumbnail.png'), mode='rb') as fp:
            resource.thumb.save('thumbnail.png', fp)
        self.addCleanup(os.unlink, resource.thumb.path)
        resource.thumb_url = ''
        resource.save()
        response = self.client.get(reverse('api_ressource', args=[resource.pk]))
        result = response.json()
        self.assertEqual(
            result['Vignette'],
            'https://bdper.plandetudes.ch/uploads/resource_thumbs/thumbnail.png'
        )
        # Check when res_file is defined instead of url
        resource.url = ''
        resource.save()
        with open(os.path.join(ressource_test_dir, 'sample.pdf'), mode='rb') as fp:
            r1 = ResourceFile.objects.create(resource=resource, rfile=File(fp), title='sample')
        response = self.client.get(reverse('api_ressource', args=[resource.pk]))
        result = response.json()
        self.assertEqual(
            result['URL'],
            'https://bdper.plandetudes.ch/uploads/ressources/%d/sample.pdf' % resource.pk
        )

    def test_resource_with_codesrn(self):
        cat1 = ResourceGroup.objects.create(title='Catalogue 1', objectif=self.objectif)
        cat2 = ResourceGroup.objects.create(title='Catalogue 2', objectif=self.objectif)
        r1 = Resource.objects.create(status='validee', title='Ressource 1')
        ResourceGroupLink.objects.create(group=cat1, resource=r1, number='A')
        ResourceGroupLink.objects.create(group=cat2, resource=r1, number='B')
        response = self.client.get(reverse('api_ressource', args=[r1.pk]) + '?format=raw')
        self.assertEqual(response.json()['Codes RN'], ['A', 'B'])

    def test_resource_eleve(self):
        res = Resource.objects.create(
            status='validee', title='Ressource 1', descr_eleve="Description", target_gr=['teacher', 'learner']
        )
        ShortLink.get_for_object(res, create=True)
        response = self.client.get(reverse('api_ressource', args=[res.pk]))
        res_resp = response.json()
        self.assertEqual(res_resp["Description élève"], "Description")
        self.assertEqual(res_resp['URL élève'], f'http://testserver/ressources/{res.pk}/appr/')
        self.assertTrue(res_resp['URL courte'].startswith('https://ciip-rn.ch/'))

    def test_resource_allids(self):
        r1 = self._create_resource(rich=True)
        self._create_resource(rich=False, broken=True)
        self._create_resource(rich=False, partner='VD')
        response = self.client.get(reverse('api_ressource_ids'))
        self.assertEqual(response.json(), [r1.pk])

    def test_logged_in_access(self):
        resource = self._create_resource()
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('change_resource', 'ressource', 'resource')
        )
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.get(reverse('api_ressource', args=[resource.pk]))
        json = response.json()
        self.assertEqual(json['Nécessite une authentification sur ce site'], 'Non')

    def test_video_urls(self):
        resource = self._create_resource(url="https://youtu.be/rD24sWx")
        response = self.client.get(reverse('api_ressource', args=[resource.pk]))
        self.assertEqual(
            response.json()['URL'],
            f'https://bdper.plandetudes.ch/ressources/{resource.pk}/video/yt/rD24sWx/'
        )
        self.assertEqual(
            response.json()['URL originale'],
            'https://youtu.be/rD24sWx'
        )

    def test_resource_group_list_api(self):
        cat1 = resource_group = ResourceGroup.objects.create(
            title='Catalogue 1', grp_status='valide', objectif=self.objectif
        )
        r1 = Resource.objects.create(status='validee', title='Ressource 1')
        r2 = Resource.objects.create(status='validee', title='Ressource 2')
        r3 = Resource.objects.create(status='atraiter', title='Ressource 3')
        ResourceGroupLink.objects.create(group=resource_group, resource=r1, number='A')
        ResourceGroupLink.objects.create(group=resource_group, resource=r2, number='B')
        ResourceGroupLink.objects.create(group=resource_group, resource=r3, number='C')
        ResourcePerLink.objects.create(
            catalog=resource_group, content_object=Cellule.objects.first(),
        )
        cat2 = ResourceGroup.objects.create(
            domain=self.domain, title='Catalogue 2', grp_status='valide',
        )
        ResourceGroup.objects.create(
            domain=self.domain, title='Catalogue en édition', grp_status='avalider',
        )
        response = self.client.get(reverse('api_catalogues'))
        result = response.json()
        self.assertEqual(len(result), 2)  # grp_status='avalider' should not appear
        self.assertEqual(result[0]['titre'], 'Catalogue 1')
        self.assertEqual(result[0]['nombre de ressources'], 3)
        self.assertEqual(
            result[0]['domaine'],
            {'nom': 'Langues', 'URL': '/api/v1/domaines/%d' % self.domain.pk}
        )
        self.assertEqual(result[0]['objectif']['nom'], 'L 11: Objectif 1')
        self.assertEqual(result[1]['objectif']['nom'], 'Sans objectif')
        self.assertIsNone(result[1]['objectif']['URL'])
        # Test updated_from param
        response = self.client.get(reverse('api_catalogues') + '?updated_from=1.1.2022')
        self.assertEqual(response.status_code, 400)
        response = self.client.get(reverse('api_catalogues') + '?updated_from=2022-01-01')
        self.assertEqual(len(result), 2)
        response = self.client.get(reverse('api_catalogues') + '?updated_from=2022-01-01T14:33:02%2B02:00')
        self.assertEqual(len(result), 2)
        # Test Ids API
        response = self.client.get(reverse('api_catalogues_ids'))
        self.assertEqual(response.json(), [cat1.pk, cat2.pk])

    def test_resource_group_api(self):
        resource_group = ResourceGroup.objects.create(title='Catalogue 1', objectif=self.objectif)
        SubTheme.objects.create(name="Theme2", num='2', group=resource_group)
        SubTheme.objects.create(name="Theme1", num='1', group=resource_group)
        r1 = Resource.objects.create(status='validee', title='Ressource 1')
        r2 = Resource.objects.create(status='validee', title='Ressource 2')
        r3 = Resource.objects.create(status='atraiter', title='Ressource 3')
        ResourceGroupLink.objects.create(group=resource_group, resource=r1, number='A')
        ResourceGroupLink.objects.create(group=resource_group, resource=r2, number='B')
        ResourceGroupLink.objects.create(group=resource_group, resource=r3, number='C')

        with self.assertNumQueries(9):
            # catalogs, specifications, resourceperlinks, subthemes, ordered_resources (incl 4 prefetch)
            response = self.client.get(reverse('api_catalogue', args=[resource_group.pk]))
        result = response.json()
        self.assertEqual(result['Titre'], 'Catalogue 1')
        self.assertEqual(
            result['Objectif'],
            {'nom': 'L 11: Objectif 1',
             'URL': f'http://testserver/api/v1/objectifs/{self.spec.pk}',
             'URL PPER': '',
            }
        )
        self.assertEqual(
            result['sous-thèmes'],
            [{'nom': 'Theme1', 'numéro': '1'}, {'nom': 'Theme2', 'numéro': '2'}]
        )
        # Ensure validating resource doesn't appear
        self.assertEqual(len(result['ressources']), 2)
        self.assertEqual(set(r['Numéro'] for r in result['ressources']), set(['A', 'B']))

    def test_resource_emedia(self):
        """
        The emedia resource list API accepts a from=ISO_date GET param to only get
        resources updated from that date.
        """
        r1 = Resource.objects.create(status='validee', title='Ressource 1', emedia=False)
        with patch('ressource.models.timezone') as mock_timezone:
            dt = datetime(2016, 1, 1, tzinfo=get_current_timezone())
            mock_timezone.now.return_value = dt
            r2 = Resource.objects.create(status='validee', title='Ressource 2', emedia=True)
            dt = datetime(2017, 11, 1, tzinfo=get_current_timezone())
            mock_timezone.now.return_value = dt
            r3 = Resource.objects.create(status='validee', title='Ressource 3', emedia=True)

        with self.assertNumQueries(1):
            response = self.client.get(reverse('api_e-media'))
        self.assertEqual(len(response.json()), 2)
        response = self.client.get(reverse('api_e-media') + '?from=2016-12-31T23:30:00Z')
        self.assertEqual(len(response.json()), 1)
        response = self.client.get(reverse('api_e-media') + '?from=2016123123:30')
        self.assertEqual(response.content, b'Date invalid or not well-formatted (should be ISO 8601)')

    def test_resource_search(self):
        resource = self._create_resource(rich=True, title='Ressource à voir')
        # Cannot use bulk_create as indexation happens in save()
        for i in range(7):
            Resource.objects.create(title='Ressource n°%d' % i, status='validee')
        # One is an old one
        with patch('ressource.models.timezone') as mock_timezone:
            dt = datetime(2018, 1, 1, tzinfo=get_current_timezone())
            mock_timezone.now.return_value = dt
            Resource.objects.create(status='validee', title='Ressource n°7')

        # Errors
        response = self.client.get(reverse('api_ressource_search'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['error'], 'Veuillez fournir au moins un critère de recherche.')
        response = self.client.get(reverse('api_ressource_search') + '?types_doc=rien')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['error'], 'Le formulaire contient des erreurs.')
        self.assertEqual(
            response.json()['errors'],
            {'types_doc': ["Sélectionnez un choix valide. rien n’en fait pas partie."]}
        )

        response = self.client.get(reverse('api_ressource_search') + '?types_doc=audio')
        self.assertEqual(response.status_code, 200)
        result = response.json()
        self.assertEqual(len(result['content']), 1)
        res = result['content'][0]
        self.assertEqual(res['Titre'], 'Ressource à voir')
        self.assertEqual(res['Niveau scolaire'], 'Cycle 1, Cycle 1 - 1re-2e')
        refs = resource.refs().order_by('cell')
        self.assertCountEqual(
            res['Liens PER'],
            [{
                'titre': 'L 11',
                'URL': 'https://www.plandetudes.ch/web/guest/L_11/#%d' % refs[0].cell_id,
                'objectif': 'http://testserver/api/v1/objectifs/%d' % refs[0].specification_id,
                'catalogue': None
            }, {
                'titre': 'L 11',
                'URL': 'https://www.plandetudes.ch/web/guest/L_11/',
                'objectif': 'http://testserver/api/v1/objectifs/%d' % refs[1].specification_id,
                'catalogue': None
            }]
        )

        # Test updated from
        response = self.client.get(reverse('api_ressource_search') + '?updated_from=2020-01-31')
        result = response.json()
        self.assertEqual(len(result['content']), 8)
        response = self.client.get(reverse('api_ressource_search') + '?updated_from=2022-01-01T14:33:02%2B02:00')
        result = response.json()
        self.assertEqual(len(result['content']), 8)

        # Aucun résultat
        response = self.client.get(reverse('api_ressource_search') + '?text=sblurzk')
        result = response.json()
        self.assertEqual(len(result['content']), 0)

        # Pagination
        response = self.client.get(reverse('api_ressource_search') + '?text=Ressource')
        result = response.json()
        self.assertEqual(len(result['content']), 9)
        self.assertEqual(result['totalElements'], 9)
        self.assertEqual(result['totalPages'], 1)
        response = self.client.get(reverse('api_ressource_search') + '?text=Ressource&page=2&size=5')
        result = response.json()
        self.assertEqual(len(result['content']), 4)
        self.assertEqual(result['totalElements'], 9)
        self.assertEqual(result['totalPages'], 2)
        # Page out of range => return page 1
        response = self.client.get(reverse('api_ressource_search') + '?text=Ressource&page=4&size=5')
        result = response.json()
        self.assertEqual(len(result['content']), 5)

    def test_resource_search_pdf(self):
        resource = self._create_resource(rich=True, title='Ressource à voir')
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.get(
            reverse('api_ressource_search') + '?text=Ressource&size=10', HTTP_ACCEPT='application/pdf'
        )
        self.assertRegex(
            response['Content-Disposition'], r'attachment; filename="PER-Ressources_\d{4}-\d{2}-\d{2}\.pdf"'
        )

    def test_resource_create_errors(self):
        resp = self.client.post(
            reverse('api_ressource_search'),
            b'non valid json[',
            content_type="application/json"
        )
        self.assertEqual(resp.json(), {'error': 'Malformed JSON data'})
        resp = self.client.post(
            reverse('api_ressource_search'),
            json.dumps({'title': 'Suggest by API'}),
            content_type="application/json"
        )
        self.assertEqual(resp.json()['error'], "Le formulaire contient des erreurs.")
        self.assertEqual(resp.json()['errors']['description'], ["Ce champ est obligatoire."])
        self.assertEqual(
            resp.json()['errors']['__all__'],
            ["Vous devez remplir soit le champ URL, soit le champ Fichier, soit au moins une image de gallerie."]
        )

    def test_resource_create_success(self):
        res_data = {
            'title': 'Nouveau site',
            'description': 'Un super nouveau site',
            'url': 'http://www.example.org',
            'languages': ['fr'],
            'subm_name': 'Alphonse Daudet',
            'subm_addr': 'Rue du Bruit 3',
            'subm_email': 'submitter@example.org',
            'subm_tel': '032 999 99 99',
        }
        resp = self.client.post(
            reverse('api_ressource_search'),
            json.dumps(res_data),
            content_type="application/json",
            follow=False,
        )
        self.assertEqual(resp.status_code, 201)
        res = Resource.objects.get(title='Nouveau site')
        self.assertEqual(resp['Location'], 'http://testserver' + reverse('api_ressource', args=[res.pk]))
        self.assertEqual(res.status, 'atraiter')
        self.assertEqual(res.type_r, 'med')
        self.assertEqual(res.validated, None)
        self.assertEqual(len(mail.outbox), 1)


class ResourceApiBSNLikeTests(ResourceApiBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = get_user_model().objects.create_user('edu-vd', 'eduvd@example.org')
        cls.user.user_permissions.add(
            Permission.objects.get_by_natural_key('add_resource', 'ressource', 'resource'),
            Permission.objects.get_by_natural_key('change_resource', 'ressource', 'resource'),
        )

    def test_auth_by_keys(self):
        """BSN-like auth to get token"""
        from OpenSSL import crypto
        pk = crypto.PKey()
        pk.generate_key(crypto.TYPE_RSA, 2048)
        Path(settings.MEDIA_ROOT, 'pub_keys').mkdir()
        with Path(settings.MEDIA_ROOT, 'pub_keys', 'edu-vd.pem').open('wb') as fh:
            fh.write(crypto.dump_publickey(crypto.FILETYPE_PEM, pk))
        str_now = str(now())
        signed = crypto.sign(pk, str_now.encode(), 'sha1')
        response = self.client.post(reverse('auth_token'), data={
            'user': 'edu-vd',
            'vector': str_now, 'signature': base64.b64encode(signed).decode(),
        }, content_type="application/json")
        self.assertIn('token', response.json())
        self.assertIn('expire', response.json())
        token = response.json()['token']
        response = self.client.post(
            reverse('api_ressource_create'), data={}, headers={"X-TOKEN-KEY": 'abcd1234'}
        )
        self.assertEqual(response.status_code, 403)

    def _test_resource_create_bsn_like(self, post_type='jsondata'):
        from ressource.tests.resource_get_sample import sample_result

        token = Token.get_token(user=self.user)
        if post_type == 'jsondata':
            response = self.client.post(
                reverse('api_ressource_create'), data=sample_result,
                headers={"X-TOKEN-KEY": token.sid}, content_type="application/json"
            )
        elif post_type == 'formdata':
            response = self.client.post(
                reverse('api_ressource_create'), data={'description': json.dumps(sample_result)},
                headers={"X-TOKEN-KEY": token.sid},
            )
        self.assertIn('lomId', response.json())
        res = Resource.objects.get(title="L'eau : plus qu'un jeu")
        self.assertEqual(res.partner, 'VD')
        self.assertEqual(res.duration, '12min42')

        response = self.client.get(
            reverse('api_lom_ressource', args=[res.uuid]), headers={"X-TOKEN-KEY": token.sid}
        )
        self.assertEqual(response.json()['general']['title']['fr'], "L'eau : plus qu'un jeu")
        self.assertGreater(res.resourceperlink_set.count(), 0)
        self.assertGreater(res.resourceperindex_set.count(), 0)

    def test_resource_create_bsn_like_jsondata(self):
        self._test_resource_create_bsn_like(post_type="jsondata")

    def test_resource_create_bsn_like_formdata(self):
        self._test_resource_create_bsn_like(post_type="formdata")

    def test_resource_edit_bsn_like(self):
        from ressource.tests.resource_get_sample import sample_result

        res = Resource.objects.create(
            title="Test1", url="https://www.example.org", uuid="de305d54-75b4-431b-adb2-eb6b9e546013", partner='VD'
        )
        ResourceRelation.objects.create(
            resource=res, rtype='has_part_of', url='https://example.org', description='Example'
        )
        ResourcePerLink.objects.create(resource=res, content_object=self.spec)
        token = Token.get_token(user=self.user)
        response = self.client.put(
            reverse('api_lom_ressource', args=[res.uuid]), data=sample_result,
            headers={"X-TOKEN-KEY": token.sid}, content_type="application/json"
        )
        self.assertEqual(response.json()['lomId'], res.uuid)
        res.refresh_from_db()
        res = Resource.objects.get(title="L'eau : plus qu'un jeu")
        self.assertEqual(res.resourceperlink_set.count(), 2)
        self.assertEqual(res.contribs.count(), 3)
        self.assertEqual(res.relations.count(), 1)
        self.assertEqual(res.partner, 'VD')
        self.assertEqual(res.duration, '12min42')

    def test_resource_delete_bsn_like(self):
        res = Resource.objects.create(
            title="Test1", url="https://www.example.org", uuid="de305d54-75b4-431b-adb2-eb6b9e546013", partner='VD'
        )
        token = Token.get_token(user=self.user)
        response = self.client.delete(
            reverse('api_lom_ressource', args=[res.uuid]),
            headers={"X-TOKEN-KEY": token.sid}
        )
        self.assertEqual(response.json()['lomId'], res.uuid)
        self.assertFalse(Resource.objects.filter(pk=res.pk).exists())

    def test_resource_details_bsn_like(self):
        res = self._create_resource(
            rich=True, uuid="de305d54-75b4-431b-adb2-eb6b9e546013", partner='VD'
        )
        token = Token.get_token(user=self.user)
        response = self.client.get(
            reverse('api_lom_ressource', args=[res.uuid]),
            headers={"X-TOKEN-KEY": token.sid}
        )
        self.assertEqual(response.json()['lomId'], res.uuid)

    def test_resource_search_bsn_like(self):
        res1 = Resource.objects.create(
            title="Test1", url="https://www.example.org", uuid="de307d54-75b4-431b-adb2-eb6b9e546013",
            partner='CIIP'
        )
        res2 = Resource.objects.create(
            title="Ressource de test", url="https://www.example.org",
            uuid="de305d54-75b4-431b-adb2-eb6b9e546013", partner='VD', cost=True, languages=['fr'],
            type_doc=['text'], keywords="exemple, test",
        )
        ResourcePerLink.objects.create(
            resource=res2,
            content_type=ContentType.objects.get_for_model(Specification),
            object_id=self.spec.pk,
        )
        token = Token.get_token(user=self.user)
        # additionalFields or additionalFields[] should give the same result
        qstring = (
            'query=test&additionalFields=perCycle&additionalFields=perDiscipline&'
            'additionalFields[]=perObjectiveCode'
            '&additionalFields=["difficulty","context"]'
        )
        response = self.client.get(
            reverse('api_lom_search') + f'?{qstring}',
            headers={"X-TOKEN-KEY": token.sid}
        )
        json_resp = response.json()
        self.assertEqual(json_resp['numFound'], 1)
        self.assertEqual(len(json_resp['result']), 1)
        self.assertEqual(json_resp['result'][0], {
            'title': "Ressource de test",
            'teaser': "",
            'previewImage': "",
            'lomId': "de305d54-75b4-431b-adb2-eb6b9e546013",
            # additionalFields
            'context': [{
                'ontologyId': 'compulsory education',
                'ontologyName': {'fr': 'Scolarité obligatoire'}
            }],
            'difficulty': '',
            'perCycle': ['1', 'Cycle 1'],
            'perDiscipline': ['Français'],
            'perObjectiveCode': ['L 11'],
        })
        # Search with filters
        qstring = (
            'filters[educaSchoolLevels][]="7th_and_8th_year"&'
            'filters[educaSchoolSubjects][]="french school language"&'
            'filters={"learningResourceType":["text","image"]}&'
            'filters={"keywords":["exemple","blabla"]}'
        )
        response = self.client.get(
            reverse('api_lom_search') + f'?{qstring}',
            headers={"X-TOKEN-KEY": token.sid}
        )
        json_resp = response.json()
        self.assertEqual(json_resp['numFound'], 0)
        res2.level = ['7th and 8th year']  # No underscore in our ontology
        res2.save()
        response = self.client.get(
            reverse('api_lom_search') + f'?{qstring}',
            headers={"X-TOKEN-KEY": token.sid}
        )
        json_resp = response.json()
        self.assertEqual(json_resp['numFound'], 1)
        # JSON-formatted filter
        qstring = 'filters={"educaSchoolSubjects":["french school language"]}'
        response = self.client.get(
            reverse('api_lom_search') + f'?{qstring}',
            headers={"X-TOKEN-KEY": token.sid}
        )
        self.assertEqual(response.json()['numFound'], 1)
