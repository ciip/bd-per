from django.conf import settings
from django.urls import reverse

from pper.filters import DomainLinkFilter, SpecLinkFilter


class JSONDomainLinkFilter(DomainLinkFilter):
    @staticmethod
    def replace_link_dom(match):
        from pper.models import Domain
        code, cont = match.groups()
        try:
            dom = Domain.objects.get(abrev=code)
        except Domain.DoesNotExist:
            return '''<font color="red">no domain found for code '%s'</font>''' % code
        return '<a href="https://%s">%s</a>' % (settings.SITE_DOMAIN + reverse('api_domaine', args=[dom.pk]), cont)


class JSONSpecLinkFilter(SpecLinkFilter):
    @classmethod
    def reverse_spec_url(cls, spec):
        return f"https://{settings.SITE_DOMAIN}{reverse('api_objectif', args=[spec.pk])}"

    @classmethod
    def replace_link_spec_ct(cls, match):
        code, link_cont = match.groups()
        return link_cont
