import json
from datetime import timedelta

from django.conf import settings
from django.core.signing import BadSignature, Signer, b64_decode, b64_encode
from django.db import models
from django.utils.crypto import constant_time_compare, get_random_string
from django.utils.encoding import force_bytes


class Token(models.Model):
    sid = models.CharField(max_length=32, db_index=True)
    issued_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)

    valid_duration = timedelta(days=1)

    @classmethod
    def get_token(cls, user=None):
        return Token.objects.create(sid=get_random_string(32), user=user)

    def make_jwt(self):
        payload = {
            # Standards
            'iss': 'bdper.plandetudes.ch',
            'iat': int(self.issued_at.timestamp()),
            'exp': int((self.issued_at + timedelta(days=7)).timestamp()),
            # Custom
            '_id': self.user_id,
            'email': self.user.email,
            'role': 'editor' if self.user.has_perm('ressource.change_resource') else 'user',
        }
        return signer.make_token(payload)


class JWTSigner(Signer):
    salt = 'bdper'
    header = {
        "alg": "HS256",
        "typ": "JWT",
    }
    def signature(self, value):
        # In Django 3.1, hopefully we should be able to:
        #return super().signature(dumped_value)
        # In Django 3.0, we have to simulate salted_hmac to provide our own algo:
        import hashlib
        import hmac
        hasher = hashlib.sha256
        key = hasher(force_bytes(self.salt) + force_bytes(settings.SECRET_KEY)).digest()
        hm = hmac.new(key, msg=force_bytes(value), digestmod=hasher)
        return b64_encode(hm.digest()).decode()

    def make_token(self, payload):
        def encode(mapping):
            return b64_encode(force_bytes(json.dumps(mapping, separators=(",", ":")))).decode()

        segments = [encode(self.header), encode(payload)]
        sig = self.signature('.'.join(segments))
        return '.'.join(segments + [sig])

    def decode_token(self, jwt):
        header, payload, sig = jwt.split('.')
        if not constant_time_compare(sig, self.signature('.'.join([header, payload]))):
            raise BadSignature
        return json.loads(b64_decode(force_bytes(payload)).decode())

signer = JWTSigner()
