import itertools
import json
from collections import OrderedDict

from django.contrib.auth import authenticate
from django.contrib.postgres.aggregates import ArrayAgg
from django.core.exceptions import PermissionDenied
from django.core.mail import mail_admins
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import CharField, Count, F, Func, Q, Value
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.dateparse import parse_datetime
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from pper.models import Specification, Cellule, ContenuCellule, Discipline
from ressource import ontologies
from ressource.educa import BSNImporter, as_struct
from ressource.forms import (
    ResourceSearchForm, ResourceSearchFormDateTime, ResourceSearchFormLOM,
    ResourceSuggestionForm
)
from ressource.models import (
    PARTNER_MAP, Imagier, Imagier3, Resource, ResourceGroup, ResourcePerLink
)
from ressource.views import (
    RenderingMixin, ResourceAccessMixin, ResourceGroupListMixin, ResourceHomeView
)
from shortener.models import ShortLink

from .base import BaseView

RESOURCE_MAPPING_SHORT = {'id': 'pk', 'titre': 'title', 'href': 'api_ressource', 'updated': 'updated', 'type': 'type_r'}
RESOURCE_GROUP_MAPPING_SHORT = {'id': 'pk', 'titre': 'title', 'href': 'api_catalogue', 'updated': 'updated'}


class SplitPart(Func):
    function = 'split_part'
    arity = 3


class APIAccessCheckMixin:
    """Mixin checking for a valid X-Token-Key."""
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous and request.headers.get('X-Token-Key'):
            request.user = authenticate(request)
        if not request.user or request.user.is_anonymous:
            raise PermissionDenied("Token not valid")
        return super().dispatch(request, *args, **kwargs)


class ResourceCreateView(APIAccessCheckMixin, View):
    def post(self, request, *args, **kwargs):
        resource = Resource()
        try:
            data = json.loads(request.body.decode('utf-8'))
        except json.JSONDecodeError:
            # Fallback on form-data
            data = json.loads(request.POST.get('description'))
        if data is None:
            return HttpResponseBadRequest("No input data was found in the request.")

        resource.partner = PARTNER_MAP.get(request.user.username, 'CIIP')
        try:
            BSNImporter.populate_from_bsn(resource, None, data=data)
        except Exception as err:
            mail_admins("BD-PER Error", f"Error while POSTing new ressource: {err}\nData: {data}")
            return HttpResponseBadRequest("Resource data error: {err}")
        return JsonResponse({'lomId': resource.uuid})


class ResourceLOMDetailsView(APIAccessCheckMixin, View):
    def get(self, request, *args, **kwargs):
        res = get_object_or_404(Resource, uuid=kwargs['uuid'])
        return JsonResponse(as_struct(res))

    def put(self, request, *args, **kwargs):
        res = get_object_or_404(Resource, uuid=kwargs['uuid'])
        try:
            data = json.loads(request.body.decode('utf-8'))
        except json.JSONDecodeError:
            return HttpResponseBadRequest("No input data was found in the request.")
        if res.partner != PARTNER_MAP.get(request.user.username, 'CIIP'):
            raise PermissionDenied(
                f"Vous n’avez pas les droits d’édition sur les ressources du partenaire {res.partner}"
            )
        try:
            BSNImporter.populate_from_bsn(res, res.uuid, data=data)
        except Exception as err:
            mail_admins("BD-PER Error", f"Error while PUTting (update) ressource: {err}\nData: {data}")
            return HttpResponseBadRequest("Resource data error: {err}")
        return JsonResponse({'lomId': res.uuid})

    def delete(self, request, *args, **kwargs):
        res = get_object_or_404(Resource, uuid=kwargs['uuid'])
        if res.partner != PARTNER_MAP.get(request.user.username, 'CIIP'):
            raise PermissionDenied(
                f"Vous n’avez pas les droits d’édition sur les ressources du partenaire {res.partner}"
            )
        res.delete()
        return JsonResponse({'lomId': res.uuid})


class ResourcesLOMSearchView(APIAccessCheckMixin, View):
    def get(self, request, *args, **kwargs):
        form = ResourceSearchFormLOM(request.user, data=request.GET)
        if not form.is_valid():
            return JsonResponse(
                {'error': "Le formulaire contient des erreurs.", 'errors': form.errors},
                status=400
            )
        qs = form.search()
        no_page = self.request.GET.get('', 1)
        page_size = form.cleaned_data.get('size', 10) or 10

        total = qs.count()
        limit = int(request.GET.get('limit', 20))
        offset = int(request.GET.get('offset', 0))
        assert qs.ordered
        results = qs[offset:offset + limit].prefetch_related(
            'contribs', 'relations', 'resourceperlink_set',
        )
        add_fields = request.GET.getlist('additionalFields') + request.GET.getlist('additionalFields[]')
        # Aplatir ['perCycle', '["difficulty","context"]', 'perObjectiveCode']
        add_fields = [
            item for sublist in [json.loads(f) if '[' in f else [f] for f in add_fields]
            for item in sublist
        ]
        return JsonResponse({
            'numFound': total,
            'result': [self.serialize_resource(res, add_fields) for res in results],
            'facets': {}
        })

    def serialize_resource(self, res, add_fields=()):
        fields_map = {
            'difficulty': 'difficulty',
            #'context': 'educ_level',
            'learningResourceType': 'type_doc',
            'keywords': 'keywords',
            'identifier': 'url',
            #'educaSchoolLevels': '',
            #'educaSchoolSubjects': '',
            #'classificationSchoolContext': '',
            #'classificationSchoolSubjects': '',
        }
        base = {
            'title': res.title,
            'teaser': res.description,
            'previewImage': res.get_thumb_url(),
            'lomId': res.uuid,
        }
        if any(fname.startswith('per') for fname in add_fields):
            objs = list(filter(None, [rpl.get_spec() for rpl in res.resourceperlink_set.all()]))
        for field_name in add_fields:
            if field_name == 'perCycle':
                value = []
                for c in sorted(set(spec.objectif.cycle for spec in objs)):
                    value.extend([str(c), f"Cycle {c}"])
            elif field_name == 'perObjectiveCode':
                value = list(set([spec.code for spec in objs]))
            elif field_name == 'perDiscipline':
                value = [
                    d.name for d in ResourcePerLink.disciplines_from_per_links(res.resourceperlink_set.all())
                ]
            elif field_name == 'context':
                value = [{
                    "ontologyId": level,
                    "ontologyName": {"fr": dict(ontologies.EDUCATION_LEVEL_CHOICES)[level]}
                } for level in res.educ_level]
            elif field_name not in fields_map:
                continue
            else:
                value = getattr(res, fields_map[field_name], '')
            base[field_name] = value
        return base


class ResourceDetailMixin:
    def _serialize(self, per_link):
        per_object = per_link.content_object
        if isinstance(per_object, Specification):
            from .per import SPECIFICATION_MAPPING
            data = self.serialize(
                per_object, mapping=dict(SPECIFICATION_MAPPING, href='api_objectif')
            )
            data['element'] = 'Objectif'
        elif isinstance(per_object, Discipline):
            from .per import DISCIPLINE_MAPPING
            per_object.specs = []
            data = self.serialize(
                per_object, mapping=dict(DISCIPLINE_MAPPING, href='api_discipline')
            )
            data['element'] = 'Discipline'
        elif isinstance(per_object, (Cellule, ContenuCellule)):
            from .per import OBJECTIF_MAPPING_SHORT
            cell = per_link.get_cell()
            per_object.objectif = cell.get_super().get_objectif()
            mapping = {'objectif': ('objectif', OBJECTIF_MAPPING_SHORT, 'api_objectif')}
            if isinstance(per_object, ContenuCellule):
                mapping['texte'] = 'render'
            data = self.serialize(per_object, mapping=mapping)
            data['element'] = "Contenu d'objectif"
        else:
            raise ValueError("Unable to serialize %s" % per_object)
        return data

    def serialize_resource(self, res, fmt):
        if fmt and fmt not in ('normal', 'raw', 'emedia'):
            raise ValueError("format should be either 'normal', 'raw' or 'emedia'")
        result = OrderedDict()
        exported_fields = [
            'id', 'created', 'updated', 'title', 'description', 'descr_eleve',
            'url', 'type_r', 'cantons', 'status'
            ] + Resource.METADATA_FIELD_NAMES + [
            'emedia', 'sdm', 'cinema', 'emedia_categ', 'mitic_asp', 'embed_code', 'no_player',
            'submitted_mat', 'num_pages', 'size', 'ages', 'difficulty', 'learn_duration', 'description_ped',
            'codes_rn',
        ]
        if self.request.user.has_perm('ressource.change_resource'):
            exported_fields.extend([
                'validated', 'status_comment', 'search_hidden', 'need_auth', 'show_refs',
                'qual_decisive', 'qual_collab', 'qual_counsels', 'qual_tested', 'qual_eval', 'qual_guarant',
            ])
        for field_name in exported_fields:
            label = Resource.verbose_field_name(field_name)
            if field_name == 'per_links':
                for link in res.refs():
                    result.setdefault("Liens PER", []).append({
                        'titre': link.title(),
                        'URL': link.to_per_url(),
                        'objectif': self.request.build_absolute_uri(
                            reverse('api_objectif', args=[link.specification_id])
                        ) if link.specification_id else None,
                        'catalogue': self.request.build_absolute_uri(
                            reverse('api_catalogue', args=[link.group_id])
                        ) if link.group_id else None,
                    })
            elif field_name == 'url':
                result[label] = res.get_url()
                if res.url.startswith('http') and res.url != result[label]:
                    result['URL originale'] = res.url
                if res.is_apprenant():
                    result['URL élève'] = self.request.build_absolute_uri(
                        reverse('resource-display-appr', args=[res.pk])
                    )
                    try:
                        sh = ShortLink.get_for_object(res)
                        result['URL courte'] = f'https://{sh.short_url()}'
                    except ShortLink.DoesNotExist:
                        pass
            elif field_name == 'codes_rn' and not hasattr(res, 'codes_rn'):
                continue
            else:
                result[label] = res.get_value(field_name, fmt=fmt, request=self.request)
        if 'prod_media' in (res.mitic_asp or []):
            # The 'prod_media' value of res.mitic_asp auto-generates the emedia_categ
            # 'Production scolaire'/'prod-scol' value
            if fmt in ('raw', 'emedia'):
                result['Catégorie e-media'] = result['Catégorie e-media'] or [] + ['prod-scol']
            else:
                current = [cat for cat in result['Catégorie e-media'].split(', ') if cat]
                result['Catégorie e-media'] = ', '.join(current + ['Production scolaire'])
        if res.mitic_asp:
            categs = [cat for cat in res.get_mitic_categs() if cat not in result['Aspects MITIC']]
            if fmt in ('raw', 'emedia'):
                result['Aspects MITIC'].extend(categs)
            else:
                result['Aspects MITIC'] += ', ' + ', '.join(categs)
        if res.level and fmt == 'emedia':
            result['Niveau scolaire'] = [lev.replace(' ', '_') for lev in res.level]
        for per_link in res.resourceperlink_set.all():
            if per_link.content_object is not None:
                result.setdefault("Plan d'études", []).append(self._serialize(per_link))
        for relation in res.relations.all():
            result.setdefault("Relations", []).append({
                'ID': relation.pk, 'Type de relation': relation.rtype,
                'URL': relation.get_url(), 'Description': relation.description,
                'Autre ressource': reverse('api_ressource', args=[relation.other_id]) if relation.other_id else None,
            })
        for link in res.resourcegrouplink_set.all():
            result.setdefault("Catalogues", []).append({
                'ID': link.pk,
                'URL': reverse('api_catalogue', args=[link.group_id]),
                'Titre': link.group.title_with_oa()
            })
            if link.number:
                result["Catalogues"][-1]['Code RN'] = link.number 
        result['Vignette'] = res.get_thumb_url()
        result['Contributeurs'] = [str(contrib) for contrib in res.contribs.all()]
        try:
            imagier = res.imagier
        except Imagier.DoesNotExist:
            pass
        else:
            imagier_fields = ['nom', 'numero', 'caract', 'categorie', 'type_img', 'num_syll', 'attaque', 'rime']
            imagier_dict = {'Type': 'Imagier 1-2'}
            for field_name in imagier_fields:
                label = Imagier.verbose_field_name(field_name)
                imagier_dict[label] = imagier.get_type_img_display() if field_name == 'type_img' else getattr(imagier, field_name)
            result['Imagier'] = imagier_dict
        try:
            imagier = res.imagier3
        except Imagier3.DoesNotExist:
            pass
        else:
            imagier_fields = [
                'mot', 'no_imagier', 'decodable', 'decodable_ent', 'decodable_aide',
                'phonemes', 'type_img', 'illustrateur',
            ]
            imagier_dict = {'Type': 'Imagier 3'}
            for field_name in imagier_fields:
                label = Imagier3.verbose_field_name(field_name)
                imagier_dict[label] = (
                    getattr(imagier, f'get_{field_name}_display')() if field_name in [
                        'decodable', 'decodable_ent', 'decodable_aide', 'type_img'
                    ]
                    else getattr(imagier, field_name)
                )
            result['Imagier'] = imagier_dict

        return result


class ResourcesSearch(ResourceDetailMixin, RenderingMixin, BaseView):
    def get(self, request, *args, **kwargs):
        if not request.GET:
            return JsonResponse(
                {'error': "Veuillez fournir au moins un critère de recherche."},
                status=400
            )
        form = ResourceSearchFormDateTime(request.user, data=request.GET)
        if not form.is_valid():
            return JsonResponse(
                {'error': "Le formulaire contient des erreurs.", 'errors': form.errors},
                status=400
            )
        qs = form.search(base_qs=Resource.objects.select_related('imagier', 'imagier3'))
        no_page = self.request.GET.get('page', 1)
        page_size = form.cleaned_data.get('size', 10) or 10
        if 'application/pdf' in self.request.headers.get('Accept', ''):
            return self.render_to_pdf(qs, form=form)
        return self.render_to_json(qs, no_page, page_size)

    def post(self, request, *args, **kwargs):
        """Possibilité d'envoyer une suggestion de ressource par l'API."""
        try:
            data = json.loads(request.body.decode('utf-8'))
        except Exception:
            return JsonResponse({'error': "Malformed JSON data"})
        # Artificially add minimal formset management data
        data.update({
            "files-TOTAL_FORMS": "1",
            "files-INITIAL_FORMS": "0",
            "gallery-TOTAL_FORMS": "1",
            "gallery-INITIAL_FORMS": "0",
            "relations-TOTAL_FORMS": "1",
            "relations-INITIAL_FORMS": "0",
            "author-TOTAL_FORMS": "1",
            "author-INITIAL_FORMS": "0",
            "editor-TOTAL_FORMS": "1",
            "editor-INITIAL_FORMS": "0",
            "publisher-TOTAL_FORMS": "1",
            "publisher-INITIAL_FORMS": "0",
            "resourceperlink_set-TOTAL_FORMS": "1",
            "resourceperlink_set-INITIAL_FORMS": "0",
        })

        form = ResourceSuggestionForm(data=data)
        if form.is_valid():
            resource = form.save()
            form.send_email_confirmation()
            resp = HttpResponse(status=201)
            resp['Location'] = request.build_absolute_uri(reverse('api_ressource', args=[resource.pk]))
            return resp
        else:
            errs = form.errors
            errs["__all__"] = form.non_field_errors()
            return JsonResponse({
                "error": "Le formulaire contient des erreurs.",
                "errors": errs,
            })

    def render_to_json(self, qs, no_page, page_size):
        try:
            page = Paginator(qs, page_size).page(no_page)
        except (EmptyPage, PageNotAnInteger):
            page = Paginator(qs, page_size).page(1)
        result = []
        for resource in page.object_list.prefetch_related(
                'relations', 'resourcegrouplink_set', 'resourceperlink_set'
            ):
            result.append(self.serialize_resource(resource, fmt='normal'))
        return JsonResponse({
            'content': result,
            'totalElements': page.paginator.count,
            'totalPages': page.paginator.num_pages,
        }, json_dumps_params={'indent': 2})


class ResourcesAllIDs(BaseView):
    """
    List of all accessible resource IDs that could be returned by ResourcesSearch.
    """
    def get_data(self):
        form = ResourceSearchForm(
            self.request.user, data={'status': ['validee'], 'broken': False}
        )
        assert form.is_valid()
        return list(
            form.search(prefetch=False).values_list('pk', flat=True).order_by().order_by('pk')
        )


class ResourcesEMedia(BaseView):
    """
    List of all resources flagged as e-media resources.
    Allow a from=ISO_date GET param to only get resources updated from that date.
    """
    ids_only = False

    def get_data(self):
        result = []
        qs = Resource.objects.filter(status='validee', emedia=True)

        from_date_str = self.request.GET.get('from')
        if from_date_str:
            try:
                from_date = parse_datetime(from_date_str)
            except ValueError:
                from_date = None
            if from_date_str and not from_date:
                raise ValueError("Date invalid or not well-formatted (should be ISO 8601)")
            qs = qs.filter(updated__gt=from_date)

        if self.ids_only:
            result = list(qs.values_list('pk', flat=True).order_by('pk'))
        else:
            for resource in qs:
                result.append(self.serialize(resource, mapping=RESOURCE_MAPPING_SHORT))
        return result


class ResourceDetails(ResourceAccessMixin, ResourceDetailMixin, BaseView):
    """
    Details for a single resource.
    'format' GET parameter can be absent, 'normal', 'raw' or 'emedia'.
    The 'emedia' format is specific to the access by the www.e-media.ch website.
    """
    model = Resource.objects.annotate(
        codes_rn=ArrayAgg(
            'resourcegrouplink__number', filter=Q(resourcegrouplink__number__isnull=False), distinct=True
        )
    )

    def get_data(self):
        res = self.get_object()
        fmt = self.request.GET.get('format') or 'normal'
        return self.serialize_resource(res, fmt)


CATALOG_LIST_MAPPING = {
    'id': 'pk', 'statut': 'grp_status', 'updated': 'updated',
    'titre': 'title', 'sous-titre': 'subtitle',
    'sous-titre secondaire': 'subtitle2', 'regroupement': 'groupment',
    'domaine': 'domaine', 'objectif': 'objectif',
    'nombre de ressources': 'num_resources',
}
GALLERY_PK_BASE = 100000


class ResourceGroups(ResourceGroupListMixin, BaseView):
    def serialize_group(self, grp, mapping):
        struct = {'id': grp.pk, 'url': reverse('api_catalogue', args=[grp.pk])}
        for k, v in mapping.items():
            if k == 'domaine':
                struct[k] = {
                    'nom': grp.domaine.name,
                    'URL': reverse('api_domaine', args=[grp.domaine.pk]) if grp.domaine.pk else None,
                }
            elif k == 'objectif':
                struct[k] = {
                    'nom': str(grp.objectif) if grp.objectif else (grp.groupment or 'Sans objectif'),
                    'URL': reverse('api_objectif', args=[grp.objectif.pk]) if grp.objectif else None,
                    'URL PPER': grp.objectif_link if grp.objectif else None,
                }
            else:
                struct[k] = getattr(grp, v)
        return struct

    def serialize_gallery(self, gallery):
        grp_pk = GALLERY_PK_BASE + gallery.pk
        cat = next(iter(gallery.resourcegroup_set.all()), None)
        domaine = cat.domaine() if cat else None
        objectif = cat.objectif if cat else None
        struct = {
            'id': grp_pk, 'url': reverse('api_catalogue', args=[grp_pk]),
            'statut': 'valide', 'updated': gallery.updated, 'titre': gallery.title,
            'domaine': {
                'nom': domaine.name,
                'URL': reverse('api_domaine', args=[domaine.pk]) if domaine else None
            } if cat else {},
            'objectif': {
                'nom': str(objectif),
                'URL': reverse('api_objectif', args=[objectif.pk]),
                'URL PPER': cat.objectif_link,
            } if objectif else {},
            'nombre de ressources': gallery.num_images,
        }
        return struct

    def get_data(self):
        catalogs = self.get_catalogs(self.request.user)
        # Also insert Gallery resources like catalogs
        gallery_cats = Resource.objects.galleries().filter(
            status='validee'
        ).prefetch_related('resourcegroup_set')
        return (
            [self.serialize_group(cat, CATALOG_LIST_MAPPING) for cat in catalogs] +
            [self.serialize_gallery(gal) for gal in gallery_cats]
        )


class ResourceGroupsAllIDs(BaseView):
    """
    List of all accessible catalog IDs that could be returned by ResourceGroups.
    """
    def get_data(self):
        gallery_cats = Resource.objects.galleries().filter(status='validee')
        return list(
            ResourceGroup.all_by_user(self.request.user).order_by('pk').values_list('pk', flat=True)
        ) + sorted([GALLERY_PK_BASE + gal.pk for gal in gallery_cats])


class ResourceGroupDetails(BaseView):
    """Details of a resource group with links to individual resources."""
    def get_data(self):
        pk = self.kwargs['id']
        if pk > GALLERY_PK_BASE:
            return self._get_gallery_data()
        return self._get_catalog_data()

    def _get_catalog_data(self):
        grp = get_object_or_404(
            ResourceGroup.all_by_user(self.request.user).select_related('domain', 'objectif__domain'),
            pk=self.kwargs['id']
        )
        result = OrderedDict()
        for field_name in ('title', 'subtitle', 'subtitle2', 'groupment', 'description', 'mer_url', 'levels'):
            field = ResourceGroup._meta.get_field(field_name)
            if hasattr(grp, 'get_%s_display' % field_name):
                value = getattr(grp, 'get_%s_display' % field_name)()
            else:
                value = getattr(grp, field_name)
            result[field.verbose_name] = value
        result['updated'] = grp.updated
        result['statut'] = grp.grp_status
        objectif = grp.objectif
        if objectif:
            obj_spec = objectif.specification_set.first()
            result['Objectif'] = {
                'nom': str(objectif),
                'URL': self.request.build_absolute_uri(reverse('api_objectif', args=[obj_spec.pk])),
                'URL PPER': grp.objectif_link,
            }
        else:
            result['Objectif'] = {
                'nom': grp.groupment or 'Sans objectif',
                'URL': None,
                'URL PPER': None,
            }
        dom = grp.domaine()
        result['domaine'] = {'nom': dom.name, 'url': reverse('api_domaine', args=[dom.pk])} if dom else None
        result['sous-thèmes'] = [
            {'nom': th.name, 'numéro': th.num} for th in grp.subthemes.all().ordered() if th != 'BREAK'
        ]

        result['ressources'] = []
        for link in grp.ordered_resources(self.request.user):
            result['ressources'].append(self.serialize(link.resource, mapping=RESOURCE_MAPPING_SHORT))
            result['ressources'][-1]['Numéro'] = link.number
            result['ressources'][-1]['sous-thèmes'] = [
                {'nom': th.name, 'numéro': th.num} for th in link.subthemes.all()
            ]
        return result

    def _get_gallery_data(self):
        res = get_object_or_404(Resource, pk=self.kwargs['id'] - GALLERY_PK_BASE)
        cat = next(iter(res.resourcegroup_set.all()), None)
        domaine = cat.domaine() if cat else None
        objectif = cat.objectif if cat else None
        images_ress = res.relations.filter(
            other__type_doc__contains=['image']
        ).select_related('other').annotate(
            # sort by filename in 'ressources/44444/real_file_name.jpg'
            filename=SplitPart(F('other__files__rfile'), Value('/'), -1, output_field=CharField())
        ).order_by('filename')
        result = {
            'Titre': res.title, 'Description': res.description, 'sous-thèmes': [],
            'updated': res.updated, 'statut': 'valide',
            'domaine': {'nom': domaine.name, 'url': reverse('api_domaine', args=[domaine.pk])} if domaine else None,
            'Objectif': {
                'nom': str(objectif) if objectif else 'Sans objectif',
                'URL': self.request.build_absolute_uri(
                    reverse('api_objectif', args=[objectif.specification_set.first().pk])
                ) if objectif else None,
            },
            'ressources': [
                self.serialize(sub.other, mapping=RESOURCE_MAPPING_SHORT)
                for sub in images_ress
            ],
        }
        return result


class ResourceOntologies(BaseView):
    def get_data(self):
        return [{
            key: value for key, value in ontologies.__dict__.items()
            if not key.startswith('_')
        }]
