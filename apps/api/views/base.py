from django.http import HttpResponseBadRequest, JsonResponse
from django.urls import reverse
from django.views.generic import View

from pper import filters
from pper.models import ContenuCellule
from .. import filters as api_filters


class BaseView(View):
    def __init__(self, *args, **kwargs):
        self.content_filters = self.get_content_filters()
        super().__init__(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        try:
            json_data = self.get_data()
        except ValueError as exc:
            return HttpResponseBadRequest(str(exc))
        return JsonResponse(json_data, safe=False, json_dumps_params={'indent': 4})

    @classmethod
    def get_content_filters(cls, embed_images=True):
        return filters.FilterChain([
            filters.HTMLNiveauContentFilter(),
            filters.HTMLComposanteContentFilter(),
            filters.HTMLLexiqueFilter(),
            filters.HTMLImageFilter(),
            api_filters.JSONDomainLinkFilter(),
            api_filters.JSONSpecLinkFilter(),
        ])

    def get_data(self):
        raise NotImplementedError

    def render_contenu(self, contenu, nature=''):
        cc_id = None
        if isinstance(contenu, ContenuCellule):
            cc_id = contenu.pk
            contenu = contenu.contenu
        if not nature:
            return contenu.render(self.content_filters, {})
        response = {
            'id': contenu.pk,
            'texte': contenu.render(self.content_filters, {}),
            'href': self.request.build_absolute_uri(reverse('api_%s' % nature, args=[contenu.pk]))
        }
        if cc_id is not None:
            response['id_cc'] = cc_id
        return response

    def serialize(self, obj, mapping=None, **extra_kwargs):
        """
        Generic serialization method using a dictionary mapping to produce a
        data-filled dictionary ready to be JSON-serialized.
        """
        if mapping is None:
            mapping = self.mapping
        struct = {'id': obj.pk}
        struct.update(extra_kwargs)
        for k, v in mapping.items():
            if isinstance(v, tuple):
                if k == 'objectifs_lies':
                    attr = obj.get_linked_objectifs(reverse=True)
                    attr = [obj.specification_set.first() for obj in attr]
                else:
                    attr = getattr(obj, v[0])
                if hasattr(attr, 'all'):
                    # transform related QuerySet into list
                    attr = list(getattr(obj, v[0]).all())
                elif callable(attr):
                    attr = attr()
                if isinstance(attr, list):
                    struct[k] = []
                    for subobj in attr:
                        struct[k].append(self.serialize(subobj, mapping=v[1]))
                        if v[2]:
                            struct[k][-1]['href'] = self.request.build_absolute_uri(reverse(v[2], args=[subobj.pk]))
                else:
                    struct[k] = self.serialize(attr, mapping=v[1])
                    if v[2]:
                        struct[k]['href'] = self.request.build_absolute_uri(reverse(v[2], args=[attr.pk]))
            elif k == 'href':
                struct[k] = self.request.build_absolute_uri(reverse(v, args=[obj.pk]))
            elif k in {'pré-contenu', 'post-contenu'}:
                contenu = getattr(obj, v)
                struct[k] = self.render_contenu(contenu) if contenu else None
            elif v.startswith('render'):
                # Rendering Contenu, optionally as rich content (e.g. render_attente, etc)
                nature = v.split('_')[1] if '_' in v else None
                if nature:
                    struct.update(self.render_contenu(obj, nature))
                else:
                    struct[k] = self.render_contenu(obj, nature)
            else:
                struct[k] = getattr(obj, v)
                if callable(struct[k]):
                    struct[k] = struct[k]()
        return struct
