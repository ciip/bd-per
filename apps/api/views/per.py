from collections import defaultdict
from pathlib import Path

from django.apps import apps
from django.db.models import Prefetch, Q
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.html import urlize
from django.views.generic import TemplateView

from pper import filters
from pper.models import (
    Domain, Discipline, Thematique, Specification, Contenu, ContenuCellule,
    TYPE_CELL, TYPE_TITRE1,
)
from public.utils import get_spec_neighbours
from ressource.models import ResourceGroup, ResourcePerLink

from .base import BaseView
from .resources import RESOURCE_MAPPING_SHORT, RESOURCE_GROUP_MAPPING_SHORT


GENERIC_MAPPING_SHORT = {'id': 'pk', 'nom': 'name'}

OBJECTIF_MAPPING_SHORT = {'id': 'pk', 'code': 'code'}

DOMAINE_MAPPING = {
    'nom': 'name',
    'abreviation': 'abrev',
    'visee': 'visee',
    'couleur_foncee': 'col_dark',
    'couleur_moyenne': 'col_medium',
    'couleur_claire': 'col_light',
    'disciplines': ('disciplines', GENERIC_MAPPING_SHORT, 'api_discipline'),
    'thematiques': ('thematiques', GENERIC_MAPPING_SHORT, 'api_thematique'),
    'objectifs': ('objectifs', OBJECTIF_MAPPING_SHORT, 'api_objectif'),
}

DISCIPLINE_MAPPING = {
    'nom': 'name',
    'description': 'description',
    'domaine': ('domain', GENERIC_MAPPING_SHORT, 'api_domaine'),
    'objectifs': ('specs', OBJECTIF_MAPPING_SHORT, 'api_objectif'),
    'cantons': 'cantons',
}

THEMATIQUES_MAPPING = {
    'nom': 'name',
    'no_ordre': 'order_no',
    'domaine': ('domain', GENERIC_MAPPING_SHORT, 'api_domaine'),
    'href': 'api_thematique',
}

THEMATIQUE_MAPPING = {
    'nom': 'name',
    'no_ordre': 'order_no',
    'domaine': ('domain', GENERIC_MAPPING_SHORT, 'api_domaine'),
    'disciplines': ('get_disciplines', GENERIC_MAPPING_SHORT, 'api_discipline'),
    'objectifs': ('specs', OBJECTIF_MAPPING_SHORT, 'api_objectif'),
}

OBJECTIF_MAPPING = {
    'nom': 'title',
    'code': 'code',
    'cycle': 'cycle',
    'domaine': ('domain', GENERIC_MAPPING_SHORT, 'api_domaine'),
    'thematiques': ('thematiques', GENERIC_MAPPING_SHORT, 'api_thematique'),
    'objectifs_lies': ('linked', OBJECTIF_MAPPING_SHORT, 'api_objectif'),
    'composantes': ('composantes', {'titre': 'title', 'code': 'get_letter'}, None),
    'cantons': 'cantons',
}

SPECIFICATION_MAPPING = {
    'code': 'code',
    'disciplines': ('disciplines', GENERIC_MAPPING_SHORT, 'api_discipline'),
    'pré-contenu': 'pre_content',
    'post-contenu': 'post_content',
    'pdf': 'printable',
}

PROGRESSION_MAPPING = {
    'texte': 'render',
    'objectif': ('objectif', OBJECTIF_MAPPING_SHORT, 'api_objectif'),
    'lignes': 'lines',
    'attentes': ('attentes', {'contenu': 'render_attente'}, None),
    'indications': ('indications', {'contenu': 'render_indication'}, None),
}

ATTENTE_MAPPING = {
    'texte': 'render',
    'objectif': ('objectif', OBJECTIF_MAPPING_SHORT, 'api_objectif'),
    'lignes': 'lines',
    'progressions': ('items', {'contenu': 'render_progression'}, None),
}

INDICATION_MAPPING = ATTENTE_MAPPING


class APIOverview(TemplateView):
    template_name = "api/overview.html"


class TableauRenderingMixin:
    def render_subtable(self, tableau, filters=None):
        struct = {'titre': tableau.title, 'tableau': []}
        cell_data = tableau.get_celltablestruct(editable=False, filters=filters or self.content_filters)
        type_c_dict = {tp[0]: tp[2] for tp in TYPE_CELL}
        for line_idx, cell_dict in cell_data:
            struct['tableau'].append([])
            for key in sorted(cell_dict.keys()):
                cell_info = cell_dict[key]
                struct['tableau'][-1].append({
                    'id': cell_info['cell'].pk,
                    'rowspan': cell_info['rowspan'], 'colspan': cell_info['colspan'],
                    'type': type_c_dict[cell_info['cell'].type_c],
                    'bordure': cell_info['cell'].type_bord,
                    'contenus': [
                        {'id': cont[0], 'id_cc': cont[2], 'texte': cont[1]}
                        for cont in cell_info['cell'].get_content_list()
                    ],
                })
                self.set_cell_resources(cell_info['cell'].pk, struct['tableau'][-1][-1])
        return struct


class CapaciteList(BaseView):
    def get(self, request, *args, **kwargs):
        path = Path(apps.get_app_config('pper').path) / 'capacites_transversales.json'
        return FileResponse(path.open('rb'))


class DomaineBaseView(BaseView):
    mapping = DOMAINE_MAPPING

    def get_domaine_details(self, dom):
        dom.disciplines = dom.discipline_set.filter(published=True)
        # Skip Objectif layer and directly provide Specification objects
        dom.objectifs = Specification.objects.filter(objectif__domain=dom, status='published')
        dom.thematiques = dom.thematique_set.order_by('order_no')
        return self.serialize(
            dom,
            lexique=self.request.build_absolute_uri(reverse('api_lexique', args=[dom.pk])),
            commentaires=[
                {
                    'id': com.pk,
                    'href': self.request.build_absolute_uri(
                        reverse('api_domaine_com_gen', args=[dom.pk, com.pk])
                    ),
                    'type': com.get_type_t_display(),
                }
                for com in dom.com_gen.all()
            ],
        )


class DomaineList(DomaineBaseView):
    def get_data(self):
        domains = []
        for domain in Domain.objects.all().prefetch_related(
                Prefetch('discipline_set', queryset=Discipline.objects.filter(published=True), to_attr='disciplines')):
            domains.append(self.get_domaine_details(domain))
        return domains


class DomaineDetails(DomaineBaseView):
    mapping = DOMAINE_MAPPING

    def get_data(self):
        dom = get_object_or_404(Domain, pk=self.kwargs['id'])
        return self.get_domaine_details(dom)


class DomaineComGen(TableauRenderingMixin, BaseView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Use absolute links for images
        self.content_filters = self.get_content_filters(embed_images=False)

    def get_data(self):
        dom = get_object_or_404(Domain, pk=self.kwargs['id'])
        com_gen = get_object_or_404(dom.com_gen, pk=self.kwargs['id_com'])
        data = {
            'id': com_gen.pk,
            'domaine': {
                'id': dom.pk,
                'href': self.request.build_absolute_uri(reverse('api_domaine', args=[dom.pk])),
            },
            'titre': com_gen.title,
            'type': com_gen.get_type_t_display(),
        }
        tbl = self.render_subtable(com_gen)
        one_cell = len(tbl['tableau']) == 1 and len(tbl['tableau'][0]) == 1
        if one_cell:
            data['contenu'] = "\r\n".join(cont['texte'] for cont in tbl['tableau'][0][0]['contenus'])
        else:
            data['tableau'] = tbl['tableau']
        return data

    def set_cell_resources(self, *args, **kwargs):
        pass


class LexiqueDetails(BaseView):
    def get_data(self):
        dom = get_object_or_404(Domain, pk=self.kwargs['id'])
        data = self.serialize(dom, mapping=GENERIC_MAPPING_SHORT)
        data['termes'] = []
        for lex in dom.lexique_set.all().prefetch_related('disciplines').order_by('term'):
            data['termes'].append({
                'terme': lex.term,
                'définition': lex.definition,
                'disciplines': [d.name for d in lex.disciplines.all()],
            })
        return data


class DisciplineBase(BaseView):
    mapping = DISCIPLINE_MAPPING
    queryset = Discipline.objects.filter(published=True).prefetch_related(
            Prefetch('specification_set', queryset=Specification.objects.filter(status='published'),
                     to_attr='specs')
        )


class DisciplineList(DisciplineBase):

    def get_data(self):
        return [self.serialize(disc) for disc in self.queryset]


class DisciplineDetails(DisciplineBase):
    mapping = DISCIPLINE_MAPPING

    def get_data(self):
        disc = get_object_or_404(self.queryset, pk=self.kwargs['id'])
        return self.serialize(disc)


class ThematiqueList(BaseView):
    mapping = THEMATIQUES_MAPPING

    def get_data(self):
        return [self.serialize(them) for them in Thematique.objects.all()]


class ThematiqueDetails(BaseView):
    mapping = THEMATIQUE_MAPPING

    def get_data(self):
        th = get_object_or_404(Thematique, pk=self.kwargs['id'])
        th.specs = Specification.objects.filter(objectif__in=th.objectifs.all())
        return self.serialize(th)


class ObjectifBase(BaseView):
    mapping = SPECIFICATION_MAPPING
    queryset = Specification.objects.filter(status='published'
            ).select_related('objectif', 'objectif__objectif2', 'objectif__objectif3'
            ).prefetch_related('objectif__composante_set')

    def preload_resources(self, spec_or_table):
        # Collect resources and resource groups
        res_per_links = ResourcePerLink.from_cell_ids(
            spec_or_table.cellule_set.values_list('id', flat=True)
        ).filter(
            Q(resource__status='validee') | Q(catalog__grp_status__in=['valide', 'limite'])
        )
        for lk in res_per_links:
            if lk.resource_id:
                self.resources[lk.object_id].append(lk.resource)
            else:
                self.resource_groups[lk.object_id].append(lk.catalog)

    def set_cell_resources(self, cell_pk, cell_data):
        resources = []
        if cell_pk in self.resources:
            resources.extend([
                self.serialize(res, mapping=RESOURCE_MAPPING_SHORT, type_princ='ressource')
                for res in self.resources[cell_pk]
            ])
        if cell_pk in self.resource_groups:
            resources.extend([
                self.serialize(grp, mapping=RESOURCE_GROUP_MAPPING_SHORT, type_princ='catalogue')
                for grp in self.resource_groups[cell_pk]
            ])
        if resources:
            cell_data['ressources'] = resources

    def serialize_spec(self, spec, minimal=False):
        self.resources = defaultdict(list)
        self.resource_groups = defaultdict(list)
        self.preload_resources(spec)
        # Objectif-related data
        data = self.serialize(spec.objectif, mapping=OBJECTIF_MAPPING)

        data.update(self.serialize(spec))

        if minimal:
            return data

        cellules = spec.cellule_set.prefetch_related(
            Prefetch(
                'contenucellule_set',
                queryset=ContenuCellule.objects.select_related('contenu').order_by('position')
            ),
        ).order_by('position')
        data['titres'], data['liens'], data['attentes'], data['indications'] = [], [], [], []
        data['progressions'] = [
            {'sous-cycle': subh['txt'], 'annees': subh['annees'], 'items': []}
            for subh in spec.sub_headers()
        ]
        previous_attente_ids, previous_indication_ids = None, None

        for cellule in cellules:
            contenusc = list(cellule.contenucellule_set.all())
            if not contenusc and not cellule.pk in self.resources and not cellule.pk in self.resource_groups:
                continue  # Completely empty cell
            contenus_ids = [cc.contenu.id for cc in contenusc]
            if cellule.is_attente():
                if contenus_ids != previous_attente_ids:
                    data['attentes'].append({
                        'contenus': [self.render_contenu(cc, 'attente') for cc in contenusc],
                        'lignes': [cellule.position],
                    })
                    self.set_cell_resources(cellule.pk, data['attentes'][-1])
                    previous_attente_ids = contenus_ids
                else:
                    data['attentes'][-1]['lignes'].append(cellule.position)
            elif cellule.is_indication():
                if contenus_ids != previous_indication_ids:
                    data['indications'].append({
                        'contenus': [self.render_contenu(cc, 'indication') for cc in contenusc],
                        'lignes': [cellule.position]
                    })
                    self.set_cell_resources(cellule.pk, data['indications'][-1])
                    previous_indication_ids = contenus_ids
                else:
                    data['indications'][-1]['lignes'].append(cellule.position)
            elif cellule.is_title():
                annees = []
                for idx, scycle in enumerate([cellule.scycle1, cellule.scycle2, cellule.scycle3]):
                    if scycle:
                        annees.extend(spec.sub_headers()[idx]['annees'])
                data['titres'].append({
                    'annees': annees,
                    'contenus': [self.render_contenu(cc) for cc in contenusc],
                    'lignes': [cellule.position],
                    'niveau': 1 if cellule.type_c == TYPE_TITRE1 else 2,
                })
                self.set_cell_resources(cellule.pk, data['titres'][-1])
            elif cellule.is_liens():
                data['liens'].append({
                    'contenus': [self.render_contenu(cc) for cc in contenusc],
                    'lignes': [cellule.position],
                })
                self.set_cell_resources(cellule.pk, data['liens'][-1])
            else:
                # Progression cells
                sous_cycles = [idx for idx, scycle in enumerate([cellule.scycle1, cellule.scycle2, cellule.scycle3]) if scycle]
                rendered_content = [self.render_contenu(cc, 'progression') for cc in contenusc]
                for idx in sous_cycles:
                    if (data['progressions'][idx]['items'] and
                            rendered_content == data['progressions'][idx]['items'][-1]['contenus']):
                        # Same content as line above, just append the line number
                        data['progressions'][idx]['items'][-1]['lignes'].append(cellule.position)
                    else:
                        data['progressions'][idx]['items'].append({
                            'contenus': rendered_content,
                            'lignes': [cellule.position],
                        })
                        self.set_cell_resources(cellule.pk, data['progressions'][idx]['items'][-1])
        data['pdf'] = ''
        return data


class ObjectifList(ObjectifBase):
    def get_data(self):
        return [self.serialize_spec(spec, minimal=True) for spec in self.queryset]


class ObjectifListByDomaine(ObjectifBase):
    def get_data(self):
        self.queryset = self.queryset.filter(objectif__domain__pk=self.kwargs['id'])
        return [self.serialize_spec(spec, minimal=True) for spec in self.queryset]


class ObjectifListByDiscipline(ObjectifBase):
    def get_data(self):
        self.queryset = self.queryset.filter(disciplines__pk=self.kwargs['id'])
        return [self.serialize_spec(spec, minimal=True) for spec in self.queryset]


class ObjectifDetails(ObjectifBase):
    def get_data(self):
        spec = get_object_or_404(self.queryset, pk=self.kwargs['id'])
        return self.serialize_spec(spec)


class ObjectifTable(TableauRenderingMixin, ObjectifBase):
    """
    Produce a JSON structure similar to the tables produced on the web site.
    """
    @classmethod
    def get_content_filters(cls, **kwargs):
        # We should have used BaseView.content_filters, but now that portail
        # consumer adapted to this "bug" we should keep backwards compatibility until further notice.
        # These filters are equal to spec.default_content_filters
        return filters.FilterChain([
            filters.HTMLNiveauContentFilter(),
            filters.HTMLComposanteContentFilter(),
            filters.HTMLLexiqueFilter(),
            filters.HTMLImageFilter(),
            filters.DomainLinkFilter(),
            filters.SpecLinkFilter(),
        ])

    def get_data(self):
        spec = get_object_or_404(self.queryset, pk=self.kwargs['id'])
        data = self.serialize_spec(spec, minimal=True)
        data['navigation'] = {
            key: self.serialize(
                _spec.objectif, mapping={**OBJECTIF_MAPPING_SHORT, 'href': 'api_objectif'}
            ) if _spec else None
            for key, _spec in get_spec_neighbours(spec).items()
        }
        headers = spec.get_headers()
        # List of lines (first two are headers)
        data['tableau'] = [
            [{'rowspan': 1, 'colspan': cell['colspan'], 'type': 'en-tête',
              'contenus': {'texte': cell['label']}}
             for cell in headers['ligne1']],
            [{'rowspan': 1, 'colspan': cell['colspan'], 'type': 'en-tête',
              'contenus': {'texte': cell['label']}}
             for cell in headers['ligne2']],
        ]
        struct = spec.get_celltablestruct(editable=False, filters=self.content_filters)
        type_c_dict = {tp[0]: tp[2] for tp in TYPE_CELL}
        base_url = self.request.get_host()
        for line_idx, cell_dict in struct:
            data['tableau'].append([])
            for key in sorted(cell_dict.keys()):
                cell_info = cell_dict[key]
                type_c = cell_info['cell'].type_c
                data['tableau'][-1].append({
                    'id': cell_info['cell'].pk,
                    'rowspan': cell_info['rowspan'], 'colspan': cell_info['colspan'],
                    'type': type_c_dict[type_c],
                    'bordure': cell_info['cell'].type_bord,
                    'contenus': [{'id': cont[0], 'id_cc': cont[2], 'texte': cont[1]}
                                 for cont in cell_info['cell'].get_content_list()],
                    'illustrations': [
                        {'id': illustr.pk,
                         'titre': illustr.titre,
                         'texte': illustr.texte.replace(
                            'src="/uploads/', f'src="https://{base_url}/uploads/'
                         ),
                         'source': urlize(illustr.source).replace("\r\n", "<br>"),
                        }
                        for illustr in cell_info['cell'].illustration_set.all() if illustr.published
                    ],
                })
                if cell_info['cell'].is_title():
                    data['tableau'][-1][-1]['niveau'] = 1 if type_c == TYPE_TITRE1 else 2
                self.set_cell_resources(cell_info['cell'].pk, data['tableau'][-1][-1])
        # Compléments pré/post tableaux
        data['pré-tableaux'] = []
        data['post-tableaux'] = []
        for alt_table in spec.objectif.pre_tableaux(spec.disciplines.all()):
            self.preload_resources(alt_table)
            data['pré-tableaux'].append(self.render_subtable(alt_table, filters=self.content_filters))
        for alt_table in spec.objectif.tableaux.filter(position__gte=0):
            self.preload_resources(alt_table)
            # See note above for get_celltablestruct on why we use default_content_filters
            data['post-tableaux'].append(self.render_subtable(alt_table, filters=self.content_filters))
        data['pdf'] = ''
        return data


class ContenuDetails(BaseView):
    def get_data(self):
        contenu = get_object_or_404(Contenu, pk=self.kwargs['id'])
        if 'pid' in self.kwargs:
            contenu.objectif = get_object_or_404(Specification, pk=self.kwargs['pid'])
            self.spec = contenu.objectif
            cells = contenu.contenucellule_set.filter(cellule__specification=contenu.objectif
                ).select_related('cellule').order_by('cellule__position')
            contenu.lines = [c.cellule.position for c in cells]
        else:
            # No specification context, simply output the text content
            self.spec = None
            self.mapping = {'texte': 'render'}
        return contenu  # Serialization is done by subclasses


class ProgressionDetails(ContenuDetails):
    mapping = PROGRESSION_MAPPING

    def get_data(self):
        contenu = super().get_data()
        if self.spec:
            contenu.attentes = ContenuCellule.objects.filter(
                cellule__specification=contenu.objectif, cellule__position__in=contenu.lines
                ).filter(cellule__attentes=True)
            contenu.indications = ContenuCellule.objects.filter(
                cellule__specification=contenu.objectif, cellule__position__in=contenu.lines
                ).filter(cellule__indications=True)
        return self.serialize(contenu)


class AttenteDetails(ContenuDetails):
    mapping = ATTENTE_MAPPING

    def get_data(self):
        contenu = super().get_data()
        if self.spec:
            progr_items = ContenuCellule.objects.filter(
                cellule__specification=contenu.objectif, cellule__position__in=contenu.lines
            ).filter(Q(cellule__scycle1=True) | Q(cellule__scycle2=True) | Q(cellule__scycle3=True)
            ).order_by('cellule__position')
            contenu.items = [cc.contenu for cc in progr_items]
        return self.serialize(contenu)


class IndicationDetails(AttenteDetails):
    mapping = INDICATION_MAPPING
