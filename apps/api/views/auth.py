import base64
import json
import time
from pathlib import Path

from OpenSSL import crypto

from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from api.models import Token


class GetSIDView(View):
    def get(self, request, *args, **kwargs):
        sid = Token.get_token().sid
        return JsonResponse({
            'loginURL': request.build_absolute_uri(reverse('login')) + f'?sid={sid}',
            'targetURL': request.build_absolute_uri(reverse('afterlogin', args=[sid])),
            'sid': sid,
        })


class AfterLoginView(View):
    """
    Once a user is logged in with a sid, complete the Token and redirect the
    user to the client-side app.
    """
    def get(self, request, *args, **kwargs):
        token = get_object_or_404(Token, sid=kwargs['sid'])
        if not request.user.is_authenticated:
            return JsonResponse({'result': 'Error', 'error': 'User is not authenticated'})
        token.user = request.user
        token.save()
        # Content asked by ressources.ciip.ch devs.
        return HttpResponse('<script>window.close()</script>')


class GetTokenView(View):
    def get(self, request, *args, **kwargs):
        token = get_object_or_404(Token, sid=kwargs['sid'])
        timeout = time.time() + 60  # 1 minute from now
        while True:
            token.refresh_from_db()
            if token.user:
                jwt = token.make_jwt()
                return JsonResponse({'result': 'OK', 'jwt': jwt})
            elif time.time() > timeout:
                break
            else:
                time.sleep(2)
        return JsonResponse({'result': 'Error', 'error': 'Request timed out'})


class GetTokenWithKeysView(View):
    """Getting Token based on a signed vector."""
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body.decode('utf-8'))
        except json.JSONDecodeError:
            # Fallback on form-data
            data = request.POST
        User = get_user_model()
        user = get_object_or_404(User, username=data.get("user"))
        # Try to get public key of user
        pub_key_path = Path(settings.MEDIA_ROOT, 'pub_keys', f'{user.username}.pem')
        if not pub_key_path.exists():
            return JsonResponse({'error': "No public key found for user."})
        # Check signed value with the user public key
        with pub_key_path.open('r') as f:
            pub_key = crypto.load_publickey(crypto.FILETYPE_PEM, f.read())
            x509 = crypto.X509()
            x509.set_pubkey(pub_key)
        vector = data.get("vector")
        signature = data.get("signature")
        try:
            crypto.verify(x509, base64.b64decode(signature), vector, 'sha1')
        except Exception:
            return JsonResponse({'error': "Signature not valid."})
        # Create token for user
        token = Token.get_token()
        token.user = user
        token.save()
        return JsonResponse({
            'token': token.sid,
            'expire': (token.issued_at + token.valid_duration).isoformat(timespec='seconds')}
        )
