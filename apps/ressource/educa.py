"""
This file contains functions to handle LOM-CH/Archibald data structures.

LOM-CH: http://biblio.educa.ch/sites/default/files/20140708/lom-chv1.1_fr.pdf
"""

import json
import re
import uuid
from copy import deepcopy
from datetime import date
from functools import reduce
from operator import getitem

from datetime import datetime

from django.conf import settings
from django.db import transaction
from django.utils.html import strip_tags
from django.utils import timezone

from api.views.base import BaseView
from pper.models import Cellule, Contenu, ContenuCellule, Discipline, Domain, Specification
from .models import ResourceContrib, ResourcePerLink, ResourceRelation
from .ontologies import EDUCATION_LEVELS, LEVEL_CHOICES, LIFECYCLE_ROLE_CHOICES, META_ROLE_CHOICES

auth_token = None
auth_token_expire = None

# When adding a new mapping line, don't forget to update also "as_struct()"
FIELD_MAPPING_V11 = {  # LOM-CHv1.1
    # '*'=list, '**'=dict
    'title': ['general', 'title'],
    'description': ['general', 'description'],
    'url': ['general', 'identifier', '*', 'entry'],
    'keywords': ['general', 'keyword', '*'],
    'languages': ['general', 'language', '*'],
    'granularity': ['general', 'aggregationLevel', 'value'],
    'version': ['lifeCycle', 'version'],
    'cost': ['rights', 'cost', 'value'],
    'conditions': ['rights', 'description'],
    'educ_level': ['education', 'context', '*', 'value'],
    'level': ['classification', '*', 'taxonPath', '*', 'taxon', '*', 'id'],
    'target_gr': ['education', 'intendedEndUserRole', '*', 'value'],
    'type_ped': ['education', 'learningResourceType', 'pedagogical', '*', 'value'],
    'type_doc': ['education', 'learningResourceType', 'documentary', '*', 'value'],
    'thumb_url': ['technical', 'previewImage', 'image'],
    'size': ['technical', 'size'],
    'duration': ['technical', 'duration'],
    'tec_req': ['technical', 'otherPlatformRequirements'],
    'contribs1': ['metaMetadata', 'contribute', '*', 'CONTRIBUTOR'],
    'contribs2': ['lifeCycle', 'contribute', '*', 'CONTRIBUTOR'],
}

FIELD_MAPPING_V12 = {  # LOM-CHv1.2
    # '*'=list, '**'=dict
    'title': ['general', 'title'],
    'description': ['general', 'description', '*'],
    'url': ['general', 'identifier', '*', 'entry'],
    'keywords': ['general', 'keyword', '*'],
    'languages': ['general', 'language', '*'],
    'granularity': ['general', 'aggregationLevel', 'value'],
    'version': ['lifeCycle', 'version'],
    'cost': ['rights', 'cost', 'value'],
    'conditions': ['rights', 'copyright', '*', 'description'],
    'ages': ['education', '*', 'typicalAgeRange', '*', '0'],
    'educ_level': ['education', '*', 'context', '*', 'value'],
    'level': ['classification', '*', 'taxonPath', '*', 'taxon', '*', 'id'],
    'target_gr': ['education', '*', 'intendedEndUserRole', '*', 'value'],
    'type_ped': ['education', '*', 'learningResourceType', 'pedagogical', '*', 'value'],
    'type_doc': ['education', '*', 'learningResourceType', 'documentary', '*', 'value'],
    'description_ped': ['education', '*', 'description', '*'],
    'thumb_url': ['technical', 'previewImage', 'image'],
    'size': ['technical', 'size'],
    'format': ['technical', 'format'],
    'duration': ['technical', 'duration'],
    'tec_req': ['technical', 'otherPlatformRequirements'],
    'contribs1': ['metaMetadata', 'contribute', '*', 'CONTRIBUTOR'],
    'contribs2': ['lifeCycle', 'contribute', '*', 'CONTRIBUTOR'],
}

FIELD_MAPPING_V13 = deepcopy(FIELD_MAPPING_V12)  # LOM-CHv1.3
FIELD_MAPPING_V13['ages'] = ['education', '*', 'typicalAgeRange', '*']


class BSNImporter:
    @classmethod
    def populate_from_bsn(cls, resource, source_uuid, data=None, save=True):
        # Fields that can be list from BSN but one-valued in BD-PER
        flat_fields = ['description', 'description_ped']
        # We'll use add() instead of set() later as there are 2 sources for contribs
        if resource.pk:
            resource.contribs.clear()
        if data is None:
            raise Exception("data is a mandatory argument")
        elif not source_uuid:
            resource.uuid = uuid.uuid4()
        else:
            resource.uuid = source_uuid

        try:
            lom_version = data['metaMetadata']['metaDataSchema'][0]
        except KeyError:
            try:
                lom_version = data['metaMetadata']['metadataSchema'][0]
            except KeyError:
                lom_version = None
        if lom_version == 'LOM-CHv1.1':
            field_mapping = FIELD_MAPPING_V11
        elif lom_version == 'LOM-CHv1.2':
            field_mapping = FIELD_MAPPING_V12
        else:
            field_mapping = FIELD_MAPPING_V13

        def get_key(data, keys):
            if keys and data is not None:
                key = keys.pop()
            else:
                # End of recursion
                if isinstance(data, dict) and ('fr' in data or 'en' in data):
                    return data.get('fr', data.get('en', data.get('de', '')))
                else:
                    return data
            if key == '*':
                if data is None:
                    return data
                vals = []
                for subelement in data:
                    subvalue = get_key(subelement, list(keys))
                    if subvalue is None:
                        continue
                    if isinstance(subvalue, list):
                        vals.extend(subvalue)
                    else:
                        vals.append(subvalue)
                return vals
            elif key == '**':
                return ','.join([get_key(subd, list(keys)) for subd in data.values()])
            elif key == 'CONTRIBUTOR':
                date = get_key(data, ['date'])
                vcard = get_key(data, ['entity'])
                if not vcard:
                    return None
                # vcard is a list, but until now, only the first element makes sense
                return ResourceContrib(
                    vcard=vcard[0], date=date, role=get_key(data, ['value', 'role'])
                )
            else:
                if data == []:
                    # Avoid a bug when an empty key which is normally a dict is empty list instead.
                    data = {}
                return get_key(data.get(key), keys)

        contributors = []
        for field, path in field_mapping.items():
            if field.startswith('contribs'):
                field = 'contribs'
            try:
                value = reduce(getitem, path, data)
            except (KeyError, TypeError):
                value = get_key(data, list(reversed(path)))
            else:
                if isinstance(value, dict) and ('fr' in value or 'en' in value):
                    value = value.get('fr', value.get('en'))
            if isinstance(value, str):
                value = value.strip()
            if value not in (None, [], {}, ''):
                if field in flat_fields and isinstance(value, list):
                    value = value[0]
                elif field == 'granularity' and value in {'2', '3'}:
                    value += 'a'  # 2a or 3a as 2/3 do not exist in our DB.
                elif field == 'conditions':
                    # is an ontology, but not transmitted as an ontology
                    rev_dict = dict([(c[1], c[0]) for c in resource._meta.get_field(field).choices])
                    if isinstance(value, list):
                        value = ", ".join(value)
                    value = strip_tags(value)  # Sometimes enclosed inside <a href></a>
                    # We cannot accept values not in choices => ''
                    value = rev_dict.get(value, rev_dict.get(value.replace("'", "’"), ''))
                elif field == 'level':
                    # Ignore level values not in LEVEL_CHOICES
                    value = [val for val in value if val in dict(LEVEL_CHOICES).keys()]
                elif field == 'duration':
                    value = duration_from_iso8601(value)
                elif field == 'format':
                    format_choices_reverse = dict([
                        (choice[1], choice[0]) for group in [
                            c for c in resource._meta.get_field('format').choices if isinstance(c[1], (tuple, list))
                        ] for choice in group[1]
                    ])
                    value = format_choices_reverse.get(value[0], value[0])
                elif field == 'keywords':
                    value = ", ".join(value)
                elif field in ['url', 'ages']:
                    # Take the first identifier URL or age range
                    value = value[0]
                elif resource._meta.get_field(field).get_internal_type() == 'BooleanField':
                    value = {'no': False, 'yes': True}.get(value)

                if resource._meta.get_field(field).get_internal_type() == 'ManyToManyField':
                    if field.startswith('contribs'):
                        contributors.extend(value)
                    else:
                        getattr(resource, field).set(value)
                else:
                    setattr(resource, field, value)

        relations = []
        for relation in data.get('relation', []):
            try:
                kind = relation['kind']['value']
                description = relation['resource'][0]['description']['fr']
                ident = [res for res in relation['resource'] if res['identifier']['catalog'] == 'URL']
                if not ident:
                    continue
                url = relation['resource'][0]['identifier']['entry']
            except (KeyError, IndexError):
                continue
            relations.append(
                ResourceRelation(resource=resource, rtype=kind, url=url, description=description)
            )

        per_links = cls.collect_per_links(data, resource)

        updated = None
        if data.get("updateDate"):
            updated = date.fromtimestamp(int(data["updateDate"]))
            updated = timezone.make_aware(datetime(updated.year, updated.month, updated.day, 12, 0, 0))

        if save:
            with transaction.atomic():
                resource.save(updated=updated, index=False)
                resource.contribs.all().delete()
                if contributors:
                    ResourceContrib.objects.bulk_create(contributors)
                    resource.contribs.add(*contributors)
                resource.relations.all().delete()
                if relations:
                    ResourceRelation.objects.bulk_create(relations)
                resource.resourceperlink_set.all().delete()
                if per_links:
                    ResourcePerLink.objects.bulk_create(per_links)
            # Wait related object created before indexing
            resource.index()
        else:
            # Return a dict of M2M field values, possibly for prefilling a formset
            return {
                'contribs': contributors,
                'resourcerelation_set': relations,
                'resourceperlink_set': per_links,
            }

    def collect_per_links(struct, resource):
        per_links = []
        seen_disciplines = set()
        for per_data in struct.get('curriculum', []):
            if per_data.get('source', {}).get('name') != 'per':
                continue  # Ignore non-PER data
            if 'entity' in 'per_data':
                per_data_decoded = json.loads(per_data['entity'])
                for cycle, disc_data in per_data_decoded.items():
                    for disc, obj_data in disc_data.items():
                        for obj_k, obj_v in obj_data.items():
                            if obj_v['object_elements']:  # Link to ContenuCellule
                                for elem in obj_v['object_elements']:
                                    for detail in elem['details']:
                                        cellule_id = int(detail['ident'].split('#')[1])
                                        try:
                                            cc = Cellule.objects.get(pk=cellule_id).contenucellule_set.first()
                                        except Cellule.DoesNotExist:
                                            continue
                                        per_links.append(
                                            ResourcePerLink(resource=resource, content_object=cc)
                                        )
                            else:  # Link to spec
                                try:
                                    spec = Specification.objects.get(code=obj_v['code'])
                                except Specification.DoesNotExist:
                                    continue
                                per_links.append(
                                    ResourcePerLink(resource=resource, content_object=spec)
                                )
            elif 'taxonTree' in per_data:
                def link_objectif(obj_data):
                    obj_title = obj_data['entry']['fr']
                    code = re.fullmatch(r'[^\(]*\(([^\)]*)\)', obj_title)
                    if not code:
                        code = re.match(r'[^\(]*\(([^\)]*)\)', obj_title)
                    if not code:
                        print(f"Unable to extract objectif code from '{obj_title}'")
                        return False
                    code = code.groups()[0]
                    try:
                        spec = Specification.objects.get(code=code.replace('&amp;', '&'), status='published')
                    except Specification.DoesNotExist:
                        return False
                    per_links.append(
                        ResourcePerLink(resource=resource, content_object=spec)
                    )
                    seen_disciplines.add(per_links[-1].get_discipline())
                    return True

                def import_per_link(taxon):
                    if not taxon['childTaxons']:
                        # leaf node
                        if taxon['type'] == 'domaines':
                            obj = Domain.objects.get(name=taxon['entry']['fr'])
                        elif taxon['type'] == 'disciplines':
                            obj = Discipline.objects.get(name=taxon['entry']['fr'])
                            seen_disciplines.add(obj)
                        elif taxon['type'] == 'objectifs':
                            link_objectif(taxon)
                            return
                        elif taxon['type'] == 'progressions':
                            contenu = Contenu.objects.get(pk=taxon['id'])
                            obj = contenu.contenucellule_set.first()
                        else:
                            print(f"[{resource.uuid}] Unknown taxon type «{taxon['type']}»")
                            return
                        per_links.append(
                            ResourcePerLink(resource=resource, content_object=obj)
                        )
                    else:
                        for ctaxon in taxon['childTaxons']:
                            import_per_link(ctaxon)

                for cycle_data in per_data['taxonTree']:
                    import_per_link(cycle_data)

        for section in struct.get('classification', []):
            if section["purpose"]["value"] == 'discipline':
                for disc_section in section["taxonPath"]:
                    for taxon_item in disc_section["taxon"]:
                        disc_slug = taxon_item["id"]
                        try:
                            discipline = Discipline.objects.get(lom_id=disc_slug)
                        except Discipline.DoesNotExist:
                            continue
                        if discipline in seen_disciplines:
                            continue
                        per_links.append(
                            ResourcePerLink(resource=resource, content_object=discipline)
                        )
                        seen_disciplines.add(discipline)
        return per_links


def as_struct(resource):
    """Convert a resource instance into a DSB-compliant structure"""
    type_docs = [{
            "source": "LREv3.0",
            "value": td,
        } for td in (resource.type_doc or [])
    ]

    description = strip_tags(resource.description) if '<' in resource.description else resource.description
    # Base with mandatory fields
    struct = {
        'general': {  # 1.
            'identifier': [  # 1.1
                {
                    "catalog": "URL",
                    "entry": resource.get_url(),
                    #"title":{
                    #     "fr":"Identifier title"
                    #}
                },
            ],
            'title': {  # 1.2
                'fr': resource.title,
            },
            'language': resource.languages or ["fr"],  #1.3
            'description': [{  # 1.4
                'fr': description,
            }],
        },
        "lifeCycle": {},  #2.
        "metaMetadata": {  # 3.
            "identifier": [{  # 3.1
                "catalog": 'BD-PER',
                "entry": str(resource.pk),
            }],
            # First contribute element is CIIP with its logo (displayed as provider logo)
            "contribute": [{  # 3.2
                'date': datetime.now(timezone.get_current_timezone()).replace(microsecond=0).isoformat(),
                'entity': ['BEGIN:VCARD\r\nVERSION:3.0\r\nFN:CIIP\r\nN:;;;;\r\nLOGO;VALUE:uri:https://bdper.plandetudes.ch/static/img/logo-ciip-16.png\r\nORG:CIIP;;\r\nEND:VCARD'],
                'role': {
                    'source': 'LOM-CHv1.1',
                    'value': 'creator',
                }
            }],
            "metadataSchema": ["LOM-CHv1.3"],  # 3.3
            "language": "fr",  # 3.4
        },
        "technical": {},  # 4.
        "education": [{  # 5.
            "learningResourceType": {  # 5.2
                "documentary": type_docs,
            },
            "context": [],  # 5.6
        }],
        "rights": {  # 6.
            "cost": {  # 6.1
                "source": "LOMv1.0",
                "value": "yes" if resource.cost else "no",
            },
            "copyright": [{
                "description": {  # 6.3
                    "fr": resource.get_conditions_display(),
                },
            }],
        },
        'relation': [],  # 7.
        'classification': [],  # 9.
        'curriculum': [],  # 10.
        'updateDate': int(resource.updated.timestamp()),
    }
    if resource.uuid:
        struct["lomId"] = resource.uuid
    if resource.ages:
        struct["education"][0]["typicalAgeRange"] = [{'0': resource.ages}]

    per_links = resource.resourceperlink_set.all().select_related('content_type')
    if per_links:
        struct["curriculum"].append({
            "source": {"name": "per", "hierarchy": []},  # 10.1
            "taxonTree": [],  # 10.2
        })
    # Build tree
    link_tree = {}
    for link in per_links:
        cycle = link.get_cycle()
        if cycle not in link_tree:
            link_tree[cycle] = {}
        domain = link.get_domain()
        if domain not in link_tree[cycle]:
            link_tree[cycle][domain] = {}
        discipline = link.get_discipline()
        if discipline and discipline not in link_tree[cycle][domain]:
            link_tree[cycle][domain][discipline] = {}
        if discipline:
            spec = link.get_spec()
            if spec and spec not in link_tree[cycle][domain][discipline]:
                link_tree[cycle][domain][discipline][spec] = []
            if isinstance(link.content_object, ContenuCellule):
                link_tree[cycle][domain][discipline][spec].append(link)
    # Render tree
    content_filters = BaseView.get_content_filters()
    for cycle, domains in link_tree.items():
        cycle_struct = {
            "id": f"{cycle}",
            "type": "cycles",
            "purpose": {"source": "LOM-CHv1.2", "value": "educational level"},
            "entry": {"fr": f"Cycle {cycle}"},
            "childTaxons": [],
        }
        for domain, discs in domains.items():
            cycle_struct["childTaxons"].append({
                "id": str(domain.pk),
                "type": "domaines",
                "purpose": {"source": "LOM-CHv1.2", "value": "discipline"},
                "entry": {"fr": domain.name},
                "childTaxons": [],
            })
            for disc, specs in discs.items():
                cycle_struct["childTaxons"][-1]["childTaxons"].append({
                    "id": str(disc.pk),
                    "type": "disciplines",
                    "purpose": {"source": "LOM-CHv1.2", "value": "discipline"},
                    "entry": {"fr": disc.name},
                    "childTaxons": [],
                })
                for spec, progrs in specs.items():
                    cycle_struct["childTaxons"][-1]["childTaxons"][-1]["childTaxons"].append({
                        "id": str(spec.pk),
                        "type": "objectifs",
                        "purpose": {"source": "LOM-CHv1.2", "value": "objective"},
                        "entry": {"fr": f"{spec.objectif.title} ({spec.code})"},
                        "childTaxons": [],
                    })
                    for progr in progrs:
                        cycle_struct["childTaxons"][-1]["childTaxons"][-1]["childTaxons"][-1]["childTaxons"].append({
                            "id": str(progr.pk),
                            "type": "progressions",
                            "purpose": {"source": "LOM-CHv1.2", "value": "objective"},
                            "entry": {
                                "fr": progr.content_object.contenu.render(content_filters, {})
                            },
                            "childTaxons": [],
                        })
        struct["curriculum"][-1]["taxonTree"].append(cycle_struct)

    if resource.keywords:
        struct["general"]["keyword"] = [{  # 1.5
            'fr': kw.strip(),
        } for kw in resource.keywords.split(',')]

    if resource.granularity:
        struct["general"]["aggregationLevel"] = {  # 1.8
            'source': 'LOMv1.0',
            'value': resource.granularity[0],  # PER may have a or b suffix not LOM-compatible
        }

    if resource.version:
        struct["lifeCycle"]["version"] = {'en': resource.version}  # 2.1

    if resource.contribs.exists():
        for contrib in resource.contribs.all():
            if contrib.role in dict(META_ROLE_CHOICES):
                struct["metaMetadata"]["contribute"].append({  # 3.2
                    'date': contrib.date.isoformat(), # '2013-10-11T13:06:15+00:00'
                    'entity': [contrib.vcard],
                    'role': {
                        'source': 'LOM-CHv1.1',
                        'value': contrib.role,
                    },
                })
            elif contrib.role in dict(LIFECYCLE_ROLE_CHOICES):
                struct["lifeCycle"].setdefault("contribute", []).append({  # 2.3
                    'entity': [contrib.vcard],
                    'role': {
                        'source': 'LOM-CHv1.1',
                        'value': contrib.role,
                    },
                })

    for relation in resource.relations.all():
        struct["relation"].append({
            'kind': {
                'source': 'LOMv1.0',
                'value': relation.rtype,
            },
            'resource': [{
                'description': {'fr': relation.description},
                'identifier': {
                    'catalog': 'REL_URL',
                    'entry': relation.get_url(),
                }
            }]
        })

    if resource.duration:
        struct["technical"]["duration"] = duration_to_iso8601(resource.duration)

    if resource.size is not None:
        struct["technical"]["size"] = resource.size

    if resource.tec_req:
        struct["technical"]["otherPlatformRequirements"] = {"fr": resource.tec_req}  # 4.6

    if resource.format:
        struct["technical"]["format"] = [resource.get_format_display()]

    if resource.thumb:
        struct["technical"]["previewImage"] = {  # 4.8
            "image": 'https://%s%s' % (settings.SITE_DOMAIN, resource.thumb.url)
        }
    elif resource.thumb_url:
        struct["technical"]["previewImage"] = {"image": resource.thumb_url}

    if resource.type_ped:
        struct["education"][0]["learningResourceType"]["pedagogical"] = [{  # 5.2.2
            'source': 'LOM-CHv1.1',
            'value': tp,
        } for tp in resource.type_ped]

    if resource.target_gr:
        struct["education"][0]["intendedEndUserRole"] = [{  # 5.5
            'source': 'LOMv1.0',
            'value': tg,
        } for tg in resource.target_gr]

    sources = dict((lev[0], lev[2]) for lev in EDUCATION_LEVELS)
    for lev in resource.educ_level:
        struct["education"][0]["context"].append({  # 5.6
            'source': sources[lev],
            'value': lev,
        })

    if resource.description_ped:
        struct["education"][0]["description"] = [{'fr': resource.description_ped}]

    if resource.level:
        struct["classification"].append({
            'purpose': {  # 9.1
                'source': 'LOMv1.0',
                'value': 'educational level',
            },
            'taxonPath': [],
        })
        for cycle, level in levels_for_bsn(resource.level):
            struct["classification"][0]['taxonPath'].append({
                'source': {"en": "classification system"},
                'taxon': [
                    {"id": "compulsory education", "entry": {"fr": "Scolarité obligatoire"}},
                    {"id": cycle},
                ],
            })
            if level:
                struct["classification"][0]['taxonPath'][-1]['taxon'].append({"id": level})
    disciplines = ResourcePerLink.disciplines_from_per_links(per_links)
    if disciplines:
        struct["classification"].append({
            'purpose': {  # 9.1
                'source': 'LOMv1.0',
                'value': 'discipline',
            },
            'taxonPath': [],
        })
        for disc in disciplines:
            struct["classification"][-1]['taxonPath'].append({
                'source': {'en': 'classification system'},
                'taxon': [
                    {'id': disc.domain.lom_id, 'entry': {'fr': disc.domain.name}},  # domaine
                    {'id': disc.lom_id, 'entry': {'fr': disc.name}},  # discipline
                ],
            })

    return struct


def levels_for_bsn(orig_levels):
    levels = []
    for level in orig_levels:
        # Transform levels not accepted by the BSN
        if level in ('1st year', '2nd year') and '1st and 2nd year' not in orig_levels:
            levels.append('1st and 2nd year')
        elif level in ('3rd year', '4th year') and '3rd and 4th year' not in orig_levels:
            levels.append('3rd and 4th year')
        elif level in ('5th year', '6th year') and '5th and 6th year' not in orig_levels:
            levels.append('5th and 6th year')
        elif level in ('7th year', '8th year') and '7th and 8th year' not in orig_levels:
            levels.append('7th and 8th year')
        elif level not in levels:
            levels.append(level)
    final_levels = []
    cycle_levels = set()
    for level in levels:
        # BSN requires 'intermediate' cycle levels
        if level.startswith(('1st', '3rd')):
            final_levels.append(('cycle 1', level))
        elif level.startswith(('5th', '7th')):
            final_levels.append(('cycle 2', level))
        elif level.startswith(('9th', '10th', '11th')):
            final_levels.append(('cycle 3', level))
        else:
            cycle_levels.add(level)
    for cycle in cycle_levels:
        if cycle not in [lev[0] for lev in final_levels]:
            final_levels.append((cycle, None))
    return final_levels


def link_as_struct(link):
    # link is a ResourcePERIndex instance
    return {  # 10.2
        'Cycle %d' % link.specification.objectif.cycle: {
            link.discipline.domain.name: {
                link.specification.code: {
                    "discipline": link.discipline.name,
                    "object": link.specification.objectif.get_thematique_name(),
                    "code": link.specification.code,
                    "url_part": link.specification.code_slug(),
                    "description": link.specification.objectif.title,
                    "object_elements": [],
                }
            }
        }
    }


def duration_to_iso8601(duration):
    m = re.match(r'((?P<min>[\d]+)\s?min)?(?P<sec>[\d]+)?.*', duration)
    if m:
        value = 'PT'
        if m.group('min'):
            value += '%dM' % int(m.group('min'))
        if m.group('sec'):
            value += '%dS' % int(m.group('sec'))
        return value
    return ''


ISO8601_PERIOD_REGEX = re.compile(
    r"^(?P<sign>[+-])?"
    r"P(?!\b)"
    r"(?P<years>[0-9]+([,.][0-9]+)?Y)?"
    r"(?P<months>[0-9]+([,.][0-9]+)?M)?"
    r"(?P<weeks>[0-9]+([,.][0-9]+)?W)?"
    r"(?P<days>[0-9]+([,.][0-9]+)?D)?"
    r"((?P<separator>T)(?P<hours>[0-9]+([,.][0-9]+)?H)?"
    r"(?P<minutes>[0-9]+([,.][0-9]+)?M)?"
    r"(?P<seconds>[0-9]+([,.][0-9]+)?S)?)?$")

def duration_from_iso8601(isostring):
    """Simplified implementation taken from https://github.com/gweis/isodate"""
    match = ISO8601_PERIOD_REGEX.match(isostring)
    if not match:
        return ''
    groups = match.groupdict()
    duration = ''
    for unit, unit_str in (
            ('days', 'j'), ('hours', 'h'), ('minutes', 'min'), ('seconds', 's')
        ):
        if groups[unit]:
            if unit == 'seconds' and duration:
                unit_str = ''
            number = "".join([c for c in groups[unit] if c in '0123456789'])
            duration += '%s%s' % (number, unit_str)
    return duration
