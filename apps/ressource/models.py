from collections import OrderedDict
from datetime import datetime
from itertools import chain
from urllib.parse import parse_qs, quote, urlsplit
import locale
import os
import re

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import storages
from django.core.mail import mail_admins
from django.db import connection, models
from django.db.models.fields.files import FieldFile
from django.templatetags.static import static as _static
from django.urls import NoReverseMatch, reverse
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from linkcheck.models import Link

from pper.fields import ChoiceArrayField
from pper.models import Cellule, ContenuCellule, Discipline, Domain, Specification, Tableau
from shortener.models import ShortLink

from .ontologies import (
    CONDITIONS_CHOICES, DIFFICULTY_CHOICES, EDUCATION_LEVEL_CHOICES,
    EMEDIA_CATEG_CHOICES, FORMAT_CHOICES, GENRE_CHOICES, GRANLEVEL_CHOICES,
    LANGUAGE_CHOICES, LEARN_DURATION_CHOICES, LEVEL_CHOICES,
    MITIC_CHOICES, RELATION_TYPES, META_ROLE_CHOICES, LIFECYCLE_ROLE_CHOICES,
    TARGETGROUP_CHOICES, TYPE_PED_CHOICES, TYPE_DOC, TYPE_DOC_CHOICES, TYPE_RESOURCE,
)
from .utils import (
    auto_exposant, generate_thumbnail_for_img, generate_thumbnail_for_pdf,
    get_infomaniak_token, get_infomaniak_videopath
)

PARTNER_MAP = {
    'edu-vd': 'VD',
}


def ow_storage():
    return storages["upload"]


def s3_storage():
    try:
        return storages["upload-s3"]
    except KeyError:
        return storages["upload"]


def static(*args, **kwargs):
    # static wrapper to swallow exceptions
    # depending on the storage backend, it can generate exceptions,
    # typically ValueError: Missing staticfiles manifest entry for 'path/image.jpg'
    try:
        return _static(*args, **kwargs)
    except Exception:
        return settings.STATIC_URL + "unknown.jpg"


class ResourceContrib(models.Model):
    vcard = models.TextField()
    date = models.DateTimeField(null=True, blank=True)
    role = models.CharField(max_length=20, choices=META_ROLE_CHOICES + LIFECYCLE_ROLE_CHOICES)

    class Meta:
        db_table = 't_resource_contrib'
        verbose_name = 'Contributeur de ressource'
        verbose_name_plural = 'Contributeurs de ressource'

    def __str__(self):
        fields = self._parse_vcard()
        name = fields.get('FN', fields.get('N', fields.get('ORG', '??')))
        return "%s (%s)" % (name, self.get_role_display())

    def _parse_vcard(self):
        """Parse vcard content and returns an ordered dict"""
        disp_fields = ('LOGO', 'FN', 'N', 'EMAIL', 'ORG', 'ADR', 'URL')
        disp_values = OrderedDict()
        fn_present = False
        for line in self.vcard.split('\r\n'):
            m = re.match('([^:]*):(.*)', line)
            if not m:
                continue
            prefix, value = m.groups()
            if prefix.startswith(disp_fields):
                field = prefix.split(':')[0] if ':' in prefix else (prefix.split(';')[0] if ';' in prefix else prefix)
                value = value.replace(';', ' ').strip()
                if value == '':
                    continue
                if prefix.startswith('FN'):
                    fn_present = True
                if fn_present and (prefix == 'N' or prefix.startswith('N;')):
                    continue  # FN has priority over N
                if prefix.startswith('LOGO'):
                    if not 'http' in value:
                        continue
                    url = value.split('uri:')[-1]
                    value = '<img src="%s"/>' % url
                elif prefix.startswith('ORG') and value.startswith('default'):
                    continue
                disp_values[field] = value.replace(';', ' ').strip()
        return disp_values

    def pprint(self, fmt='all'):
        """
        Parse and pretty print vcard content.
        (vobject lib might be used at some time for more advanced use)
        """
        disp_values = self._parse_vcard()
        if fmt == 'res_html':
            # John Doe (NASA)
            name = disp_values.get('FN', disp_values.get('N'))
            if name:
                if 'ORG' in disp_values:
                    name += ' (%s)' % disp_values['ORG']
            else:
                name = disp_values.get('ORG', '??')
            return name
        else:
            return "\n".join(disp_values.values())


STATUS_CHOICES = (
    ('atraiter', 'À traiter'),
    ('essai', 'Essais/divers'),
    ('bsn_seul', 'Publication BSN uniquement'),
    ('encours', "En cours d'analyse"),
    ('validee', 'Validée'),
    ('refusee', 'Refusée'),
)

AFFECTATION_CHOICES = (
    ('A', 'e-media'),
    ('B', 'e-media + PPER/ESPER/BSN'),
    ('C', 'à définir'),
)

def default_educ_level():
    return ['compulsory education']


class ResourceManager(models.Manager):
    def galleries(self):
        return self.annotate(
            num_images=models.Count(
                'relations', filter=models.Q(relations__other__type_doc__contains=['image'])
            )
        ).filter(num_images__gt=0)


class Resource(models.Model):
    created = models.DateTimeField("Date de création de la notice", auto_now_add=True)
    updated = models.DateTimeField("Date de dernière modification")
    validated = models.DateTimeField("Date de dernière validation", null=True, blank=True)
    status = models.CharField("Statut de validation", max_length=20, choices=STATUS_CHOICES, default='atraiter', db_index=True)
    status_comment = models.TextField("Suivi du traitement", blank=True)
    search_hidden = models.BooleanField("Masquer dans la recherche publique", default=False)
    broken = models.BooleanField("Lien cassé", default=False)
    need_auth = models.BooleanField("Nécessite une authentification sur ce site", default=False)
    show_refs = models.BooleanField("Montrer les références à l'affichage", default=True)
    no_player = models.BooleanField("Ne pas afficher de lecteur (audio/vidéo…)", default=False)
    type_r = models.CharField("Type de ressource", max_length=6, choices=TYPE_RESOURCE, default='med')
    cantons  = models.CharField(max_length=30, blank=True, default='',
        help_text="Liste de cantons séparés par des virgules (VD,FR), vide pour tous les cantons")
    # UUID for the resource
    uuid = models.UUIDField(null=True, blank=True)
    partner = models.CharField(max_length=10, default='CIIP')
    submitted_mat = models.TextField("Matériel soumis", blank=True)

    # LOM 1 - General informations
    # url can only be empty if the resource has files (enforced at form level)
    url = models.URLField("URL", max_length=255, blank=True)  # LOM 1.1 (simplified)

    title = models.TextField("Titre")  # LOM 1.2
    # E-media fields
    emedia = models.BooleanField("Ressource e-media", default=False)
    sdm = models.BooleanField("Semaine des médias", default=False)
    cinema = models.BooleanField("Fiche cinéma", default=False)
    affectation = models.CharField(
        "Affectation", max_length=1, choices=AFFECTATION_CHOICES, blank=True
    )
    emedia_categ = ChoiceArrayField(
        models.CharField(max_length=10, choices=EMEDIA_CATEG_CHOICES, blank=True),
        verbose_name="Catégorie e-media", blank=True, null=True,
    )
    # Temporary field to store the old id from the e-media platform
    num_emedia = models.SmallIntegerField(blank=True, null=True)

    languages = ChoiceArrayField(
        models.CharField(max_length=2, choices=LANGUAGE_CHOICES, blank=True),
        verbose_name="Langues de la ressource", blank=True, null=True,
    )  # LOM 1.3
    description = models.TextField("Description générale", blank=True,
        help_text="Résume brièvement le contenu de la ressource (entre 2 et 6 phrases)")  # LOM 1.4
    descr_eleve = models.TextField(
        "Description élève", blank=True, help_text="Description de la ressource à destination des élèves"
    )
    keywords = models.TextField("Mots clé", blank=True)  # LOM 1.5
    granularity = models.CharField("Niveau de granularité", max_length=10, choices=GRANLEVEL_CHOICES, blank=True)  # LOM 1.8
    # Various metadata:
    genre = models.CharField("Genre", max_length=10, choices=GENRE_CHOICES, blank=True)
    source = models.TextField("Source", blank=True)
    # Temporary invisible, maybe delete at some point:
    producer = models.TextField("Réalisation", blank=True)

    # LOM 2 - Lifecycle
    version = models.CharField("Version", max_length=50, blank=True)  # LOM 2.1
    # Used for both LOM 2.3 (for resource) and LOM 3.2 (for notice)
    contribs = models.ManyToManyField(ResourceContrib, blank=True, verbose_name="Contributeurs")  # LOM 3.2

    # LOM 3 - Meta-metadata

    # LOM 4 - Technical informations
    format = models.CharField("Format", max_length=30, choices=FORMAT_CHOICES, blank=True)  # LOM 4.1
    num_pages = models.IntegerField("Nombre de pages", null=True, blank=True)
    size = models.BigIntegerField("Taille du fichier", null=True, blank=True)  # LOM 4.2
    phys_loc = models.TextField("Emplacement physique", blank=True)  # LOM 4.3
    tec_req = models.TextField("Exigences techniques", blank=True)  # LOM 4.4 (simplified)
    duration = models.CharField("Durée", max_length=30, blank=True)  # LOM 4.7
    # LOM 4.8 (simplified)
    thumb = models.ImageField(
        "Vignette", storage=s3_storage, upload_to='resource_thumbs', max_length=150, blank=True
    )
    thumb_url = models.URLField("URL de la vignette", max_length=255, blank=True)
    embed_code = models.TextField("Code d'intégration HTML", blank=True)

    # LOM 5 - Pedagogical informations
    type_doc = ChoiceArrayField(
        models.CharField(max_length=20, choices=TYPE_DOC_CHOICES, blank=True),
        verbose_name="Type documentaire", blank=True, null=True,  # LOM 5.2.1
    )
    type_ped = ChoiceArrayField(
        models.CharField(max_length=40, choices=TYPE_PED_CHOICES, blank=True),
        verbose_name="Type pédagogique", blank=True, null=True,  # LOM 5.2.2
    )
    target_gr = ChoiceArrayField(
        models.CharField(max_length=20, choices=TARGETGROUP_CHOICES, blank=True),
        verbose_name="Utilisateur final", blank=True, null=True,  # LOM 5.5
    )
    educ_level = ChoiceArrayField(
        models.CharField(max_length=40, choices=EDUCATION_LEVEL_CHOICES, blank=True),
        verbose_name="Degré de formation", blank=True, null=True, default=default_educ_level,  # LOM 5.6
    )
    ages = models.CharField("Âge", max_length=5, blank=True)  # LOM 5.7
    difficulty = models.CharField("Difficulté", max_length=30, choices=DIFFICULTY_CHOICES,
        blank=True)  # LOM 5.8
    learn_duration = models.CharField("Durée d'apprentissage", max_length=30,
        choices=LEARN_DURATION_CHOICES, blank=True,
        help_text="Estimation du temps moyen d'apprentissage ou de la durée moyenne "
                  "d'utilisation de la ressource pour l'utilisateur final")  # LOM 5.9
    description_ped = models.TextField("Description pédagogique", blank=True)  # LOM 5.10
    level = ChoiceArrayField(
        models.CharField(max_length=20, choices=LEVEL_CHOICES, blank=True),
        verbose_name="Niveau scolaire", blank=True, null=True,
    )
    mitic_asp = ChoiceArrayField(
        models.CharField(max_length=20, choices=MITIC_CHOICES, blank=True),
        verbose_name="Aspects MITIC", blank=True, null=True,
    )  # non-LOM

    # LOM 6 - Copyright
    cost = models.BooleanField("Coûts", default=False)  # LOM 6.1
    conditions = models.CharField("Conditions d'utilisation", max_length=400, choices=CONDITIONS_CHOICES, blank=True)  # LOM 6.3

    # Quality
    qual_decisive = models.TextField("Élément décisif", blank=True, help_text=(
        "Un élément décisif a-t-il conduit au développement de la ressource pédagogique ?\n"
        "Si oui, lequel ?"))
    qual_collab = models.TextField("Collaboration intercantonale", blank=True, help_text=(
        "La ressource résulte-t-elle d'une collaboration intercantonale ? Si oui, sous quelle forme ?"))
    qual_counsels = models.TextField("Conseils spécialisés", blank=True, help_text=(
        "Lors du développement de la ressource, avez-vous bénéficié de conseils spécialisés "
        "de professionnels et/ou de spécialistes ?\n"
        "Si oui, par qui et de quelle nature (scientifique, pédagogique, etc.) ?"))
    qual_tested = models.TextField("Tests pratiques", blank=True, help_text=(
        "La ressource a-t-elle été testée dans la pratique ?\n"
        "Si oui, veuillez préciser (années scolaire / nombre et type de classes / cantons)."))
    qual_eval = models.TextField("Procédure d'approbation", blank=True, help_text=(
        "La ressource a-t-elle ou sera-t-elle encore soumise à une procédure d'approbation ? "
        "Si oui, laquelle (par ex. commissions cantonales de moyens d'enseignement) ?"))
    qual_guarant = models.TextField("Mesures de garanties", blank=True, help_text=(
        "D'autres mesures de garanties ont-elles été appliquées ? Si oui, lesquelles ?"))

    objects = ResourceManager()

    # tsvector type column dedicated to full text search. It is updated automatically through a trigger
    # (also added by the migration).
    # The `fts` column indexes `title`, `description`, `descr_eleve` `keywords`, `genre`,
    # `source`, `type_ped` (see the index() method).
    fts = SearchVectorField(blank=True)

    # django-linkcheck Link model
    checked_links = GenericRelation(Link)
    # shortener app
    short_links = GenericRelation(ShortLink)

    METADATA_FIELD_NAMES = [
        'type_doc', 'duration', 'genre', 'producer', 'format', 'source', 'per_links',
        'target_gr', 'keywords', 'version', 'languages', 'phys_loc', 'tec_req',
        'type_ped', 'educ_level', 'level', 'granularity', 'cost', 'conditions',
    ]
    METADATA_FIELD_HTML = ('source',)
    # Limited subset of fields displayed on resource group lists (both HTML and PDF)
    METADATA_SUBSET_NAMES = [
        'type_doc', 'type_ped', 'duration', 'genre', 'producer', 'format', 'source',
        'per_links', 'target_gr', 'languages', 'keywords',
    ]
    MANDATORY_LOM_FIELDS = ('title', 'description', 'url', 'type_doc', 'educ_level', 'conditions')
    GROUPS = [
        ('Informations générales',
            ('title', 'description', 'descr_eleve', 'keywords', 'url', 'need_auth', 'broken', 'languages')),
        ('Informations spécifiques',
            ('source', 'producer', 'genre', 'submitted_mat', 'cantons')),
        ('E-media',
            ('emedia', 'sdm', 'cinema', 'emedia_categ', 'mitic_asp')),
        ('Développement qualité',
            ('qual_decisive', 'qual_collab', 'qual_counsels', 'qual_tested', 'qual_eval', 'qual_guarant')),
        ('Gestion et suivi',
            ('type_r', 'status', 'search_hidden', 'show_refs', 'no_player', 'granularity')),
        ('Cycle de vie', ('version', 'contribs')),
        ('Informations techniques',
            ('format', 'num_pages', 'size', 'phys_loc', 'tec_req', 'duration',
             'thumb', 'thumb_url', 'embed_code')),
        ('Informations pédagogiques',
            ('type_doc', 'type_ped', 'target_gr', 'educ_level', 'level', 'ages',
             'difficulty', 'learn_duration', 'description_ped')),
        ('Droits', ('cost', 'conditions')),
        ('Relations', ('relations',)),
        ("Plan d'études", ()),
    ]

    class Meta:
        db_table = 't_resource'
        verbose_name = 'Ressource'
        constraints = [
            models.UniqueConstraint(name='uuid_unique', fields=['uuid']),
        ]
        indexes = [
            models.Index(name='partner_index', fields=['partner']),
            GinIndex(name='resource_fts_index', fields=['fts']),
        ]

    def __str__(self):
        return ", ".join(filter(None, [self.title, self.get_url()]))

    @classmethod
    def verbose_field_name(cls, fname, plural=False):
        plural_map = {
            'type_doc': 'Types documentaires',
            'type_ped': 'Types pédagogiques',
            'target_gr': 'Utilisateurs finaux',
        }
        if fname == 'per_links':
            return 'Liens PER'
        elif fname == 'codes_rn':
            return 'Codes RN'
        if plural and fname in plural_map:
            return plural_map[fname]
        return Resource._meta.get_field(fname).verbose_name

    def save(self, *args, updated=None, index=True, **kwargs):
        self.updated = updated or timezone.now()
        if self.status == 'validee' and not self.validated:
            self.validated = timezone.now()

        super().save(*args, **kwargs)

        self.create_qrcode()
        if index:
            self.index()

    def delete(self, **kwargs):
        if self.thumb:
            self.thumb.delete(save=False)
        for rfile in self.files.all():
            if rfile.thumb:
                rfile.thumb.delete()
        return super().delete(**kwargs)

    def create_qrcode(self):
        """Create a QR code pointing to the destination URL."""
        try:
            import qrcode
        except ImportError:
            pass
        else:
            img = qrcode.make(self.get_url())
            img.save(os.path.join(settings.MEDIA_ROOT, 'qrcodes', 'code-%d.png' % self.id))

    def is_apprenant(self):
        return self.descr_eleve.strip() != '' and 'learner' in self.target_gr

    def get_absolute_url(self, from_short_link=False):
        if from_short_link and self.is_apprenant():
            return reverse('resource-display-appr', args=[self.pk])
        return reverse('resource_display', args=[self.pk])

    def can_view(self, user):
        return self.status == 'validee' or user.has_perm('ressource.change_resource')

    def can_view_file(self, user):
        return self.can_view(user) and (not self.need_auth or user.is_authenticated)

    # See https://code.djangoproject.com/ticket/24858
    def get_target_gr_display(self):
        return ", ".join([dict(TARGETGROUP_CHOICES)[v] for v in self.target_gr]) if self.target_gr else ''

    def get_languages_display(self):
        return ", ".join([dict(LANGUAGE_CHOICES)[v] for v in self.languages]) if self.languages else ''

    def get_type_ped_display(self):
        return ", ".join([dict(TYPE_PED_CHOICES)[v] for v in self.type_ped]) if self.type_ped else ''

    def get_type_doc_display(self, mode='normal'):
        """
        normal: ['audio', 'video'] => 'document sonore, document vidéo'
        pdf: '<font face="">I</font> <font face="">0</font>'
        """
        if not self.type_doc:
            return ''
        if mode == 'pdf':
            mapping = {tp[0]: tp[2] if len(tp) > 2 else tp[1] for tp in TYPE_DOC}
            return ' '.join([
                ('<font face="MultimediaIcons">%s</font>' % mapping[v]) if len(mapping[v]) == 1 else mapping[v]
                for v in self.type_doc
            ])
        else:
            return ", ".join([dict(TYPE_DOC_CHOICES)[v] for v in self.type_doc])

    def get_educ_level_display(self):
        return ", ".join([dict(EDUCATION_LEVEL_CHOICES)[v] for v in self.educ_level]) if self.educ_level else ''

    def get_level_display(self):
        return ", ".join([dict(LEVEL_CHOICES)[v] for v in self.level]) if self.level else ''

    def get_mitic_asp_display(self):
        choices = sum([list(vals) for gr, vals in MITIC_CHOICES], [])  # flatten tuple of tuples
        return ", ".join([dict(choices)[v] for v in self.mitic_asp]) if self.mitic_asp else ''

    def get_mitic_categs(self):
        categs = []
        for label, opts in MITIC_CHOICES:
            # Test intersection between mitic_asp and opts
            if set([v[0] for v in opts]) & set(self.mitic_asp):
                categs.append(label)
        return categs

    def index(self):
        # Updated search index
        ResourcePERIndex.objects.filter(resource=self).delete()
        per_links = self.resourceperlink_set.all().select_related('content_type')
        disciplines = ResourcePerLink.disciplines_from_per_links(per_links)
        disciplines_seen = set()
        for link in per_links:
            parent = link.get_spec()
            if isinstance(parent, Tableau):
                try:
                    parent = parent.get_objectif().specification_set.all()[0]
                except IndexError:
                    continue
            if not isinstance(parent, Specification):
                continue
            cell = link.content_object if link.content_type.model == 'cellule' else (
                link.content_object.cellule if link.content_type.model == 'contenucellule' else None
            )
            for disc in parent.disciplines.all():
                if disc not in disciplines:
                    continue
                disciplines_seen.add(disc)
                ResourcePERIndex.objects.create(
                    resource=self,
                    group=link.catalog if link.catalog_id else None,
                    cell=cell,
                    discipline=disc,
                    specification=parent
                )
        for disc in set(disciplines) - disciplines_seen:
            ResourcePERIndex.objects.create(
                resource=self,
                cell=None,
                discipline=disc,
                specification=None
            )

        for gr in self.resourcegroup_set.all():
            for link in gr.resourceperlink_set.all():
                spec = link.get_spec()
                cell = link.get_cell()
                for disc in spec.disciplines.all():
                    ResourcePERIndex.objects.create(
                        resource=self, group=gr, cell=cell,
                        discipline=disc, specification=spec
                    )
        # Update the full-text search field of `t_resource`. Unfortunately, a
        # trigger cannnot be used due to get_genre_display app-level data.
        cursor = connection.cursor()
        cursor.execute(
            "UPDATE t_resource SET fts = to_tsvector('fr', unaccent(%s)) WHERE id=%s", [
                " ".join([
                     self.title, self.description, self.descr_eleve, self.keywords,
                     self.get_genre_display(), self.source, self.get_type_ped_display()
                ]),
                self.pk
            ]
        )

    @property
    def qrcode(self):
        if os.access(os.path.join(settings.MEDIA_ROOT, 'qrcodes', 'code-%d.png' % self.id), os.R_OK):
            return os.path.join('qrcodes', 'code-%d.png' % self.id)
        else:
            return None

    def format_title(self):
        """Set title in uppercase and try to guess superscript chars."""
        if self.title == '' and self.cantons:
            return f"Précision cantonale ({self.cantons})".upper()
        title = auto_exposant(self.title)
        return mark_safe(re.sub(r'(<SUP>[^<]*</SUP>)', lambda m: m.group(1).lower(), title.upper()))

    def get_value(self, field_name, fmt='normal', request=None):
        """
        Return a normalized version of a field value:
        | value        | fmt=normal  | fmt=raw
        =====================================
        | None         | ''          | ''
        | True/False   | 'Oui'/'Non' | True/False
        | field w/display | 'disp_val'| ['raw_val']
        | Choices      | verbose val | raw val
        | M2M fields   | 'str1\nstr2'
        """
        if fmt == 'normal' and hasattr(self, 'get_%s_display' % field_name):
            return getattr(self, 'get_%s_display' % field_name)()
        try:
            value = getattr(self, field_name)
        except ObjectDoesNotExist:
            return ''
        if value is True:
            return "Oui" if fmt == 'normal' else True
        elif value is False:
            return "Non" if fmt == 'normal' else False
        elif value is None:
            return ""  # ask Webexpert? if fmt == 'normal' else None
        elif isinstance(value, (int, float)):
            return value
        elif isinstance(value, datetime):
            return value.strftime('%Y-%m-%d %H:%M')
        elif isinstance(value, list):
            return ", ".join(value) if fmt == 'normal' else value
        elif isinstance(value, FieldFile):
            if value._file is None:
                return ''
            elif request is not None:
                return request.build_absolute_uri(value.url)
            return value.url
        elif hasattr(value, 'all'):
            return "\n".join(str(v) for v in value.all())
        return str(value)

    @cached_property
    def has_files(self):
        return len(self.files.all())

    @cached_property
    def get_images(self):
        """
        Obtention des images sous forme de relations (cas des galeries d'images).
        Renvoie une liste de ResourceFile (des relations) triés par nom.
        """
        imgs = []
        for rel in self.relations.all().select_related('other').prefetch_related('other__files'):
            if rel.other and 'image' in rel.other.type_doc:
                imgs.append(rel.other.files.first())
        # Sort by file name
        return sorted(imgs, key=lambda i: os.path.basename(i.rfile.name))

    @cached_property
    def has_images(self):
        return any('image' in rel.other.type_doc for rel in self.relations.all() if rel.other_id)

    @cached_property
    def is_image(self):
        return len(self.files.all()) == 1 and self.files.first().is_image

    def get_galleries(self):
        if self.type_doc and 'image' in self.type_doc:
            cache = getattr(self, '_prefetched_objects_cache', {})
            if 'other_relations' in cache:
                return [rel.resource for rel in cache['other_relations'] if rel.rtype == 'is_part_of']
            else:
                return [rel.resource for rel in self.other_relations.filter(rtype='is_part_of')]
        return []

    @property
    def content_type(self):
        if self.url:
            return 'url'
        elif self.has_files:
            return 'files'
        elif self.has_images:
            return 'gallery'
        return None

    def get_url(self, request=None):
        """
        Should always return an absolute URL, as other sites may refer to
        them through the API.
        `request` is used to generate Infomaniak VOD tokens, when needed.
        """
        if self.need_auth and request is not None:
            if not request.user.is_authenticated:
                return request.build_absolute_uri(reverse('login')) + '?next=%s' % quote(request.get_full_path())
        url = None
        num_files = self.files.count()
        if num_files == 1:
            url = self.files.all()[0].rfile.url
        elif num_files > 1:
            # Redirect to zip download
            url = reverse('resource_download', args=[self.pk])
        elif self.has_images:
            # An image gallery, direct to a gallery viewer Web page.
            url = reverse('gallery', args=[self.pk])
        else:
            url_obj = MediaURL.detect(self)
            if url_obj:
                url = url_obj.url_path()
        if url and not url.startswith('https://'):
            url = 'https://%s%s' % (settings.SITE_DOMAIN, url)
        return url or self.url

    def media_data(self, request=None):
        """
        Return a dict with player data (or empty dict) when we detect a media
        that can be displayed in a local player (YouTube, Dailymotion, Vimeo, gallery).
        """
        data = {}
        need_auth = self.need_auth and request is not None and not request.user.is_authenticated
        url_obj = MediaURL.detect(self)
        if not url_obj:
            return {}
        data = url_obj.data(request)
        data['need_auth'] = need_auth
        return data

    def get_thumb_url(self):
        if self.thumb_url:
            return self.thumb_url
        elif self.thumb:
            return self.thumb.url
        elif self.has_files:
           return next((f.thumb.url for f in self.files.all() if f.thumb), '')
        elif self.has_images:
            # Gallery: take first image as thumbnail
            return self.get_images[0].thumbnail_url()
        else:
            return ''

    def refs(self, fmt=None):
        """
        Return locations in the PER where this resource is referenced, as a
        queryset of ResourcePERIndex denormalized objects.
        Currently only for links to specifications, but may be improved in the
        future to support other link types.
        """
        try:
            results = self._prefetched_objects_cache['resourceperindex_set']
        except (AttributeError, KeyError):
            results = self.resourceperindex_set.select_related(
                'discipline', 'specification__objectif', 'group'
            ).order_by('discipline', 'specification__objectif')
        if fmt == 'hyperlinks':
            vals = OrderedDict()
            # Deduplicate spec codes, and show anchor only if code is once
            for lk in results:
                code = lk.title()
                if code in vals:
                    vals[code][1] += 1
                else:
                    vals[code] = [lk, 1]
            results = [
                lk.as_hyperlink(with_anchor=cnt < 2, as_group=False)
                for lk, cnt in vals.values()
            ]
        return results

    def grouped_per_links(self):
        """
        Regroup ResourcePerLink instances by cycle, domaine, discipline, objectif, contenu.
        """
        cycles = OrderedDict()
        for rpl in self.resourceperlink_set.all():
            spec = rpl.get_spec()
            if not spec:
                # FIXME: Find a way to display links to disciplines (without cycle refs)
                continue
            cycle = spec.objectif.cycle
            if not cycle in cycles:
                cycles[cycle] = OrderedDict()
            domain = spec.objectif.domain
            if not domain in cycles[cycle]:
                cycles[cycle][domain] = OrderedDict()
            disc = spec.disciplines.first()
            if not disc in cycles[cycle][domain]:
                cycles[cycle][domain][disc] = OrderedDict()
            objectif = spec.objectif
            if not objectif in cycles[cycle][domain][disc]:
                cycles[cycle][domain][disc][objectif] = []
            per_obj = rpl.content_object
            if isinstance(per_obj, ContenuCellule):
                cycles[cycle][domain][disc][objectif].append(per_obj)
        return cycles

    def metadata(self, subset='standard', even_empty=False, pdf=False):
        if subset == 'appr':
            fields = [fn for fn in self.METADATA_SUBSET_NAMES if fn not in ['per_links', 'target_gr']]
        elif subset == 'standard':
            fields = self.METADATA_SUBSET_NAMES
        elif subset == 'full':
            fields = self.METADATA_FIELD_NAMES
        data = []
        for field_name in fields:
            if field_name == 'languages' and self.languages == ['fr'] and not even_empty:
                # No need to display language when 'fr' only
                continue
            is_plural = False
            if field_name == 'contribs':
                value = ', '.join(obj.pprint(fmt='res_html') for obj in self.contribs.all())
                if not pdf:
                    value = mark_safe(value)
            elif field_name == 'type_doc':
                value = self.get_type_doc_display(mode='pdf' if pdf else 'normal')
                is_plural = self.type_doc and len(self.type_doc) > 1
            elif field_name == 'languages':
                value = self.get_languages_display()
            elif field_name == 'per_links':
                refs = self.refs(fmt='hyperlinks')
                value = mark_safe(', '.join(refs))
            else:
                value = self.get_value(field_name)
                raw_value = self.get_value(field_name, fmt='raw')
                is_plural = isinstance(raw_value, list) and len(raw_value) > 1
            if not value and not even_empty:
                continue
            if field_name in self.METADATA_FIELD_HTML:
                value = mark_safe(value.replace('<p>', '').replace('</p>', ''))
            if field_name == 'format' and self.num_pages:
                value += " (%d page%s)" % (self.num_pages, 's' if self.num_pages > 1 else '')
            data.append((
                self.verbose_field_name(field_name, plural=is_plural),
                value,
            ))
        return data

    def relations_data(self):
        """Return a list of relations, with each label only once and potentially pluralized."""
        data = []
        last_label = None
        # As relations are potentially cached, sort objects in Python
        relations = sorted(
            [rel for rel in self.relations.all() if not rel.other_id or not 'image' in rel.other.type_doc],
            key=lambda rel: rel.order
        )
        for relation in relations:
            label = relation.get_label()
            if label == last_label:
                if data[-1][0] and not data[-1][0].endswith('s'):
                    # Pluralize previous label
                    data[-1][0] = " ".join(w + 's' for w in data[-1][0].split())
                data.append(['', relation])
            else:
                data.append([label, relation])
            last_label = label
        return data

    def get_logo(self):
        """ Return a tuple: image path, image title """
        logo_type_map = {
            'mer': "t-mer2.png",  # "t-mer.png" can be deleted after all specs have been republished
            'merx': "t-mer-ext.png",
            'med': "logo_m.png",
            'rea': "logo_rea.png",
            'epro': "logo_eprocom.svg",
        }
        logo = logo_type_map.get(self.type_r, 'resource_ped.png')

        return (
            static('resources/public/images/' + logo),
            dict(TYPE_RESOURCE).get(self.type_r, "Type inconnu"),
        )

    def get_cant_logos(self):
        return [static('resources/public/images/%s.jpg' % can.strip().lower())
                for can in self.cantons.split(',')]


class MediaURL:
    def __init__(self, res):
        self.res = res

    @property
    def url(self):
        return self.res.url

    @classmethod
    def detect(cls, res):
        if res.no_player:
            return None
        url = res.url
        if 'rts.ch/play/tv' in url or 'rts.ch/play/radio' in url:
            return RTSURL(res)
        elif 'player.vod2.infomaniak.com' in url:
            return InfomaniakURL(res)
        elif 'youtu' in url:
            return YouTubeURL(res)
        elif 'dailymotion' in url:
            return DailyMotionURL(res)
        elif 'vimeo.com' in url:
            return VimeoURL(res)
        elif res.has_images:
            return GalleryURL(res)
        elif len(res.files.all()) == 1 and res.files.all()[0].rfile.url.endswith('.mp3'):
            return LocalAudioURL(res)
        return None


class RTSURL(MediaURL):
    def url_path(self):
        return reverse('video-rts', args=[self.res.pk, self.key])

    @cached_property
    def key(self):
        key = None
        query = urlsplit(self.url).query
        if not query:
            last_path_item = urlsplit(self.url).path.split('/')[-1]
            if last_path_item.isdigit():
                return last_path_item
            return key
        try:
            return parse_qs(query).get('id')[0]
        except (TypeError, IndexError):
            try:
                key = parse_qs(query).get('urn')[0]
            except (TypeError, IndexError):
                return None
            return key.split(':')[-1]

    def data(self, *args):
        if not self.key:
            return {}
        try:
            path = self.url_path()
        except NoReverseMatch:
            return {}
        typ = 'video' if 'rts.ch/play/tv' in self.url else 'audio'
        query = urlsplit(self.url).query
        return {
            'player': 'rts' if typ == 'video' else 'rts-audio',
            'base': 'https://rts.ch/play' if typ == 'video' else 'https://tp.srgssr.ch/p/srf/',
            'vkey': self.key,
            'path': path,
            'subdivisions': parse_qs(query).get('subdivisions') == ['true'],
        }


class InfomaniakURL(MediaURL):
    """VOD Infomaniak."""
    def url_path(self):
        return reverse('video-imk', args=[self.res.pk])

    def video_path(self):
        return get_infomaniak_videopath(self.url)

    def data(self, request):
        parts = urlsplit(self.url)
        key = parts.path.split('/')[-1]
        data = {
            'player': 'infomaniak2',
            'base': 'https://player.vod2.infomaniak.com',
            'vkey': key,
            'path': self.url_path(),
        }
        if self.res.need_auth and request and request.user.is_authenticated:
            data['skey'] = get_infomaniak_token(request, self.url)
        return data


class YouTubeURL(MediaURL):
    def url_path(self):
        return reverse('video-yt', args=[self.res.pk, self.key])

    @cached_property
    def key(self):
        parts = urlsplit(self.url)
        key = None
        if parts.netloc == 'youtu.be':
            key = parts.path.strip('/')
        elif parts.netloc == 'www.youtube.com':
            try:
                key = parse_qs(parts.query).get('v')[0]
            except (TypeError, IndexError):
                pass
        return key

    def data(self, *args):
        if not self.key:
            return {}
        try:
            path = self.url_path()
        except NoReverseMatch:
            return {}
        return {
            'player': 'youtube',
            'base': 'https://www.youtube-nocookie.com',
            'vkey': self.key,
            'path': path,
        }


class DailyMotionURL(MediaURL):
    # https://developer.dailymotion.com/player
    def url_path(self):
        return reverse('video-dm', args=[self.res.pk, self.key])

    @property
    def key(self):
        parts = urlsplit(self.url)
        if parts.netloc != 'www.dailymotion.com':
            return None
        return parts.path.lstrip('/video/').split('_')[0]

    def data(self, *args):
        try:
            path = self.url_path()
        except NoReverseMatch:
            return {}
        return {
            'player': 'dailymotion',
            'base': 'https://www.dailymotion.com',
            'vkey': self.key,
            'path': path,
        }


class VimeoURL(MediaURL):
    def url_path(self):
        return reverse('video-vm', args=[self.res.pk, self.key])

    @property
    def key(self):
        parts = urlsplit(self.url)
        if not parts.netloc.endswith('vimeo.com'):
            return None
        return parts.path.lstrip('/video/').strip('/')

    def data(self, *args):
        if not self.key:
            return {}
        return {
            'player': 'vimeo',
            'base': 'https://player.vimeo.com',
            'vkey': self.key,
            'path': self.url_path(),
        }


class GalleryURL(MediaURL):
    def url_path(self):
        return reverse('gallery', args=[self.res.pk])

    def data(self, *args):
        return {
            'player': 'galerie',
            'path': self.url_path(),
        }


class LocalAudioURL(MediaURL):
    def __init__(self, res):
        self.res = res
        self.rfile = self.res.files.first().rfile

    def url_path(self):
        return reverse('audio-local', args=[self.res.pk])

    def data(self, *args):
        return {
            'player': 'local-audio',
            'base': self.rfile.url,
            'path': self.url_path(),
        }


def resource_upload_path(instance, filename):
    # Used for both Resource and ResourceRelation
    return 'ressources/{0}/{1}'.format(
        getattr(instance, 'resource_id', instance.pk), os.path.basename(filename)
    )


class ResourceRelation(models.Model):
    resource = models.ForeignKey(
        Resource, on_delete=models.CASCADE, related_name='relations')
    rtype = models.CharField("Type de relation", max_length=20, choices=RELATION_TYPES)
    order = models.SmallIntegerField(default=0)
    url = models.URLField("URL", max_length=255, blank=True)
    rel_file = models.FileField(
        upload_to=resource_upload_path, storage=ow_storage, verbose_name="Fichier", blank=True
    )
    other = models.ForeignKey(
        Resource, null=True, blank=True, on_delete=models.CASCADE, related_name='other_relations',
        verbose_name="Autre ressource"
    )
    description = models.TextField("Description")

    class Meta:
        db_table = 't_resource_relation'
        verbose_name = 'Relation de ressource'
        verbose_name_plural = 'Relations de ressource'

    def __str__(self):
        return "Relation %s (%s)" % (self.get_rtype_display(), self.get_url())

    def get_label(self):
        return "Téléchargement" if self.rtype in ('has_version', 'has_format') else "Ressource liée"

    def get_url(self):
        if self.rel_file:
            return 'https://%s%s' % (settings.SITE_DOMAIN, self.rel_file.url)
        else:
            return self.url

    def as_html(self):
        return mark_safe(", ".join([
            self.description, '<a href="%(url)s">%(url)s</a>' % {'url': self.get_url()}
        ]))


class ResourceFile(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE, related_name='files')
    rfile = models.FileField(
        "Fichier", upload_to=resource_upload_path, storage=ow_storage, max_length=150
    )
    title = models.CharField("Titre", max_length=200)
    description = models.TextField("Description", blank=True)
    thumb = models.ImageField(
        "Vignette", storage=s3_storage, upload_to='resource_thumbs', max_length=150, blank=True
    )

    # '.webp' should be added as soon as we upgrade to Buster
    image_exts = ('.png', '.jpg', '.jpeg', '.tif', '.tiff', '.gif', '.svg')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if kwargs.get("update_fields") == ["thumb"]:
            return
        # Auto-update size of parent resource
        new_size = sum([f.rfile.size for f in self.resource.files.all()])
        if new_size != self.resource.size:
            self.resource.size = new_size
            self.resource.save(update_fields=['size'])
        # If no thumbnail, generate auto thumbnail
        if not self.thumb:
            self.produce_thumbnail(save=True)

    def produce_thumbnail(self, save=False):
        if self.thumb:
            # Remove any pre-existing thumbnail
            self.thumb.delete(save=False)
        try:
            if self.rfile.name.lower().endswith('pdf'):
                self.thumb = generate_thumbnail_for_pdf(self.rfile, self.thumb.storage)
            elif self.is_image and not self.rfile.name.lower().endswith('svg'):
                self.thumb = generate_thumbnail_for_img(self.rfile, self.thumb.storage)
            else:
                return
        except Exception as err:
            mail_admins(
                "BD-PER Error",
                f"Error while generating thumbnail for resource {self.resource} ({err})"
            )
        else:
            if save:
                self.save(update_fields=['thumb'])

    @property
    def is_image(self):
        return os.path.splitext(self.rfile.name.lower())[-1] in self.image_exts

    def thumbnail_url(self):
        return self.thumb.url if self.thumb else (
            self.rfile.url if self.rfile.name.lower().endswith('svg') else ''
        )

    @property
    def icon(self):
        """Return Font Awesome code."""
        mapping = {
            ('pdf',): 'fa-file-pdf',
            ('doc', 'docx'): 'fa-file-word',
            self.image_exts: 'fa-file-image',
        }
        facode = 'fa-file'  # The generic one
        for exts, code in mapping.items():
            if self.rfile.name.lower().endswith(exts):
                facode = code
                break
        return format_html('<i class="far {} fa-2x"></i>', facode)


class ResourceCheck(models.Model):
    CHECK_STATUS_CHOICES = (
        ('ok', "OK"),
        ('unk', "Incertain"),
        ('ko', "Pas OK"),
    )
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE, related_name='checks')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    status = models.CharField("État", max_length=5, choices=CHECK_STATUS_CHOICES)
    when = models.DateTimeField("Date du contrôle")
    comment = models.TextField("Commentaire", blank=True)

    def __str__(self):
        return f"{self.when}, ressource {self.resource}, statut {self.get_status_display()}, par {self.user}"


class ResourceGroupTag(models.Model):
    tag = models.CharField("Étiquette", max_length=30, unique=True)

    def __str__(self):
        return self.tag


class ResourceGroup(models.Model):
    GROUP_STATUS_CHOICES = (
        ('avalider', 'À valider'),
        # Not anonymously visible in catalog list.
        ('limite', 'Validé sans publication'),
        ('valide', 'Validé'),
    )

    created = models.DateTimeField("Date de création du catalogue", auto_now_add=True)
    updated = models.DateTimeField("Date de dernière modification")
    domain = models.ForeignKey('pper.Domain', verbose_name="Domaine",
        null=True, blank=True, on_delete=models.PROTECT,
        help_text="Utile lorsque le catalogue n'est pas positionné dans une cellule du PER")
    objectif = models.ForeignKey('pper.Objectif', verbose_name="Objectif",
        null=True, blank=True, on_delete=models.PROTECT)
    weight = models.IntegerField("Poids", default=0,
        help_text="Ordre quand plusieurs groupes apparaissent dans une même cellule")
    title = models.CharField("Titre", max_length=200)
    subtitle = models.CharField("Sous-titre", max_length=200, blank=True)
    subtitle2 = models.CharField("Sous-titre secondaire", max_length=200, blank=True,
        default="Ressources numériques")
    groupment = models.CharField("Regroupement", max_length=200, blank=True,
        help_text="Les groupes de ressources avec le même texte de regroupement apparaissent groupés sur la PPER")
    description = models.TextField("Description", blank=True)
    tags = models.ManyToManyField(ResourceGroupTag, blank=True)
    mer_url = models.URLField("Lien MER", blank=True)
    levels = ChoiceArrayField(
        models.CharField(max_length=20, choices=LEVEL_CHOICES, blank=True),
        verbose_name="Degré scolaire", blank=True, null=True,
    )
    grp_status = models.CharField(
        "Statut de validation", max_length=10, choices=GROUP_STATUS_CHOICES,
        default='valide', db_index=True
    )
    resources = models.ManyToManyField('Resource', through='ResourceGroupLink', blank=True)

    class Meta:
        db_table = 't_resource_grp'
        verbose_name = 'Catalogue de ressources'
        verbose_name_plural = 'Catalogues de ressources'

    def __str__(self):
        return self.title

    def __lt__(self, other):
        obj_code = self.objectif.code if self.objectif else 'z'
        other_code = other.objectif.code if other.objectif else 'z'
        if (
            (self.domaine, obj_code, self.groupment) ==
            (other.domaine, other_code, other.groupment)
        ):
            # Compare on title
            try:
                return self._title_tuples < other._title_tuples
            except TypeError:
                self.title < other.title
        else:
            return (
                (self.domaine, obj_code, locale.strxfrm(self.groupment)) <
                (other.domaine, other_code, locale.strxfrm(other.groupment))
            )

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

    @classmethod
    def all_by_user(cls, user):
        if user.is_anonymous:
            return cls.objects.filter(grp_status='valide')
        if user.has_perm('ressource.change_resourcegroup'):
            return cls.objects.all()
        return cls.objects.exclude(grp_status='avalider')

    @property
    def _title_tuples(self):
        """ Transform 'Géo 9e' into ['Géo ', 9, 'e'], to sort embedded numbers smartly."""
        lst = [self.title[0]]
        for c in self.title[1:]:
            if (lst[-1][-1].isdigit() and c.isdigit()) or (
                not lst[-1][-1].isdigit() and not c.isdigit()):
                lst[-1] += c
            else:
                lst.append(c)
        return tuple([int(part) if part[0].isdigit() else locale.strxfrm(part) for part in lst])

    def get_absolute_url(self):
        return reverse('resource_grp_display', args=[self.pk])

    def title_with_oa(self):
        obj = self.objectif
        return self.title if not obj else '%s (%s)' % (self.title, obj.code)

    def duplicate(self, to_cell):
        # There are 4 entities to duplicate: ResourceGroup, SubTheme, ResourceGroupLink
        # and through model between ResourceGroupLink and SubTheme
        resourcelinks = list(self.resourcegrouplink_set.all())
        subthemes = list(self.subthemes.all())
        self.pk = None
        self.cell = to_cell
        self.updated = timezone.now()
        self.save()
        for theme in subthemes:
            theme.pk = None
            theme.group = self
            theme.save()
        for link in resourcelinks:
            old_themes = link.subthemes.all()
            link.pk = None
            link.group = self
            link.save()
            link.subthemes.set(SubTheme.objects.filter(group=self, name__in=old_themes.values('name')))
        return self

    @property
    def url(self):
        return reverse('resource_grp_display', args=[self.pk])

    def get_levels_display(self):
        return ", ".join([dict(LEVEL_CHOICES)[v] for v in self.levels]) if self.levels else ''

    def add_resource(self, res, position=None):
        if position is None:
            position = self.resourcegrouplink_set.all().aggregate(models.Max('position'))['position__max']
            if position is None:
                position = 0
            position += 1
        link = ResourceGroupLink.objects.create(
            group=self,
            resource=res,
            position=position
        )
        self.save()  # updated date
        res.index()
        return link

    def ordered_resources(self, user, filters=None):
        query = self.resourcegrouplink_set.all().select_related('resource'
            ).prefetch_related(
                'subthemes', 'resource__files', 'resource__relations',
                models.Prefetch(
                    'resource__resourceperindex_set',
                    queryset=ResourcePERIndex.objects.select_related(
                        'discipline', 'specification__objectif', 'group').all(
                    ).order_by('discipline', 'specification__objectif')
                )
            ).order_by('position')
        if user.is_anonymous or not user.has_perm('ressource.change_resource'):
            query = query.filter(resource__status='validee', resource__broken=False)
        if filters:
            query = query.filter(subthemes__num__in=filters).distinct()
        return query

    def domaine(self):
        if self.domain_id:
            return self.domain
        elif self.objectif:
            return self.objectif.domain
        else:
            specs = list(self.specs)
            return specs[0].get_domain() if specs else None

    @cached_property
    def specs(self):
        return set([
            sp for sp in [link.get_spec() for link in self.resourceperlink_set.all()]
            if sp is not None
        ])

    @property
    def objectif_link(self):
        if not self.specs:
            return ''
        home = list(self.specs)[0]
        if hasattr(home, 'pper_url'):
            return home.pper_url
        elif hasattr(home.per_object, 'pper_url'):
            return home.per_object.pper_url
        return ''

    def disciplines(self):
        return list(set(chain(*[spec.disciplines.all() for spec in self.specs])))


class OrderingQuerySet(models.QuerySet):
    def ordered(self):
        """Final clause to get specially-ordered SubTheme's"""
        # alphanum to do natural sort
        convert = lambda text: int(text) if text.isdigit() else text
        alphanum = lambda key: [convert(c) for c in re.split('([0-9]+)', key)  if c != '']
        def get_key(item):
            try:
                return 1, int(item.num)
            except ValueError:
                return 2 if str(item.num).startswith(('Ms', 'S')) else 0, alphanum(item.num)

        prev_nums = []
        breaked = False
        for subt in sorted(self, key=get_key):
            if subt.num in ('0', '1') and len(prev_nums) > 1 and prev_nums[-1] != '0' and not breaked:
                # Special marker to signal the start of a new list
                yield 'BREAK'
                breaked = True
            yield subt
            prev_nums.append(subt.num)


class SubTheme(models.Model):
    name = models.CharField(max_length=250)
    num = models.CharField(max_length=3, default='')
    group = models.ForeignKey(ResourceGroup, related_name="subthemes", on_delete=models.CASCADE)

    objects = OrderingQuerySet.as_manager()

    class Meta:
        db_table = 't_resource_subtheme'
        ordering = ('num',)

    def __str__(self):
        return "%s %s" % (self.num, self.name)


class ResourceGroupLink(models.Model):
    """
    M2M through table to link Resource to ResourceGroup.
    """
    created = models.DateTimeField("Date d’ajout dans le catalogue", auto_now_add=True)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    group = models.ForeignKey(ResourceGroup, db_column='resourcegroup_id', on_delete=models.CASCADE)
    position = models.SmallIntegerField(default=0)
    number = models.CharField("Numéro", max_length=20, blank=True)
    subthemes = models.ManyToManyField(SubTheme, blank=True)

    class Meta:
        db_table = 't_resource_grp_resources'
        ordering = ['position']
        unique_together = ['resource', 'group']

    def classes(self):
        return " ".join('cls_%s' % th.num for th in self.subthemes.all())


class ResourcePerLink(models.Model):
    """
    Generic link between a resource and a PER (generic) element (Content,
    Cellule, Discipline, Domain, etc.
    """
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE, blank=True, null=True)
    catalog = models.ForeignKey(ResourceGroup, on_delete=models.CASCADE, blank=True, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    # Denormalized sort key as it is not possible to sort with the DB due to generic relation
    sort_key = models.CharField(max_length=20, blank=True, db_index=True)

    class Meta:
        ordering = ('sort_key',)
        verbose_name = "Lien vers PER"
        verbose_name_plural = "Liens vers PER"

    def __str__(self):
        return "Link between %s and %s" % (self.resource if self.resource_id else self.catalog, self.content_object)

    def save(self, **kwargs):
        # Compute denormalized sort key
        spec = self.get_spec()
        if spec:
            self.sort_key = self.get_spec().code
            cell = self.get_cell()
            if cell:
                self.sort_key += str(cell.position)
        else:
            try:
                self.sort_key = self.content_object.domain.abrev
            except AttributeError:
                self.sort_key = self.abrev
        super().save(**kwargs)

    @classmethod
    def from_cell_ids(cls, cell_ids):
        return cls.objects.filter(
            content_type__model='cellule',
            object_id__in=cell_ids
        ).select_related('resource', 'catalog').order_by(
            'resource__type_r', 'catalog__weight', 'catalog__title'
        )

    def get_cell(self):
        per_obj = self.content_object
        if isinstance(per_obj, ContenuCellule):
            return per_obj.cellule
        elif isinstance(per_obj, Cellule):
            return per_obj
        return None

    def get_spec(self):
        """Return Specification (or Tableau) instance."""
        per_obj = self.content_object
        if isinstance(per_obj, (Domain, Discipline)):
            return None
        elif isinstance(per_obj, ContenuCellule):
            spec = per_obj.cellule.specification
        elif isinstance(per_obj, Cellule):
            spec = per_obj.get_super()
        else:
            spec = per_obj
        if isinstance(spec, Tableau):
            try:
                spec = spec.get_objectif().specification_set.all()[0]
            except IndexError:
                pass
        return spec

    def get_cycle(self):
        per_obj = self.content_object
        if isinstance(per_obj, (Domain, Discipline)):
            return 1  # Impossible de connaître le cycle à partir du domaine ou de la discipline
        else:
            spec = self.get_spec()
            return spec.objectif.cycle

    def get_domain(self):
        per_obj = self.content_object
        if isinstance(per_obj, Domain):
            return per_obj
        elif isinstance(per_obj, Discipline):
            return per_obj.domain
        else:
            spec = self.get_spec()
            return spec.objectif.domain

    def get_discipline(self, multi=False):
        per_obj = self.content_object
        if isinstance(per_obj, Domain):
            return None
        elif isinstance(per_obj, Discipline):
            return [per_obj] if multi else per_obj
        else:
            spec = self.get_spec()
            return list(spec.disciplines.all()) if multi else spec.disciplines.first()

    @classmethod
    def disciplines_from_per_links(cls, per_links):
        """Extraction des disciplines en fonction de la liste de `per_links`."""
        discs = set()
        # Utiliser les objectifs à plusieurs disciplines (L27, etc.) en dernier recours
        discs_multi = set()
        for link in per_links:
            found_discs = link.get_discipline(multi=True)
            if not found_discs:
                continue
            if len(found_discs) == 1:
                discs.add(found_discs[0])
            else:
                discs_multi.update(found_discs)
        return list(discs or discs_multi)


class ResourcePERIndex(models.Model):
    """
    Denormalized resource index dedicated to search (built from ResourcePerLink generic relations).
    Updated at each Resource.save().
    Search is currently requiring domain/discipline/cycle.
    Also used by Resource.refs().
    """
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    group = models.ForeignKey(ResourceGroup, null=True, on_delete=models.SET_NULL)
    cell = models.ForeignKey(Cellule, null=True, on_delete=models.SET_NULL)
    specification = models.ForeignKey(Specification, null=True, on_delete=models.SET_NULL)
    discipline = models.ForeignKey(Discipline, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        if self.group:
            return f"Link between {self.resource.title} and {self.title()} (group {self.group})"
        return f"Link between {self.resource.title} and {self.title()}"

    def title(self):
        return self.specification.code if self.specification_id else self.discipline.name

    def to_per_url(self, with_anchor=True):
        return ('{url}#{anch}' if with_anchor and self.cell_id else '{url}').format(
            url=self.specification.pper_url if self.specification_id else self.discipline.pper_url,
            anch=self.cell_id
        )

    def as_hyperlink(self, with_anchor=True, as_group=True):
        if as_group and self.group:
            return format_html('<a href="{}#res{}">{}</a> (<a href="{}">{}</a>)',
                self.group.get_absolute_url(), self.resource.pk, self.group.title,
                self.to_per_url(with_anchor=with_anchor), self.title())
        else:
            return format_html(
                '<a href="{url}">{txt}</a>',
                url=self.to_per_url(with_anchor=with_anchor),
                txt=self.title()
            )


class ResourceSubmitter(models.Model):
    """
    Personal data of people suggesting new resources.
    """
    resource = models.OneToOneField(Resource, on_delete=models.CASCADE)
    name = models.CharField("Nom et prénom", max_length=100)
    email = models.EmailField("Adresse courriel")
    tel = models.CharField("Téléphone", max_length=20)
    institution = models.CharField("Institution", max_length=100, blank=True)
    function = models.CharField("Fonction", max_length=100, blank=True)
    addr = models.TextField("Adresse postale")
    remarks = models.TextField("Remarques", blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Proposant de ressource"
        verbose_name_plural = "Proposants de ressource"


class ResourceClick(models.Model):
    """A model to get resource click stats."""
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    stamp = models.DateTimeField(auto_now_add=True)


class TypeImageChoices(models.TextChoices):
    DESSIN_C = 'dessin c', 'Dessin couleur'
    DESSIN_NB = 'dessin nb', 'Dessin noir-blanc'
    PHOTO = 'photo', 'Photo'
    PICTO = 'picto', 'Pictogramme'


class Imagier(models.Model):
    CARACT_CHOICES = (
        ("Conte de randonnée", "Conte de randonnée"),
        ("Comptine", "Comptine"),
        ("Documentaire", "Documentaire"),
        ("Recette", "Recette"),
        ("Récit de vie", "Récit de vie"),
        ("Album", "Album"),
        ("Présentation de soi", "Présentation de soi"),
        ("Marche à suivre", "Marche à suivre"),
        ("Conscience phonologique", "Conscience phonologique"),
        ("Passage de l’oral à l’écrit", "Passage de l’oral à l’écrit"),
        ("Stratégies de compréhension", "Stratégies de compréhension"),
        ("Vocabulaire", "Vocabulaire"),
    )
    CATEG_CHOICES = (
        ('aliment', 'aliment'),
        ('animaux', 'animaux'),
        ('bâtiment', 'bâtiment'),
        ('corps', 'corps'),
        ('couleur', 'couleur'),
        ('école', 'école'),
        ('forme', 'forme'),
        ('fruit', 'fruit'),
        ('instrument de musique', 'instrument de musique'),
        ('jeux', 'jeux'),
        ('légumes', 'légumes'),
        ('maison', 'maison'),
        ('météo', 'météo'),
        ('nature', 'nature'),
        ('objet', 'objet'),
        ('personnage', 'personnage'),
        ('prépositions', 'prépositions'),
        ('ustensile', 'ustensile'),
        ('végétal', 'végétal'),
        ('véhicule', 'véhicule'),
        ('verbes', 'verbes'),
        ('vêtement', 'vêtement'),
    )
    resource = models.OneToOneField(Resource, on_delete=models.CASCADE)
    nom = models.CharField("Nom", max_length=50)
    numero = models.PositiveSmallIntegerField("Numéro de carte")
    caract = ChoiceArrayField(
        models.CharField(max_length=30, choices=CARACT_CHOICES, blank=True),
        verbose_name="Caractéristiques", blank=True, null=True,
    )
    categorie = models.CharField("Catégorie", max_length=30, choices=CATEG_CHOICES)
    type_img = models.CharField("Type", max_length=15, choices=TypeImageChoices, blank=True)
    num_syll = models.PositiveSmallIntegerField("Syllabes", blank=True, null=True)
    attaque = models.CharField("Attaques complexes", max_length=8, blank=True)
    rime = models.CharField("Rimes", max_length=8, blank=True)

    def __str__(self):
        return f"Image «{self.nom}» [{self.numero}]"

    @classmethod
    def verbose_field_name(cls, fname):
        return Imagier._meta.get_field(fname).verbose_name


class Imagier3(models.Model):
    class DecodableChoices(models.IntegerChoices):
        T1 = 1, "T1"
        T2 = 2, "T2"
        T3 = 3, "T3"
        T4 = 4, "T4"
        T5 = 5, "T5"
        T6 = 6, "T6"

    class PhonemeChoices(models.TextChoices):
        A = "a"
        U = "y"
        I = "i"
        L = "l"
        R = "r"
        AU = "o"
        AI = "e"
        F = "f"
        G = "ʒ"
        V = "v"
        B = "b"
        OU = "u"
        T = "t"
        CH = "ʃ"
        M = "m"
        P = "p"
        S = "s"
        N = "n"
        WA = "wa"
        ON = "ɔ̃"
        K = "k"
        D = "d"
        AN = "ɑ̃"
        EST = "ɛ"
        Z = "z"
        IN = "ɛ̃"
        GU = "g"
        EU = "ø/œ"
        E = "ə"
        GN = "ɲ"
        J = "J"
        OUE = "w"

    resource = models.OneToOneField(Resource, on_delete=models.CASCADE, blank=True, null=True)
    mot = models.CharField("Mot", max_length=50)
    predecoupe = models.BooleanField("Prédécoupé classe", default=False)
    no_imagier = models.CharField("N° imagier", max_length=5, blank=True)
    decodable = models.PositiveSmallIntegerField(
        "Mot décodable", choices=DecodableChoices, blank=True, null=True
    )
    decodable_ent = models.PositiveSmallIntegerField(
        "Mot décodable entièrement", choices=DecodableChoices, blank=True, null=True
    )
    decodable_aide = models.PositiveSmallIntegerField(
        "Mot décodable avec aide", choices=DecodableChoices, blank=True, null=True
    )
    # Phonemes provient du fichier tableur, à convertir en phonétique?
    phonetique = models.CharField("Mot en phonétique", max_length=50, blank=True)
    phonemes = ChoiceArrayField(
        models.CharField(max_length=3, choices=PhonemeChoices, blank=True),
        verbose_name="Phonèmes", blank=True, null=True,
    )
    type_img = models.CharField("Type", max_length=15, choices=TypeImageChoices, blank=True)
    illustrateur = models.CharField("Illustrateur", max_length=30, blank=True)

    def __str__(self):
        return f"Image «{self.mot}»"

    @classmethod
    def verbose_field_name(cls, fname):
        return cls._meta.get_field(fname).verbose_name
