import itertools
import os
import re

from reportlab.lib.colors import HexColor, black, blue, lightgrey, white
from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY, TA_RIGHT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.lib import utils
from reportlab.pdfbase.pdfmetrics import registerFont, stringWidth
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import BaseDocTemplate, PageTemplate, SimpleDocTemplate
from reportlab.platypus import Frame, Image, KeepTogether, PageBreak, Paragraph, Spacer, Table, TableStyle

from django.conf import settings
from django.contrib.staticfiles.finders import find
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import mail_admins
from django.utils import timezone
from django.utils.dateformat import format
from django.utils.html import smart_urlquote
from django.utils.text import normalize_newlines

from pper import filters
from ressource.ontologies import LEVEL_CHOICES


def register_fonts():
    registerFont(TTFont('MultimediaIcons', find('fonts/multimediaicons.ttf')))


class DocTemplate(BaseDocTemplate):
    def afterInit(self):
        frame1 = Frame(self.leftMargin + 2.5*cm, self.bottomMargin, self.width - self.leftMargin - 2.5*cm, self.height, id='first')
        frame2 = Frame(self.leftMargin, self.bottomMargin, self.width, self.height, id='later', topPadding=3.5*cm)
        self.addPageTemplates([
            PageTemplate(id='First', frames=frame1, pagesize=self.pagesize),
            PageTemplate(id='Later', frames=frame2, pagesize=self.pagesize)
        ])

    def handle_pageBegin(self):
        '''override base method to add a change of page template after the firstpage.
        '''
        self._handle_pageBegin()
        self._handle_nextPageTemplate('Later')


class CleanParagraph(Paragraph):
    """A custom paragraph class replacing text when it cannot be parsed."""
    error_text = "Erreur de mise en forme du contenu en PDF."
    current_obj = None

    def __init__(self, text, style, **kwargs):
        cleaned = clean_content(text)
        try:
            super().__init__(cleaned, style, **kwargs)
        except ValueError as err:
            mail_admins(
                "BD-PER Error",
                f"Error while formatting content for {self.current_obj} ({self.current_obj.pk})\n\n{err}"
            )
            super().__init__("<i>%s</i>" % self.error_text, style, **kwargs)


class HyperlinkedImage(Image, object):
    # From http://stackoverflow.com/questions/18114820/is-it-possible-to-get-a-flowables-coordinate-position-once-its-rendered-using
    # The only variable I added to __init__() is hyperlink. I default it to None for the if statement I use later.
    def __init__(self, filename, hyperlink, **kwargs):
        super().__init__(filename, **kwargs)
        self.hyperlink = hyperlink

    def drawOn(self, canvas, x, y, _sW=0):
        x1 = self._hAlignAdjust(x, _sW) # This is basically adjusting the x coordinate according to the alignment given to the flowable (RIGHT, LEFT, CENTER)
        y1 = y
        x2 = x1 + self._width
        y2 = y1 + self._height
        canvas.linkURL(url=self.hyperlink, rect=(x1, y1, x2, y2), thickness=0, relative=1)
        super().drawOn(canvas, x, y, _sW)


def get_image(path, width, link=None):
    if path.startswith('http'):
        # One possible improvement would be to temp download the image locally,
        # to prevent a crash later in PDF build if the image is not accessible.
        path = smart_urlquote(path)
    try:
        img = utils.ImageReader(path)
    except IOError:
        return ''
    iw, ih = img.getSize()
    aspect = ih / float(iw)
    img.fp.close()
    if link:
        return HyperlinkedImage(path, link, width=width, height=(width * aspect))
    else:
        return Image(path, width=width, height=(width * aspect), lazy=1)


class ResourcePDFBase:
    def __init__(self):
        self.style_p = ParagraphStyle('Para', fontName="Helvetica", fontSize=10)
        self.style_pjust = ParagraphStyle('ParaJust', fontName="Helvetica", fontSize=10, alignment=TA_JUSTIFY)
        self._closable_imgs = []

    def _close(self):
        for img in self._closable_imgs:
            img._img.fp.close()

    def build_resource(self, story, resource, res_link=None):
        """
        Output a single resource in the pdf story structure.
        reslink is an optional ResourceGroupLink instance.
        """
        # Note: gradient (for image shadow) are available in reportlab from Jessie...
        CleanParagraph.current_obj = resource
        qr = resource.qrcode
        title = resource.format_title()
        if res_link is not None:
            if res_link.number:
                title = "%s – %s" % (res_link.number, title)
            themes = ", ".join(th.num for th in res_link.subthemes.all().ordered() if th != 'BREAK')
            if themes:
                title = "%s (%s)" % (title, themes)

        metadata_lines = [
            (label, re.sub(r'<img [^>]*>', '', content))
            for label, content in resource.metadata(subset='standard', pdf=True)
        ]
        relation_lines = []
        line = ''
        for label, relation in resource.relations_data():
            if label:
                if line:
                    relation_lines.append(line)
                line = '<b>%s :</b> ' % label
            else:
                line += ', '
            line += '<a href="%s" color="blue">%s</a>' % (
                relation.get_url(), relation.description)
        if line:
            relation_lines.append(line)

        content = "<br/>".join(itertools.chain(
            ["<b>%s :</b> %s" % (label, content) for label, content in metadata_lines],
            relation_lines
        ))
        metadata_par = CleanParagraph(content, self.style_p)
        if resource.thumb:
            thumb_image = get_image(resource.thumb.url, width=3.8*cm, link=resource.get_url())
        elif resource.thumb_url:
            thumb_image = get_image(resource.thumb_url, width=3.8*cm, link=resource.get_url())
        else:
            thumb_image = ''
        qr_image = get_image(os.path.join(settings.MEDIA_ROOT, qr), width=2.2*cm) if qr else ''
        table_data = [
             ['', Paragraph('<b>%s</b>' % title, self.style_p), '', ''],
             [thumb_image, metadata_par,
              [CleanParagraph(resource.description, self.style_pjust)],
              qr_image
             ],
        ]
        story.append(KeepTogether([Table(
            table_data,
            colWidths=[4*cm, 8.5*cm, 10.7*cm, 2.5*cm],
            style=TableStyle([
                ('SPAN', (1, 0), (2, 0)),
                ('VALIGN', (0,0), (2,-1), 'TOP'),
                ('VALIGN', (3,0), (3,-1), 'MIDDLE'),
            ], spaceAfter=25),
        )]))
        if qr_image:
            self._closable_imgs.append(qr_image)


class ResourceGroupPDF(ResourcePDFBase):
    def __init__(self, group, user, filters=None):
        super().__init__()
        self.group = group
        self.user = user
        self.filters = [name[-1] for name in filters] if filters else []
        self.objectif = group.objectif
        if self.objectif:
            self.dark_color = HexColor(self.objectif.domain.col_dark)
            self.light_color = HexColor(self.objectif.domain.col_light)
        else:
            self.dark_color = black
            self.light_color = lightgrey

    def draw_header(self, canvas, doc):
        self.draw_header_left(canvas, doc)
        canvas.saveState()
        canvas.setFillColor(self.light_color)
        canvas.rect(doc.leftMargin+1.5*cm, doc.height+doc.bottomMargin-0.6*cm, doc.width-1.5*cm, 0.6*cm, fill=True, stroke=False)
        
        # link "Sommaire"
        sum_left = doc.width+doc.leftMargin-5*cm
        sum_top = doc.height+doc.bottomMargin-2*cm
        canvas.linkAbsolute("Sommaire", "summary",
            (sum_left, sum_top-0.4*cm, sum_left+2*cm, sum_top+1*cm))
        canvas.setFillColor(blue)
        textobject = canvas.beginText()
        textobject.setTextOrigin(sum_left, sum_top)
        textobject.setFont("Helvetica", 10)
        textobject.textOut("Sommaire")
        canvas.drawText(textobject)

        # Obj code and title
        if self.objectif is not None:
            canvas.setFillColor(self.dark_color)
            textobject = canvas.beginText()
            textobject.setTextOrigin(doc.leftMargin+1.8*cm, doc.height+doc.bottomMargin-0.4*cm)
            textobject.setFont("Helvetica-Bold", 10)
            textobject.textOut("%s - %s" % (self.objectif.code, self.objectif.title))
            canvas.drawText(textobject)

        canvas.restoreState()

    def draw_header_left(self, canvas, doc):
        """
        Draw vertical text: objectif extended code + thematique name on the left.
        """
        if self.objectif is None:
            return
        canvas.saveState()
        canvas.setFillColor(self.dark_color)
        canvas.rect(doc.leftMargin, doc.height+doc.bottomMargin-3*cm, 1.5*cm, 3*cm, fill=True, stroke=False)
        # Thematique (vertical text)
        text = self.objectif.get_thematique_name().upper()
        width = stringWidth(text, "Helvetica-Bold", 10)
        textobject = canvas.beginText()
        textobject.setTextOrigin(doc.height+doc.bottomMargin-4*cm-width, -(doc.leftMargin+0.9*cm))
        textobject.setFont("Helvetica-Bold", 10)
        textobject.textOut(text)
        canvas.rotate(90)
        canvas.drawText(textobject)

        # Extended objectif code
        canvas.setFillColor(white)
        text = self.objectif.extended_code
        width = stringWidth(text, "Helvetica-Bold", 10)
        textobject = canvas.beginText()
        textobject.setTextOrigin(doc.height+doc.bottomMargin-width-((3*cm-width)/2), -(doc.leftMargin+0.9*cm))
        textobject.setFont("Helvetica-Bold", 10)
        textobject.textOut(text)
        canvas.drawText(textobject)

        canvas.restoreState()

    def produce(self, response):
        CleanParagraph.current_obj = self.group
        doc = DocTemplate(response, pagesize=landscape(A4),
            topMargin=0.5*cm, bottomMargin=0.5*cm, leftMargin=0.5*cm, rightMargin=0.5*cm)

        doc.pageTemplates[0].onPage = self.draw_header_left
        doc.pageTemplates[1].onPage = self.draw_header

        # First page
        disciplines = "/".join(d.name.upper() for d in self.group.disciplines())
        if self.group.levels:
            levels = [dict(LEVEL_CHOICES)[v] for v in self.group.levels]
            levels = ', '.join([v if len(v) <=7 else v.split()[-1] for v in levels])
            disciplines += " - %s" % levels
        story = [
            Paragraph(disciplines,
                      ParagraphStyle("Disc", fontName="Helvetica", fontSize=60, leading=60,
                                     textColor=self.dark_color, alignment=TA_RIGHT)),
            Spacer(0, 1.5*cm),
            Paragraph('<a name="summary"/><b>%s</b>' % self.group.title,
                      ParagraphStyle("Title", fontName="Helvetica", fontSize=20, leading=26, spaceBefore=30,
                                     borderPadding=20, backColor=self.light_color,)),
        ]
        if self.group.subtitle:
            story.extend([
                Paragraph(self.group.subtitle,
                          ParagraphStyle("Title", fontName="Helvetica", fontSize=14, leading=16,
                                         borderPadding=20, backColor=self.light_color, spaceBefore=20, spaceAfter=20))
            ])
        story.append(Spacer(0, 0.8*cm))
        if self.group.subtitle2:
            story.extend([
                Paragraph(self.group.subtitle2,
                    ParagraphStyle("RDM", fontName="Helvetica", fontSize=20, leading=22, textColor=self.dark_color)),
                Spacer(0, 0.8*cm),
            ])
        descr_style = ParagraphStyle(
            "Descr", fontName="Helvetica", fontSize=12, leading=14,
            borderPadding=20, backColor=self.light_color, spaceBefore=20, spaceAfter=20
        )
        if self.group.description:
            story.append(CleanParagraph(self.group.description, descr_style))

        descr_content = ''
        for theme in self.group.subthemes.all().ordered():
            if theme == 'BREAK':
                continue
            if descr_content:
                descr_content += '<br/>'
            if self.filters and theme.num not in self.filters:
                descr_content += '<font color="gray">' + theme.num + ". " + theme.name + "</font>"
            else:
                descr_content += theme.num + ". " + theme.name
        if descr_content:
            story.append(CleanParagraph(descr_content, descr_style))

        story.append(PageBreak())

        # Other pages with all resources
        for res_link in self.group.ordered_resources(self.user, self.filters):
            self.build_resource(story, res_link.resource, res_link=res_link)

        doc.multiBuild(story)
        self._close()


class ResourceSearchPDF(ResourcePDFBase):
    """
    PDF reprensentation of resource search results.
    """
    def __init__(self, resources, search_criteria=None):
        super().__init__()
        self.search_criteria = search_criteria
        self.resources = resources

    def produce(self, response, title='Ressources'):
        doc = DocTemplate(response, pagesize=landscape(A4),
            topMargin=0.5*cm, bottomMargin=0.5*cm, leftMargin=0.5*cm, rightMargin=0.5*cm)
        title_style = ParagraphStyle(
            "Title", fontName="Helvetica", fontSize=20, leading=26, spaceBefore=30,
            borderPadding=20, alignment=TA_CENTER
        )
        story = [
            # Output some title/header:
            Paragraph(title, title_style),
            Spacer(0, 1*cm),
        ]
        table_cols = [2*cm, A4[0] - doc.leftMargin - doc.rightMargin - 2*cm]
        table_style = TableStyle([
            ('VALIGN', (0,0), (0,-1), 'TOP'),
        ])
        table_data = []
        if self.search_criteria is not None:
            search_crits = ", ".join(["%s : %s" % (k, v) for k, v in self.search_criteria.items()])
            table_data.append(
                [Paragraph("Critères :", self.style_p), Paragraph(search_crits, self.style_p)]
            )
        table_data.append(
            [Paragraph("Date :", self.style_p), Paragraph(format(timezone.now(), "d F Y, H:i"), self.style_p)]
        )
        story.append(Table(table_data, colWidths=table_cols, style=table_style))
        story.append(Spacer(0, 1*cm))

        for res in self.resources.prefetch_related(
                'relations', 'resourcegrouplink_set', 'resourceperlink_set'):
            links = res.resourcegrouplink_set.all()
            self.build_resource(story, res, res_link=links[0] if len(links) == 1 else None)
        doc.multiBuild(story)
        self._close()


class ResourcePDF:
    # Thumbnail is handled separately
    excluded_fields = ('status', 'show_refs', 'thumb', 'thumb_url', 'embed_code')

    def __init__(self, resource):
        self.resource = resource

    def produce(self, response, exclude=None):
        CleanParagraph.current_obj = self.resource
        if exclude is not None:
            excluded_fields = list(self.excluded_fields) + list(exclude)
        else:
            excluded_fields = self.excluded_fields

        doc = SimpleDocTemplate(response, pagesize=A4,
              topMargin=1*cm, bottomMargin=1*cm, leftMargin=1*cm, rightMargin=1*cm)
        style_p = ParagraphStyle('Para', fontName="Helvetica", fontSize=10)
        story = [
            Paragraph("Proposition de ressource pour le Plan d’études romand (%s)" % self.resource.created.strftime("%d.%m.%Y"),
                ParagraphStyle('Para', fontName="Helvetica", fontSize=18, leading=20))
        ]

        thumbnail = ''
        if self.resource.thumb:
            thumbnail = get_image(self.resource.thumb.path, width=3.8*cm, link=self.resource.get_url())
        elif self.resource.thumb_url:
            get_image(self.resource.thumb_url, width=3.8*cm, link=self.resource.get_url())
        if thumbnail:
            story.append(thumbnail)

        group_style = ParagraphStyle('Para', fontName="Helvetica", fontSize=14, leading=16, spaceBefore=12)
        table_cols = [3*cm, A4[0] - doc.leftMargin - doc.rightMargin - 3*cm]
        table_style = TableStyle([
            ('VALIGN', (0,0), (0,-1), 'TOP'),
        ])

        try:
            submitter = self.resource.resourcesubmitter
        except ObjectDoesNotExist:
            submitter = None
        if submitter:
            story.append(Paragraph('<b>Ressource soumise par :</b>', group_style))
            table_data = []
            for field in submitter._meta.get_fields():
                if field.name in ('id', 'resource'):
                    continue
                value = getattr(submitter, field.name)
                if value == '':
                    continue
                table_data.append((
                    Paragraph(field.verbose_name, style_p),
                    Paragraph(value, style_p)
                ))
            story.append(Table(table_data, colWidths=table_cols, style=table_style))

        for group_title, field_names in self.resource.GROUPS:
            table_data = []
            for fname in field_names:
                if fname in excluded_fields:
                    continue
                if fname == 'relations':
                    for label, rel in self.resource.relations_data():
                        table_data.append([
                            Paragraph(label, style_p), Paragraph(rel.as_html(), style_p)
                        ])
                else:
                    value = clean_content(self.resource.get_value(fname))
                    if value == '':
                        continue
                    label = self.resource._meta.get_field(fname).verbose_name
                    table_data.append([
                        Paragraph(label, style_p), CleanParagraph(value, style_p)
                    ])

            if group_title == "Plan d'études":
                struct = self.resource.grouped_per_links()
                content_filters = filters.FilterChain([filters.TxtFilter()])
                cycle_style = ParagraphStyle('Para', fontName="Helvetica", fontSize=12, spaceBefore=8)
                domain_style = ParagraphStyle('Para', fontName="Helvetica", fontSize=12, leftIndent=20, spaceBefore=8)
                disc_style = ParagraphStyle('Para', fontName="Helvetica", fontSize=12, leftIndent=40, spaceBefore=8)
                obj_style = ParagraphStyle('Para', fontName="Helvetica", fontSize=12, leftIndent=60)
                contenu_style = ParagraphStyle('Para', fontName="Helvetica", fontSize=12, leftIndent=80)
                if struct:
                    story.append(Paragraph("<b>Plan d’études</b>", group_style))
                for cycle, domains in struct.items():
                    story.append(Paragraph("CYCLE %d" % cycle, cycle_style))
                    for domain, disciplines in domains.items():
                        story.append(Paragraph(domain.name.upper(), domain_style))
                        for disc, objectifs in disciplines.items():
                            story.append(Paragraph('<b>' + str(disc) + '</b>', disc_style))
                            for objectif, contenus in objectifs.items():
                                story.append(Paragraph('<b>' + str(objectif) + '</b>', obj_style))
                                conts = "".join(
                                    '• %s<br/>' % cont.contenu.render(content_filters, {})
                                    for cont in contenus
                                )
                                if conts:
                                    story.append(Paragraph(conts, contenu_style))

            if table_data:
                story.append(Paragraph('<b>%s</b>' % group_title, group_style))
                story.append(Table(table_data, colWidths=table_cols, style=table_style))
        doc.multiBuild(story)


def linebreaksbr(value):
    return normalize_newlines(value).replace('\n', '<br/>')

def clean_content(value):
    value = str(value)
    if '\n' in value:
        value = value.replace('\n', '<br/>')
    # ReportLab doesn't support target attribute for <a>, no '<br>', no embedded <p></p>
    value = value.replace('<br>', '<br/>')
    value = re.sub(r'</p>[\s]*<p>', '<br/><br/>', value)
    value = re.sub(r'(target|rel)="[\w]*"', '', value)
    if '<a href=' in value:
        value = value.replace('<a href=', '<a color="blue" href=')
    return value
