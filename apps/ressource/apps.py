from django.apps import AppConfig


class RessourceConfig(AppConfig):
    name = 'ressource'

    def ready(self):
        from .pdf_gen import register_fonts
        register_fonts()
