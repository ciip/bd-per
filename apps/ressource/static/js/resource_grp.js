var number_shown = 3;

function showNFirst(num) {
    // Exclude .per-hidden which are hidden by some theme filter
    var entries = $(".resource-wrapper").not('.per-hidden');
    entries.slice(0, num).removeClass('pagin-hidden');
    if (entries.length > num) {
        entries.slice(num).addClass('pagin-hidden');
        $("div#more").show();
    } else {
        $("div#more").hide();
    }
    entries.length > 0 ? $("div#empty").hide() : $("div#empty").show();
    // Count displayed on the "Afficher tout" button
    $("#res-count").text(entries.length);
}

function get_state(state) {
    if (state !== undefined && state !== null) return state;
    if (history.state === undefined || history.state === null) {
        // Default values (no filter (or filters from URL), 3 shown resources)
        return {classes: get_filter_params(), num_shown: number_shown};
    } else {
        return history.state;
    }
}

function get_filter_params() {
    // Get the filters querystring, if present (...?filters=cls_1,cls_2)
    const params = window.location.search.substr(1).split('&');
    var filters = [[], []];
    for (var i=0; i<params.length; i++) {
        var comps = params[i].split('=');
        if (comps.length != 2) continue;
        if (comps[0] == 'filters') {
            const clsList = decodeURIComponent(comps[1].replace(/\+/g, " ")).split(',');
            const themeDivs = $('.themes_container').children('.themes');
            // Re-split classes in groups if there are multiple module groups on the page
            clsList.forEach(cls => {
                for (let i=0; i<themeDivs.length; i++) {
                    if ($(themeDivs[i]).find('div.' + cls).length) {
                        filters[i].push(cls);
                    }
                }
            });
        }
    }
    return filters;
}

function filterTheme(state) {
    // Classes to filter are passed through state.data
    data = get_state(state);
    var filtered = false;
    var tables_to_keep = [];
    data.classes.forEach(clsGroup => {
        if (!clsGroup.length) return;
        const classesToKeep = $.map(clsGroup, function(cls) { return '.' + cls}).join(', ');
        const line_filtered = $.map(clsGroup, function(cls) { return 'div.' + cls});
        const parent_theme = $(line_filtered[0]).closest("div.themes");
        // Set li items style
        parent_theme.children().not(line_filtered.join(', ')).removeClass('filtered');
        $(line_filtered.join(', ')).addClass('filtered');
        parent_theme.children().not(".filtered").css("color", "gray");
        // Set resource table visibility
        if (!filtered) {
            $(".resource-wrapper").not(classesToKeep).addClass('per-hidden');
            $(classesToKeep).removeClass('per-hidden');
        } else {
            // Second filtering pass
            $(".resource-wrapper").not('per-hidden').not(classesToKeep).addClass('per-hidden');
        }
        filtered = true;
    });
    if (!filtered) {
        // Reset and show all
        $("div.themes > div").removeClass('filtered').css("color", "black");
        $(".resource-wrapper").removeClass('per-hidden');
    }
    showNFirst(data.num_shown);
}

$(document).ready(function() {
  $(".filter_wrapper").click(function() {
    $(this).closest('.filter_top').toggleClass('filtered');
    const filter_groups = $(this).closest('div.themes_container').children();
    var classes = [], allClasses = [];
    for (let i=0; i < filter_groups.length; i++) {
        const lines_to_filter = $(filter_groups[i]).find('div.filtered');
        classes.push(lines_to_filter.map(function() {return $(this).data("filter");}).get());
        allClasses.push(...classes[classes.length - 1]);
    }
    history.pushState({classes: classes, num_shown: number_shown}, '', "?filters=" + allClasses.join(','));
    filterTheme();
  });
  $("#btn-more").click(function(ev) {
    ev.preventDefault();
    var hidden_entries = $(".resource-wrapper.pagin-hidden").not('.per-hidden');
    hidden_entries.slice(0, number_shown).removeClass('pagin-hidden');
    $('html, body').animate({scrollTop: $("div#more").offset().top}, 1000);
    if (hidden_entries.length <= number_shown) $("div#more").hide();
    var current_state = get_state();
    history.replaceState({classes: current_state.classes, num_shown: current_state.num_shown + number_shown}, null);
  });
  $("#btn-all").click(function(ev) {
    ev.preventDefault();
    $(".resource-wrapper").not('.per-hidden').removeClass('pagin-hidden');
    $("div#more").hide();
    var current_state = get_state();
    history.replaceState({classes: current_state.classes, num_shown: $(".resource-wrapper").not('.per-hidden').length}, null);
  });
  $("a#pdf-link").click(function(ev) {
    // Limit printed resources to those filtered in the interface.
    ev.preventDefault();
    var url = this.href;
    var classes = $("div.themes div.filtered").map(function() {
        return $(this).data('filter');
    }).get();
    url += "?flt=" + classes.join(',');
    window.location.href = url;
  });

  window.onpopstate = function(event) {
      filterTheme(event.state);
  };
  filterTheme();
  if (window.location.hash) {
    var resTable = $("table#" + window.location.hash.substring(1));
    resTable.removeClass('per-hidden').removeClass('pagin-hidden');
  }

    // ResourceGroup edition form
    $('form#resource_edit').on('click', '.unlink_resource', function(ev) {
        ev.preventDefault();
        var resp = confirm("Voulez-vous vraiment enlever cette ressource de ce groupe ?");
        if (!resp) return;
        var link = $(this);
        $.post(link.attr('href'), {csrfmiddlewaretoken: Cookies.get('csrftoken')}, function(data) {
            link.parents('fieldset').remove();
        });
    });
    $('button#delete-cat').click(function (ev) {
        if (!confirm($(this).data('confirm'))) {
            ev.preventDefault();
            return;
        }
    });
    $('button#delete-all').click(function (ev) {
        ev.preventDefault();
        if (!confirm($(this).data('confirm'))) return;
        $.post($(this).data('url'), {csrfmiddlewaretoken: Cookies.get('csrftoken')}, function(data) {
            $('fieldset.resource').remove();
        });
    });

    $('form#link_resource').on('click', '.submit_local, .submit_bsn', function(ev) {
        // Link resource to group and redisplay group
        ev.preventDefault();
        var self = $(this);
        var pk = self.parent().find('.chosen_result_pk').val();
        if (pk === undefined || pk == '') {
            alert("Veuillez sélectionner une ressource");
            return;
        }
        $.post(this.form.action, {csrfmiddlewaretoken: Cookies.get('csrftoken'), res_pk: pk}, function(data) {
            if (data != '') {
                alert(data);
            } else {
                window.location.reload();
            }
        });
    })
});
