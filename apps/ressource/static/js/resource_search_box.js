/* Using autocomplete from jQuery-UI */

function init_search_boxes() {
    $(".ajax_search_section").each((idx, section) => {
        const searchBox = section.querySelector("input[type=text]");
        const searchURL = searchBox.dataset.autourl;
        $(searchBox).autocomplete({
            source: function(req, resp) {
                $.getJSON(searchURL, {term: req.term}, (data) => {
                    if (data.error) { alert(data.error); }
                    section.querySelector('.result_length').textContent = `${data.total} résultat(s)`;
                    resp(data.results);
                });
            },
            search: function() { section.querySelector('.result_length').textContent = ''; },
            select: function(event, ui) {
                section.querySelector('.chosen_result_pk').value = ui.item.pk;
            }
        });
    });
}

