/* Return a list of input labels inside the section */
function chosen_labels(section) {
    var labels = [];
    if (section.hasClass('dates')) {
        section.find('input[type=date]').each(function(idx) {
            if (this.value) { labels.push(this.value); }
        });
    } else {
        section.find('input:checked').each(function(idx) {
            if ($(this).next()[0] !== undefined && $(this).next()[0].tagName == 'LABEL') {
                // Label is *after* input, we can trust its entire content
                labels.push($(this).next().html());
            } else labels.push($("label[for='"+this.id+"']").text().trim());
        });
    }
    return labels;
}

function update_filter_state(parent) {
    var choice_labels = chosen_labels(parent);
    var state_span = parent.find('span.filter_state');
    if (choice_labels.length)
        state_span.html(choice_labels.join(', ')).addClass('active');
    else state_span.text(state_span.data('default')).removeClass('active');
}

function update_visible_disciplines() {
    /* Depending on chosen domains, hide disciplines not in chosen domains
       and uncheck hidden disciplines.
    */
    var chosen_ids = [];
    $("ul#id_domains").find('input:checked').each(function(idx) {
        chosen_ids.push(this.value);
    });
    if (chosen_ids.length) {
        $("ul#id_disciplines li").hide();
        var all_visible_discs = [];
        for(var i=0; i<chosen_ids.length; i++) {
            // domains array is globally defined in ressource/home.html
            var disc_to_show = domains[chosen_ids[i]];
            $.each(disc_to_show, function(idx, value) {
                $("ul#id_disciplines").find("input[value=" + value + "]").closest('li').show();
            });
            all_visible_discs = all_visible_discs.concat(disc_to_show);
        }
        // Unselect disciplines from unchosen domains
        $("ul#id_disciplines input:checked:hidden").each(function() {
            if (all_visible_discs.indexOf(this.value) < 0)
                $(this).prop('checked', false);
        });
        update_filter_state($("ul#id_disciplines").closest('div.filter_div'));
    } else {
        $("ul#id_disciplines li").show();
    }
}

function storePage(numP) {
    // Only keep one history state, intermediary states do not interest us.
    if (location.search.indexOf('&p=') !== -1 || location.search.indexOf('?p=') !== -1) {
        var url = location.pathname + location.search.replace(/([&|?])p=\d*/, '$1p=' + numP);
    } else {
        var sep = (location.search == '') ? '?' : '&';
        var url = location.pathname + location.search + sep + 'p=' + numP;
    }
    if (history.state) {
        history.replaceState({p: numP}, '', url);
    } else {
        history.pushState({p: numP}, '', url);
    }
}

$(document).ready(function() {
    $('span.filter_label').click(function (ev) {
        var span = $(this);
        span.parent().find('div.filter_choices').toggle(200, function () {
            if ($(this).is(':visible')) span.text('▼' + span.text().substring(1));
            else span.text('▶' + span.text().substring(1));
            span.next().toggle();
        });
    });
    $("div.filter_choices input").change(function() {
        update_filter_state($(this).closest('div.filter_div'));
    });
    $("ul#id_domains input").change(function() {
        update_visible_disciplines();
    });

    $("button#reinit").click(function(ev) {
        var rep = confirm("Souhaitez-vous vraiment réinitialiser le formulaire ?");
        if (!rep) return;
        $(this).closest('form').trigger("reset");
        setTimeout(function() {
            $('div.filter_div').each(function(i) {
                update_filter_state($(this));
            });
        }, 1);
    });

    $("#btn-more-search").click(function(ev) {
        ev.preventDefault();
        var nextP = $("div#more").data("nextpage");
        var url = $("div#more").data("searchurl") + '&p=' + nextP + '&output=naked';
        $("div#progress").show();
        $.get(url, function (data) {
            $("div#progress").hide();
            $("div#results").append(data);
            $("div#more").data("nextpage", nextP + 1);
            storePage(nextP);
            // TODO: hide more button if all resources are shown
        });
    });
    $("#btn-all-search").click(function(ev) {
        ev.preventDefault();
        var nextP = $("div#more").data("nextpage");
        var url = $("div#more").data("searchurl") + '&p=' + nextP + '&output=naked&remain=true';
        $("div#progress").show();
        $.get(url, function (data) {
            $("div#progress").hide();
            $("div#results").append(data);
            $("div#more").hide();
            storePage(nextP);
        });
    });
    $("a#pdf-link").click(function(ev) {
        ev.preventDefault();
        var title = prompt("Choisissez un titre pour votre liste", $(this).data('title'));
        if (title == null || title == "") return;
        var url = this.href;
        url += "&title=" + encodeURIComponent(title);
        window.location.href = url;
    });

    // Set initial state
    $("div.filter_div").each(function(idx) {
        update_filter_state($(this));
    });
    update_visible_disciplines();
});
