/* Included in pages showing resource references */
jQuery(document).ready(function(){
  jQuery('img.toggle-rsrc').click(function(ev) {
    var ul = $(this).next().next();
    // Switch images
    var src = jQuery(this).attr("src");
    var altsrc = jQuery(this).data("altsrc");
    jQuery(this).attr("src", altsrc);
    jQuery(this).data("altsrc", src);

    ul.slideToggle();
  });
});
