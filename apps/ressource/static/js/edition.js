"use strict";

/* This function adds a new per-link widget, based on the empty_form template
 * included in the form.
 */
function addPerlinkItem() {
    var count = $('#plan-detudes-formset-container').children().length;
    var tmplMarkup = $('#plan-detudes-template').html();
    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
    $('div#plan-detudes-formset-container').append(compiledTmpl);
    // update form count
    $('#id_resourceperlink_set-TOTAL_FORMS').attr('value', count+1);
}

$(document).ready(function(){
    $("ul.ftabs li, div.tab-navigation a").click(function(ev) {
        $('.form-group').removeClass('selected');
        $("ul.ftabs li").removeClass('selected');
        var target = $(this).data('target');
        $('li#tab-' + target).addClass('selected');
        $('#' + target).addClass('selected');
    });
    $('a.link-anchor').on('click', function (ev) {
        $('.content-panel').hide();
        $('#' + $(this).data('target')).show();
    });
    $('button.continue').on('click', function (ev) {
        $(this).closest('.content-panel').hide();
        $('div#form-main').show();
    });
    $('button.back').on('click', function (ev) {
        $(this).closest('.content-panel').hide();
        $('div#form-main').show();
    });
    $('button.cancel').on('click', function (ev) {
        ev.preventDefault();
        let ok = true;
        if ($(this).hasClass('suggestion'))
            ok = confirm("Souhaitez-vous vraiment annuler la soumission de cette ressource ?");
        if (ok) {
            const next = $('input[name="next"]').val();
            if (next.length) window.location.href = next;
            else window.location.href = '/';
        }
    });
    $('form#resource_edit').on('submit', function (ev) {
        const conf_div = $('div#confirmation');
        if (conf_div.length) {
            if (!$('div#confirmation').is(':visible')) {
                ev.preventDefault();
                $('div#form-main').hide();
                $('div#confirmation').show();
            } else {
                // Check input validity of the submitter part.
                $('div#confirmation input').each(function(idx, el) {
                    if (!el.checkValidity()) {
                        ev.preventDefault();
                        el.reportValidity();
                    }
                });
            }
        }
    });
    $('img.form-delete').on('click', function (ev) {
        const msg = $(this).data('confirm') || "Voulez-vous vraiment supprimer cette entrée ?";
        if (confirm(msg)) {
            const parent = $(this).closest('fieldset');
            parent.find("input[name$='DELETE']").val("on");
            parent.hide();
        }
    });
    $('form').on('click', '.parent-delete', function (ev) {
        $(this).parent().remove();
    });

    $('select#content-type-selector').change(function (ev) {
        $('#input-url').hide();
        $('#input-files').hide();
        $('#input-gallery').hide();
        if ($(this).val() == 'gallery') {
            $('#input-gallery').show();
            $('#input-files').show();
        } else $('#input-' + $(this).val()).show();
    });
    $('button#add-image-to-gallery').click(function (ev) {
        ev.preventDefault();
        if (!$('#resource_pk').val()) return;
        const form_idx = $('#id_gallery-TOTAL_FORMS').val();
        $('#gallery-resources').append($('#empty_form').html().replace(/__prefix__/g, form_idx));
        $('#id_gallery-TOTAL_FORMS').val(parseInt(form_idx) + 1);
        // Populate new formset
        const otherInput = $('#id_gallery-' + parseInt(form_idx) + '-other');
        otherInput.val($('#resource_pk').val());
        otherInput.parent().find('.image-title').html($('#resource_search_box').val());
        // Reinit search box
        $('#resource_search_box').val('');
        $('#resource_pk').val('');
    });

    $('input[id^=id_files]').change(function (ev) {
        // Set title with file name without extension
        if (this.files.length > 1) {
            var title = '';
        } else if (this.files.length == 1) {
            var title = this.files[0].name.replace(/\.[^/.]+$/, "");
        } else var title = '';
        const titleId = this.id.replace('rfile', 'title');
        document.getElementById(titleId).value = title;
    });

    // Custom widget for ResourcePerLink content
    $("div#plan-detudes-formset-container").on('click', 'button.per-link-delete', function (ev) {
        ev.preventDefault();
        // Mark a formset item for deletion with a button
        if (confirm("Souhaitez-vous vraiment supprimer ce lien avec le Plan d'études ?")) {
            $('input#' + $(this).data('target')).prop("checked", true);
            $(this).closest('fieldset').hide();
        }
    });
    $("div#plan-detudes-formset-container").on('click', 'button.per-link-add', function (ev) {
        // Button "Ajouter le(s) lien(s)"
        ev.preventDefault();
        var choices = $(this).closest('fieldset').find('input:checked');
        if (!choices.length) {
            alert("Vous devez d'abord cocher une ou plusieurs cases.");
            return;
        }
        var perChoiceElement = choices[0].closest('.per-choice');
        // Extract and display the chosen items before the widget, then hide it
        $.each(choices, function(key, choice) {
            perChoiceElement.before($(choice).parent()[0]);
        });
        $(perChoiceElement).hide();
        $(this).hide();
        var delWidget = $(this).closest('fieldset').find('.delete-widget');
        delWidget.show();
        addPerlinkItem();  // Allow more selection with new widget
    });
    $("div#plan-detudes-formset-container").on('click', 'div.per-choice-label', function (ev) {
        var thisWidget = $(this).closest('ul.per-link');
        var widgetName = thisWidget.data('name');

        function getCurrentCycle() {
            return parseInt(
                thisWidget.find('li.per-link-cycle').find('span.value').data('value')
            );
        }

        const choice_ul = $(this).closest('ul');
        const element_value = $(this).closest('li').data('value');
        choice_ul.parent().find('span.value').text($(this).text()).data('value', element_value);
        choice_ul.hide();
        // Populate next level
        const nextLevel = choice_ul.parent().next();
        let url = nextLevel.data('url');
        if (url === undefined) return;
        if (url.indexOf("/0") > -1) url = url.replace(/\/0/, '/' + element_value);
        return fetch(url).then(resp => resp.json()).then(data => {
            const container = nextLevel.find('.per-choice');
            container.empty();
            if (nextLevel.hasClass('progressions')) {
                $.each(data.tableau, function(key, line) {
                    if (line[0].type == "en-tête") return;
                    var tr = "<tr>";
                    $.each(line, function(key2, cell) {
                        if (cell.type != "progression" && cell.type != "titre") return;
                        var td = '<td colspan="' + cell.colspan + '" rowspan="' + cell.rowspan + '" class="'+ cell.type + '">';
                        $.each(cell.contenus, function(key3, content) {
                            td += '<div><input type="checkbox" name="' + widgetName + '" value="contenucellule-' + content.id_cc + '"> ' + content.texte + '</div>';
                        });
                        td += "</td>";
                        tr += td;
                    });
                    tr += "</tr>";
                    container.append(tr);
                });
            } else {
                const currentCycle = getCurrentCycle();
                nextLevel.find('span.value').text('');
                container.show();
                if (data.disciplines !== undefined) data = data.disciplines;
                $.each(data, function(key, val) {
                    if (currentCycle && val.cycle && currentCycle != val.cycle) {
                        return;  // N/A for this cycle
                    }
                    var label = val.code ? val.code + " " + val.nom : val.nom;
                    var li = '<li data-value="' + val.id + '">';
                    if (nextLevel.hasClass('objectifs')) {
                        li += '<input type="checkbox" name="' + widgetName + '" value="specification-' + val.id + '"> ';
                    } else if (nextLevel.hasClass('disciplines')) {
                        li += '<input type="checkbox" name="' + widgetName + '" value="discipline-' + val.id + '"> ';
                    }
                    li += '<div class="per-choice-label">' + label + '</div></li>';
                    container.append(li);
                });
            }
            container.parent().show();
        });
    });
    $('div#plan-detudes-formset-container').on('click', 'span.per-link-levellabel', function (ev) {
        if ($(this).parent().hasClass('progressions')) return;
        $(this).next().text('');
        $(this).parent().find("ul.per-choice").show();
        $(this).parent().nextAll("li.per-link-level").hide();
        $(this).parent().parent().find("input:checked").each(function() {
            $(this).prop('checked', false);
        });
    });

    // Manually sortable elements
    var sortables = document.querySelectorAll(".sortable");
    for (var i = 0; i < sortables.length; i++) {
        new Sortable(sortables[i], {handle: '.handler', onEnd: function (evt) {
            // Update values of the order inputs
            var orders = evt.to.querySelectorAll('input[name$=order]');
            for (var i = 0; i < orders.length; i++) {
                // if (!isFormEmpty())the line is not empty
                orders[i].value = i + 1;
            }
        }});
    }

    init_search_boxes();
});

$(document).scroll(function() {
    if (!$('.form-tabs').length) return;
    // Ensure the form tabs are always visible at the top of the window.
    $('.form-tabs').css('position', '');
    $('.form-tabs').css('top', '');
    $('.form-tabs').css('width', '');
    const top = $('.form-tabs')[0].getBoundingClientRect().top;
    if (top < 0) {
        const width = $('.form-tabs').width();
        $('.form-tabs').next().css('margin-top', 60);
        $('.form-tabs').css('position', 'fixed');
        $('.form-tabs').css('top', 0);
        $('.form-tabs').css('width', width);
    } else {
        $('.form-tabs').next().css('margin-top', '');
    }
});
