/* selection is the current list of selected resources. */
var selection = {
    cookieName: 'resource_selection',
    setUp: function () {
        this.current = Cookies.get(this.cookieName) || [];
        if (this.current.length) {
            this.current = this.current.split(',');
            // Search selected in current display and update state
            for (var i=0; i<this.current.length; i++) {
                var tbl = $('table#res' + this.current[i]);
                if (tbl.length) {
                    this.swapState(tbl.find('img.add-to-select'));
                }
            }
        }
        this.updateCount();
    },

    updateCount: function () {
        const countDiv = document.getElementById('select-count'),
              labelDiv = document.getElementById('select-label');
        if (!countDiv) return;
        if (this.current.length) {
            var url = labelDiv.getElementsByTagName('a')[0].href;
            countDiv.innerHTML = '<a href="' + url + '">' + this.current.length + '</a>';
            labelDiv.classList.remove('nolink');
        } else {
            countDiv.innerHTML = this.current.length;
            labelDiv.classList.add('nolink');
        }
    },

    addOrRemove: function (resId) {
        var wasSelected = this.current.indexOf(resId) >= 0;
        if (wasSelected) this.current.splice(this.current.indexOf(resId), 1);
        else this.current.push(resId);
        Cookies.set(this.cookieName, this.current.join());
        this.updateCount();
    },

    clear: function () {
        Cookies.remove(this.cookieName);
        this.current = [];
        this.updateCount();
    },

    swapState: function (img) {
        // Swap img source and title
        var src = img.attr('src');
        var title = img.attr('title');
        img.attr('src', img.data('altsrc'));
        img.attr('title', img.data('alttitle'));
        img.data('altsrc', src);
        img.data('alttitle', title);
        img.closest('table').find('table.resource_block').toggleClass('selected');
    }
};

$(document).ready(function() {
    // Open/close top-right dropdown menu.
    $('div.ddLabel').click(function (e) {
        $(this).next().toggle();
    });
    $('img.help-icon').click(function (e) {
        $('#' + $(this).data('target')).dialog();
    });
    $('img.video-start').click(function (e) {
        var videoDiv = $(this).closest('table').find('div.media-container');
        var videoLine = videoDiv.closest('tr');
        if (videoLine.is(":hidden")) {
            videoDiv.load(videoDiv.data('url'));
            videoLine.show();
        }
    });
    $('div.video-closer').click(function (e) {
        var videoDiv = $(this).closest('table').find('div.media-container');
        var videoLine = videoDiv.closest('tr');
        videoDiv.html('');
        videoLine.hide();
    });

    selection.setUp();
    $("div#content-wrapper").on('click', "img.add-to-select", function (ev) {
        selection.addOrRemove($(this).data('resid').toString());
        selection.swapState($(this));
    });
    document.querySelectorAll('.resource-link').forEach(link => {
        link.addEventListener('click', (ev) => {
            var data = new FormData();
            data.append("csrfmiddlewaretoken", Cookies.get('csrftoken'));
            data.append("pk", ev.currentTarget.dataset.pk);
            fetch(`/ressources/stats/ping/`, {method: 'POST', body: data});
        });
    });
});
