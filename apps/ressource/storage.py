import mimetypes
import shutil
from tempfile import SpooledTemporaryFile
from urllib.parse import urljoin

import boto3
from botocore.exceptions import ClientError

from django.conf import settings
from django.core.files.base import File
from django.core.files.storage import FileSystemStorage, Storage
from django.core.files.storage import Storage
from django.utils.encoding import filepath_to_uri
from django.utils.functional import cached_property


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, **kwargs):
        if self.exists(name):
            self.delete(name)
        return super().get_available_name(name, **kwargs)


class ExoS3Storage(Storage):
    def __init__(self, **options):
        super().__init__()
        if options.get('MOCK_CLASS'):
            self.s3 = options['MOCK_CLASS']()
        self.endpoint = options['ENDPOINT']
        self.region_name = options['REGION_NAME']
        self.bucket_name = options['BUCKET']
        self._key = options['KEY']
        self._secret = options['SECRET']

    @cached_property
    def s3(self):
        return boto3.client(
            's3', endpoint_url=self.endpoint, region_name=self.region_name,
            aws_access_key_id=self._key, aws_secret_access_key=self._secret,
        )

    def exists(self, name):
        try:
            self.s3.head_object(Bucket=self.bucket_name, Key=name)
            return True
        except ClientError as err:
            if err.response["ResponseMetadata"]["HTTPStatusCode"] == 404:
                return False
            # Some other error was encountered. Re-raise it.
            raise

    def listdir(self, path):
        contents = self.s3.list_objects_v2(Bucket=self.bucket_name, Prefix=path)
        if contents['KeyCount'] == 0:
            return []
        return [(path, item['Key']) for item in contents['Contents']]

    def _save(self, name, content):
        kwargs = {}
        content_type, _ = mimetypes.guess_type(name, strict=False)
        content_type = content_type or "application/octet-stream"
        kwargs["ExtraArgs"] = {"ContentType": content_type}
        if 'image' in content_type:
            kwargs["ExtraArgs"]["ContentDisposition"] = 'inline'
        if self._is_path_public(name):
            kwargs["ExtraArgs"]["ACL"] = 'public-read'
        self.s3.upload_fileobj(content, self.bucket_name, name, **kwargs)
        return name

    def _open(self, name, mode="rb"):
        if mode not in ("rb", "rt", "r"):
            raise ValueError("S3 files can only be opened in read-only mode")
        obj = self.s3.get_object(Bucket=self.bucket_name, Key=name)
        content = SpooledTemporaryFile(max_size=1024 * 1024 * 10)  # 10 MB.
        shutil.copyfileobj(obj["Body"], content)
        content.seek(0)
        return File(content, name)

    def _is_path_public(self, name):
        return name.startswith('resource_thumbs/')

    def delete(self, name):
        self.s3.delete_object(Bucket=self.bucket_name, Key=name)

    def size(self, name):
        meta = self.s3.head_object(Bucket=self.bucket_name, Key=name)
        return meta["ContentLength"]

    def url(self, name):
        if self._is_path_public(name):
            return urljoin(
                settings.S3_PUBLIC_URL.format(bucket=self.bucket_name),
                filepath_to_uri(name)
            )
        response = self.s3.generate_presigned_url(
            'get_object',
            Params={'Bucket': self.bucket_name, 'Key': name},
            ExpiresIn=300
        )
        # The response contains the presigned URL
        return response


class FallbackStorage(Storage):
    """
    Try to first on local filesystem storage, then fallback to S3 storage if
    file not found locally.
    Saving is giving priority to S3 storage.
    """
    def __init__(self, **options):
        self.s3_storage = ExoS3Storage(**options)
        fs_options = {
            k:v for k, v in options.items()
            if k not in ['MOCK_CLASS', 'ENDPOINT', 'REGION_NAME', 'BUCKET', 'KEY', 'SECRET']
        }
        self.fs_storage = OverwriteStorage(**fs_options)

    def move_to_s3(self, name):
        with self.fs_storage._open(name) as fsfile:
            self._save(name, fsfile)
        self.fs_storage.delete(name)

    def _open(self, name, mode="rb"):
        # Try first on local file system, then fallback to S3.
        if self.fs_storage.exists(name):
            return self.fs_storage._open(name, mode=mode)
        return self.s3_storage._open(name, mode=mode)

    def _save(self, name, content):
        # Give priorty to s3 storage for newly saved files.
        return self.s3_storage._save(name, content)

    def get_available_name(self, name, **kwargs):
        # Overwrite with same name instead of Django new name's default.
        if self.exists(name):
            self.delete(name)
        return super().get_available_name(name, **kwargs)

    def exists(self, name):
        return self.fs_storage.exists(name) or self.s3_storage.exists(name)

    def delete(self, name):
        if self.fs_storage.exists(name):
            self.fs_storage.delete(name)
        else:
            self.s3_storage.delete(name)

    def size(self, name):
        if self.fs_storage.exists(name):
            return self.fs_storage.size(name)
        return self.s3_storage.size(name)

    def path(self, name):
        if self.fs_storage.exists(name):
            return self.fs_storage.path(name)
        raise NotImplementedError()

    def url(self, name):
        if self.fs_storage.exists(name):
            return self.fs_storage.url(name)
        return self.s3_storage.url(name)
