from linkcheck import Linklist
from ressource.models import Resource, ResourceRelation


class ResourceLinklist(Linklist):
    model = Resource
    object_filter = {'broken': False}
    object_exclude = {
        'need_auth': True,
        'url__contains': 'vod.infomaniak',
        'url__contains': 'pedagogie.edu-vd.ch',
    }
    url_fields = ['url']
    ignore_empty = ['url']
    #html_fields = ['content', 'extra_content']


class RelationLinkList(Linklist):
    model = ResourceRelation
    url_fields = ['url']
    ignore_empty = ['url']


linklists = {
    'Ressources': ResourceLinklist,
    'Relations': RelationLinkList,
}
