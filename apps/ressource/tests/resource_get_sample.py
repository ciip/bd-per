sample_result = {
    'general': {
        'identifier': [{'catalog': 'URL',
                        'entry': 'http://www.globaleducation.ch/globaleducation_fr/pages/MA/MA_displayDetails?L=fr&Q=detail&MaterialID=1003314',
                        'title': {'fr': u"L'eau : plus qu'un jeu"}}
                      ],
        'title': {'fr': "L'eau : plus qu'un jeu"},
        'language': ['fr'],
        'description': [{'fr': 'Jeu \xe9ducatif dont  l\u2019objectif est d\u2019am\xe9nager un territoire (urbanisation, industrialisation, agriculture biologique, barrage hydro\xe9lectrique, etc.), durant 80 ans, avec un budget donn\xe9 et en maintenant les variables \xabqualit\xe9 de vie\xbb, \xabforce \xe9conomique\xbb et \xabdiversit\xe9 des esp\xe8ces\xbb \xe0 un niveau aussi \xe9lev\xe9 que possible, et en faisant face \xe0 des intemp\xe9rie ou autres difficult\xe9s qui se produisent au hasard. '}],
        'keyword': [{'fr': 'D\xe9veloppement durable'}],
        'coverage': [],
        'aggregationLevel': {
            'source': 'LOMv1.0',
            'value': '3 '},  # Don't ask why a trailing space...
    },
    'lifeCycle': {
       'contribute': [
          {'entity': [   u"BEGIN:VCARD\r\nVERSION:3.0\r\nFN: \r\nN:;;;;\r\nADR;WORK:;;;Bern;;3003;Suisse\r\nTEL;WORK;VOICE:+41313229311\r\nEMAIL;PREF;INTERNET:info@bafu.admin.ch\r\nORG:Office F\xe9d\xe9ral de l'Environnement OFEV;;\r\nURL;PREF:http://www.bafu.admin.ch/index.html?lang=fr\r\nUID:38f28ceafa72eecd2a437e8da7d0c1eb\r\nEND:VCARD"],
           'role': {'source': 'LOMv1.0',
                    'value': 'publisher'},
          }
        ],
        'version': {'en': '2012'}
    },
    'metaMetadata': {
        'identifier': [{'catalog': 'ARCHIBALD',
                        'entry': 'f041bbd2b6939aeffcc39612d17004ac'}],
        'contribute': [
          {'date': '2013-10-11T13:06:15+00:00',
           'entity': ['BEGIN:VCARD\r\nVERSION:3.0\r\nFN:\r\nN:;;;;\r\nLOGO;VALUE:uri:http://dsb-api.educa.ch/v2/file/default/24ea00165920b7cea133cea17908321e.jpeg\r\nORG:default;;\r\nEND:VCARD'],
           'role': {'source': 'LOM-CHv1.1',
                    'value': 'creator'}
          },
          {'date': '2013-10-15T15:28:11+00:00',
           'entity': ['BEGIN:VCARD\r\nVERSION:3.0\r\nFN:Val\xe9rie Pidoux-Nyffeler\r\nN:Pidoux-Nyffeler;Val\xe9rie;;;\r\nADR;WORK:;;;Bern;;3011;Suisse\r\nEMAIL;PREF;INTERNET:valerie.pidoux-nyffeler@education21.ch\r\nORG:education21;;\r\nURL;PREF:http://www.education21.ch\r\nUID:e541f989f0ab6f4f355d1663272cc36f\r\nEND:VCARD'],
           'role': {'source': 'LOMv1.0',
                    'value': 'creator'}
          },
          {'date': '2011-07-21T16:40:49+00:00',
           'role': {'source': 'LOMv1.0',
                    'value': 'creator'
                },
            'entity': []
          }
        ],
        'language': 'fr',
        'metaDataSchema': ['LOM-CHv1.2'],
    },
    'technical': {
        'duration': 'PT12M42S',
        'location': [''],
        'otherPlatformRequirements': {   },
        'previewImage': {'copyright': '',
                         'image': 'http://dsb-api.educa.ch/v2/file/default/538f2155a25b510a665c415f511a24ff.jpeg'},
        'size': 0,
        'format': ['pdf'],
    },
    'education': [{
        'learningResourceType': {
            'documentary': [
                {'source': 'LREv3.0',
                 'value': 'website'}
            ],
            'pedagogical': [
                {'source': 'LREv3.0',
                 'value': 'educational game'},
            ]
        },
        'intendedEndUserRole': [
            {'source': 'LOMv1.0',
             'value': 'teacher'}
        ],
        'context': [
            {'source': 'LREv3.0',
             'value': 'compulsory education'},
            {'source': 'LOM-CHv1.0',
             'value': 'post-compulsory education'}
        ],
        'description': [{
            'fr': u"Jeu \xe9ducatif \xe0 t\xe9l\xe9charger gratuitement sur le site de l\u2019OFEV. \xe0 proposer aux \xe9l\xe8ves en classe ou \xe0 la maison.  Exploitable en cours de g\xe9ographie, d'\xe9conomie, de formation g\xe9n\xe9rale, de sciences ou de math\xe9matiques. Les r\xe8gles sont simples et l\u2019interface graphique, d\xe9coup\xe9e en zones d\u2019intervention comme sur un \xe9chiquier, est agr\xe9able. L\u2019\xe9l\xe8ve se rend vite compte qu'une politique de gestion de l'eau touche plusieurs domaines et que la seule fa\xe7on d\u2019\xeatre efficace est de tenir compte des relations entre ces divers domaines (am\xe9nagement du territoire, agriculture, \xe9nergie, \xe9conomie, approvisionnement de la population en eau potable, modernisation des stations d\u2019\xe9puration, protection des habitats contre les crues ou contre l\u2019infiltration des eaux de pluie, pratiques agricoles et industrielles respectueuses de l\u2019environnement, protection des nappes phr\xe9atiques, production d\u2019\xe9nergie hydro\xe9lectrique, renaturation des cours d\u2019eau, etc.)"
        }],
        'difficultyLevel': {'source': 'LOM-CHv1.1',
                            'value': 'easy'},
        'typicalAgeRange': [
            {'0': '7-12'}
        ],
        'typicalLearningTime': {
            'learningTime': {'source': 'LOM-CHv1.1',
                             'value': ''}
        },
    }],
    'rights': {
        'cost': {
            'source': 'LOMv1.0',
            'value': 'no'},
        'copyright': [{
            'description': {'de': 'Alle Rechte vorbehalten',
                            'en': 'all rights reserved',
                            'fr': 'Tous droits r\xe9serv\xe9s'}
        }],
    },
    'relation': [{
        'kind': {
            'source': 'LOMv1.0',
            'value': 'has_part_of'},
        'resource': [{
            'description': {'fr': 'Mat\xe9riel p\xe9dagogique (en format PDF) de Michael Andres. Traduction : Martine Besse '},
            'identifier': {
                'catalog': 'URL',
                'entry': 'http://www.filmeeinewelt.ch/francais/files/40217.pdf'
            }
        }]
    }],
    'classification': [
        {
            'purpose': {
                'source': 'LOMv1.0',
                'value': 'educational level'
            },
            'taxonPath': [{'source': {'en': 'educa'},
                           'taxon': [{'entry': {'de': 'Obligatorische Schule',
                                                'en': 'Compulsory education',
                                                'fr': '\xc9cole obligatoire'},
                                      'id': 'compulsory education'},
                                     {'entry': {'de': '3. Zyklus (9. bis 11. Schuljahr)',
                                                'en': '3rd cycle (9th to 11th school year)',
                                                'fr': '3e cycle (9\xe8me \xe0 11\xe8me ann\xe9e scolaire)'},
                                      'id': 'cycle 3'},
                                     {'entry': {'en': 'Sport', 'fr': 'Corps et mouvement'},
                                      'id': 'sport'},
                                    ]
                          }]
        }
    ],
  "curriculum": [
    {
      "source": {
        "name": "per",
        "hierarchy": [
          {
            "type": "cycles",
            "childTypes": [
              "domaines"
            ],
            "entry": {
              "fr": "Cycle"
            }
          },
          {
            "type": "domaines",
            "childTypes": [
              "disciplines"
            ],
            "entry": {
              "fr": "Domaine"
            }
          },
          {
            "type": "disciplines",
            "childTypes": [
              "objectifs"
            ],
            "entry": {
              "fr": "Discipline"
            }
          },
          {
            "type": "objectifs",
            "childTypes": [
              "progressions"
            ],
            "entry": {
              "fr": "Objectif"
            }
          },
          {
            "type": "progressions",
            "childTypes": [],
            "entry": {
              "fr": "Progressions d'apprentissage"
            }
          }
        ]
      },
      "taxonTree": [
        {
            "id": "3",
            "type": "cycles",
            "purpose": {
                "source": "LOM-CHv1.2",
                "value": "educational level"
            },
            "entry": {
                "fr": "Cycle 3"
            },
            "childTaxons": [
                {
                    "id": "1",
                    "type": "domaines",
                    "purpose": {
                        "source": "LOM-CHv1.2",
                        "value": "discipline"
                    },
                    "entry": {
                        "fr": "Langues"
                    },
                    "childTaxons": []
                }
            ]
        },
        {
          "id": "3",
          "type": "cycles",
          "purpose": {
            "source": "LOM-CHv1.2",
            "value": "educational level"
          },
          "entry": {
            "fr": "Cycle 3"
          },
          "childTaxons": [
            {
              "id": "6",
              "type": "domaines",
              "purpose": {
                "source": "LOM-CHv1.2",
                "value": "discipline"
              },
              "entry": {
                "fr": "MSN"
              },
              "childTaxons": [
                {
                  "id": "17",
                  "type": "disciplines",
                  "purpose": {
                    "source": "LOM-CHv1.2",
                    "value": "discipline"
                  },
                  "entry": {
                    "fr": "Mathématiques"
                  },
                  "childTaxons": [
                    {
                      "id": "137",
                      "type": "objectifs",
                      "purpose": {
                        "source": "LOM-CHv1.2",
                        "value": "objective"
                      },
                      "entry": {
                        "fr": "Analyser des phénomènes naturels (MSN 36)"
                      },
                      "childTaxons": []
                    }
                  ]
                },
                {
                  "id": "137",
                  "type": "objectifs",
                  "purpose": {
                    "source": "LOM-CHv1.2",
                    "value": "objective"
                  },
                  "entry": {
                    "fr": "Analyser des phénomènes naturels (MSN 36)"
                  },
                  "childTaxons": []
                }
              ]
            },
            {
              "id": "1",
              "type": "domaines",
              "purpose": {
                "source": "LOM-CHv1.2",
                "value": "discipline"
              },
              "entry": {
                "fr": "Langues"
              },
              "childTaxons": [
                {
                  "id": "1",
                  "type": "disciplines",
                  "purpose": {
                    "source": "LOM-CHv1.2",
                    "value": "discipline"
                  },
                  "entry": {
                    "fr": "Fran\u00e7ais"
                  },
                  "childTaxons": [
                    {
                      "id": "8",
                      "type": "objectifs",
                      "purpose": {
                        "source": "LOM-CHv1.2",
                        "value": "objective"
                      },
                      "entry": {
                        "fr": "Production de l&#039;\u00e9crit (L 11)"
                      },
                      "childTaxons": []
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ],
    'lomId': 'archibald###f041bbd2b6939aeffcc39612d17004ac',
    'updateDate': '1688713504',
    'ownerUsername': 'info@education21.ch',
}

example2 = {
 'lomId': 'archibald###d223ffba495c089d123e90e4a919d20d',
 'ownerUsername': 'info@friportail.ch',
 'general': {'coverage': [{'fr': 'Fribourg (Suisse)'}],
  'description': [{'fr': u"Cette ressource propose de nombreuses activit\xe9s autour des sculptures en plein air de la ville de Fribourg: rencontrer l'art au coin de la rue, regarder diff\xe9remment ce qui est sous nos yeux tous les jours, d\xe9couvrir des artistes."}],
  'identifier': [{'catalog': 'URL',
    'entry': 'http://www.artfribourg.ch/'}],
  'keyword': [{'fr': 'SEnOF-ACMC2'},
   {'fr': 'Enseignement primaire'},
   {'fr': 'SEnOF-ACMC1'},
   {'fr': 'Ressource recommand\xe9e Fribourg'},
   {'fr': 'Art'},
   {'fr': 'Sculpture'}],
  'language': ['fr'],
  'title': {'fr': u"L'art en ville : \xe0 la d\xe9couverte d'une sculpture"}},
 'classification': [{'purpose': {'source': 'LOMv1.0',
    'value': 'discipline'},
   'taxonPath': [{'source': {'en': 'classification system'},
     'taxon': [{'entry': {'de': 'Musik, Kunst und Gestalten',
        'en': 'Arts',
        'fr': 'Arts',
        'it': 'Arte',
        'rm': 'Arts'},
       'id': 'arts'},
      {'entry': {'de': 'Bildnerisches Gestalten',
        'en': 'Visual arts',
        'fr': 'Arts visuels',
        'it': 'Arti visive',
        'rm': 'Roh_Visual arts'},
       'id': 'visual arts'}]},
    {'source': {'en': 'classification system'},
     'taxon': [{'entry': {'de': 'Musik, Kunst und Gestalten',
        'en': 'Arts',
        'fr': 'Arts',
        'it': 'Arte',
        'rm': 'Arts'},
       'id': 'arts'},
      {'entry': {'de': 'Textiles und Technisches Gestalten',
        'en': 'Art, Craft and Design',
        'fr': 'Activit\xe9s cr\xe9atrices et manuelles',
        'it': 'Attivit\xe0 creative',
        'rm': 'Roh_Creative activities'},
       'id': 'art craft design'}]}]},
  {'purpose': {'source': 'LOMv1.0', 'value': 'educational level'},
   'taxonPath': [{'source': {'en': 'classification system'},
     'taxon': [{'entry': {'de': 'Obligatorische Schule',
        'en': 'Compulsory education',
        'fr': 'Scolarit\xe9 obligatoire',
        'it': u"Scuola dell'obbligo",
        'rm': 'Scola obligatorica'},
       'id': 'compulsory education'},
      {'entry': {'de': '1. Zyklus (bis 4. Schuljahr)',
        'en': '1st cycle (up to 4th school year)',
        'fr': 'Cycle 1 (1\xe8re \xe0 4\xe8me ann\xe9e scolaire) ',
        'it': '1\xb0 ciclo',
        'rm': 'Emprim ciclus'},
       'id': 'cycle 1'}]},
    {'source': {'en': 'classification system'},
     'taxon': [{'entry': {'de': 'Obligatorische Schule',
        'en': 'Compulsory education',
        'fr': 'Scolarit\xe9 obligatoire',
        'it': u"Scuola dell'obbligo",
        'rm': 'Scola obligatorica'},
       'id': 'compulsory education'},
      {'entry': {'de': '2. Zyklus (5. bis 8. Schuljahr)',
        'en': '2nd cycle (5th to 8th school year)',
        'fr': 'Cycle 2 (5\xe8me \xe0 8\xe8me ann\xe9e scolaire) ',
        'it': '2\xb0 ciclo',
        'rm': 'Segund ciclus'},
       'id': 'cycle 2'}]},
    {'source': {'en': 'classification system'},
     'taxon': [{'entry': {'de': 'Obligatorische Schule',
        'en': 'Compulsory education',
        'fr': 'Scolarit\xe9 obligatoire',
        'it': u"Scuola dell'obbligo",
        'rm': 'Scola obligatorica'},
       'id': 'compulsory education'},
      {'entry': {'de': '3. Zyklus (9. bis 11. Schuljahr)',
        'en': '3rd cycle (9th to 11th school year)',
        'fr': 'Cycle 3 (9\xe8me \xe0 11\xe8me ann\xe9e scolaire)',
        'it': '3\xb0 ciclo',
        'rm': 'Terz ciclus'},
       'id': 'cycle 3'}]}]}],
 'curriculum': [{'source': {'hierarchy': [{'childTypes': ['domaines'],
      'entry': {'fr': 'Cycle'},
      'type': 'cycles'},
     {'childTypes': ['disciplines'],
      'entry': {'fr': 'Domaine'},
      'type': 'domaines'},
     {'childTypes': ['objectifs'],
      'entry': {'fr': 'Discipline'},
      'type': 'disciplines'},
     {'childTypes': ['progressions'],
      'entry': {'fr': 'Objectif'},
      'type': 'objectifs'},
     {'childTypes': [],
      'entry': {'fr': "Progressions d'apprentissage"},
      'type': 'progressions'}],
    'name': 'per'},
   'taxonTree': [{'childTaxons': [{'childTaxons': [{'childTaxons': [{'childTaxons': [],
           'entry': {'fr': 'Culture (A 14 AC&M)'},
           'id': '85',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'}],
         'entry': {'fr': 'Activit\xe9s cr\xe9atrices et manuelles'},
         'id': '13',
         'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
         'type': 'disciplines'},
        {'childTaxons': [{'childTaxons': [],
           'entry': {'fr': 'Expression et repr\xe9sentation (A 11 AV)'},
           'id': '77',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Perception (A 12 AV)'},
           'id': '80',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Acquisition de techniques (A 13 AV)'},
           'id': '83',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Culture (A 14 AV)'},
           'id': '86',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'}],
         'entry': {'fr': 'Arts visuels'},
         'id': '11',
         'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
         'type': 'disciplines'}],
       'entry': {'fr': 'Arts'},
       'id': '4',
       'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
       'type': 'domaines'}],
     'entry': {'fr': 'Cycle 1'},
     'id': '1',
     'purpose': {'source': 'LOM-CHv1.2', 'value': 'educational level'},
     'type': 'cycles'},
    {'childTaxons': [{'childTaxons': [{'childTaxons': [{'childTaxons': [],
           'entry': {'fr': 'Culture (A 24 AC&M)'},
           'id': '97',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'}],
         'entry': {'fr': 'Activit\xe9s cr\xe9atrices et manuelles'},
         'id': '13',
         'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
         'type': 'disciplines'},
        {'childTaxons': [{'childTaxons': [],
           'entry': {'fr': 'Expression et repr\xe9sentation (A 21 AV)'},
           'id': '89',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Perception (A 22 AV)'},
           'id': '92',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Acquisition de techniques (A 23 AV)'},
           'id': '95',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Culture (A 24 AV)'},
           'id': '98',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'}],
         'entry': {'fr': 'Arts visuels'},
         'id': '11',
         'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
         'type': 'disciplines'}],
       'entry': {'fr': 'Arts'},
       'id': '4',
       'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
       'type': 'domaines'}],
     'entry': {'fr': 'Cycle 2'},
     'id': '2',
     'purpose': {'source': 'LOM-CHv1.2', 'value': 'educational level'},
     'type': 'cycles'},
    {'childTaxons': [{'childTaxons': [{'childTaxons': [{'childTaxons': [],
           'entry': {'fr': 'Culture (A 34 AC&M)'},
           'id': '109',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'}],
         'entry': {'fr': 'Activit\xe9s cr\xe9atrices et manuelles'},
         'id': '13',
         'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
         'type': 'disciplines'},
        {'childTaxons': [{'childTaxons': [],
           'entry': {'fr': 'Expression et repr\xe9sentation (A 31 AV)'},
           'id': '101',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Perception (A 32 AV)'},
           'id': '104',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Acquisition de techniques (A 33 AV)'},
           'id': '107',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'},
          {'childTaxons': [],
           'entry': {'fr': 'Culture (A 34 AV)'},
           'id': '110',
           'purpose': {'source': 'LOM-CHv1.2', 'value': 'objective'},
           'type': 'objectifs'}],
         'entry': {'fr': 'Arts visuels'},
         'id': '11',
         'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
         'type': 'disciplines'}],
       'entry': {'fr': 'Arts'},
       'id': '4',
       'purpose': {'source': 'LOM-CHv1.2', 'value': 'discipline'},
       'type': 'domaines'}],
     'entry': {'fr': 'Cycle 3'},
     'id': '3',
     'purpose': {'source': 'LOM-CHv1.2', 'value': 'educational level'},
     'type': 'cycles'}]}],
 'education': [{'context': [{'ontologyName': {'de': 'Obligatorische Schule',
      'en': 'Compulsory education',
      'fr': 'Scolarit\xe9 obligatoire',
      'it': 'Scuola dell\u2019obbligo',
      'rm': 'Scola obligatorica'},
     'source': 'LREv3.0',
     'value': 'compulsory education'}],
   'description': [{'fr': u"Les activit\xe9s pr\xe9sent\xe9es se r\xe9partissent entre des activit\xe9s \xe0 r\xe9aliser sur place en privil\xe9giant les aspects de perception et d\u2019observation, et des activit\xe9s \xe0 r\xe9aliser en classe ax\xe9es sur l\u2019expression, l\u2019imagination, l'exploration technique. Enfin, certaines activit\xe9s sont pr\xe9vues avec les reproductions des \u0153uvres, davantage centr\xe9es sur la s\xe9lection et la comparaison. De nombreuses approches sont possibles, le mat\xe9riel propos\xe9 est une ressource modulable, ouverte. Une version imprim\xe9e est disponible pour le pr\xeat au Centre de documentation de la HEP Fribourg."}],
   'difficultyLevel': {'ontologyName': {'de': 'Mittel',
     'en': 'Medium',
     'fr': 'Moyen',
     'it': 'Medio',
     'rm': 'Medio'},
    'source': 'LOMv1.0',
    'value': 'medium'},
   'intendedEndUserRole': [{'ontologyName': {'de': 'Lernende',
      'en': 'Learners',
      'fr': 'Apprenants',
      'it': 'Apprendenti',
      'rm': 'Scolaras e scolars'},
     'source': 'LOMv1.0',
     'value': 'learner'}],
   'learningResourceType': {'documentary': [{'ontologyName': {'de': 'Bild/Grafik',
       'en': 'Image ',
       'fr': 'Image fixe',
       'it': 'Immagine fissa / grafica',
       'rm': ''},
      'source': 'LREv3.0',
      'value': 'image'},
     {'ontologyName': {'de': 'Textdokument',
       'en': 'Text',
       'fr': 'Document textuel',
       'it': 'Documento testuale',
       'rm': ''},
      'source': 'LREv3.0',
      'value': 'text'}],
    'pedagogical': [{'ontologyName': {'de': 'Erkundung/Studie',
       'en': 'Exploration',
       'fr': 'Exploration',
       'it': 'Esplorazione',
       'rm': ''},
      'source': 'LOMv1.0',
      'value': 'exploration'}]},
   'typicalAgeRange': [{'0': '4-12'}],
   'typicalLearningTime': {'learningTime': {'ontologyName': {'de': 'Mehr als f\xfcnf Lektionen',
      'en': 'More than five lessons',
      'fr': 'Plus de cinq le\xe7ons',
      'it': 'Pi\xf9 di cinque lezioni',
      'rm': ''},
     'source': 'LOMv1.0',
     'value': 'more_than_five_lessons'}}}],
 'lifeCycle': {'contribute': [{'entity': ['BEGIN:VCARD\r\nVERSION:3.0\r\nREV:2016-12-23T13:58:34Z\r\nN;CHARSET=utf-8: Catherine;Liechti;;;\r\nFN;CHARSET=utf-8:Liechti  Catherine\r\nEND:VCARD\r\n'],
    'role': {'ontologyName': {'de': 'Autor',
      'en': 'Author',
      'fr': 'Auteur',
      'it': 'Autore',
      'rm': 'Autur'},
     'source': 'LOMv1.0',
     'value': 'author'}},
   {'entity': ['BEGIN:VCARD\r\nVERSION:3.0\r\nREV:1970-01-01T01:00:00Z\r\nADR;WORK;POSTAL;CHARSET=utf-8:Haute Ecole p\xe9dagogique Fribourg (HEP-PH F\r\n R);;;;Rue de Morat 34-36;Fribourg;;1700;\r\nORG;CHARSET=utf-8:Haute Ecole p\xe9dagogique Fribourg (HEP-PH FR);;\r\nURL;WORK:http://www.hepfr.ch/\r\nEND:VCARD\r\n'],
    'role': {'ontologyName': {'de': 'Herausgeber',
      'en': 'Publisher',
      'fr': '\xc9diteur',
      'it': 'Editore',
      'rm': ''},
     'source': 'LOMv1.0',
     'value': 'publisher'}}],
  'version': {'fr': '2009'}},
 'metaMetadata': {'contribute': [{'date': '2017-02-08T10:51:00+01:00',
    'entity': ['BEGIN:VCARD\r\nVERSION:3.0\r\nREV:2016-12-15T08:45:57Z\r\nN;CHARSET=utf-8:Friportail;;;;\r\nFN;CHARSET=utf-8:Friportail\r\nADR;WORK;POSTAL;CHARSET=utf-8:Friportail (info@friportail.ch);Friportail;\r\n ;Fribourg;;1700;CH\r\nORG;CHARSET=utf-8:Friportail\r\nEMAIL;INTERNET;WORK:info@friportail.ch\r\nLOGO:https://dsb-api.educa.ch/v2/file/default/e28779356a538df4eeccfb22f18e401f.png\r\nEND:VCARD\r\n'],
    'role': {'ontologyName': {'de': 'Creator',
      'en': 'Creator',
      'fr': 'Creator',
      'it': 'Creator',
      'rm': 'Creator'},
     'source': 'LOMv1.0',
     'value': 'creator'}}],
  'identifier': [{'catalog': 'ARCHIBALD',
    'entry': 'https://dev-archibald.friportail.ch/fr/node/383'}],
  'language': 'fr',
  'metadataSchema': ['LOM-CHv1.2']},
 'relation': [],
 'rights': {'copyright': [{'description': {'de': 'Tous droits r\xe9serv\xe9s',
     'en': 'Tous droits r\xe9serv\xe9s',
     'fr': 'Tous droits r\xe9serv\xe9s',
     'it': 'Tous droits r\xe9serv\xe9s'}}],
  'cost': {'ontologyName': {'de': 'Nein',
    'en': 'No',
    'fr': 'Non',
    'it': 'No',
    'rm': ''},
   'source': 'LOMv1.0',
   'value': 'no'}},
 'technical': {
    'previewImage': {'image': 'https://dsb-api.educa.ch/v2/file/default/9bc1b2a91e77b7780a01fee18ca4faea.jpeg'},
 }
}
