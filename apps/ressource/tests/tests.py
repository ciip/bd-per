import json
import os
import re
from datetime import date, timedelta
from pathlib import Path
from unittest.mock import MagicMock
from uuid import UUID

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser, Permission
from django.contrib.postgres.fields import ArrayField
from django.core import mail
from django.core.files import File
from django.test import RequestFactory
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.timezone import now

from botocore.exceptions import ClientError
from tinymce.widgets import TinyMCE

from pper.tests.tests import BDPERTests
from pper.models import (
    Discipline, Domain, Cellule, Contenu, Specification
)
from shortener.models import ShortLink
from ressource import educa
from ressource.forms import ResourceEditForm, ResourceSearchForm
from ressource.models import (
    Resource, ResourceContrib, ResourceFile, ResourceGroup, ResourceGroupLink,
    ResourcePerLink, ResourcePERIndex, ResourceRelation, SubTheme,
    default_educ_level,
)
from ressource.storage import FallbackStorage
from ressource.utils import XLSX_MIMETYPE

MODEL = 'django.contrib.auth.backends.ModelBackend'

# Required to validate a form instance
POST_MINIMAL_DATA = {
    "status": "validee",
    "title": "Istanbul",
    "url": "https://www.google.ch/maps/",
    "type_r": "med",
    "files-TOTAL_FORMS": "1",
    "files-INITIAL_FORMS": "0",
    "gallery-TOTAL_FORMS": "1",
    "gallery-INITIAL_FORMS": "0",
    "relations-TOTAL_FORMS": "1",
    "relations-INITIAL_FORMS": "0",
    "author-TOTAL_FORMS": "1",
    "author-INITIAL_FORMS": "0",
    "editor-TOTAL_FORMS": "1",
    "editor-INITIAL_FORMS": "0",
    "publisher-TOTAL_FORMS": "1",
    "publisher-INITIAL_FORMS": "0",
    "resourceperlink_set-TOTAL_FORMS": "1",
    "resourceperlink_set-INITIAL_FORMS": "0",
}

POST_DATA = dict(POST_MINIMAL_DATA, **{
    "emedia": "",
    "cantons": "",
    "description": "<p>Vue aérienne</p>",
    "genre": "",
    "duration": "",
    "source": "<p>Google Maps</p>",
    "producer": "",
    "target_gr": ["learner", "teacher"],
    "keywords": "Istanbul, mégalopole, étalement urbain, infrastructure, mutation",
    "version": "",
    "languages": [],
    "size": "",
    "phys_loc": "",
    "tec_req": "",
    "educ_level": ["compulsory education"],
    "level": ["5th and 6th year"],
    "type_doc": ["website"],
    "ages_0": "15", "ages_1": "17",
    "granularity": "",
    "thumb": "",
    "thumb_url": "",
    "conditions": "cc_by_sa",
})


class ResourceTestsBase(BDPERTests):
    def setUp(self):
        super().setUp()
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('add_resource', 'ressource', 'resource'),
            Permission.objects.get_by_natural_key('change_resource', 'ressource', 'resource'),
            Permission.objects.get_by_natural_key('change_resourcegroup', 'ressource', 'resourcegroup'),
        )

    def create_resource(self, **kwargs):
        """Create a resource with a wide range of possible values."""
        values = {
            k: v for k, v in POST_DATA.items() if k in (
                'title', 'status', 'url', 'type_r', 'description', 'source',
                'target_gr', 'keywords', 'type_doc', 'conditions', 'level',
            )
        }
        values.update({'ages': '15-17', 'languages': ['fr'], 'size': int(12.2 * 1024 * 1024)})
        values.update(**kwargs)
        res = Resource.objects.create(**values)
        ResourceRelation.objects.create(
            resource=res, rtype='has_version', url='http://www.example.org/1'
        )
        contrib = ResourceContrib.objects.create(
            role='author',
            vcard='BEGIN:VCARD\r\nVERSION:3.0\r\nFN:Pierre\r\nN:;;;;\r\nORG:CIIP;;\r\nEND:VCARD',
        )
        res.contribs.set([contrib])
        res.refresh_from_db()
        return res


class ResourceTests(ResourceTestsBase):
    def test_title_format(self):
        test_titles = (
            ('Ressource 1', 'RESSOURCE 1'),
            ('Ressource (3e)', 'RESSOURCE (3<sup>e</sup>)'),
            ('Ressource (1re)', 'RESSOURCE (1<sup>re</sup>)'),
            ('Le 1er août', 'LE 1<sup>er</sup> AOÛT'),
            ('aux Ier et IIe siècles', 'AUX I<sup>er</sup> ET II<sup>e</sup> SIÈCLES'),
            ('Du Ve siècle', 'DU V<sup>e</sup> SIÈCLE'),
            ('La ferme de Verberie', 'LA FERME DE VERBERIE'),
            ('Du XVIIe siècle', 'DU XVII<sup>e</sup> SIÈCLE'),
            ('XVIe au XVIIIe siècles', 'XVI<sup>e</sup> AU XVIII<sup>e</sup> SIÈCLES'),
            ('XVIe-XVIIIe siècles', 'XVI<sup>e</sup>-XVIII<sup>e</sup> SIÈCLES'),
            ('La vie au VIe siècle', 'LA VIE AU VI<sup>e</sup> SIÈCLE'),
        )
        r1 = Resource()
        self.assertEqual(r1.educ_level, default_educ_level())
        for title, expected in test_titles:
            r1.title = title
            self.assertEqual(r1.format_title(), expected)

    def test_language_metadata(self):
        """
        Language is only shown when different from 'français'.
        """
        r1 = Resource.objects.create(title='Ressource 1', languages=['fr'])
        r2 = Resource.objects.create(title='Ressource 2', languages=['de'])
        self.assertEqual(len(r1.metadata()), 0)
        self.assertIn(('Langues de la ressource', 'allemand'), r2.metadata())

    def test_emedia_home(self):
        response = self.client.get(reverse('emedia_home'))
        self.assertContains(response, 'Ressources numériques')
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('emedia_home'))
        self.assertContains(response, 'Ressources numériques')

    def test_rts_url(self):
        links = [
            'https://www.rts.ch/play/tv/19h30/video/suisse-une-aop-pour-le-gruyere-?id=1488799',
            'https://www.rts.ch/play/radio/tout-un-monde/audio/suisse-une-aop-?id=1488799',
            'https://www.rts.ch/play/tv/ya-pas-ecole-/video/vrai-ou-faux-?urn=urn:rts:video:1488799',
            'https://www.rts.ch/play/tv/redirect/detail/1488799',
        ]
        r1 = Resource(pk=1, title='Ressource 1')
        for link in links:
            r1.url = link
            self.assertEqual(
                r1.get_url(),
                'https://bdper.plandetudes.ch/ressources/1/video/rts/1488799/'
            )
        r1.url = 'https://www.rts.ch/play/tv/ya-pas-ecole-/video/vrai-ou-faux-?urn=urn:rts:video:1488799&subdivisions=true'
        self.assertEqual(
            r1.media_data(None),
            {
                'player': 'rts',
                'base': 'https://rts.ch/play',
                'vkey': '1488799',
                'path': reverse('video-rts', args=[r1.pk, 1488799]),
                'subdivisions': True,
                'need_auth': False,
            }
        )

    def test_youtube_url(self):
        """YouTube links are redirected to a local player."""
        links = [
            'https://www.youtube.com/watch?v=lDCotCdCt88',
            'https://youtu.be/lDCotCdCt88',
            'http://www.youtube.com/watch?feature=player_embedded&v=lDCotCdCt88',
        ]
        r1 = Resource(pk=1, title='Ressource 1')
        for link in links:
            r1.url = link
            self.assertEqual(
                r1.get_url(),
                'https://bdper.plandetudes.ch/ressources/1/video/yt/lDCotCdCt88/'
            )

    def test_dailymotion_url(self):
        links = [
            'http://www.dailymotion.com/video/xj7w17_d-ou-vient-l-eau_news',
            'https://www.dailymotion.com/video/xj7w17',
        ]
        r1 = Resource(pk=1, title='Ressource 1')
        for link in links:
            r1.url = link
            self.assertEqual(
                r1.get_url(),
                'https://bdper.plandetudes.ch/ressources/1/video/dm/xj7w17/'
            )

    def test_vimeo_url(self):
        links = [
            'https://vimeo.com/66098151',
            'https://player.vimeo.com/video/66098151?autoplay=1',
        ]
        r1 = Resource(pk=1, title='Ressource 1')
        for link in links:
            r1.url = link
            self.assertEqual(
                r1.get_url(),
                'https://bdper.plandetudes.ch/ressources/1/video/vm/66098151/'
            )

    @override_settings(VOD_INFOMANIAK_TOKEN='xxxxx')
    def test_infomaniak_url(self):
        r1 = Resource(
            pk=1, title='Ressource 1', need_auth=True,
            url='https://player.vod2.infomaniak.com/embed/1jhvl2uqnide0'
        )
        self.assertEqual(
            r1.get_url(),
            'https://bdper.plandetudes.ch/ressources/1/video/imk/'
        )

        self.factory = RequestFactory()
        request = self.factory.get('/ressources/5847/?display=1')
        request.user = self.user
        self.assertEqual(
            r1.media_data(request=request),
            {
                'player': 'infomaniak2',
                'base': 'https://player.vod2.infomaniak.com',
                'vkey': '1jhvl2uqnide0',
                'skey': 'token-test',
                'path': '/ressources/1/video/imk/',
                'need_auth': False
            }
        )

        request.user = AnonymousUser()
        self.assertEqual(
            r1.get_url(request=request),
            'http://testserver/login/?next=/ressources/5847/%3Fdisplay%3D1'
        )

    def test_no_player_url(self):
        r1 = Resource(
            pk=1, title='Ressource sans player', no_player=True,
            url='https://www.rts.ch/play/tv/ya-pas-ecole-/video/vrai-ou-faux-?urn=urn:rts:video:1488799'
        )
        self.assertEqual(r1.get_url(), r1.url)

    def test_video_page(self):
        r1 = Resource.objects.create(
            title='Ressource 1', status='validee',
            url='http://www.rts.ch/play/tv/19h30/video/suisse-une-aop-pour-le-gruyere-?id=1488799'
        )
        response = self.client.get(r1.get_url())
        self.assertRedirects(response, r1.get_absolute_url())
        response = self.client.get(r1.get_url(), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'https://rts.ch/play/embed?urn=urn:rts:video:1488799')

    def test_create_gallery_resource(self):
        png_path = Path(__file__).parent / 'thumbnail.png'
        # Create 2 image resources
        rimg1 = Resource.objects.create(
            status='validee', title='Image 1', type_doc=['image'],
        )
        rimg2 = Resource.objects.create(
            status='validee', title='Image 2', type_doc=['image'],
        )
        with png_path.open('rb') as fp:
            ResourceFile.objects.create(resource=rimg1, rfile=File(fp, name='thumb1.png'), title="Img1")
            ResourceFile.objects.create(resource=rimg2, rfile=File(fp, name='thumb2.png'), title="Img2")

        # Create the gallery resource
        post_data = dict(POST_MINIMAL_DATA, **{
            "title": "Galerie 1",
            "url": '',
            "gallery-TOTAL_FORMS": "3",
            "gallery-INITIAL_FORMS": "0",
            "gallery-0-ressource": '',
            "gallery-0-other": rimg1.pk,
            "gallery-1-ressource": '',
            "gallery-1-other": rimg2.pk,
        })
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(reverse('resource_new'), data=post_data)
        res = Resource.objects.get(title="Galerie 1")
        self.assertTrue(res.has_images)
        self.assertEqual(len(res.get_images), 2)
        self.assertNotEqual(res.get_thumb_url(), '')
        self.assertEqual(res.relations.first().rtype, 'is_part_of')
        self.assertIn(res, Resource.objects.galleries())

        response = self.client.get(reverse('resource_display', args=[rimg1.pk]))
        # Le détail d'une image de galerie affiche un lien vers la galerie parente
        self.assertContains(response, f'<div><a href="{res.get_absolute_url()}">Galerie 1</a></div>')

    def test_add_file_to_gallery_resource(self):
        """When a file is added to a gallery, it is transformed in a resource."""
        gallery = Resource.objects.create(title="Gallery 1")
        png_path = Path(__file__).parent / 'thumbnail.png'
        # Create 2 image resources
        rimg1 = Resource.objects.create(
            status='validee', title='Image 1', type_doc=['image'],
        )
        rimg2 = Resource.objects.create(
            status='validee', title='Image 2', type_doc=['image'],
        )
        with png_path.open('rb') as fp:
            ResourceFile.objects.create(resource=rimg1, rfile=File(fp, name='thumb1.png'), title="Img1")
            ResourceFile.objects.create(resource=rimg2, rfile=File(fp, name='thumb2.png'), title="Img2")
        rr1 = ResourceRelation.objects.create(resource=gallery, rtype='is_part_of', other=rimg1)
        rr2 = ResourceRelation.objects.create(resource=gallery, rtype='is_part_of', other=rimg2)
        # refresh_from_db not sufficient to clear cached props at least in Django 2.2
        gallery = Resource.objects.get(title=gallery.title)
        self.assertEqual(gallery.content_type, 'gallery')

        # Add a file to the gallery
        post_data = dict(POST_MINIMAL_DATA, **{
            "title": gallery.title,
            "url": '',
            "gallery-TOTAL_FORMS": "2",
            "gallery-INITIAL_FORMS": "2",
            "gallery-0-id": str(rr1.pk),
            "gallery-0-ressource": str(gallery.pk),
            "gallery-0-other": str(rimg1.pk),
            "gallery-1-id": str(rr2.pk),
            "gallery-1-ressource": str(gallery.pk),
            "gallery-1-other": str(rimg2.pk),
        })
        with png_path.open('rb') as fp:
            post_data.update({
                'files-TOTAL_FORMS': "1",
                'files-0-rfile': File(fp, name='thumb3.png'),
                'files-0-title': 'un fichier',
            })
            self.client.force_login(self.user, backend=MODEL)
            response = self.client.post(reverse('resource_edit', args=[gallery.pk]), post_data)
        if response.status_code == 200:
            self.fail(response.context['form'].all_errors())
        self.assertRedirects(response, gallery.get_absolute_url())
        gallery = Resource.objects.get(title=gallery.title)
        self.assertEqual(len(gallery.get_images), 3)
        self.assertFalse(gallery.has_files)

    def _test_auto_thumbnail(self, filename):
        """Une vignette est automatiquement créée à la création d'un fichier de ressource."""
        res = Resource.objects.create(title="Ressource 1")
        with open(os.path.join(os.path.dirname(__file__), filename), 'rb') as fp:
            rf = ResourceFile.objects.create(resource=res, rfile=File(fp), title="A sample")
        thumb_path = rf.thumb.path
        self.assertTrue(thumb_path.endswith('.png'))
        self.assertTrue(os.path.exists(thumb_path))
        res.delete()
        # thumbnail deleted with resource
        self.assertFalse(os.path.exists(thumb_path))

    def test_auto_pdf_thumbnail(self):
        self._test_auto_thumbnail("sample.pdf")

    def test_auto_img_thumbnail(self):
        self._test_auto_thumbnail("thumbnail.png")

    def test_display_resource(self):
        r1 = Resource.objects.create(title='Ressource 1', url="http://www.example-org/main")
        ResourceRelation.objects.create(resource=r1, rtype='has_version', url='http://www.example-org/1')
        ResourceRelation.objects.create(resource=r1, rtype='has_version', url='http://www.example-org/2')
        ResourcePerLink.objects.create(resource=r1, content_object=self.discipline)
        response = self.client.get(r1.get_absolute_url())
        # Anonymous users only see validated resources
        self.assertEqual(response.status_code, 403)
        r1.status = 'validee'
        r1.save()
        response = self.client.get(r1.get_absolute_url())
        self.assertContains(response,
            '<h2><a class="discrete resource-link" data-pk="{pk}" href="http://www.example-org/main" '
            'target="_blank">RESSOURCE 1</a> <a class=" resource-link" data-pk="{pk}" target="_blank" '
            'href="http://www.example-org/main">'
            '<img src="/static/img/open_window.png" title="Aller à l\'URL de la ressource"></a></h2>'.format(
                pk=r1.pk
            ),
            html=True)
        self.assertContains(
            response,
            '<div><b>Liens PER :</b> '
            '<a href="https://www.plandetudes.ch/web/guest/francais/">Français</a></div>',
            html=True
        )
        self.assertNotContains(response, "Galerie")
        # Even with 2 downloadable relations, "Téléchargement" appears only once.
        self.assertContains(response, "Téléchargement", count=1)

        # RN is displayed if at least one ResourceGroupLink.number = RN is found for the resource
        gr = ResourceGroup.objects.create(title="Test Resource Group")
        ResourceGroupLink.objects.create(resource=r1, group=gr, number='RN')
        ResourcePerLink.objects.create(catalog=gr, content_object=Cellule.objects.first())
        response = self.client.get(r1.get_absolute_url())
        self.assertContains(response, '<div class="number">RN</div>')
        self.assertContains(response, 'Catalogue :')

        r1.show_refs = False
        r1.save()
        response = self.client.get(r1.get_absolute_url())
        self.assertNotContains(response, 'Catalogue :')

    def test_type_doc_display(self):
        res = Resource(
            title='Ressource 1', url="http://www.example-org/main", type_doc=['map', 'audio']
        )
        self.assertEqual(res.get_type_doc_display(), 'carte, document sonore')
        self.assertEqual(
            res.get_type_doc_display(mode='pdf'),
            'carte <font face="MultimediaIcons">I</font>'
        )

    def test_display_resource_full(self):
        r1 = self.create_resource()
        ResourcePerLink.objects.create(resource=r1, content_object=self.spec)
        ResourcePerLink.objects.create(resource=r1, content_object=self.discipline)
        response = self.client.get(reverse('resource_display_full', args=[str(r1.pk)]))
        self.assertContains(response, 'Istanbul')
        self.assertContains(
            response,
            '<tr class="value"><th>Type documentaire</th><td>page web</td></tr>',
            html=True
        )
        # Relations
        self.assertContains(response, 'http://www.example.org/1')
        self.assertContains(response, 'Contributeurs')
        self.assertContains(response, "Plan d'études")
        self.assertContains(response, '<div class="objectif colorL">L 11: Objectif 1</div>')
        # thumb is excluded (floating top right)
        self.assertNotContains(response, '<th>Vignette</th>')
        # Only show when value is yes
        self.assertNotContains(response, '<th>Fiche e-media</th>')

        # Minimal resource
        r2 = Resource.objects.create(
            title='Ressource 1', url="http://www.example-org/main", status='validee'
        )
        response = self.client.get(reverse('resource_display_full', args=[str(r2.pk)]))
        # Empty value lines shouldn't appear
        self.assertNotContains(response, 'Nombre de pages')
        self.assertNotContains(response, '<h2 class="group-head">Cycle de vie</h2>')

    def test_selected_resource(self):
        r1 = Resource.objects.create(title='Ressource 1', url='http://example.org')
        r2 = Resource.objects.create(title='Ressource 2', url='http://example.com')
        response = self.client.get(reverse('resource_selected'))
        self.assertContains(response, "Aucune ressource à afficher.")
        self.client.cookies.load({'resource_selection': ','.join([str(r1.pk), str(r2.pk)])})
        response = self.client.get(reverse('resource_selected'))
        self.assertContains(response, r1.url)
        self.assertContains(response, r2.url)
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.get(reverse('resource_selected') + '?output=pdf')
        self.assertEqual(response['Content-Type'], 'application/pdf')
        response = self.client.get(reverse('resource_selected') + '?output=xlsx')
        self.assertEqual(response['Content-Type'], XLSX_MIMETYPE)
        self.client.cookies.load({'resource_selection': ''})
        response = self.client.get(reverse('resource_selected'))
        self.assertContains(response, "Aucune ressource à afficher.")

    def test_link_resource_to_cell(self):
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('change_specification', 'pper', 'specification')
        )
        self.client.force_login(self.user, backend=MODEL)
        c1 = Cellule.objects.first()
        r1 = Resource.objects.create(status='validee', title='Ressource 1')
        response = self.client.get(reverse('resource_link_to_cell', args=[c1.pk]))
        self.assertContains(response, '<form')
        response = self.client.post(
            reverse('resource_link_to_cell', args=[c1.pk]),
            data={
                "resource_pk": str(r1.pk),
            }
        )
        self.assertEqual(response.json()['result'], 'OK')
        c1.refresh_from_db()
        self.assertEqual(c1.get_resources()['med'][0].resource.title, 'Ressource 1')

    def test_edit_resource(self):
        """
        Test editing a resource
        """
        r1 = Resource.objects.create(status='validee', title='Ressource 1')
        contrib = ResourceContrib.objects.create(role='author', vcard='BEGIN:VCARD\r\nFN:Pierre\r\nEND:VCARD')
        r1.contribs.set([contrib])
        post_data = dict(POST_DATA, **{
            'author-0-id': str(contrib.pk), 'author-0-fn': "Jacques", "author-INITIAL_FORMS": "1",
        })
        # Test user without perms
        get_user_model().objects.create_user('jill', 'jill@example.org', 'jillpw')
        self.client.login(username='jill', password='jillpw')
        response = self.client.post(reverse('resource_edit', args=[r1.pk]), post_data, follow=True)
        self.assertEqual(response.status_code, 403)

        self.client.login(username='john', password='johnpw')
        response = self.client.post(reverse('resource_edit', args=[r1.pk]), post_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, r1.get_absolute_url())
        r1.refresh_from_db()
        for key in POST_DATA:
            if 'FORMS' in key or key.startswith('ages_'):
                continue
            if isinstance(Resource._meta.get_field(key), ArrayField):
                self.assertEqual(getattr(r1, key), POST_DATA[key] if isinstance(POST_DATA[key], list) else [POST_DATA[key]])
            else:
                if POST_DATA[key] == '':
                    self.assertIn(getattr(r1, key), ('', False, None))
                else:
                    self.assertEqual(getattr(r1, key), POST_DATA[key])
        self.assertEqual(r1.ages, "15-17")

        contrib.refresh_from_db()
        self.assertEqual(contrib.vcard, 'BEGIN:VCARD\r\nVERSION:3.0\r\nFN:Jacques\r\nEND:VCARD')
        # Try deleting a contrib
        post_data['author-0-DELETE'] = 'on'
        response = self.client.post(reverse('resource_edit', args=[r1.pk]), post_data)
        r1.refresh_from_db()
        self.assertEqual(r1.contribs.count(), 0)

    def test_edit_resource_clean_html(self):
        r1 = Resource.objects.create(status='validee', title='Ressource 1')
        form = ResourceEditForm(
            instance=r1, data=dict(POST_MINIMAL_DATA, description=(
                '<p>dzadz à adza<img src="https://malicious.net "><script>document.location.href="https://malicious.net";</script></p>'
            ))
        )
        self.assertTrue(form.is_valid())
        self.assertIsInstance(form.fields['description'].widget, TinyMCE)
        form.save()
        r1.refresh_from_db()
        self.assertEqual(r1.description, '<p>dzadz à adza</p>')

    def test_edit_resource_size(self):
        r1 = Resource.objects.create(
            status='validee', title='Ressource 1', size=12.2 * 1024 * 1024
        )
        form = ResourceEditForm(instance=r1)
        # Initial value is displayed in Mb unit
        self.assertHTMLEqual(
            str(form['size']),
            '<input type="text" name="size" value="12.2" id="id_size"> Mo'
        )
        form = ResourceEditForm(instance=r1, data=dict(POST_MINIMAL_DATA, size='12.5'))
        self.assertTrue(form.is_valid())
        form.save()
        r1.refresh_from_db()
        self.assertEqual(r1.size, 12.5 * 1024 * 1024)
        form = ResourceEditForm(instance=r1, data=dict(POST_MINIMAL_DATA, size='12,5'))
        self.assertTrue(form.is_valid())
        form = ResourceEditForm(instance=r1, data=dict(POST_MINIMAL_DATA, size='12,a'))
        self.assertFalse(form.is_valid())
        # Even with a bad value, the form should be able to be re-displayed (for correction)
        self.assertHTMLEqual(
            str(form['size']),
            '<input type="text" name="size" value="12,a" id="id_size" aria-invalid="true"> Mo'
        )

    @override_settings(MEDIA_URL='/uploads/')
    def test_add_resource_with_file(self):
        """An uploaded file can replace the URL field (but not both)."""
        self.client.force_login(self.user, backend=MODEL)
        post_data = dict(POST_DATA)
        path_main = os.path.join(os.path.dirname(__file__), 'sàmple2.pdf')
        path_rel = os.path.join(os.path.dirname(__file__), 'sample.pdf')
        # Using sample2 as main because we can test thumbnail generation with a 2-pages PDF
        with open(path_main, 'rb') as fp_main, open(path_rel, 'rb') as fp_rel:
            post_data.update({
                'files-0-rfile': File(fp_main),
                'files-0-title': 'un fichier',
            })
            response = self.client.post(reverse('resource_new'), data=post_data)
            self.assertContains(response, "Vous ne pouvez pas remplir à la fois le champ URL et le champ Fichier.")

            post_data.update({
                'url': '',
                'relations-0-rtype': 'is_version_of',
                'relations-0-url': '',
                'relations-0-rel_file': File(fp_rel),
                'relations-0-description': 'autre fichier',
                'relations-0-order': '0',
            })
            post_data['files-0-rfile'].seek(0)
            response = self.client.post(reverse('resource_new'), data=post_data)
        res = Resource.objects.get(title="Istanbul")
        self.assertRedirects(response, res.get_absolute_url())
        self.assertEqual(res.get_url(),
            'https://bdper.plandetudes.ch/uploads/ressources/{}/s%C3%A0mple2.pdf'.format(res.pk))
        self.assertEqual(res.size, 35740)
        self.assertEqual(res.relations.count(), 1)
        self.assertEqual(res.relations.first().get_url(),
            'https://bdper.plandetudes.ch/uploads/ressources/%d/sample.pdf' % res.pk)
        self.assertEqual(res.files.count(), 1)
        # Test auto-generated thumbnail
        self.assertIsNotNone(
            re.match(r'/uploads/resource_thumbs/[-0-9a-f]{36}\.png', res.get_thumb_url())
        )
        self.assertTrue(os.path.exists(res.files.first().thumb.path))
        # Test download resource
        response = self.client.get(res.get_url())
        self.assertEqual(response['Content-Disposition'], "inline; filename*=utf-8''s%C3%A0mple2.pdf")
        self.assertIn(res.files.first().rfile.path.encode('utf-8'), response.serialize_headers())

        # Uploading more files should lead to a zip download
        with open(path_main, 'rb') as fp_main, open(path_rel, 'rb') as fp_rel, open(path_rel, 'rb') as fp_rel2:
            del post_data['files-0-rfile']
            post_data.update({
                'files-TOTAL_FORMS': '2',
                'files-INITIAL_FORMS': '1',
                'files-0-id': str(res.files.first().pk),
                'files-0-resource': str(res.pk),
                'files-1-rfile': [File(fp_main), File(fp_rel)],
                'files-1-title': 'Deuxième fichier',
                'relations-0-rel_file': File(fp_rel2),
            })
            response = self.client.post(reverse('resource_edit', args=[res.pk]), data=post_data)
        self.assertRedirects(response, res.get_absolute_url())
        res.refresh_from_db()
        self.assertEqual(res.files.count(), 3)
        self.assertEqual(res.get_url(),
            'https://bdper.plandetudes.ch/ressources/%d/telecharger/' % res.pk)
        self.assertEqual(res.relations.first().get_url(),
            'https://bdper.plandetudes.ch/uploads/ressources/%d/sample.pdf' % res.pk)

    def test_delete_resource_with_file(self):
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('delete_resource', 'ressource', 'resource')
        )
        png_path = Path(__file__).parent / 'thumbnail.png'
        rimg1 = Resource.objects.create(
            status='validee', title='Image 1', type_doc=['image'],
        )
        with png_path.open('rb') as fp:
            ResourceFile.objects.create(resource=rimg1, rfile=File(fp, name='thumb1.png'), title="Img1")
        res_dir = Path(settings.MEDIA_ROOT, 'ressources', str(rimg1.pk))
        self.assertTrue(res_dir.exists())
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(reverse('resource_delete', args=[rimg1.pk]))
        self.assertRedirects(response, reverse('resource_home'))
        # No file deletion (for now), can be useful to restore a deleted resource.
        # Might be useful to purge from time to time the orphaned resource dirs.
        self.assertTrue(res_dir.exists())

    def test_download_compat_file(self):
        response = self.client.get(reverse('download', args=[4172, 'some_file.pdf']))
        self.assertEqual(response.status_code, 301)

    def test_add_resource_with_manual_thumbnail(self):
        self.client.force_login(self.user, backend=MODEL)
        with open(os.path.join(os.path.dirname(__file__), 'thumbnail.png'), 'rb') as fp:
            post_data = dict(POST_DATA, thumb=File(fp))
            response = self.client.post(reverse('resource_new'), data=post_data)
        res = Resource.objects.get(title="Istanbul")
        self.addCleanup(os.unlink, res.thumb.path)
        thumb_name = res.thumb.name
        self.assertTrue(thumb_name.startswith('resource_thumbs/thumbnail'))
        self.assertTrue(thumb_name.endswith('.png'))
        self.assertEqual(
            res.get_thumb_url(),
            os.path.join(settings.MEDIA_URL, thumb_name)
        )
        self.assertTrue(os.path.exists(res.thumb.path))

    @override_settings(RESOURCE_ADMINS=['admin@example.org', 'cc@example.org'])
    def test_suggest_resource(self):
        ResourceContrib.objects.create(role='author', vcard='BEGIN:VCARD\r\nFN:Nom\r\nEND:VCARD')
        response = self.client.get(reverse('resource_new_public'))
        self.assertContains(response, '<button type="submit">Soumettre la ressource</button>', html=True)
        self.assertNotContains(response, "Relations")
        self.assertContains(
            response, '<input name="author-0-fn" id="id_author-0-fn" maxlength="50" type="text">', html=True
        )

        post_data = {
            "title": "Istanbul",
            "url": "https://www.google.ch/maps/",
            "description": "<p>Vue a&eacute;rienne</p>",
            "keywords": "Istanbul, mégalopole, étalement urbain, infrastructure, mutation",
            "languages": ["fr"],
            "qual_decisive": "Une idée forte",
            "files-TOTAL_FORMS": "1",
            "files-INITIAL_FORMS": "0",
            "gallery-TOTAL_FORMS": "1",
            "gallery-INITIAL_FORMS": "0",
            "author-TOTAL_FORMS": "1",
            "author-INITIAL_FORMS": "0",
            "editor-TOTAL_FORMS": "1",
            "editor-INITIAL_FORMS": "0",
            "publisher-TOTAL_FORMS": "1",
            "publisher-INITIAL_FORMS": "0",
            "publisher-0-fn": "Les Éditions",
            "publisher-0-url": "http://www.example.org",
            "resourceperlink_set-TOTAL_FORMS": "1",
            "resourceperlink_set-INITIAL_FORMS": "0",
            # Submitter data
            "subm_name": "John",
            "subm_email": "john@example.org",
            "subm_tel": "044 555 66 77",
            "subm_institution": "Research institute",
            "subm_function": "",
            "subm_addr": "Main Street\nPo BOX 12",
            "subm_remarks": "",
        }
        response = self.client.post(reverse('resource_new_public'), data=post_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('resource_home'))
        res = Resource.objects.filter(title="Istanbul").first()
        self.assertEqual(res.status, 'atraiter')
        self.assertEqual(res.type_r, 'med')
        self.assertEqual(res.validated, None)
        self.assertEqual(res.url, "https://www.google.ch/maps/")
        self.assertEqual(res.qual_decisive, "Une idée forte")
        self.assertEqual(res.contribs.count(), 1)
        publ = res.contribs.first()
        self.assertTrue("URL:http://www.example.org" in publ.vcard)
        self.assertEqual(publ.role, "publisher")
        self.assertEqual(str(res.resourcesubmitter), "John")
        # A mail is sent to resource admins
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, ["admin@example.org"])
        self.assertEqual(mail.outbox[0].cc, ["cc@example.org"])
        # The submitter is displayed in the edit form
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_edit', args=[res.pk]))
        self.assertContains(response, "John, john@example.org")

    def test_edit_resource_initial_data(self):
        initial = {
            'contribs': [
                ResourceContrib(role='author', vcard='BEGIN:VCARD\r\nFN:Nom\r\nEND:VCARD'),
            ],
            'resourcerelation_set': [
                ResourceRelation(rtype='is_part_of', url='http://example.org', description="Descr"),
            ],
            'resourceperlink_set': [
                ResourcePerLink(content_object=Specification.objects.get(code='L 11')),
            ],
        }
        form = ResourceEditForm(initial=initial)
        # Contribs
        self.assertInHTML(
            '<input type="hidden" name="author-TOTAL_FORMS" value="2" id="id_author-TOTAL_FORMS">',
            form.formsets['author'].as_div()
        )
        self.assertInHTML(
            '<input type="text" name="author-0-fn" value="Nom" id="id_author-0-fn" maxlength="50">',
            form.formsets['author'].as_div()
        )
        # Relations
        self.assertInHTML(
            '<input type="hidden" name="relations-TOTAL_FORMS" value="2" id="id_relations-TOTAL_FORMS">',
            form.formsets['relations'].as_div()
        )
        self.assertInHTML(
            '<option value="is_part_of" selected>Est une partie de</option>',
            form.formsets['relations'].as_div()
        )
        # PER links
        self.assertInHTML(
            '<input type="hidden" name="resourceperlink_set-TOTAL_FORMS" value="2" id="id_resourceperlink_set-TOTAL_FORMS">',
            form.formsets['per_link'].as_div()
        )
        self.assertInHTML(
            '<span class="linkL">L 11 (Français)</span>',
            form.formsets['per_link'].as_div()
        )


    def test_edit_resource_per_link(self):
        """
        per_link is a specialized widget to link a resource with PER Content
        instance(s).
        """
        cell1 = self.spec.cellule_set.filter(scycle1=True)[0]
        r1 = Resource.objects.create(title='Ressource 1')
        contenus = list(Contenu.objects.all()[:2])
        cc1 = cell1.add_content(contenus[0], None, None)
        cc2 = cell1.add_content(contenus[1], None, None)

        self.client.force_login(self.user, backend=MODEL)
        POST_DATA.update({
            'title': '',  # Will cause redisplay with error
            'resourceperlink_set-0-resource': str(r1.pk),
            'resourceperlink_set-0-per_link': ['contenucellule-%d' % c.pk for c in [cc1, cc2]]
        })
        response = self.client.post(reverse('resource_edit', args=[r1.pk]), POST_DATA)
        self.assertContains(response, 'Le formulaire contient des erreurs de saisie')
        # Now correct data
        POST_DATA['title'] = r1.title
        response = self.client.post(reverse('resource_edit', args=[r1.pk]), POST_DATA)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, r1.get_absolute_url())
        r1.refresh_from_db()
        self.assertEqual(r1.resourceperlink_set.count(), 2)
        self.assertEqual(set(l.content_object for l in r1.resourceperlink_set.all()), set([cc1, cc2]))
        self.assertEqual(r1.resourceperindex_set.count(), 2)
        self.assertEqual(r1.resourceperindex_set.first().cell, cell1)
        # Delete a 'contenu' link and add 'discipline' and 'specification' links
        POST_DATA.update({
            "resourceperlink_set-TOTAL_FORMS": "4",
            "resourceperlink_set-INITIAL_FORMS": "2",
            'resourceperlink_set-0-id': str(r1.resourceperlink_set.all()[0].pk),
            'resourceperlink_set-0-DELETE': 'on',
            'resourceperlink_set-1-id': str(r1.resourceperlink_set.all()[1].pk),
            'resourceperlink_set-2-resource': str(r1.pk),
            'resourceperlink_set-2-per_link': [f'specification-{self.spec.pk}'],
            'resourceperlink_set-3-resource': str(r1.pk),
            'resourceperlink_set-3-per_link': [f'discipline-{self.discipline.pk}'],
        })
        response = self.client.post(reverse('resource_edit', args=[r1.pk]), POST_DATA)
        r1.refresh_from_db()
        self.assertEqual(
            r1.resourceperlink_set.filter(content_type__model='contenucellule').count(), 1
        )
        self.assertEqual(
            r1.resourceperlink_set.filter(content_type__model='specification').count(), 1
        )
        self.assertEqual(
            r1.resourceperlink_set.filter(content_type__model='discipline').count(), 1
        )

    def test_create_shortlink(self):
        r1 = Resource.objects.create(title='Ressource 1')
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(reverse('resource_create_shortlink', args=[r1.pk]), {}, follow=True)
        self.assertIsNotNone(ShortLink.get_for_object(r1, create=False))

    def test_delete_resource(self):
        r1 = Resource.objects.create(title='Ressource 1')
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('delete_resource', 'ressource', 'resource')
        )
        self.client.login(username='john', password='johnpw')
        response = self.client.post(reverse('resource_delete', args=[r1.pk]), {}, follow=True)
        self.assertEqual(Resource.objects.count(), 0)
        self.assertRedirects(response, reverse('resource_home'))
        self.assertContains(response, "La ressource « Ressource 1 » a bien été supprimée.")
        # Text 'next' redirection
        r1 = Resource.objects.create(title='Ressource 1')
        response = self.client.post(
            reverse('resource_delete', args=[r1.pk]), {
                'next': reverse('resource_new_public')
            }
        )
        self.assertRedirects(response, reverse('resource_new_public'))

    def test_unlink_resource_from_cell(self):
        c1 = Cellule.objects.all()[0]
        r1 = Resource.objects.create(title='Ressource 1')
        res_link = ResourcePerLink.objects.create(resource=r1, content_object=c1)
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(reverse('resource_unlinkcell'), {'obj_id': str(res_link.pk)})
        self.assertEqual(eval(response.content), {"cell_id": c1.pk, "reload_url": reverse('cell_content', args=[c1.pk])})
        # Resource itself is not deleted
        self.assertEqual(Resource.objects.count(), 1)

    def test_cut_and_paste_resource(self):
        c1, c2 = Cellule.objects.all()[0:2]
        r1 = Resource.objects.create(title='Ressource 1')
        link = ResourcePerLink.objects.create(resource=r1, content_object=c1)

        self.client.login(username='john', password='johnpw')
        response = self.client.post(reverse('resource_cut'), {'obj_id': str(link.pk)})
        self.assertIn('clipboard', self.client.session)
        response = self.client.post(reverse('resource_paste', args=[c2.pk]))
        self.assertNotIn('clipboard', self.client.session)
        link.refresh_from_db()
        self.assertEqual(link.content_object, c2)
        # Test non-existing pk
        response = self.client.post(reverse('resource_cut'), {'obj_id': str(link.pk)})
        response = self.client.post(reverse('resource_paste', args=['99999']))
        self.assertEqual(response.json()['status'], 'error')

    def test_export_resources(self):
        Resource.objects.create(title='Ressource 1')
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_export'))
        self.assertEqual(response['Content-Type'], XLSX_MIMETYPE)


class S3Mock(MagicMock):
    uploaded = {}

    def head_object(self, *args, Key=None, **kwargs):
        if not Key in self.uploaded:
            raise ClientError(
                {"ResponseMetadata": {"HTTPStatusCode": 404}}, 'head_object'
            )
        return {"ContentLength": 3450}

    def get_object(self, **kwargs):
        if kwargs['Key'] in self.uploaded:
            return {"Body": self.uploaded[kwargs['Key']]}
        raise FileNotFoundError()

    def upload_fileobj(self, content, bucket, name, **kwargs):
        self.uploaded[name] = content

    def generate_presigned_url(self, args, Params=None, **kwargs):
        return f"https://bdper.plandetudes.ch/uploads/{Params['Key']}"


class FallbackStorageTests(ResourceTestsBase):
    def setUp(self):
        super().setUp()
        self.old_storage = ResourceFile.rfile.field.storage
        fback_storage = FallbackStorage(**{
            'MOCK_CLASS': S3Mock,
            'ENDPOINT': 'https://sos-ch-gva-2.exo.io',
            'REGION_NAME': 'ch-gva-2',
            'BUCKET': 'testing-only',
            'KEY': 'ABCasdljalsdjasiduaiutzd24b2b3jh',
            'SECRET': 'unsecure_secret',
        })
        ResourceFile.rfile.field.storage = fback_storage
        ResourceFile.thumb.field.storage = fback_storage
        Resource.thumb.field.storage = fback_storage

    def tearDown(self):
        super().tearDown()
        ResourceFile.rfile.field.storage = self.old_storage
        ResourceFile.thumb.field.storage = self.old_storage
        Resource.thumb.field.storage = self.old_storage

    def test_add_resource_with_file(self):
        self.client.force_login(self.user, backend=MODEL)
        post_data = dict(POST_DATA)
        path_main = os.path.join(os.path.dirname(__file__), 'sample.pdf')
        with open(path_main, 'rb') as fp_main:
            post_data.update({
                'url': '',
                'files-0-rfile': File(fp_main),
                'files-0-title': 'un fichier',
            })
            response = self.client.post(reverse('resource_new'), data=post_data)

        res = Resource.objects.get(title="Istanbul")
        self.assertRedirects(response, res.get_absolute_url())
        self.assertEqual(
            res.get_url(),
            f'https://bdper.plandetudes.ch/uploads/ressources/{res.pk}/sample.pdf'
        )
        self.assertTrue(res.get_thumb_url().startswith(
            'https://testing-only.sos-ch-gva-2.exo.io/resource_thumbs/'
        ))


class ResourceGroupTests(ResourceTestsBase):
    def setUp(self):
        super().setUp()
        self.c1 = Cellule.objects.first()
        self.group = ResourceGroup.objects.create(title="Test Resource Group")
        # By-pass ResourceGroup.save()
        ResourceGroup.objects.filter(pk=self.group.pk).update(
            created=now() - timedelta(days=30),
            updated=now() - timedelta(days=10)
        )
        self.group_link = ResourcePerLink.objects.create(catalog=self.group, content_object=self.c1)

    def test_add_resource_into_group(self):
        self.client.login(username='john', password='johnpw')
        url = reverse('resource_new') + '?add_in_group=%d' % self.group.pk
        response = self.client.get(url)
        self.assertContains(response, 'action="%s"' % url.split('?')[0])
        response = self.client.post(url,
            {'title': "Titre ressource",
             'url': "http://example.org",
             'type_r': 'mer',
             'status': 'validee',
             'add_in_group': str(self.group.pk),
             'educ_level': ['compulsory education'],
             'files-TOTAL_FORMS': "1",
             'files-INITIAL_FORMS': "0",
             'gallery-TOTAL_FORMS': "1",
             'gallery-INITIAL_FORMS': "0",
             'relations-TOTAL_FORMS': "1",
             'relations-INITIAL_FORMS': "0",
             'relations-0-rtype': "is_version_of",
             'relations-0-url': "http://www.example.org",
             'relations-0-description': "Desc",
             'relations-0-resource': "",
             'relations-0-id': "",
             'relations-0-order': "0",
             'author-TOTAL_FORMS': "1",
             'author-INITIAL_FORMS': "0",
             'editor-TOTAL_FORMS': "1",
             'editor-INITIAL_FORMS': "0",
             'publisher-TOTAL_FORMS': "1",
             'publisher-INITIAL_FORMS': "0",
             'resourceperlink_set-TOTAL_FORMS': "1",
             'resourceperlink_set-INITIAL_FORMS': "0",
            })
        self.assertEqual(
            response.content,
            b'',
            response.context['form'].errors if response.context else ''
        )
        self.group.refresh_from_db()
        self.assertEqual(self.group.resources.all().count(), 1)
        self.assertEqual(self.group.resources.first().relations.count(), 1)
        self.assertEqual(self.group.updated.date(), date.today())

    def test_display_resource_group(self):
        grp = ResourceGroup.objects.create(
            title="Test Resource Group", grp_status='avalider', objectif=self.objectif
        )
        r1 = Resource.objects.create(title='Ressource 1', status='atraiter')
        ResourceGroupLink.objects.create(group=grp, resource=r1, number='RN44')
        ResourcePerLink.objects.create(catalog=grp, content_object=self.c1)

        response = self.client.get(reverse('resource_grp_display', args=[grp.pk]))
        self.assertEqual(response.status_code, 403)
        grp.grp_status = 'limite'
        grp.save()
        response = self.client.get(reverse('resource_grp_display', args=[grp.pk]))
        self.assertContains(response, grp.title)
        self.assertContains(response, self.discipline.name)
        self.assertNotContains(response, 'RESSOURCE 1')
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_grp_display', args=[grp.pk]))
        self.assertContains(response, 'RESSOURCE 1')
        self.assertContains(response, '<div class="number">RN44</div>')

    def test_resource_group_list(self):
        grp1 = ResourceGroup.objects.create(title="Group 10", groupment="Normal")
        grp2 = ResourceGroup.objects.create(title="Group 9", groupment="Normal")
        grp3 = ResourceGroup.objects.create(title="Group 2", groupment="À trier")
        grp4 = ResourceGroup.objects.create(title="4 Group", groupment="À trier")
        response = self.client.get(reverse('resource_grp_home'))
        cat_list = response.context['catalogs']
        # Smart-sort of numbers embedded inside titles
        self.assertGreater(cat_list.index(grp1), cat_list.index(grp2))
        # Sorting is done Unicode-aware ('À' before 'N')
        self.assertGreater(cat_list.index(grp2), cat_list.index(grp3))

    def test_resource_group_edition(self):
        edit_url = reverse('resource_grp_edit', args=[self.group.pk])
        self.client.login(username='john', password='johnpw')
        response = self.client.get(edit_url)
        self.assertContains(response, self.group.title)
        self.assertContains(response, 'action="%s"' % edit_url)
        response = self.client.post(edit_url, data={
            'title': "Test Resource Group Edited",
            'subtitle': '',
            'subtitle2': 'Ressources numériques',
            'description': '<p>Modules du MER :</p>',
            'level': '',
            'grp_status': 'valide',
            'cell': str(self.c1.pk),
            'groupment': '',
            'mer_url': '',
            'resourcegrouplink_set-TOTAL_FORMS': 0,
            'resourcegrouplink_set-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.content, b'')
        self.group.refresh_from_db()
        self.assertEqual(self.group.title, "Test Resource Group Edited")
        self.assertEqual(self.group.updated.date(), date.today())

    def test_link_unlink_resource(self):
        r1 = Resource.objects.create(title='Ressource 1')
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(
            reverse('resource_link', args=[self.group.pk]),
            data={'res_pk': r1.pk}
        )
        self.assertEqual(response.content, b'')
        self.assertEqual(self.group.resources.all().count(), 1)
        self.assertEqual(self.group.updated.date(), date.today())
        # Check indexation
        self.assertEqual(ResourcePERIndex.objects.filter(resource=r1, group=self.group).count(), 1)
        # Linking a second time should return a warning
        response = self.client.post(reverse('resource_link', args=[self.group.pk]),
            {'res_pk': r1.pk})
        self.assertEqual(response.content, 'Cette ressource est déjà contenue dans ce groupe'.encode('utf-8'))
        # Unlink the resource
        response = self.client.post(reverse('resource_unlink', args=[self.group.pk, r1.pk]), {})
        self.assertContains(response, '')
        self.assertEqual(self.group.resources.all().count(), 0)
        # Check indexation
        self.assertEqual(ResourcePERIndex.objects.filter(resource=r1, group=self.group).count(), 0)

    def test_link_unlink_catalog_from_cell(self):
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('change_specification', 'pper', 'specification')
        )
        grp = ResourceGroup.objects.create(title="Unlink test")
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(
            reverse('resource_link_to_cell', args=[self.c1.pk]),
            data={'catalog_pk': grp.pk}
        )
        self.assertEqual(response.json()['result'], 'OK')

        res_link = ResourcePerLink.objects.get(object_id=self.c1.pk, catalog=grp)
        response = self.client.post(reverse('resource_unlinkcell'), data={'obj_id': str(res_link.pk)})
        self.assertEqual(
            response.json(),
            {"cell_id": self.c1.pk, "reload_url": reverse('cell_content', args=[self.c1.pk])}
        )
        # ResourceGroup itself is not deleted
        self.assertEqual(ResourceGroup.objects.filter(pk=grp.pk).count(), 1)

    def test_cut_and_paste_resource_group(self):
        c2 = Cellule.objects.all()[1]

        self.client.force_login(self.user, backend=MODEL)
        response = self.client.post(reverse('resource_cut'), {'obj_id': str(self.group_link.pk)})
        self.assertIn('clipboard', self.client.session)
        response = self.client.post(reverse('resource_paste', args=[c2.pk]))
        self.assertNotIn('clipboard', self.client.session)
        new_link = ResourcePerLink.objects.get(catalog=self.group)
        self.assertEqual(new_link.object_id, c2.pk)

    def test_duplicate_resource_group(self):
        c2 = Cellule.objects.all()[1]
        # Add resources/subthemes
        th1 = SubTheme.objects.create(name="Theme1", num='1', group=self.group)
        th2 = SubTheme.objects.create(name="Theme2", num='2', group=self.group)
        r1 = Resource.objects.create(title='Ressource 1')
        r2 = Resource.objects.create(title='Ressource 2')
        lk1 = ResourceGroupLink.objects.create(group=self.group, resource=r1)
        lk1.subthemes.set([th1, th2])
        ResourceGroupLink.objects.create(group=self.group, resource=r2)

        self.group.duplicate(to_cell=c2)
        self.assertEqual(ResourceGroup.objects.filter(title=self.group.title).count(), 2)
        gr1, gr2 = ResourceGroup.objects.filter(title=self.group.title).order_by('pk')
        self.assertLess(gr1.created.date(), date.today())
        self.assertLess(gr1.updated.date(), date.today())
        self.assertEqual(gr2.created.date(), date.today())
        self.assertEqual(gr2.updated.date(), date.today())
        self.assertEqual(gr1.subthemes.count(), gr2.subthemes.count())
        self.assertEqual(gr1.resources.count(), gr2.resources.count())
        lk1 = gr1.resourcegrouplink_set.get(pk=lk1.pk)
        lk2 = gr2.resourcegrouplink_set.get(resource__title=lk1.resource.title)
        self.assertEqual(lk1.subthemes.count(), lk2.subthemes.count())
        self.assertNotEqual(lk1.subthemes.values_list('pk'), lk2.subthemes.values_list('pk'))

    def test_resource_group_subthemes_ordering(self):
        """
        As the `num` field is a CharField (to also accept 'A.'), numeric ordering
        has to be specially handled by a custom method.
        """
        for i in range(12):
            SubTheme.objects.create(name="Theme%d" % i, num=str(i), group=self.group)
        SubTheme.objects.create(name="Theme12", num="Mi", group=self.group)
        SubTheme.objects.create(name="Theme12", num="S.", group=self.group)
        SubTheme.objects.create(name="Theme12", num="Ms", group=self.group)
        # Default ordering
        self.assertEqual([st.num for st in list(self.group.subthemes.all())[-4:]], ['9', 'Mi', 'Ms', 'S.'])
        # Custom ordering
        self.assertEqual([st.num for st in list(self.group.subthemes.all().ordered())[:2]], ['Mi', '0'])
        self.assertEqual([st.num for st in list(self.group.subthemes.all().ordered())[-3:]], ['11', 'Ms', 'S.'])
        for letter in {'C', 'B', 'A'}:
            SubTheme.objects.create(name="Theme%s" % letter, num=letter, group=self.group)
        self.assertEqual(list(self.group.subthemes.all().ordered())[0].num, 'A')
        grp = ResourceGroup.objects.create(title="Order test")
        for letter in {'M1', 'M10', 'M2'}:
            SubTheme.objects.create(name="Theme%s" % letter, num=letter, group=grp)
        self.assertEqual(
            [st.num for st in grp.subthemes.all().ordered()],
            ['M1', 'M2', 'M10']
        )

    def test_order_resource_groups(self):
        grp = ResourceGroup.objects.create(title="ZZ Resource Group")
        grp_link = ResourcePerLink.objects.create(catalog=grp, content_object=self.c1)
        self.assertEqual(
            self.c1.get_resources()['grps']['none'],
            [self.group_link, grp_link]
        )
        # Change default alphabetic ordering
        self.client.login(username='john', password='johnpw')
        response = self.client.post(reverse('resource_grp_ordering'),
            data={'grp_ids[]': [str(grp.pk), str(self.group.pk)]})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')  # No response content
        cell = Cellule.objects.get(pk=self.c1.pk)
        self.assertEqual(
            cell.get_resources()['grps']['none'],
            [grp_link, self.group_link]
        )

    def test_pdf_output(self):
        r1 = Resource.objects.create(
            title='Ressource 1',
            status='validee',
            description="""
                <p>D'autres r&eacute;f&eacute;rences&nbsp;sur la&nbsp;<a href="http://www.rts.ch/la-1ere/programmes/monumental/4571174-monumental-du-27-01-2013.html#4571173" rel="noopener" target="_blank">page de l'&eacute;mission</a></p>
                """,
        )
        link = ResourceGroupLink.objects.create(group=self.group, resource=r1)
        response = self.client.get(reverse('resourcegroup_pdf', args=[str(self.group.pk)]))
        self.assertEqual(response['Content-Type'], 'application/pdf')
        # This should disturb the ReportLab HTML parser, but not crash
        r1.description = '<span style="font-weight: 400;">Ha</span>'
        r1.save()
        response = self.client.get(reverse('resourcegroup_pdf', args=[str(self.group.pk)]))
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_search_resource_groups(self):
        ResourceGroup.objects.create(title="Second Group")
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_grp_search', args=['json']) + '?term=Group')
        resp = response.json()
        self.assertEqual(resp['total'], 2)
        response = self.client.get(reverse('resource_grp_search', args=['json']) + '?term=Second')
        resp = response.json()
        self.assertEqual(resp['total'], 1)
        self.assertEqual(resp['results'][0]['label'], "Second Group")


class ResourceSearchTests(ResourceTestsBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.create_domain_chain("Arts", "Musique", obj_cycle=2)
        Resource.objects.create(status='validee', title='Ressource validée', genre='int', show_refs=False)
        c2 = Cellule.objects.filter(specification__objectif__title="Objectif Arts").first()
        r2 = Resource.objects.create(
            status='validee', title='Partition musicale',
            type_doc=['website'], type_ped=['lesson plan']
        )
        ResourcePerLink.objects.create(resource=r2, content_object=c2)
        r2.index()
        # canton-type resource or validating resource should not appear if not authentified
        Resource.objects.create(status='validee', title='Ressource cantonale', type_r='can')
        Resource.objects.create(status='atraiter', title='Ressource à valider')

    def test_search_resource(self):
        response = self.client.get(reverse('resource_home'))
        self.assertEqual(len(response.context['resources']), 0)
        self.assertNotContains(response, "Désolé")

        response = self.client.get(reverse('resource_home') + '?text=ressource')
        self.assertEqual(len(response.context['resources']), 1)
        self.assertContains(response, '<div><b>Genre :</b> interview</div>', html=True)

        # search on genre display name
        response = self.client.get(reverse('resource_home') + '?text=interview')
        self.assertEqual(len(response.context['resources']), 1)
        self.assertNotContains(response, 'Référencée depuis :')

        # search on type_ped display name
        response = self.client.get(reverse('resource_home') + '?text=scenario')
        self.assertEqual(len(response.context['resources']), 1)

        c2 = Cellule.objects.filter(specification__objectif__title="Objectif Arts").first()
        response = self.client.get(
            reverse('resource_home') + '?text=&domains=%d' % Domain.objects.get(name='Arts').pk
        )
        self.assertEqual(len(response.context['resources']), 1)
        self.assertContains(
            response, '<a href="https://www.plandetudes.ch/web/guest/A_11/#%s">A 11</a>' % c2.pk)

        response = self.client.get(reverse('resource_home') + '?cycles=2')
        self.assertEqual(len(response.context['resources']), 1)
        self.assertContains(
            response, '<a href="https://www.plandetudes.ch/web/guest/A_11/#%s">A 11</a>' % c2.pk)

        response = self.client.get(reverse('resource_home') + '?disciplines=%d' % Discipline.objects.get(name='Musique').pk)
        self.assertEqual(len(response.context['resources']), 1)
        self.assertContains(
            response, '<a href="https://www.plandetudes.ch/web/guest/A_11/#%s">A 11</a>' % c2.pk)

        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_home') + '?text=valider&status=atraiter&status=validee')
        self.assertEqual(len(response.context['resources']), 1)

    def test_search_with_accents(self):
        """accents don't matter in the search string"""
        for text in ('validée', 'valid%C3%A9e', 'validee'):
            response = self.client.get(reverse('resource_home') + '?text=%s' % text)
            self.assertEqual(
                len(response.context['resources']), 1,
                msg="No result for text '%s'" % text
            )

    def test_search_on_array_fields(self):
        response = self.client.get(
            reverse('resource_home') + '?types_doc=audio&types_doc=website'
        )
        self.assertTrue(response.context['form'].is_valid(), msg=response.context['form'].errors)
        self.assertEqual(len(response.context['resources']), 1)
        self.assertEqual(response.context['resources'][0].type_doc, ['website'])

    def test_search_by_date(self):
        days_ago = (date.today() - timedelta(days=3)).strftime('%Y-%m-%d')
        today = date.today().strftime('%Y-%m-%d')
        response = self.client.get(
            reverse('resource_home') + f'?created_from={days_ago}&created_to={today}'
        )
        self.assertTrue(response.context['form'].is_valid(), msg=response.context['form'].errors)
        self.assertEqual(len(response.context['resources']), 2)
        # Only from side
        response = self.client.get(
            reverse('resource_home') + f'?updated_from={days_ago}'
        )
        self.assertTrue(response.context['form'].is_valid(), msg=response.context['form'].errors)
        self.assertEqual(len(response.context['resources']), 2)
        # Only to side
        response = self.client.get(
            reverse('resource_home') + f'?validated_to={days_ago}'
        )
        self.assertTrue(response.context['form'].is_valid(), msg=response.context['form'].errors)
        self.assertEqual(len(response.context['resources']), 0)

    def test_search_exclude_hidden_for_anonymous(self):
        """
        Resource.search_hidden allows to exclude resource from anonmyous search
        results.
        """
        res = Resource.objects.create(status='validee', title='Sera masquée')
        total_res = Resource.objects.filter(status='validee').exclude(type_r='can').count()

        search_form = ResourceSearchForm(AnonymousUser(), data={})
        self.assertTrue(search_form.is_valid())
        self.assertEqual(search_form.search().count(), total_res)
        res.search_hidden = True
        res.save()
        search_form = ResourceSearchForm(AnonymousUser(), data={})
        self.assertTrue(search_form.is_valid())
        self.assertEqual(search_form.search().count(), total_res - 1)
        search_form = ResourceSearchForm(self.user, data={})
        self.assertTrue(search_form.is_valid())
        # + res. non validée
        self.assertEqual(search_form.search().count(), total_res + 1)

    def test_search_resource_status(self):
        """'essai' status only shown for editors"""
        response = self.client.get(reverse('resource_home'))
        self.assertNotContains(response, "Statut")  # N/A for anonymous

        get_user_model().objects.create_user('jill', 'jill@example.org', 'jillpw')
        #  user without ressource.change_resource perm.
        self.client.login(username='jill', password='jillpw')
        response = self.client.get(reverse('resource_home'))
        self.assertNotContains(response, "Statut")
        self.assertNotContains(response, "Essais/divers")

        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_home'))
        self.assertContains(response, "Essais/divers")

    def test_search_resource_apprenant(self):
        r1 = Resource.objects.create(
            status='validee', title='Ressource élève', descr_eleve='Description', target_gr=['teacher', 'learner']
        )
        ShortLink.get_for_object(r1, create=True)
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.get(reverse('resource_home') + '?apprenant=on')
        self.assertEqual(len(response.context['resources']), 1)

    def test_search_broken_links(self):
        r1 = Resource.objects.create(status='validee', title='Ressource validée', genre='int')
        r2 = Resource.objects.create(
            status='validee', title='Partition musicale', url="http://www.example.org/404/",
            type_doc=['sheet music'], type_ped=['lesson plan'], broken=True
        )

        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('resource_home') + '?broken_links=on')
        self.assertEqual(len(response.context['resources']), 1)

    def test_search_pdf_output(self):
        qs =  '?text=partition&updated_from=2021-05-26&output=pdf'
        url = reverse('resource_home') + qs
        self.client.force_login(self.user, backend=MODEL)
        response = self.client.get(url)
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_search_xlsx_output(self):
        url = reverse('resource_home') + '?text=partition&output=xlsx'
        response = self.client.get(url)
        self.assertEqual(response['Content-Type'], XLSX_MIMETYPE)

    def test_simple_search_by_title(self):
        """These searches are restricted to resource group editions to add existing resources."""
        group = ResourceGroup.objects.create(title="Test Resource Group")
        r1 = Resource.objects.create(title='Ressource bidon')
        url = reverse('resource_search', args=[group.pk, 'json']) + '?term=bidon'
        self.client.login(username='john', password='johnpw')
        response = self.client.get(url)
        self.assertEqual(response.json(), {
            'total': 1,
            'results': [{'pk': r1.pk, 'description': '', 'label': 'Ressource bidon'}]
        })

        url = reverse('resource_search', args=[group.pk, 'html']) + '?term=bidon'
        response = self.client.get(url)
        self.assertContains(response, "<b>Ressource bidon</b>")


class ResourceBSNTests(ResourceTestsBase):

    def import_bsn_sample(self, sample=None):
        if sample is None:
            from .resource_get_sample import sample_result
            sample = sample_result
        elif isinstance(sample, str) and sample.endswith('.json'):
            sample = json.loads((Path(__file__).parent / sample).read_text())
        res = Resource.objects.create(
            uuid=sample.get('lomId', 'f041bbd2b6939aeffcc39612d17004ac').replace('archibald###', '')
        )
        educa.BSNImporter.populate_from_bsn(res, res.uuid, data=sample)
        return Resource.objects.get(pk=res.pk)

    def test_populate_resource(self):
        """
        Test populating a resource's field with a resource_get API result.
        """
        # sample resource links to those
        for code in ('MSN 36', 'SHS 31', 'SHS 32'):
            Specification.objects.create(objectif=self.objectif, code=code, status='published')
        res = self.import_bsn_sample()
        self.assertEqual(res.uuid, UUID("f041bbd2b6939aeffcc39612d17004ac"))
        self.assertEqual(res.updated.date(), date(2023, 7, 7))
        self.assertEqual(res.title, "L'eau : plus qu'un jeu")
        self.assertTrue(res.description.startswith("Jeu éducatif dont  l\u2019objectif est d\u2019aménager un territoire"))
        self.assertEqual(res.url,
            'http://www.globaleducation.ch/globaleducation_fr/pages/MA/MA_displayDetails?L=fr&Q=detail&MaterialID=1003314')
        self.assertEqual(res.keywords, 'D\xe9veloppement durable')
        self.assertEqual(res.languages, ['fr'])
        self.assertEqual(res.granularity, '3a')
        self.assertEqual(res.educ_level, ['compulsory education', 'post-compulsory education'])
        self.assertEqual(res.level, ['cycle 3'])
        self.assertEqual(res.ages, "7-12")
        self.assertEqual(res.version, '2012')
        self.assertFalse(res.cost)
        self.assertEqual(res.conditions, 'all rights reserved')
        self.assertEqual(res.target_gr, ['teacher'])
        self.assertEqual(res.type_doc, ['website'])
        self.assertEqual(res.type_ped, ['educational game'])
        self.assertTrue(res.description_ped.startswith("Jeu éducatif"))
        self.assertEqual(res.thumb_url, 'http://dsb-api.educa.ch/v2/file/default/538f2155a25b510a665c415f511a24ff.jpeg')
        self.assertEqual(res.duration, '12min42')
        self.assertEqual(res.size, 0)
        self.assertEqual(res.format, "application/pdf")
        self.assertEqual(res.tec_req, '')
        self.assertEqual(res.contribs.count(), 3)
        self.assertEqual({c.role for c in res.contribs.all()}, {'publisher', 'creator'})
        self.assertTrue(
            all(vcard.startswith('BEGIN:VCARD') for vcard in res.contribs.all().values_list('vcard', flat=True))
        )
        self.assertEqual(res.relations.count(), 1)
        self.assertEqual(res.resourceperlink_set.count(), 4)

    def test_parse_vcard(self):
        vcard = ("BEGIN:VCARD\r\nVERSION:3.0\r\nFN:Val\xe9rie Pidoux-Nyffeler\r\n"
                 "N:Pidoux-Nyffeler;Val\xe9rie;;;\r\nADR;WORK:;;;Bern;;3011;Suisse\r\n"
                 "EMAIL;PREF;INTERNET:valerie.pidoux-nyffeler@education21.ch\r\n"
                 "ORG:education21;;\r\nURL;PREF:http://www.education21.ch\r\n"
                 "UID:e541f989f0ab6f4f355d1663272cc36f\r\nEND:VCARD")
        rc = ResourceContrib.objects.create(vcard=vcard, date='2011-07-21T16:40:49+0200', role='creator')
        self.assertEqual(
            rc.pprint(),
            "Valérie Pidoux-Nyffeler\nBern  3011 Suisse\n"
            "valerie.pidoux-nyffeler@education21.ch\neducation21\n"
            "http://www.education21.ch"
        )
        self.assertEqual(
            rc.pprint(fmt='res_html'),
            "Valérie Pidoux-Nyffeler (education21)"
        )
        vcard = (
            "BEGIN:VCARD\r\nVERSION:3.0\r\nFN:\r\nN:;;;;\r\n"
            "LOGO;VALUE:uri:http://dsb-api.educa.ch/v2/file/default/24ea00165920b7cea133cea17908321e.jpeg\r\n"
            "ORG:default;;\r\nEND:VCARD")
        rc = ResourceContrib.objects.create(vcard=vcard, date='2011-07-21T16:40:49+0200', role='creator')
        self.assertEqual(
            rc.pprint(),
            '<img src="http://dsb-api.educa.ch/v2/file/default/24ea00165920b7cea133cea17908321e.jpeg"/>'
        )
        self.assertEqual(
            rc.pprint(fmt='res_html'),
            "??"
        )

    def test_duration_conversions(self):
        for source, result in (
                ('5min39', 'PT5M39S'), ('53s', 'PT53S'), ('53 sec', 'PT53S'), ('11min', 'PT11M'),
                ('1min06 (bande annonce)', 'PT1M6S')):
            self.assertEqual(educa.duration_to_iso8601(source), result)

    def test_format_resource_for_bsn(self):
        res = self.import_bsn_sample()
        res.duration = "23min40"
        res.save()
        bsn_struct = educa.as_struct(res)
        self.assertEqual(
            bsn_struct['classification'][0]['taxonPath'][0]['taxon'][1]['id'],
            'cycle 3'
        )
        # Temp. deactivated until reimplementation
        '''
        self.assertEqual(
            bsn_struct['curriculum'][0]['source']['name'], 'per'
        )
        self.assertEqual(
            bsn_struct['curriculum'][0]['taxonTree'],
            '{"Cycle 1": {"Langues": {"L 11": {"discipline": "Fran\\u00e7ais", "code": "L 11", "description": "Objectif 1", "object": "Th\\u00e9matique 1", "url_part": "L_11", "object_elements": []}}}}',
        )
        '''
        self.assertTrue(
            bsn_struct['general']['description'][0]['fr'].startswith("Jeu éducatif")
        )
        self.assertEqual(bsn_struct['general']['language'], ["fr"])
        self.assertTrue(
            bsn_struct['general']['keyword'][0]['fr'], "Développement durable"
        )
        self.assertEqual(bsn_struct['general']['aggregationLevel']['value'], '3')
        self.assertEqual(
            bsn_struct['education'][0]['learningResourceType']['documentary'][0]['value'],
            'website'
        )
        self.assertEqual(
            bsn_struct['education'][0]['context'][0]['value'],
            'compulsory education'
        )
        self.assertEqual(bsn_struct['lifeCycle']['version'], {'en': u'2012'})
        self.assertEqual(len(bsn_struct['lifeCycle']['contribute']), 1)
        self.assertIn(
            'bdper.plandetudes.ch/static/img', bsn_struct['metaMetadata']['contribute'][0]['entity'][0]
        )
        self.assertEqual(bsn_struct['rights']['cost']['value'], "no")
        self.assertEqual(
            bsn_struct['technical']['previewImage']['image'],
            res.thumb_url
        )
        self.assertEqual(bsn_struct['technical']['duration'], 'PT23M40S')
        self.assertEqual(bsn_struct['technical']['size'], 0)
        # Check relation
        relation = bsn_struct['relation'][0]
        self.assertEqual(relation['kind']['value'], 'has_part_of')
        self.assertTrue(relation['resource'][0]['description']['fr'].startswith('Matériel pédagogique'))
        self.assertTrue(
            relation['resource'][0]['identifier']['entry'].startswith('http://www.filmeeinewelt.ch')
        )
        # Check when thumb is defined instead of thumb_url
        with open(os.path.join(os.path.dirname(__file__), 'thumbnail.png'), 'rb') as fp:
            res.thumb.save(
                'thumbnail.png',
                File(fp)
            )
        self.addCleanup(os.unlink, res.thumb.path)
        res.thumb_url = ''
        res.save()
        bsn_struct = educa.as_struct(res)
        im = bsn_struct['technical']['previewImage']['image']
        self.assertTrue(im.startswith('https://'))
        self.assertTrue(im.endswith(res.thumb.url))

    def test_levels_for_bsn(self):
        self.assertEqual(educa.levels_for_bsn([]), [])
        self.assertEqual(educa.levels_for_bsn(['cycle 3']), [('cycle 3', None)])
        self.assertEqual(educa.levels_for_bsn(['3rd year']), [('cycle 1', '3rd and 4th year')])
        self.assertEqual(educa.levels_for_bsn(['cycle 1', '3rd year']), [('cycle 1', '3rd and 4th year')])
        self.assertEqual(educa.levels_for_bsn(['5th and 6th year']), [('cycle 2', '5th and 6th year')])

    def test_other_bsn_imports(self):
        """ Other import samples which produced crashes at some point. """
        from .resource_get_sample import example2
        res = self.import_bsn_sample(example2)
        self.assertEqual(res.title, "L'art en ville : à la découverte d'une sculpture")
