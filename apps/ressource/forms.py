import json
import logging
import operator
import os
from collections import OrderedDict
from datetime import datetime
from functools import reduce
from io import BytesIO

from django import forms
from django.conf import settings
from django.contrib.postgres.search import SearchQuery, SearchRank
from django.core.mail import EmailMessage
from django.core.validators import URLValidator
from django.db.models import Count, Case, F, Q, When
from django.forms.models import (
    BaseInlineFormSet, inlineformset_factory, modelformset_factory, model_to_dict,
)
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import format_html, mark_safe

import nh3
from tinymce.widgets import TinyMCE

from pper.models import Cellule, ContenuCellule, Discipline, Domain, Specification

from .models import (
    PARTNER_MAP, STATUS_CHOICES, Resource, ResourceCheck, ResourceContrib, ResourceFile,
    ResourceGroup, ResourceGroupLink, ResourcePERIndex, ResourcePerLink,
    ResourceRelation, ResourceSubmitter, SubTheme,
)
from .ontologies import (
    CYCLE_CHOICES, GRANLEVEL_CHOICES, TYPE_DOC_CHOICES, TYPE_DOC_MAIN,
    TYPE_PED_CHOICES, TYPE_RESOURCE,
)
from .pdf_gen import ResourcePDF

log = logging.getLogger(__name__)


class RichTextField(forms.CharField):
    widget = TinyMCE

    def __init__(self, **kwargs):
        kwargs["widget"] = self.widget
        super().__init__(**kwargs)

    def clean(self, value):
        value = super().clean(value)
        return nh3.clean(
            value,
            tags={
                'p', 'br', 'b', 'strong', 'u', 'i', 'em', 'ul', 'li', 'a', 'sub', 'sup',
            }
        )


class TypeDocIconSelectMultiple(forms.CheckboxSelectMultiple):
    option_template_name = 'widgets/type_doc_option.html'


class ResourceSearchForm(forms.Form):
    text = forms.CharField(
        label="Texte", required=False, widget=forms.TextInput(attrs={'autofocus': True}))
    domains = forms.ModelMultipleChoiceField(
        label="Domaines", required=False, queryset=Domain.objects.all(), widget=forms.CheckboxSelectMultiple
    )
    disciplines = forms.ModelMultipleChoiceField(
        label="Disciplines", required=False, queryset=Discipline.objects.filter(published=True),
        widget=forms.CheckboxSelectMultiple
    )
    cycles = forms.MultipleChoiceField(
        label="Cycles", required=False, choices=CYCLE_CHOICES, widget=forms.CheckboxSelectMultiple)
    types_doc = forms.MultipleChoiceField(
        label="Type de document", required=False, choices=TYPE_DOC_CHOICES, widget=TypeDocIconSelectMultiple)
    # Criteria only accessible for auth users (restricted at template level):
    status = forms.MultipleChoiceField(
        label="Statut", required=False, choices=STATUS_CHOICES, widget=forms.CheckboxSelectMultiple)
    visible = forms.NullBooleanField(label="Visibilité dans la recherche", required=False)
    granularity = forms.MultipleChoiceField(
        label="Granularité", required=False, choices=GRANLEVEL_CHOICES, widget=forms.CheckboxSelectMultiple)
    type_res = forms.MultipleChoiceField(
        label="Type de ressource", required=False, choices=TYPE_RESOURCE, widget=forms.CheckboxSelectMultiple)
    type_ped = forms.MultipleChoiceField(
        label="Type pédagogique", required=False, choices=TYPE_PED_CHOICES, widget=forms.CheckboxSelectMultiple)
    created_from = forms.DateField(
        label="Du", required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )
    created_to = forms.DateField(
        label="Au", required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )
    updated_from = forms.DateField(
        label="Du", required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )
    updated_to = forms.DateField(
        label="Au", required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )
    validated_from = forms.DateField(
        label="Du", required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )
    validated_to = forms.DateField(
        label="Au", required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )
    apprenant = forms.BooleanField(label="Ressource «apprenant·e»", required=False)
    broken_links = forms.BooleanField(label="Contient des liens brisés", required=False)
    # Pagination
    size = forms.IntegerField(widget=forms.HiddenInput, max_value=50, required=False)

    def __init__(self, user, *args, **kwargs):
        kwargs['initial'] = {'status': 'validee'}
        self.user = user
        super().__init__(*args, **kwargs)
        if self.is_bound:
            # autofocus might break position in page when going back
            del self.fields['text'].widget.attrs['autofocus']
        if not user.has_perm('ressource.change_resource'):
            self.fields['status'].choices = [
                c for c in self.fields['status'].choices if c[0] != 'essai'
            ]
            self.fields['types_doc'].choices = [
                c for c in self.fields['types_doc'].choices if c[0] in TYPE_DOC_MAIN
            ]

    def search(self, base_qs=None, filtre=None, prefetch=True):
        if not self.is_bound:
            raise RuntimeError("Cannot search on unvalidated form.")

        base_filters = {'partner': 'CIIP'}
        # Base filter depending on current user permissions
        if not self.user.has_perm('ressource.change_resource'):
            base_filters.update({'status': 'validee', 'broken': False})
        else:
            status_list = self.cleaned_data.get('status')
            if not status_list:
                status_list = [c[0] for c in self.fields['status'].choices]
            base_filters.update({'status__in': status_list})
        base_qs = base_qs if base_qs is not None else Resource.objects.all()
        base_qs = base_qs.filter(**base_filters).exclude(type_r='can')
        if not self.user.has_perm('ressource.change_resource'):
            base_qs = base_qs.exclude(search_hidden=True)

        if filtre:
            base_qs = base_qs.filter(filtre)
        if self.cleaned_data['text']:
            query = SearchQuery(self.cleaned_data['text'], config='fr')
            base_qs = base_qs.filter(fts=query).annotate(rank=SearchRank(F('fts'), query)).order_by('-rank')
        # Filtres:
        idx_qs = ResourcePERIndex.objects.select_related('resource', 'specification__objectif')
        idx_used = False
        if self.cleaned_data['domains']:
            idx_qs = idx_qs.filter(discipline__domain__in=self.cleaned_data['domains'])
            idx_used = True
        if self.cleaned_data['disciplines']:
            idx_qs = idx_qs.filter(discipline__in=self.cleaned_data['disciplines'])
            idx_used = True
        if self.cleaned_data['cycles']:
            idx_qs = idx_qs.filter(specification__objectif__cycle__in=self.cleaned_data['cycles'])
            idx_used = True
        if self.cleaned_data['types_doc']:
            # resource.type_doc is an array
            base_qs = base_qs.filter(type_doc__overlap=self.cleaned_data['types_doc'])
        if self.cleaned_data.get('visible') is False or self.cleaned_data.get('visible') is True:
            base_qs = base_qs.filter(search_hidden=not self.cleaned_data['visible'])
        if self.cleaned_data['granularity']:
            base_qs = base_qs.filter(granularity__in=self.cleaned_data['granularity'])
        if self.cleaned_data['type_res']:
            base_qs = base_qs.filter(type_r__in=self.cleaned_data['type_res'])
        if self.cleaned_data['type_ped']:
            # resource.type_ped is an array
            base_qs = base_qs.filter(type_ped__overlap=self.cleaned_data['type_ped'])
        for date_f in ('created', 'updated', 'validated'):
            dfrom, dto = self.cleaned_data[f'{date_f}_from'], self.cleaned_data[f'{date_f}_to']
            if dfrom or dto:
                is_dtime = isinstance(dfrom, datetime) or isinstance(dto, datetime)
                date_expr = '' if is_dtime else '__date'
                if not dto:
                    base_qs = base_qs.filter(**{f'{date_f}{date_expr}__gte': dfrom})
                elif not dfrom:
                    base_qs = base_qs.filter(**{f'{date_f}{date_expr}__lte': dto})
                else:
                    base_qs = base_qs.filter(**{f'{date_f}{date_expr}__range': (dfrom, dto)})
        if self.cleaned_data['apprenant']:
            base_qs = base_qs.annotate(num_shorts=Count('short_links')).exclude(descr_eleve='').filter(
                num_shorts__gt=0, target_gr__contains=['learner']
            )
        if self.cleaned_data['broken_links']:
            base_qs = base_qs.filter(broken=True)

        if idx_used:
            base_qs = base_qs.filter(
                pk__in=idx_qs.values_list('resource', flat=True).distinct()
            )
        if not base_qs.ordered:
            base_qs = base_qs.order_by('title')
        if prefetch:
            base_qs = base_qs.prefetch_related('files', 'resourcegrouplink_set', 'relations', 'other_relations')
        return base_qs

    def readable_criteria(self):
        """
        Produce a human readable version of search criteria, as a dict:
            {'Label': "val1, val2", ...}
        """
        crits = OrderedDict()
        if self.cleaned_data['text']:
            crits['Texte'] = '« %s »' % self.cleaned_data['text']
        for key, value in self.cleaned_data.items():
            if value and key not in ('text', 'size'):
                if hasattr(self.fields[key], 'choices') and not hasattr(self.fields[key], 'queryset'):
                    str_val = ', '.join([dict(self.fields[key].choices).get(str(v), str(v)) for v in value])
                else:
                    try:
                        str_val = ', '.join(str(v) for v in value)
                    except TypeError:
                        str_val = str(value)
                crits[self.fields[key].label] = str_val
        return crits


class ResourceSearchFormDateTime(ResourceSearchForm):
    """Equivalent to ResourceSearchForm, but allow ISO datetime inputs, typically for API usage."""
    created_from = forms.DateTimeField(label="Du", required=False)
    created_to = forms.DateTimeField(label="Au", required=False)
    updated_from = forms.DateTimeField(label="Du", required=False)
    updated_to = forms.DateTimeField(label="Au", required=False)
    validated_from = forms.DateTimeField(label="Du", required=False)
    validated_to = forms.DateTimeField(label="Au", required=False)


class ResourceSearchFormLOM(forms.Form):
    """Search through the BSN-like API."""
    query = forms.CharField(required=False)

    def __init__(self, user, **kwargs):
        self.user = user
        self.filters = {}
        super().__init__(**kwargs)

    def clean(self):
        data = super().clean()
        for key, val in self.data.items():
            # Handle two different parameter formats
            if key.startswith('filters['):
                clean_key = key.replace('filters[', '').replace('][]', '')
                self.filters.setdefault(clean_key, []).append(val.strip('"'))
            elif key == 'filters':
                try:
                    for flt_key, value in json.loads(val).items():
                        self.filters[flt_key] = value
                except json.JSONDecodeError:
                    raise forms.ValidationError("Invalid JSON data in filters parameter.")
        return data

    def search(self, filtre=None):
        partner = PARTNER_MAP.get(self.user.username, 'CIIP')
        base_qs = Resource.objects.filter(partner=partner)
        if self.cleaned_data['query']:
            query = SearchQuery(self.cleaned_data['query'], config='fr')
            base_qs = base_qs.filter(fts=query).annotate(rank=SearchRank(F('fts'), query)).order_by('-rank')
        for key, vals in self.filters.items():
            if key == 'learningResourceType':
                base_qs = base_qs.filter(Q(type_doc__overlap=vals) | Q(type_ped__overlap=vals))
            elif key == 'keywords':
                qfilters = [Q(keywords__unaccent__icontains=val) for val in vals]
                base_qs = base_qs.filter(reduce(operator.or_, qfilters))
            elif key == 'educaSchoolLevels':
                base_qs = base_qs.filter(level__overlap=[v.replace('_', ' ') for v in vals])
            elif key == 'educaSchoolSubjects':
                idx_qs = ResourcePERIndex.objects.select_related('resource').filter(discipline__lom_id__in=vals)
                base_qs = base_qs.filter(
                    pk__in=idx_qs.values_list('resource', flat=True).distinct()
                )
        if not base_qs.ordered:
            base_qs = base_qs.order_by('-created')
        return base_qs


class CustomInlineFormSet(BaseInlineFormSet):
    """This custom class prevent creation of the 'DELETE' field for blank forms."""
    def add_fields(self, form, index):
        super().add_fields(form, index)
        if form.instance.pk is None and 'DELETE' in form.fields:
            del form.fields['DELETE']


class ResourceRelationForm(forms.ModelForm):
    class Meta:
        model = ResourceRelation
        fields = ['resource', 'rtype', 'order', 'url', 'rel_file', 'description']
        widgets = {
            'description': forms.Textarea(attrs={'rows': '3'}),
            'order': forms.HiddenInput,
        }

    def has_changed(self):
        changed = super().has_changed()
        # Avoid change flag when only order is modified for a new instance
        if changed and self.changed_data == ['order'] and not self.instance.pk:
            return False
        return changed

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('url') and not cleaned_data.get('rel_file'):
            raise forms.ValidationError(
                "Vous devez remplir soit le champ URL, soit le champ Fichier."
            )
        elif cleaned_data.get('url') and cleaned_data.get('rel_file'):
            raise forms.ValidationError(
                "Vous ne pouvez pas remplir à la fois le champ URL et le champ Fichier."
            )


class ResourceGalleryForm(forms.ModelForm):
    class Meta:
        model = ResourceRelation
        fields = ['resource', 'other']
        widgets = {
            'other': forms.HiddenInput,
        }

    def save(self, **kwargs):
        self.instance.rtype = 'is_part_of'
        return super().save()


def get_object_from_perlink_value(value):
    if isinstance(value, str):
        content_type, content_pk = value.split('-')
        Klass = {
            'contenucellule': ContenuCellule,
            'cellule': Cellule,
            'specification': Specification,
            'discipline': Discipline,
        }.get(content_type)
        return Klass.objects.get(pk=int(content_pk))
    return value


def to_perlink_value(value):
    if isinstance(value, str):
        return value
    return '%s-%s' % (type(value).__name__.lower(), value.pk)


class ResourcePerLinkWidget(forms.CheckboxSelectMultiple):
    template_name = 'widgets/per_link.html'
    # JS code is in static/js/edition.js

    def format_label(self, obj):
        if hasattr(obj, 'contenu'):  # It's a ContenuCellule
            parent_spec = obj.cellule.specification
            return format_html('<span class="link{}">{}</span><br>{}',
                               parent_spec.get_domain().abrev, parent_spec.title(), mark_safe(obj.contenu.texte))
        elif isinstance(obj, Cellule):
            spec = obj.get_super()
            return format_html(
                'Cellule <a href="{spec_url}#{cell_pk}">#{cell_pk}</a> de <span class="link{dom_abrev}">{spec_title}</span>',
                spec_url=spec.get_absolute_url(), cell_pk=obj.pk,
                dom_abrev=spec.get_domain().abrev, spec_title=str(spec),
            )
        else:
            return format_html(
                '<span class="link{}">{}</span>',
                obj.get_domain().abrev, str(obj)
            )

    def format_value(self, value):
        if value:
            if not isinstance(value, list):
                value = [value]
            self.choices = (
                (to_perlink_value(v),
                 self.format_label(get_object_from_perlink_value(v))
                ) for v in value
            )
            return value
        return []

    def create_option(self, name, value, label, selected, *args, **kwargs):
        return super().create_option(name, value, label, True, *args, **kwargs)


class ResourcePerLinkField(forms.MultipleChoiceField):
    """Specialized form to get ResourcePerLink choices."""
    widget = ResourcePerLinkWidget

    def has_changed(self, initial, data):
        if initial and [to_perlink_value(initial)] == data:
            return False
        return bool(data)

    def valid_value(self, value):
        # Checking only content type and that value is an int, for now
        content_type, value = value.split('-')
        if content_type not in {'cellule', 'contenucellule', 'specification', 'discipline'}:
            return False
        try:
            int(value)
        except Exception:
            return False
        return True


class ResourcePerLinkForm(forms.ModelForm):
    per_link = ResourcePerLinkField(label="Plan d'études romand", required=False)

    class Meta:
        model = ResourcePerLink
        fields=('per_link',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get('instance'):
            self.fields['per_link'].initial = kwargs['instance'].content_object

    def save(self, *args, **kwargs):
        # pecularity: this form can save several ResourcePerLink objects
        objs = []
        for content in self.cleaned_data['per_link']:
            objs.append(ResourcePerLink.objects.create(
                resource=self.instance.resource,
                content_object=get_object_from_perlink_value(content)
            ))
        # return only the first to parent modelformset.
        return objs[0]


class ResourceContribForm(forms.ModelForm):
    fn = forms.CharField(label="Nom", max_length=50, required=False)
    email = forms.EmailField(label="Courriel", required=False)
    org = forms.CharField(label="Organisation", max_length=80, required=False)
    adr = forms.CharField(label="Adresse", max_length=200, required=False)
    url = forms.URLField(label="URL", required=False)

    class Meta:
        model = ResourceContrib
        fields = ()

    def __init__(self, *args, **kwargs):
        if kwargs.get('instance') is not None:
            kwargs['initial'] = self._contrib_to_dict(kwargs['instance'])
        elif isinstance(kwargs.get('initial'), self._meta.model):
            kwargs['initial'] = self._contrib_to_dict(kwargs['initial'])
        super().__init__(*args, **kwargs)

    def _contrib_to_dict(self, contrib):
        """From ResourceContrib instance with .vcard to {'fn':, 'email', …}"""
        dict_res = {}
        data = contrib._parse_vcard()
        for field_name in self.declared_fields.keys():
            if data.get(field_name.upper()):
                dict_res[field_name] = data[field_name.upper()]
        return dict_res

    def clean(self):
        # Either fn or org must be supplied
        cleaned_data = super().clean()
        if not cleaned_data.get('fn') and not cleaned_data.get('org'):
            raise forms.ValidationError(
                "Vous devez renseigner au moins l'un des champs nom ou organisation"
            )

    @cached_property
    def changed_data(self):
        if self.instance.pk:
            return super().changed_data
        # For new forms, consider a field changed if it has a value (not considering initial)
        data = []
        for name, field in self.fields.items():
            prefixed_name = self.add_prefix(name)
            data_value = field.widget.value_from_datadict(self.data, self.files, prefixed_name)
            if data_value:
                data.append(name)
        return data

    def save(self, resource=None, **kwargs):
        if not self.changed_data:
            return self.instance
        lines = []
        for field, value in self.cleaned_data.items():
            if not value or field == 'id':
                continue
            if field == 'adr':
                # https://tools.ietf.org/html/rfc6350#section-6.3
                value = ';;%s;;;;' % value  # 3rd pos is street
            lines.append("%s:%s" % (field.upper(), value))
        vcard = 'BEGIN:VCARD\r\nVERSION:3.0\r\n%s\r\nEND:VCARD' % '\r\n'.join(lines)
        self.instance.vcard = vcard
        self.instance.role = self.prefix.split('-')[0]
        self.instance.save()
        if resource is not None:
            resource.contribs.add(self.instance)
        return self.instance


class BaseContribFormSet(forms.BaseModelFormSet):
    def __init__(self, resource=None, prefix=None, **kwargs):
        self.resource = resource
        if resource is not None and resource.pk is not None:
            new_resource = False
            queryset = resource.contribs.filter(role=prefix)
        else:
            new_resource = True
            queryset = ResourceContrib.objects.none()
        super().__init__(prefix=prefix, queryset=queryset, **kwargs)
        if new_resource:
            self.extra = len(kwargs['initial']) + 1

    def add_fields(self, form, index):
        super().add_fields(form, index)
        if form.instance.pk is None:
            del form.fields['DELETE']
        else:
            # The deletion is triggered by a click on a delete icon in the form
            form.fields['DELETE'].widget = forms.HiddenInput(attrs={'class': 'DELETE'})

    def save_new(self, form, commit=True):
        """Saves and returns a new model instance for the given form."""
        return form.save(commit=commit, resource=self.resource)


class BaseFilesFormSet(BaseInlineFormSet):
    def add_fields(self, form, index):
        super().add_fields(form, index)
        if form.instance.pk is None:
            del form.fields['DELETE']
        else:
            del form.fields['rfile']
            # The deletion is triggered by a click on a delete icon in the form
            form.fields['DELETE'].widget = forms.HiddenInput(attrs={'class': 'DELETE'})


class BaseGalleryFormSet(BaseInlineFormSet):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.order_by('other__files__rfile')


class SizeWidget(forms.TextInput):
    template_name = 'widgets/size.html'

    def format_value(self, value):
        if value:
            # from bytes to megabytes
            try:
                return str(round(float(value) / (1024 * 1024), 3))
            except ValueError:
                # This can happen when an entered value is wrong and redisplayed for correction
                return value
        return ''


class SizeField(forms.IntegerField):
    widget = SizeWidget

    def to_python(self, value):
        if value in self.empty_values:
            return None
        if isinstance(value, int):
            return value
        try:
            # from megabytes to bytes
            return int(float(value.replace(',', '.')) * 1024 * 1024)
        except ValueError:
            raise forms.ValidationError("Impossible de convertir %s en nombre à virgule" % value)


class AgesWidget(forms.MultiWidget):
    template_name = 'widgets/ages.html'

    def __init__(self, attrs=None):
        widgets = (
            forms.NumberInput(attrs=attrs),
            forms.NumberInput(attrs=attrs),
        )
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split('-')
        return []

    def value_from_datadict(self, *args):
        values = [v for v in super().value_from_datadict(*args) if v not in [None, '']]
        if values:
            return "-".join(str(v) for v in values)
        return values


class TypeDocBoldSelectMultiple(forms.CheckboxSelectMultiple):
    option_template_name = 'widgets/type_doc_option_txt.html'


class ClearableMultFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True

    def value_from_datadict(self, data, files, name):
        upload = super().value_from_datadict(data, files, name)
        if isinstance(upload, list):
            if len(upload):
                # Any other uploaded files should be handled later in save()
                return upload[0]
            return None
        return upload


class ResourceFileForm(forms.ModelForm):
    class Meta:
        model = ResourceFile
        fields = ('id', 'rfile', 'title')
        widgets = {
            'rfile': ClearableMultFileInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].required = False

    def save(self, **kwargs):
        if not self.instance.title:
            # Title from file name
            self.instance.title = os.path.splitext(self.instance.rfile.name)[0]

        for_gallery = False
        if self.data.get('content-type-selector') == 'gallery' or self.instance.resource.content_type == 'gallery':
            for_gallery = True
            gallery = self.instance.resource
            # Transform the file in a linked resource
            new_res = Resource.objects.create(
                title=self.instance.title, updated=timezone.now(), status=gallery.status,
                need_auth=gallery.need_auth, validated=gallery.validated,
                type_doc=['image'],
            )
            self.instance.resource = new_res
            # Link the Resource to the gallery ressource
            ResourceRelation.objects.create(resource=gallery, rtype='is_part_of', other=new_res)

        obj = super().save(**kwargs)
        up_files = self.files.getlist(self.add_prefix('rfile')) if self.files else []
        if len(up_files) > 1:
            # If multiple files were uploaded, save them all
            for f in up_files:
                if f != self.cleaned_data['rfile']:
                    img_title = os.path.splitext(f.name)[0]
                    if for_gallery:
                        new_res = Resource.objects.create(
                            title=img_title, updated=timezone.now(), status=gallery.status,
                            need_auth=gallery.need_auth, validated=gallery.validated,
                            type_doc=['image'],
                        )
                        self._meta.model.objects.create(
                            resource=new_res, rfile=f, title=img_title
                        )
                        ResourceRelation.objects.create(resource=gallery, rtype='is_part_of', other=new_res)
                    else:
                        self._meta.model.objects.create(
                            resource=obj.resource, rfile=f, title=img_title
                        )
        return obj


class ResourceEditFormBase(forms.ModelForm):
    class Meta:
        model = Resource
        fields = ()
        widgets = {
            'cell' : forms.HiddenInput,
            'title': forms.TextInput(attrs={'size': 45}),
            'url':   forms.TextInput(attrs={'size': 45}),
            'ages': AgesWidget,
            'type_doc': TypeDocBoldSelectMultiple,
        }
        field_classes = {
            'size': SizeField,
            'description': RichTextField,
            'descr_eleve': RichTextField,
            'source': RichTextField,
            'submitted_mat': RichTextField,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ResourceFileFormSet = inlineformset_factory(
            Resource, ResourceFile,
            form=ResourceFileForm, formset=BaseFilesFormSet, extra=1
        )
        GalleryFormSet = inlineformset_factory(
            Resource, ResourceRelation, form=ResourceGalleryForm, formset=BaseGalleryFormSet,
            fk_name='resource', extra=0
        )
        ContributorFormSet = modelformset_factory(
            ResourceContrib, form=ResourceContribForm, formset=BaseContribFormSet,
            can_delete=True, extra=1
        )
        per_link_initial = self.initial.get('resourceperlink_set', [])
        PerLinkFormSet = inlineformset_factory(
            Resource, ResourcePerLink, form=ResourcePerLinkForm, formset=BaseInlineFormSet,
            extra=len(per_link_initial) + 1
        )

        self.formsets = {
            'files': ResourceFileFormSet(
                data=kwargs.get('data'), files=kwargs.get('files'), instance=self.instance,
            ),
            'gallery': GalleryFormSet(
                data=kwargs.get('data'), prefix='gallery', instance=self.instance,
                queryset=ResourceRelation.objects.filter(other__isnull=False, other__type_doc=['image']),
            ),
            'author': ContributorFormSet(
                data=kwargs.get('data'), prefix='author', resource=self.instance,
                initial=[
                    contrib for contrib in self.initial.get('contribs', [])
                    if contrib.role in ['author', 'creator']
                ],
            ),
            'editor': ContributorFormSet(
                data=kwargs.get('data'), prefix='editor', resource=self.instance,
                initial=[
                    contrib for contrib in self.initial.get('contribs', [])
                    if contrib.role == 'editor'
                ],
            ),
            'publisher': ContributorFormSet(
                data=kwargs.get('data'), prefix='publisher', resource=self.instance,
                initial=[
                    contrib for contrib in self.initial.get('contribs', [])
                    if contrib.role == 'publisher'
                ],
            ),
            # Only setting initial data for initial GET display not POST, so as forms get saved
            'per_link': PerLinkFormSet(
                instance=self.instance,
                data=kwargs.get('data'),
                initial=[{'per_link': inst.content_object} for inst in per_link_initial]
                    if not kwargs.get('data') else None,
            ),
        }
        # Mark fields mandatory for the LOM
        for fname in Resource.MANDATORY_LOM_FIELDS:
            if fname not in self.fields:
                continue
            if 'class' in self.fields[fname].widget.attrs:
                self.fields[fname].widget.attrs['class'] += ' bsn_required'
            else:
                self.fields[fname].widget.attrs['class'] = 'bsn_required'

    def all_errors(self):
        return [self.errors] + [fs.errors for fs in self.formsets.values()]

    def clean(self):
        cleaned_data = super().clean()
        valid_file_forms = [
            form for form in self.formsets['files'].forms
            if form.is_valid() and form.cleaned_data and form not in self.formsets['files'].deleted_forms
        ]
        valid_gallery_forms = [
            form for form in self.formsets['gallery'].forms
            if form.is_valid() and form.cleaned_data and form not in self.formsets['gallery'].deleted_forms
        ]
        has_url = bool(cleaned_data.get('url'))
        has_files = len(valid_file_forms)
        has_images = len(valid_gallery_forms)
        num_filled = len([
            fld for fld in [cleaned_data.get('url'), valid_file_forms, valid_gallery_forms] if fld
        ])
        if not any([has_url, has_files, has_images]):
            raise forms.ValidationError(
                "Vous devez remplir soit le champ URL, soit le champ Fichier, soit au moins une image de gallerie."
            )
        elif has_url and (has_files or has_images):
            raise forms.ValidationError(
                "Vous ne pouvez pas remplir à la fois le champ URL et le champ Fichier."
            )

    def is_valid(self):
        return all(
            [super().is_valid()] + [fset.is_valid() for fset in self.formsets.values()]
        )

    def has_errors(self):
        return self.is_bound and self.errors or any (frm.errors for frm in self.formsets.values())

    @property
    def submit_label(self):
        return "Modifier" if self.instance.pk else "Ajouter %s" % self.instance._meta.verbose_name

    @cached_property
    def groups(self):
        """Filtering Resource.GROUPS with fields present in the form."""
        _groups = []
        for group, field_names in Resource.GROUPS:
            fnames = [fname for fname in field_names if fname in self.fields]
            if fnames or group in ('Relations', "Plan d'études"):
                _groups.append((group, fnames))
        return _groups

    def grouped_fields(self):
        for group, field_names in self.groups:
            fields = [f for f in [self[fname] for fname in field_names] if not f.is_hidden]
            yield (group, fields)

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        need_reindex = self.formsets['per_link'].has_changed()
        for fset in self.formsets.values():
            fset.save()
        if need_reindex:
            instance.index()
        return instance


class ResourceEditForm(ResourceEditFormBase):
    """This edit form is supposed to be used by authorized people."""
    add_in_group = forms.IntegerField(widget=forms.HiddenInput, required=False)

    class Meta(ResourceEditFormBase.Meta):
        fields = None
        # 'contribs' is managed through author/editor/publisher formsets.
        exclude = (
            'created', 'updated', 'validated', 'fts', 'contribs', 'uuid', 'partner',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        relations_initial = self.initial.get('resourcerelation_set', [])
        ResourceRelationFormSet = inlineformset_factory(
            Resource, ResourceRelation, fk_name='resource', form=ResourceRelationForm, formset=CustomInlineFormSet, extra=len(relations_initial) + 1,
        )
        self.formsets['relations'] = ResourceRelationFormSet(
            instance=self.instance, data=kwargs.get('data'), files=kwargs.get('files'),
            queryset=ResourceRelation.objects.filter(other__isnull=True).order_by('order'),
            initial=[model_to_dict(inst) for inst in relations_initial],
        )


class ResourceSuggestionForm(ResourceEditFormBase):
    """A public add-only form to allow for anyone to suggest resources."""
    class Meta(ResourceEditFormBase.Meta):
        fields = (
            'title', 'description', 'url', 'languages',  # general
            'submitted_mat',  # specifics
            'qual_decisive', 'qual_collab', 'qual_counsels', 'qual_tested', 'qual_eval', 'qual_guarant',  # quality
            'version',  # (see ResourceEditForm.Meta note for contribs) lifecycle
            'type_doc', 'type_ped', 'target_gr', 'educ_level', 'learn_duration', 'description_ped',  # pedagogical
            'conditions',  # rights
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fname in ('title', 'description', 'languages'):
            self.fields[fname].required = True
        subm_form = forms.modelform_factory(ResourceSubmitter, fields='__all__')()
        for name, field in subm_form.fields.items():
            if name == 'resource':
                continue  # Will be filled at save time
            self.fields['subm_%s' % name] = field

    @cached_property
    def groups(self):
        return [g for g in super().groups if g[0] != "Relations"]

    @property
    def submit_label(self):
        return "Soumettre la ressource"

    def save(self, **kwargs):
        self.instance.type_r = 'med'
        res = super().save(**kwargs)
        # Save submitter data
        subm_dict = {fname[5:]: value for fname, value in self.cleaned_data.items() if fname.startswith('subm_')}
        ResourceSubmitter.objects.create(resource=res, **subm_dict)
        return res

    def send_email_confirmation(self):
        # The initial intent was to directly send to the submitter (to=[resource.resourcesubmitter.email])
        # But due to from_email restriction (from @2xlibre.net address), we send to resource admins
        email = EmailMessage(
            "Soumission d'une ressource pour le Plan d'études romand",
            "De : %s\n\n"
            "Bonjour,\n"
            "Nous avons bien reçu votre proposition de ressource pour le Plan d'études romand et nous vous en remercions.\n"
            "Vous en trouverez une copie ci-jointe au format PDF.\n"
            "Nous vous informerons bientôt de la suite que nous donnerons à votre proposition.\n\n"
            "Cordiales salutations,\n"
            "L'équipe de gestion du Plan d'études romand." % self.instance.resourcesubmitter.email,
            from_email=settings.SERVER_EMAIL,
            to=[settings.RESOURCE_ADMINS[0]],
            cc=settings.RESOURCE_ADMINS[1:],
        )
        pdf_content = BytesIO()
        pdf = ResourcePDF(self.instance)
        field_groups = dict(Resource.GROUPS)
        pdf.produce(pdf_content, exclude=field_groups['E-media'] + field_groups['Gestion et suivi'])
        email.attach("soumission_ressource.pdf", pdf_content.getvalue(), "application/pdf")
        email.send()


class ResourceGroupEditForm(forms.ModelForm):
    class Meta:
        model = ResourceGroup
        fields = (
            'title', 'subtitle', 'subtitle2', 'groupment', 'description',
            'mer_url', 'domain', 'objectif', 'tags', 'levels', 'grp_status',
        )
        widgets = {
            'tags': forms.CheckboxSelectMultiple,
        }
        field_classes = {
            'description': RichTextField,
        }


class ResourceGroupLinkForm(forms.ModelForm):
    subthemes = forms.ModelMultipleChoiceField(
        queryset=SubTheme.objects.none(),
        label="Sous-thèmes",
        widget=forms.CheckboxSelectMultiple,
        required=False
    )

    class Meta:
        model = ResourceGroupLink
        fields = ('resource', 'group', 'position', 'number', 'subthemes')
        widgets = {
            'resource': forms.HiddenInput,
            'group': forms.HiddenInput,
            'position': forms.HiddenInput(attrs={'class': 'position'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not kwargs.get('instance'):
            return
        self.fields['subthemes'].queryset = kwargs['instance'].group.subthemes.order_by('num')
        if self.fields['subthemes'].queryset.count() == 0:
            del self.fields['subthemes']
        self.fields['number'].required = False


class SubThemeForm(forms.ModelForm):
    class Meta:
        model = SubTheme
        fields = ('group', 'num', 'name')
        widgets = {
            'group': forms.HiddenInput,
            'num': forms.TextInput(attrs={'class': 'num'}),
            'name': forms.TextInput(attrs={'class': 'name'}),
        }

    def clean_num(self):
        num = self.cleaned_data['num']
        if num and num.endswith('.'):
            raise forms.ValidationError("Ne terminez pas le numéro avec un point")
        return num


class ResourcePerLinkFormIntern(forms.Form):
    search_box = forms.CharField(
        widget=forms.TextInput(attrs={
            'data-autourl': reverse_lazy('resource_search', args=[0, 'json']),
            'class': 'search_box',
        }),
        required=False,
    )
    cat_search_box = forms.CharField(
        widget=forms.TextInput(attrs={
            'data-autourl': reverse_lazy('resource_grp_search', args=['json']),
            'class': 'search_box',
        }),
        required=False,
    )
    resource_pk = forms.IntegerField(
        widget=forms.HiddenInput(attrs={'class': 'chosen_result_pk'}),
        required=False,
    )
    catalog_pk = forms.IntegerField(
        widget=forms.HiddenInput(attrs={'class': 'chosen_result_pk'}),
        required=False,
    )

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('resource_pk') and not cleaned_data.get('catalog_pk'):
            raise forms.ValidationError("Vous devez indiquez une ressource ou un catalogue à lier")
        return cleaned_data


class RadioSelectCheckStatus(forms.RadioSelect):
    def optgroups(self, *args, **kwargs):
        labels = {
            'ok': mark_safe(
                '<span class="check-ok">OK</span>, tous les liens de la ressource sont accessibles '
                'et pointent effectivement vers les ressources décrites'
            ),
            'unk': mark_safe(
                '<span class="check-unk">Incertain</span>, un ou plusieurs liens de cette ressource '
                'ne semblent pas pointer vers la bonne ressource, détails dans les commentaires'
            ),
            'ko': mark_safe(
                '<span class="check-ko">KO</span>, soit le lien principal n’est plus valable, soit un '
                'autre lien dans la ressource pointe vers une ressource indésirable.'
            ),
        }
        self.choices = [(ch[0], labels.get(ch[0], ch[1])) for ch in self.choices if ch[0]]
        return super().optgroups(*args, **kwargs)


class ResourceCheckForm(forms.ModelForm):
    class Meta:
        model = ResourceCheck
        fields = ['status', 'comment']
        widgets = {
            'status': RadioSelectCheckStatus,
        }
