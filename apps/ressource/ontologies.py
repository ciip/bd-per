"""
Ontologies for resources, both BD-PER specific and Educa/LOM specific.
"""

TYPE_RESOURCE = (
    ('mer', "Moyen d'enseignement romand (officiel)"),
    ('merx', "MER - Site externe"),
    ('can', "Précision cantonale"),
    ('med', "Ressource numérique complémentaire (RN-PER)"),
    ('rno', "Ressource numérique officielle (RN-MER)"),
    ('epro', "Ressource EPROCOM"),
)

EMEDIA_CATEG_CHOICES = (
    ('sdm-act', "Semaine des médias – activités"),
    ('sdm-mat', "Semaine des médias – matériel"),
    ('jeunes', "Jeunes et médias"),
)

GENRE_CHOICES = (
    ('fiction', "fiction"),
    ('anim_film', "film d'animation"),
    ('anim_3d', "animation 3D"),
    ('images', "galerie d'images"),
    ('doc', "documentaire"),
    ('rep', "reportage"),
    ('int', "interview"),
    ('dia', "diagramme"),
    ('txt', "texte"),
    ('graph', "graphique"),
    ('sgame', "serious-game"),
)

LANGUAGE_CHOICES = (
    ('en', 'anglais'),
    ('it', 'italien'),
    ('fr', 'français'),
    ('de', 'allemand'),
    ('rh', 'rhéto-roman'),
    ('es', 'espagnol'),
    ('pt', 'portugais'),
    ('la', 'latin'),
    ('tr', 'turc'),
)

# LOM-CH, 4.1
FORMAT_CHOICES = (
    ('text', (
        ('text/html', 'html'),
        ('text/plain', 'texte pur'),
        ('text/richtext', 'texte enrichi'),
        ('text/xml', 'xml'),
    )),
    ('image', (
        ('image/jpeg', 'jpeg'),
        ('image/bmp', 'bmp'),
        ('image/gif', 'gif'),
        ('image/png', 'png'),
        ('image/tiff', 'tiff'),
        ('image/x-wmf', 'x-wmf'),
    )),
    ('video', (
        ('video/mpeg', 'mpeg'),
        ('video/avi', 'avi'),
        ('video/x-flv', 'flash'),
        ('video/quicktime', 'quicktime'),
    )),
    ('audio', (
        ('audio/mp3', 'mp3'),
        ('audio/ogg', 'ogg'),
        ('audio/basic', 'basic'),
        ('audio/midi', 'midi'),
        ('audio/x-wav', 'x-wav'),
        ('audio/mpeg', 'mpeg'),
    )),
    ('application', (
        ('application/pdf', 'pdf'),
        ('application/base64', 'base64'),
        ('application/binary', 'binary'),
        ('application/java', 'java'),
        ('application/macbinhex40', 'macbinhex40'),
        ('application/msexcel', 'msexcel'),
        ('application/msword', 'msword'),
        ('application/postscript', 'postscript'),
        ('application/ppt', 'ppt'),
        ('application/rtf', 'rtf'),
        ('application/x-compressed', 'x-compressed'),
        ('application/x-gzip-compressed', 'x-gzip-compressed'),
        ('application/zip', 'zip'),
    )),
    ('model', (
        ('model/vrml', 'vrml'),
    )),
    ('non_digital', 'Non numérique'),
)

# LOM-CH, 5.5
TARGETGROUP_CHOICES = (
    ('learner', "Apprenants"),
    ('teacher', "Enseignants"),
    ('manager', "Responsables d'établissement scolaire"),
    ('parent', "Parents"),
    ('other', "Autres"),
)

# LOM-CH, 5.2.2
TYPE_PED_CHOICES = (
    ('open activity', "activité libre"),
    ('workshop', "atelier"),
    ('self assessment', "auto-évaluation"),
    ('demonstration', "démonstration"),
    ('case study', "étude de cas"),
    ('formative evaluation', "évaluation formative"),
    ('summative evaluation', "évaluation sommative"),
    ('exercise', "exercice"),
    ('experiment', "expérience"),
    ('presentation', "exposé, présentation"),
    ('exploration', "exploration"),
    ('guide', "guide/tutoriel"),
    ('educational game', "jeu éducatif"),
    ('role play', "jeu de rôle"),
    ('methodological_tool', "outil méthodologique"),
    ('project', "projet"),
    ('enquiry-oriented activity', "recherche d'information"),
    ('lesson plan', "scénario pédagogique"),
    ('simulation', "simulation"),
    ('tutorial', "tutoriel"),
)

# LOM-CH, 5.2.1
# Third position is an optional char mapping to the multimediaicons font.
TYPE_DOC = (
    ('app', "app"),
    ('bibliography', "bibliographie"),
    ('map', "carte"),
    ('audio', "document sonore", 'I'),
    ('text', "document textuel", '='),
    ('video', "document vidéo", '0'),
    ('spreadsheet', "feuille de calcul"),
    ('guide', "guide"),
    ('animated image', "image animée"),
    ('image', "image fixe", 'B'),
    ('application', "logiciel", '@'),
    ('website', "page web", 'a'),
    ('sheet music', "partition musicale", 'd'),
    ('glossary', "référence/glossaire"),
    ('resource for IWB', "ressource pour TBI"),
)
TYPE_DOC_CHOICES = tuple((tp[0], tp[1]) for tp in TYPE_DOC)
# Shown in bold in edit form
TYPE_DOC_MAIN = ['text', 'audio', 'video', 'image', 'application', 'website']

# LOM-CH, 5.8
DIFFICULTY_CHOICES = (
    ('very easy', "Très facile"),
    ('easy', "Facile"),
    ('medium', "Moyen"),
    ('difficult', "Difficile"),
    ('very difficult', "Très difficile"),
)

# LOM-CH, 5.9
LEARN_DURATION_CHOICES = (
    ('up_to_one_lesson', "Moins d'une leçon"),
    ('one_to_five_lessons', "D'une à cinq leçons"),
    ('more_than_five_lessons', "Plus de cinq leçons"),
)

# LOM-CH, 1.8 (2a/2b and 3a/3b is a PER extension)
GRANLEVEL_CHOICES = (
    ('1', "Niveau 1 – ressource indivisible"),
    ('2a', "Niveau 2 – séquence pédagogique"),
    ('2b', "Niveau 2 – lot de ressources indivisibles"),
    ('3a', "Niveau 3 – ensemble de séquences pédagogiques"),
    ('3b', "Niveau 3 – moyen d'enseignement"),
    ('4', "Niveau 4 – collection ou portail de ressources pédagogiques"),
)

# LOM-CH, 2.3.1
LIFECYCLE_ROLE_CHOICES = (
    ('author', "Auteur"),
    ('editor', "Contributeur"),
    ('publisher', "Éditeur"),
)

# LOM-CH, 3.2
META_ROLE_CHOICES = (
    ('creator', "Auteur"),
    ('validator', "Validateur"),
)

# Used in search form
CYCLE_CHOICES = (
    ('1', "Cycle 1"),
    ('2', "Cycle 2"),
    ('3', "Cycle 3"),
)

# Note: GROUP_LEVEL_CHOICES is a subset of LEVEL_CHOICES
LEVEL_CHOICES = (
    ('cycle 1', "Cycle 1"),
    ('1st and 2nd year', "Cycle 1 - 1re-2e"),
    ('3rd and 4th year', "Cycle 1 - 3e-4e"),
    ('1st year', "Cycle 1 - 1re"), # no equiv. in the BSN ontology
    ('2nd year', "Cycle 1 - 2e"), # no equiv. in the BSN ontology
    ('3rd year', "Cycle 1 - 3e"), # no equiv. in the BSN ontology
    ('4th year', "Cycle 1 - 4e"), # no equiv. in the BSN ontology
    ('cycle 2', "Cycle 2"),
    ('5th and 6th year', "Cycle 2 - 5e-6e"),
    ('7th and 8th year', "Cycle 2 - 7e-8e"),
    ('5th year', "Cycle 2 - 5e"), # no equiv. in the BSN ontology
    ('6th year', "Cycle 2 - 6e"), # no equiv. in the BSN ontology
    ('7th year', "Cycle 2 - 7e"), # no equiv. in the BSN ontology
    ('8th year', "Cycle 2 - 8e"), # no equiv. in the BSN ontology
    ('cycle 3', "Cycle 3"),
    ('9th year', "Cycle 3 - 9e"),
    ('10th year', "Cycle 3 - 10e"),
    ('11th year', "Cycle 3 - 11e"),
)

# Keep it for historical migration purpose
GROUP_LEVEL_CHOICES = (
    ('1st_year', "Cycle 1 - 1re"), # no equiv. in the BSN ontology
    ('2nd_year', "Cycle 1 - 2e"), # no equiv. in the BSN ontology
    ('3rd_year', "Cycle 1 - 3e"), # no equiv. in the BSN ontology
    ('4th_year', "Cycle 1 - 4e"), # no equiv. in the BSN ontology
    ('5th_year', "Cycle 2 - 5e"), # no equiv. in the BSN ontology
    ('6th_year', "Cycle 2 - 6e"), # no equiv. in the BSN ontology
    ('7th_year', "Cycle 2 - 7e"), # no equiv. in the BSN ontology
    ('8th_year', "Cycle 2 - 8e"), # no equiv. in the BSN ontology
    ('9th_year', "Cycle 3 - 9e"),
    ('10th_year', "Cycle 3 - 10e"),
    ('11th_year', "Cycle 3 - 11e"),
)

# LOM-CH v.1.2, 5.6
EDUCATION_LEVELS = (
    ('early_childhood', "Petite enfance", "LOM-CHv1.1"),
    ('compulsory education', "Scolarité obligatoire", "LREv3.0"),
    ('post-compulsory education', "Degré secondaire II", "LOM-CHv1.0"),
    ('special education', "Enseignement spécialisé", "LREv3.0"),
    ('higher education', "Degré tertiaire", "LOM-CHv1.0"),
    ('continuing education', "Formation continue", "LREv3.0"),
    ('distance education', "Enseignement à distance", "LREv3.0"),
    ('Independent of levels', "Indépendant des degrés", "LOM-CHv1.0"),
    ('education context other', "Non défini", "LOMv1.0"),
)
EDUCATION_LEVEL_CHOICES = tuple((lev[0], lev[1]) for lev in EDUCATION_LEVELS)

CONDITIONS_CHOICES = (
    ('all rights reserved', 'Tous droits réservés'),
    ('cc_by', 'CC BY (Paternité)'),
    ('cc_by_sa', "CC BY-SA (Paternité - partage à l'identique)"),
    ('cc_by_nd', 'CC BY-ND (Paternité - pas de modification)'),
    ('cc_by_nc', "CC BY-NC (Paternité - pas d'utilisation commerciale)"),
    ('cc_by_nc_sa', "CC BY-NC-SA (Paternité - pas d'utilisation commerciale - partage à l'identique)"),
    ('cc_by_nc_nd', 'CC BY-NC-ND (Paternité - pas d’utilisation commerciale - pas de modification)'),
    ('cc_by_nc_nd_adapt',
     'CC BY-NC-ND-Adapt (Patern. - pas d’utilis. comm. - pas de modif. sauf pour la classe)'),
    ('gnu_gpl', 'Licence publique générale GNU (GPL)'),
    ('gnu_gfdl', 'Licence de documentation libre GNU (GFDL)'),
)

# LOM-CH v.7.1
RELATION_TYPES_HELP = (
    ('is_part_of', 'Est une partie de',
     'La ressource décrite est une partie (fragment) d’une ressource principale. '
     '(relation hiérarchique enfant-mère).'
    ),
    ('has_part_of', 'Contient',
     'La ressource décrite est une ressource générale contenant différentes '
     'parties uniques (fragments) dont la ressource liée fait partie (relation '
     'hiérarchique mère-enfant(s)).'
    ),
    ('references', 'Est lié à',
     'La ressource décrite est liée à une autre ou plusieurs autres ressources '
     'sur le même niveau (et non de manière hiérarchique).'
    ),
    ('is_version_of', 'Est une version de',
     'La ressource décrite est une version ou une adaptation de la ressource liée. '
     'Les versions différentes impliquent des modifications de contenu '
     'conséquentes sans que le format ne soit différent pour autant.'
    ),
    ('has_version', 'Existe dans une autre version',
     'La ressource décrite est un original. Elle existe dans une autre version '
     'ou adaptation, ou bien son contenu a été modifié de manière conséquente. '
     'Les différences se situent au niveau du contenu et non au niveau du format.'
    ),
    ('is_format_of', 'Est un format de',
     'La ressource décrite a le même contenu que la ressource liée. Cependant, '
     'le format de la ressource décrite est dérivé du format de la ressource liée.'
    ),
    ('has_format', 'Existe dans un autre format',
     'La ressource décrite a le même contenu que la ressource liée. Cependant, '
     'le format de la ressource liée diffère de celui de la ressource décrite.'
    ),
    ('is_referenced_by', 'Est référencé par',
     'La ressource décrite est citée dans la ressource cible liée ou bien elle '
     'est soulignée en conséquence.'
    ),
    ('is_based_on', 'Basé sur',
     'La ressource décrite est dérivée complétement ou partiellement de la '
     'ressource liée. Ce type de relation est à utiliser pour tous les types de '
     'relation qui ne sont pas couverts par « Est un format de » ou « Est une version de ».'
    ),
    ('is_basis_for', 'Sert de base pour',
     'La ressource décrite sert de point de départ pour l’utilisation de la '
     'ressource liée. Ce type de relation est à utiliser pour tous les types de '
     'relation qui ne sont pas couverts par « Est un format de » ou « Est une version de ».'
    ),
    ('is_required_by', 'Est un prérequis de',
     'L’existence et l’utilisation de la ressource décrite sont un prérequis pour '
     'l’utilisation de la ressource liée. Le contenu de la ressource décrite doit '
     'être disponible pour que l’utilisation de la ressource liée fasse du sens.'
    ),
    ('requires', 'A pour prérequis',
     'L’utilisation de la ressource décrite sous-entend l’existence et '
     'l’utilisation de la ressource liée. La ressource décrite a pour prérequis '
     'que le contenu de la ressource liée soit disponible afin que son utilisation '
     'fasse du sens.'
    ),
)
RELATION_TYPES = tuple((rel[0], rel[1]) for rel in RELATION_TYPES_HELP)

MITIC_CHOICES = (
    ('Utilisation d’un environnement multimédia', (
        ('usage_app', 'Usage des appareils'),
        ('choix_outils', 'Choix d’outils numériques'),
    )),
    ('Éducation aux médias', (
        ('ana_image', 'Analyse de l’image'),
        ('decodage', 'Décodage des messages'),
        ('ana_prod', 'Analyse de productions'),
        ('trait_qual', 'Traitement et qualité de l’info'),
        ('sources', 'Sources de l’info'),
    )),
    ('Production de réalisations médiatiques', (
        ('prod_media', 'Production de réalisations médiatiques'),
        ('prod_web', 'Production pour le web'),
    )),
    ('Échanges, communication et recherche sur Internet', (
        ('navig', 'Navigation et esprit critique'),
        ('securite', 'Sécurité et données personnelles'),
        ('comm_cit', 'Communication et citoyenneté numérique'),
    )),
)
