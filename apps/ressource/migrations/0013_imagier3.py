from django.db import migrations, models
from pper.fields import ChoiceArrayField


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0012_alter_resource_thumb_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='Imagier3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mot', models.CharField(max_length=50, verbose_name='Mot')),
                ('predecoupe', models.BooleanField(default=False, verbose_name='Prédécoupé classe')),
                ('no_imagier', models.CharField(blank=True, max_length=5, verbose_name='N° imagier')),
                ('decodable', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'T1'), (2, 'T2'), (3, 'T3'), (4, 'T4'), (5, 'T5'), (6, 'T6')], null=True, verbose_name='Mot décodable')),
                ('decodable_ent', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'T1'), (2, 'T2'), (3, 'T3'), (4, 'T4'), (5, 'T5'), (6, 'T6')], null=True, verbose_name='Mot décodable entièrement')),
                ('decodable_aide', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'T1'), (2, 'T2'), (3, 'T3'), (4, 'T4'), (5, 'T5'), (6, 'T6')], null=True, verbose_name='Mot décodable avec aide')),
                ('phonetique', models.CharField(blank=True, max_length=50, verbose_name='Mot en phonétique')),
                ('phonemes', ChoiceArrayField(base_field=models.CharField(blank=True, choices=[
                    ('a', 'A'), ('y', 'U'), ('i', 'I'), ('l', 'L'), ('r', 'R'), ('o', 'Au'), ('e', 'Ai'), ('f', 'F'), ('ʒ', 'G'), ('v', 'V'), ('b', 'B'), ('u', 'Ou'), ('t', 'T'), ('ʃ', 'Ch'), ('m', 'M'), ('p', 'P'), ('s', 'S'), ('n', 'N'), ('wa', 'Wa'), ('ɔ̃', 'On'), ('k', 'K'), ('d', 'D'), ('ɑ̃', 'An'), ('ɛ', 'Est'), ('z', 'Z'), ('ɛ̃', 'In'), ('g', 'Gu'), ('ø/œ', 'Eu'), ('ə', 'E'), ('ɲ', 'Gn'), ('J', 'J'), ('w', 'Oue')
                ], max_length=3), blank=True, null=True, size=None, verbose_name='Phonèmes')),
                ('type_img', models.CharField(blank=True, choices=[
                    ('dessin c', 'Dessin couleur'), ('dessin nb', 'Dessin noir-blanc'), ('photo', 'Photo'), ('picto', 'Pictogramme')
                ], max_length=15, verbose_name='Type')),
                ('illustrateur', models.CharField(blank=True, max_length=30, verbose_name='Illustrateur')),
                ('resource', models.OneToOneField(blank=True, null=True, on_delete=models.deletion.CASCADE, to='ressource.resource')),
            ],
        ),
    ]
