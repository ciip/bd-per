from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0008_remove_resource_guest'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resource',
            name='classe_r',
        ),
    ]
