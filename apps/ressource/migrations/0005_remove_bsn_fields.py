from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0004_catalog_created_updated'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resource',
            name='bsn_dest_lom_id',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='bsn_source_lom_id',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='last_bsn_sync_date',
        ),
    ]
