from django.conf import settings
import django.contrib.postgres.indexes
import django.contrib.postgres.operations
import django.contrib.postgres.search
from django.db import migrations, models
import django.db.migrations.operations.special
import django.db.models.deletion
import pper.fields
import ressource.models
import ressource.storage


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('pper', '__first__'),
        ('contenttypes', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        django.contrib.postgres.operations.UnaccentExtension(
        ),
        migrations.RunSQL(
            sql='CREATE TEXT SEARCH CONFIGURATION fr ( COPY = french );',
        ),
        migrations.RunSQL(
            sql='ALTER TEXT SEARCH CONFIGURATION fr ALTER MAPPING FOR hword, hword_part, word WITH unaccent, french_stem;',
        ),
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date de création de la notice')),
                ('updated', models.DateTimeField(verbose_name='Date de dernière modification')),
                ('validated', models.DateTimeField(blank=True, null=True, verbose_name='Date de dernière validation')),
                ('status', models.CharField(choices=[('atraiter', 'À traiter'), ('essai', 'Essais/divers'), ('bsn_seul', 'Publication BSN uniquement'), ('encours', "En cours d'analyse"), ('validee', 'Validée'), ('refusee', 'Refusée')], db_index=True, default='atraiter', max_length=20, verbose_name='Statut de validation')),
                ('status_comment', models.TextField(blank=True, verbose_name='Suivi du traitement')),
                ('show_refs', models.BooleanField(default=True, verbose_name="Montrer les références à l'affichage")),
                ('type_r', models.CharField(choices=[('mer', "Moyen d'enseignement romand (officiel)"), ('merx', 'MER - Site externe'), ('can', 'Précision cantonale'), ('med', 'Ressource numérique complémentaire (RN-PER)'), ('rno', 'Ressource numérique officielle (RN-MER)'), ('epro', 'Ressource EPROCOM')], default='med', max_length=6, verbose_name='Type de ressource')),
                ('classe_r', models.CharField(choices=[('std', 'Standard'), ('mer', "Moyen d'enseignement romand"), ('merx', 'MER - Site externe'), ('rts', 'Radio-Télévision Suisse')], default='std', max_length=6)),
                ('cantons', models.CharField(blank=True, default='', help_text='Liste de cantons séparés par des virgules (VD,FR), vide pour tous les cantons', max_length=30)),
                ('position', models.IntegerField(default=0)),
                ('bsn_source_lom_id', models.CharField(blank=True, max_length=50)),
                ('bsn_dest_lom_id', models.CharField(blank=True, max_length=50)),
                ('last_bsn_sync_date', models.DateTimeField(blank=True, null=True)),
                ('submitted_mat', models.TextField(blank=True, verbose_name='Matériel soumis')),
                ('url', models.URLField(blank=True, max_length=255, verbose_name='URL')),
                ('title', models.TextField(verbose_name='Titre')),
                ('emedia', models.BooleanField(default=False, verbose_name='Ressource e-media')),
                ('languages', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('en', 'anglais'), ('it', 'italien'), ('fr', 'français'), ('de', 'allemand'), ('rh', 'rhéto-roman'), ('es', 'espagnol'), ('pt', 'portugais'), ('la', 'latin'), ('tr', 'turc')], max_length=2), blank=True, null=True, size=None, verbose_name='Langues de la ressource')),
                ('description', models.TextField(blank=True, help_text='Résume brièvement le contenu de la ressource (entre 2 et 6 phrases)', verbose_name='Description générale')),
                ('descr_eleve', models.TextField(blank=True, help_text='Description de la ressource à destination des élèves', verbose_name='Description élève')),
                ('keywords', models.TextField(blank=True, verbose_name='Mots clé')),
                ('granularity', models.CharField(blank=True, choices=[('1', 'Niveau 1 – ressource indivisible'), ('2a', 'Niveau 2 – séquence pédagogique'), ('2b', 'Niveau 2 – lot de ressources indivisibles'), ('3a', 'Niveau 3 – ensemble de séquences pédagogiques'), ('3b', "Niveau 3 – moyen d'enseignement"), ('4', 'Niveau 4 – collection ou portail de ressources pédagogiques')], max_length=10, verbose_name='Niveau de granularité')),
                ('genre', models.CharField(blank=True, choices=[('fiction', 'fiction'), ('anim_film', "film d'animation"), ('anim_3d', 'animation 3D'), ('images', "galerie d'images"), ('doc', 'documentaire'), ('rep', 'reportage'), ('int', 'interview'), ('dia', 'diagramme'), ('txt', 'texte'), ('graph', 'graphique'), ('sgame', 'serious-game')], max_length=10, verbose_name='Genre')),
                ('source', models.TextField(blank=True, verbose_name='Source')),
                ('participants', models.TextField(blank=True, verbose_name='Intervenant-s')),
                ('producer', models.TextField(blank=True, verbose_name='Réalisation')),
                ('guest', models.TextField(blank=True, verbose_name='Invité-e-s')),
                ('version', models.CharField(blank=True, max_length=50, verbose_name='Version')),
                ('format', models.CharField(blank=True, choices=[('text', (('text/html', 'html'), ('text/plain', 'texte pur'), ('text/richtext', 'texte enrichi'), ('text/xml', 'xml'))), ('image', (('image/jpeg', 'jpeg'), ('image/bmp', 'bmp'), ('image/gif', 'gif'), ('image/png', 'png'), ('image/tiff', 'tiff'), ('image/x-wmf', 'x-wmf'))), ('video', (('video/mpeg', 'mpeg'), ('video/avi', 'avi'), ('video/x-flv', 'flash'), ('video/quicktime', 'quicktime'))), ('audio', (('audio/mp3', 'mp3'), ('audio/ogg', 'ogg'), ('audio/basic', 'basic'), ('audio/midi', 'midi'), ('audio/x-wav', 'x-wav'), ('audio/mpeg', 'mpeg'))), ('application', (('application/pdf', 'pdf'), ('application/base64', 'base64'), ('application/binary', 'binary'), ('application/java', 'java'), ('application/macbinhex40', 'macbinhex40'), ('application/msexcel', 'msexcel'), ('application/msword', 'msword'), ('application/postscript', 'postscript'), ('application/ppt', 'ppt'), ('application/rtf', 'rtf'), ('application/x-compressed', 'x-compressed'), ('application/x-gzip-compressed', 'x-gzip-compressed'), ('application/zip', 'zip'))), ('model', (('model/vrml', 'vrml'),)), ('non_digital', 'Non numérique')], max_length=30, verbose_name='Format')),
                ('num_pages', models.IntegerField(blank=True, null=True, verbose_name='Nombre de pages')),
                ('size', models.BigIntegerField(blank=True, null=True, verbose_name='Taille du fichier')),
                ('phys_loc', models.TextField(blank=True, verbose_name='Emplacement physique')),
                ('tec_req', models.TextField(blank=True, verbose_name='Exigences techniques')),
                ('duration', models.CharField(blank=True, max_length=30, verbose_name='Durée')),
                ('thumb', models.ImageField(blank=True, upload_to='resource_thumbs', max_length=150, verbose_name='Vignette')),
                ('thumb_url', models.URLField(blank=True, verbose_name='URL de la vignette')),
                ('embed_code', models.TextField(blank=True, verbose_name="Code d'intégration HTML")),
                ('type_doc', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('app', 'app'), ('bibliography', 'bibliographie'), ('map', 'carte'), ('audio', 'document sonore'), ('text', 'document textuel'), ('video', 'document vidéo'), ('spreadsheet', 'feuille de calcul'), ('guide', 'guide'), ('animated image', 'image animée'), ('image', 'image fixe'), ('application', 'logiciel'), ('website', 'page web'), ('sheet music', 'partition musicale'), ('glossary', 'référence/glossaire'), ('resource for IWB', 'ressource pour TBI')], max_length=20), blank=True, null=True, size=None, verbose_name='Type documentaire')),
                ('type_ped', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('open activity', 'activité libre'), ('workshop', 'atelier'), ('self assessment', 'auto-valuation'), ('demonstration', 'démonstration'), ('case study', 'étude de cas'), ('formative evaluation', 'évaluation formative'), ('summative evaluation', 'évaluation sommative'), ('exercise', 'exercice'), ('experiment', 'expérience'), ('presentation', 'exposé, présentation'), ('exploration', 'exploration'), ('guide', 'guide/tutoriel'), ('educational game', 'jeu éducatif'), ('role play', 'jeu de rôle'), ('methodological_tool', 'outil méthodologique'), ('project', 'projet'), ('enquiry-oriented activity', "recherche d'information"), ('lesson plan', 'scénario pédagogique'), ('simulation', 'simulation'), ('tutorial', 'tutoriel')], max_length=40), blank=True, null=True, size=None, verbose_name='Type pédagogique')),
                ('target_gr', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('learner', 'Apprenants'), ('teacher', 'Enseignants'), ('manager', "Responsables d'établissement scolaire"), ('parent', 'Parents'), ('other', 'Autres')], max_length=20), blank=True, null=True, size=None, verbose_name='Utilisateur final')),
                ('educ_level', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('early_childhood', 'Petite enfance'), ('compulsory education', 'Scolarité obligatoire'), ('post-compulsory education', 'Degré secondaire II'), ('special education', 'Enseignement spécialisé'), ('higher education', 'Degré tertiaire'), ('continuing education', 'Formation continue'), ('distance education', 'Enseignement à distance'), ('Independent of levels', 'Indépendant des degrés'), ('education context other', 'Non défini')], max_length=40), blank=True, default=ressource.models.default_educ_level, null=True, size=None, verbose_name='Degré de formation')),
                ('ages', models.CharField(blank=True, max_length=5, verbose_name='Âge')),
                ('difficulty', models.CharField(blank=True, choices=[('very easy', 'Très facile'), ('easy', 'Facile'), ('medium', 'Moyen'), ('difficult', 'Difficile'), ('very difficult', 'Très difficile')], max_length=30, verbose_name='Difficulté')),
                ('learn_duration', models.CharField(blank=True, choices=[('up_to_one_lesson', "Moins d'une leçon"), ('one_to_five_lessons', "D'une à cinq leçons"), ('more_than_five_lessons', 'Plus de cinq leçons')], help_text="Estimation du temps moyen d'apprentissage ou de la durée moyenne d'utilisation de la ressource pour l'utilisateur final", max_length=30, verbose_name="Durée d'apprentissage")),
                ('description_ped', models.TextField(blank=True, verbose_name='Description pédagogique')),
                ('level', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('cycle 1', 'Cycle 1'), ('1st and 2nd year', 'Cycle 1 - 1re-2e'), ('3rd and 4th year', 'Cycle 1 - 3e-4e'), ('1st year', 'Cycle 1 - 1re'), ('2nd year', 'Cycle 1 - 2e'), ('3rd year', 'Cycle 1 - 3e'), ('4th year', 'Cycle 1 - 4e'), ('cycle 2', 'Cycle 2'), ('5th and 6th year', 'Cycle 2 - 5e-6e'), ('7th and 8th year', 'Cycle 2 - 7e-8e'), ('5th year', 'Cycle 2 - 5e'), ('6th year', 'Cycle 2 - 6e'), ('7th year', 'Cycle 2 - 7e'), ('8th year', 'Cycle 2 - 8e'), ('cycle 3', 'Cycle 3'), ('9th year', 'Cycle 3 - 9e'), ('10th year', 'Cycle 3 - 10e'), ('11th year', 'Cycle 3 - 11e')], max_length=20), blank=True, null=True, size=None, verbose_name='Niveau scolaire')),
                ('cost', models.BooleanField(default=False, verbose_name='Coûts')),
                ('conditions', models.CharField(blank=True, choices=[('all rights reserved', 'Tous droits réservés'), ('cc_by', 'CC BY (Paternité)'), ('cc_by_sa', "CC BY-SA (Paternité - partage à l'identique)"), ('cc_by_nd', 'CC BY-ND (Paternité - pas de modification)'), ('cc_by_nc', "CC BY-NC (Paternité - pas d'utilisation commerciale)"), ('cc_by_nc_sa', "CC BY-NC-SA (Paternité - pas d'utilisation commerciale - partage à l'identique)"), ('cc_by_nc_nd', 'CC BY-NC-ND (Paternité - pas d’utilisation commerciale - pas de modification)'), ('cc_by_nc_nd_adapt', 'CC BY-NC-ND-Adapt (Patern. - pas d’utilis. comm. - pas de modif. sauf pour la classe)'), ('gnu_gpl', 'Licence publique générale GNU (GPL)'), ('gnu_gfdl', 'Licence de documentation libre GNU (GFDL)')], max_length=400, verbose_name="Conditions d'utilisation")),
                ('qual_decisive', models.TextField(blank=True, help_text='Un élément décisif a-t-il conduit au développement de la ressource pédagogique\xa0?\nSi oui, lequel ?', verbose_name='Élément décisif')),
                ('qual_collab', models.TextField(blank=True, help_text="La ressource résulte-t-elle d'une collaboration intercantonale ? Si oui, sous quelle forme ?", verbose_name='Collaboration intercantonale')),
                ('qual_counsels', models.TextField(blank=True, help_text='Lors du développement de la ressource, avez-vous bénéficié de conseils spécialisés de professionnels et/ou de spécialistes ?\nSi oui, par qui et de quelle nature (scientifique, pédagogique, etc.) ?', verbose_name='Conseils spécialisés')),
                ('qual_tested', models.TextField(blank=True, help_text='La ressource a-t-elle été testée dans la pratique ?\nSi oui, veuillez préciser (années scolaire / nombre et type de classes / cantons).', verbose_name='Tests pratiques')),
                ('qual_eval', models.TextField(blank=True, help_text="La ressource a-t-elle ou sera-t-elle encore soumise à une procédure d'approbation ? Si oui, laquelle (par ex. commissions cantonales de moyens d'enseignement) ?", verbose_name="Procédure d'approbation")),
                ('qual_guarant', models.TextField(blank=True, help_text="D'autres mesures de garanties ont-elles été appliquées ? Si oui, lesquelles ?", verbose_name='Mesures de garanties')),
                ('fts', django.contrib.postgres.search.SearchVectorField(blank=True)),
                ('mitic_asp', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('Utilisation d’un environnement multimédia', (('usage_app', 'Usage des appareils'), ('choix_outils', 'Choix d’outils numériques'))), ('Éducation aux médias', (('ana_image', 'Analyse de l’image'), ('decodage', 'Décodage des messages'), ('ana_prod', 'Analyse de productions'), ('trait_qual', 'Traitement et qualité de l’info'), ('sources', 'Sources de l’info'))), ('Production de réalisations médiatiques', (('prod_media', 'Production de réalisations médiatiques'), ('prod_web', 'Production pour le web'))), ('Échanges, communication et recherche sur Internet', (('navig', 'Navigation et esprit critique'), ('securite', 'Sécurité et données personnelles'), ('comm_cit', 'Communication et citoyenneté numérique')))], max_length=20), blank=True, null=True, size=None, verbose_name='Aspects MITIC')),
                ('sdm', models.BooleanField(default=False, verbose_name='Semaine des médias')),
                ('cinema', models.BooleanField(default=False, verbose_name='Fiche cinéma')),
                ('num_emedia', models.SmallIntegerField(blank=True, null=True)),
                ('emedia_categ', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('sdm-act', 'Semaine des médias – activités'), ('sdm-mat', 'Semaine des médias – matériel'), ('jeunes', 'Jeunes et médias')], max_length=10), blank=True, null=True, size=None, verbose_name='Catégorie e-media')),
                ('affectation', models.CharField(blank=True, choices=[('A', 'e-media'), ('B', 'e-media + PPER/ESPER/BSN'), ('C', 'à définir')], max_length=1, verbose_name='Affectation')),
                ('search_hidden', models.BooleanField(default=False, verbose_name='Masquer dans la recherche publique')),
                ('need_auth', models.BooleanField(default=False, verbose_name='Nécessite une authentification sur ce site')),
                ('no_player', models.BooleanField(default=False, verbose_name='Ne pas afficher de lecteur (audio/vidéo…)')),
                ('broken', models.BooleanField(default=False, verbose_name='Lien cassé')),
            ],
            options={
                'db_table': 't_resource',
                'verbose_name': 'Ressource',
            },
        ),
        migrations.CreateModel(
            name='ResourceContrib',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vcard', models.TextField()),
                ('date', models.DateTimeField(blank=True, null=True)),
                ('role', models.CharField(choices=[('creator', 'Auteur'), ('validator', 'Validateur'), ('author', 'Auteur'), ('editor', 'Contributeur'), ('publisher', 'Éditeur')], max_length=20)),
            ],
            options={
                'db_table': 't_resource_contrib',
                'verbose_name_plural': 'Contributeurs de ressource',
                'verbose_name': 'Contributeur de ressource',
            },
        ),
        migrations.CreateModel(
            name='ResourceGroupTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=30, unique=True, verbose_name='Étiquette')),
            ],
        ),
        migrations.CreateModel(
            name='ResourceGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weight', models.IntegerField(default=0, help_text='Ordre quand plusieurs groupes apparaissent dans une même cellule', verbose_name='Poids')),
                ('title', models.CharField(max_length=200, verbose_name='Titre')),
                ('subtitle', models.CharField(blank=True, max_length=200, verbose_name='Sous-titre')),
                ('subtitle2', models.CharField(blank=True, default='Ressources numériques', max_length=200, verbose_name='Sous-titre secondaire')),
                ('groupment', models.CharField(blank=True, help_text='Les groupes de ressources avec le même texte de regroupement apparaissent groupés sur la PPER', max_length=200, verbose_name='Regroupement')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('mer_url', models.URLField(blank=True, verbose_name='Lien MER')),
                ('levels', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('cycle 1', 'Cycle 1'), ('1st and 2nd year', 'Cycle 1 - 1re-2e'), ('3rd and 4th year', 'Cycle 1 - 3e-4e'), ('1st year', 'Cycle 1 - 1re'), ('2nd year', 'Cycle 1 - 2e'), ('3rd year', 'Cycle 1 - 3e'), ('4th year', 'Cycle 1 - 4e'), ('cycle 2', 'Cycle 2'), ('5th and 6th year', 'Cycle 2 - 5e-6e'), ('7th and 8th year', 'Cycle 2 - 7e-8e'), ('5th year', 'Cycle 2 - 5e'), ('6th year', 'Cycle 2 - 6e'), ('7th year', 'Cycle 2 - 7e'), ('8th year', 'Cycle 2 - 8e'), ('cycle 3', 'Cycle 3'), ('9th year', 'Cycle 3 - 9e'), ('10th year', 'Cycle 3 - 10e'), ('11th year', 'Cycle 3 - 11e')], max_length=20), blank=True, null=True, size=None, verbose_name='Degré scolaire')),
                ('grp_status', models.CharField(choices=[('avalider', 'À valider'), ('limite', 'Validé sans publication'), ('valide', 'Validé')], db_index=True, default='valide', max_length=10, verbose_name='Statut de validation')),
                ('domain', models.ForeignKey(blank=True, help_text="Utile lorsque le catalogue n'est pas positionné dans une cellule du PER", null=True, on_delete=django.db.models.deletion.PROTECT, to='pper.domain', verbose_name='Domaine')),
                ('tags', models.ManyToManyField(blank=True, to='ressource.ResourceGroupTag')),
            ],
            options={
                'db_table': 't_resource_grp',
                'verbose_name_plural': 'Catalogues de ressources',
                'verbose_name': 'Catalogue de ressources',
            },
        ),
        migrations.CreateModel(
            name='ResourcePERIndex',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cell', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pper.cellule')),
                ('discipline', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pper.discipline')),
                ('group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='ressource.resourcegroup')),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ressource.resource')),
                ('specification', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pper.specification')),
            ],
        ),
        migrations.CreateModel(
            name='ResourcePerLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype')),
                ('resource', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ressource.resource')),
                ('catalog', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ressource.resourcegroup')),
                ('sort_key', models.CharField(blank=True, db_index=True, max_length=20)),
            ],
            options={
                'ordering': ('sort_key',),
                'verbose_name_plural': 'Liens vers PER',
                'verbose_name': 'Lien vers PER',
            },
        ),
        migrations.CreateModel(
            name='ResourceRelation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rtype', models.CharField(choices=[('is_part_of', 'Est une partie de'), ('has_part_of', 'Contient'), ('references', 'Est lié à'), ('is_version_of', 'Est une version de'), ('has_version', 'Existe dans une autre version'), ('is_format_of', 'Est un format de'), ('has_format', 'Existe dans un autre format'), ('is_referenced_by', 'Est référencé par'), ('is_based_on', 'Basé sur'), ('is_basis_for', 'Sert de base pour'), ('is_required_by', 'Est un prérequis de'), ('requires', 'A pour prérequis')], max_length=20, verbose_name='Type de relation')),
                ('url', models.URLField(blank=True, max_length=255, verbose_name='URL')),
                ('description', models.TextField(verbose_name='Description')),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relations', to='ressource.resource')),
                ('rel_file', models.FileField(blank=True, storage=ressource.storage.OverwriteStorage(), upload_to=ressource.models.resource_upload_path, verbose_name='Fichier')),
                ('other', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='ressource.resource', verbose_name='Autre ressource')),
                ('order', models.SmallIntegerField(default=0)),
            ],
            options={
                'db_table': 't_resource_relation',
                'verbose_name_plural': 'Relations de ressource',
                'verbose_name': 'Relation de ressource',
            },
        ),
        migrations.CreateModel(
            name='ResourceSubmitter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom et prénom')),
                ('email', models.EmailField(max_length=254, verbose_name='Adresse courriel')),
                ('tel', models.CharField(max_length=20, verbose_name='Téléphone')),
                ('institution', models.CharField(blank=True, max_length=100, verbose_name='Institution')),
                ('function', models.CharField(blank=True, max_length=100, verbose_name='Fonction')),
                ('addr', models.TextField(verbose_name='Adresse postale')),
                ('remarks', models.TextField(blank=True, verbose_name='Remarques')),
                ('resource', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='ressource.resource')),
            ],
            options={
                'verbose_name_plural': 'Proposants de ressource',
                'verbose_name': 'Proposant de ressource',
            },
        ),
        migrations.CreateModel(
            name='SubTheme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('num', models.CharField(default='', max_length=3)),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subthemes', to='ressource.resourcegroup')),
            ],
            options={
                'db_table': 't_resource_subtheme',
                'ordering': ('num',),
            },
        ),
        migrations.CreateModel(
            name='ResourceGroupLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date d’ajout dans le catalogue')),
                ('position', models.SmallIntegerField(default=0)),
                ('number', models.CharField(blank=True, max_length=20, verbose_name='Numéro')),
                ('group', models.ForeignKey(db_column='resourcegroup_id', on_delete=django.db.models.deletion.CASCADE, to='ressource.resourcegroup')),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ressource.resource')),
                ('subthemes', models.ManyToManyField(blank=True, to='ressource.SubTheme')),
            ],
            options={
                'db_table': 't_resource_grp_resources',
                'ordering': ['position'],
                'unique_together': {('resource', 'group')},
            },
        ),
        migrations.CreateModel(
            name='ResourceFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files', to='ressource.resource')),
                ('rfile', models.FileField(max_length=150, storage=ressource.storage.OverwriteStorage(), upload_to=ressource.models.resource_upload_path, verbose_name='Fichier')),
                ('title', models.CharField(max_length=200, verbose_name='Titre')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
            ],
        ),
        migrations.AddField(
            model_name='resourcegroup',
            name='resources',
            field=models.ManyToManyField(blank=True, through='ressource.ResourceGroupLink', to='ressource.Resource'),
        ),
        migrations.AddField(
            model_name='resource',
            name='contribs',
            field=models.ManyToManyField(blank=True, to='ressource.ResourceContrib', verbose_name='Contributeurs'),
        ),
        migrations.AddIndex(
            model_name='resource',
            index=django.contrib.postgres.indexes.GinIndex(fields=['fts'], name='resource_fts_index'),
        ),
        migrations.CreateModel(
            name='ResourceCheck',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('ok', 'OK'), ('unk', 'Incertain'), ('ko', 'Pas OK')], max_length=5, verbose_name='État')),
                ('when', models.DateTimeField(verbose_name='Date du contrôle')),
                ('comment', models.TextField(blank=True, verbose_name='Commentaire')),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='checks', to='ressource.resource')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Imagier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, verbose_name='Nom')),
                ('numero', models.PositiveSmallIntegerField(verbose_name='Numéro de carte')),
                ('caract', pper.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('Conte de randonnée', 'Conte de randonnée'), ('Comptine', 'Comptine'), ('Documentaire', 'Documentaire'), ('Recette', 'Recette'), ('Récit de vie', 'Récit de vie'), ('Album', 'Album'), ('Présentation de soi', 'Présentation de soi'), ('Marche à suivre', 'Marche à suivre'), ('Conscience phonologique', 'Conscience phonologique'), ('Passage de l’oral à l’écrit', 'Passage de l’oral à l’écrit'), ('Stratégies de compréhension', 'Stratégies de compréhension'), ('Vocabulaire', 'Vocabulaire')], max_length=30), blank=True, null=True, size=None, verbose_name='Caractéristiques')),
                ('categorie', models.CharField(choices=[('aliment', 'aliment'), ('animaux', 'animaux'), ('bâtiment', 'bâtiment'), ('corps', 'corps'), ('couleur', 'couleur'), ('école', 'école'), ('forme', 'forme'), ('fruit', 'fruit'), ('instrument de musique', 'instrument de musique'), ('jeux', 'jeux'), ('légumes', 'légumes'), ('maison', 'maison'), ('météo', 'météo'), ('nature', 'nature'), ('objet', 'objet'), ('personnage', 'personnage'), ('prépositions', 'prépositions'), ('ustensile', 'ustensile'), ('végétal', 'végétal'), ('véhicule', 'véhicule'), ('verbes', 'verbes'), ('vêtement', 'vêtement')], max_length=30, verbose_name='Catégorie')),
                ('type_img', models.CharField(blank=True, choices=[('dessin c', 'Dessin couleur'), ('dessin nb', 'Dessin noir-blanc'), ('photo', 'Photo'), ('picto', 'Pictogramme')], max_length=15, verbose_name='Type')),
                ('num_syll', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Syllabes')),
                ('attaque', models.CharField(blank=True, max_length=8, verbose_name='Attaques complexes')),
                ('rime', models.CharField(blank=True, max_length=8, verbose_name='Rimes')),
                ('resource', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='ressource.resource')),
            ],
        ),
        migrations.CreateModel(
            name='ResourceClick',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ressource.resource')),
            ],
        ),
    ]
