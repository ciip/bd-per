from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0005_remove_bsn_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='resource',
            name='partner',
            field=models.CharField(default='CIIP', max_length=10),
        ),
        migrations.AddField(
            model_name='resource',
            name='uuid',
            field=models.UUIDField(blank=True, null=True),
        ),
    ]
