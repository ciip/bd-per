from django.db import migrations
from ressource.models import ResourcePerLink


def fill_objectif_field(apps, schema_editor):
    ResourceGroup = apps.get_model("ressource", "ResourceGroup")
    Objectif = apps.get_model("pper", "Objectif")

    for group in ResourceGroup.objects.all():
        def specs():
            real_links = ResourcePerLink.objects.filter(pk__in=group.resourceperlink_set.all())
            return set([link.get_spec() for link in real_links])

        def objectif():
            objectifs = set(spec.get_objectif() for spec in specs())
            return list(objectifs)[0] if len(objectifs) == 1 else None

        obj = objectif()
        if obj:
            group.objectif = Objectif.objects.get(pk=obj.pk)
            group.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '__first__'),
        ('ressource', '0002_resourcegroup_objectif'),
    ]

    operations = [
        migrations.RunPython(fill_objectif_field, migrations.RunPython.noop)
    ]
