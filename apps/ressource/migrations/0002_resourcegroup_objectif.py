from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pper', '__first__'),
        ('ressource', '0001_squashed_0050_remove_resourcegroup_cell'),
    ]

    operations = [
        migrations.AddField(
            model_name='resourcegroup',
            name='objectif',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to='pper.objectif', verbose_name='Objectif'),
        ),
    ]
