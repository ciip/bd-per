import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0003_fill_objectif_field'),
    ]

    operations = [
        migrations.AddField(
            model_name='resourcegroup',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2020, 1, 1, 0, 0), verbose_name='Date de création du catalogue'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='resourcegroup',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 1, 0, 0), verbose_name='Date de dernière modification'),
            preserve_default=False,
        ),
    ]
