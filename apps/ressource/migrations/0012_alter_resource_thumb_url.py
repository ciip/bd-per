from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0011_remove_resource_participants'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resource',
            name='thumb_url',
            field=models.URLField(blank=True, max_length=255, verbose_name='URL de la vignette'),
        ),
    ]
