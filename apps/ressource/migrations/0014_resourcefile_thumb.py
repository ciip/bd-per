import django.db.models.deletion
import pper.fields
import ressource.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0013_imagier3'),
    ]

    operations = [
        migrations.AddField(
            model_name='resourcefile',
            name='thumb',
            field=models.ImageField(
                blank=True, max_length=150, storage=ressource.models.s3_storage,
                upload_to='resource_thumbs', verbose_name='Vignette'
            ),
        ),
    ]
