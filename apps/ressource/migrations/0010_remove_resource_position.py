from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0009_remove_resource_classe_r'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resource',
            name='position',
        ),
    ]
