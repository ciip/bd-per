from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0010_remove_resource_position'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resource',
            name='participants',
        ),
    ]
