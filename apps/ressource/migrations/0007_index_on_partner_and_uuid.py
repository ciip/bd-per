from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0006_resource_partner_resource_uuid'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='resource',
            index=models.Index(fields=['partner'], name='partner_index'),
        ),
        migrations.AddConstraint(
            model_name='resource',
            constraint=models.UniqueConstraint(fields=('uuid',), name='uuid_unique'),
        ),
    ]
