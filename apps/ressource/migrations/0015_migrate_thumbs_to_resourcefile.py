from django.db import migrations
from django.db.models import Count, F


def migrate_thumb_fields(apps, schema_editor):
    """Move Resource.thumb to ResourceFile.thumb for all resources having only one file."""
    Resource = apps.get_model('ressource', 'Resource')
    ResourceFile = apps.get_model('ressource', 'ResourceFile')
    res_to_migrate = Resource.objects.exclude(thumb='').annotate(numf=Count('files')).filter(numf=1)
    to_update = []
    for rf in ResourceFile.objects.filter(resource__pk__in=res_to_migrate).select_related("resource"):
        rf.thumb = rf.resource.thumb
        to_update.append(rf)
    ResourceFile.objects.bulk_update(to_update, ["thumb"], batch_size=500)
    res_to_migrate.update(thumb="")


class Migration(migrations.Migration):

    dependencies = [
        ('ressource', '0014_resourcefile_thumb'),
    ]

    operations = [migrations.RunPython(migrate_thumb_fields)]
