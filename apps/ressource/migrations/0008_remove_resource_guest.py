from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("ressource", "0007_index_on_partner_and_uuid"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="resource",
            name="guest",
        ),
    ]
