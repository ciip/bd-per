from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command

from ressource.models import (
    ResourceContrib, ResourceSubmitter, ResourceRelation, ResourceFile,
    ResourceGroupLink, ResourcePerLink, SubTheme,
)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--pks', dest='primary_keys',
            help="Only dump objects with given primary keys. Accepts a comma-separated list of keys.",
        )

    def handle(self, *args, **options):
        pks = [pk.strip() for pk in options['primary_keys'].split(',')]
        if not pks:
            raise CommandError("You must provide at least one pk")

        call_command(
            'dumpdata', 'ressource.Resource', indent=2, primary_keys=options['primary_keys'],
            output='dump/resources.json'
        )

        contrib_pks = list(ResourceContrib.objects.filter(resource__pk__in=pks).values_list('id', flat=True))
        if contrib_pks:
            call_command(
                'dumpdata', 'ressource.ResourceContrib', indent=2, primary_keys=to_str(contrib_pks),
                output='dump/contributors.json'
            )

        subm_pks = list(ResourceSubmitter.objects.filter(resource__pk__in=pks).values_list('id', flat=True))
        if subm_pks:
            call_command(
                'dumpdata', 'ressource.ResourceSubmitter', indent=2, primary_keys=to_str(subm_pks),
                output='dump/submitters.json'
            )

        rel_pks = list(ResourceRelation.objects.filter(resource__pk__in=pks).values_list('id', flat=True))
        if rel_pks:
            call_command(
                'dumpdata', 'ressource.ResourceRelation', indent=2, primary_keys=to_str(rel_pks),
                output='dump/relations.json'
            )

        file_pks = list(ResourceFile.objects.filter(resource__pk__in=pks).values_list('id', flat=True))
        if file_pks:
            call_command(
                'dumpdata', 'ressource.ResourceFile', indent=2, primary_keys=to_str(file_pks),
                output='dump/files.json'
            )

        subt_pks = list(SubTheme.objects.filter(resourcegrouplink__resource__pk__in=pks).values_list('id', flat=True))
        if subt_pks:
            call_command(
                'dumpdata', 'ressource.SubTheme', indent=2, primary_keys=to_str(subt_pks),
                output='dump/subthemes.json'
            )

        grplink_pks = list(ResourceGroupLink.objects.filter(resource__pk__in=pks).values_list('id', flat=True))
        if grplink_pks:
            call_command(
                'dumpdata', 'ressource.ResourceGroupLink', indent=2, primary_keys=to_str(grplink_pks),
                output='dump/group_links.json'
            )

        perlink_pks = list(ResourcePerLink.objects.filter(resource__pk__in=pks).values_list('id', flat=True))
        if perlink_pks:
            call_command(
                'dumpdata', 'ressource.ResourcePerLink', indent=2, primary_keys=to_str(perlink_pks),
                output='dump/per_links.json'
            )

def to_str(lst):
    return ','.join(str(i) for i in lst)
