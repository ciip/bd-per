import os
from functools import cache
from urllib.parse import quote as urlquote

from django import template
from django.conf import settings
from django.forms import CheckboxInput
from django.templatetags.static import static
from django.urls import reverse
from django.utils.html import escape, format_html
from django.utils.safestring import mark_safe

from ressource.ontologies import TYPE_DOC, TYPE_DOC_MAIN
from ressource.utils import auto_exposant as auto_exposant_utils

register = template.Library()

TYPE_DOC_MAPPING = dict((tp[1], tp[2]) for tp in TYPE_DOC if len(tp) > 2)


@register.filter
def human_name(user):
    if 'ssoeel.geneveid.ch' in user.username:
        return user.username.split('!')[-1]
    return user.username


@register.filter
def logout_link(user):
    if user.password == '' or not user.has_usable_password():
        return '/Shibboleth.sso/Logout?return=https://bdper.plandetudes.ch%s' % reverse('home')
    return reverse('logout')


@register.filter
def get_url(res, request):
    return res.get_url(request=request)


@register.filter
def format_resource(res):
    if res.type_r == 'can':
        img_paths = res.get_cant_logos()
        return "<a target='_blank' href='%s' title='%s'>%s</a>" % (
            res.get_url(), res.title, " ".join(["<img src='%s'/>" % img_path for img_path in img_paths])
        )
    else:
        return "<a target='_blank' href='%s'>%s</a> %s" % (
            res.get_url(), res.title, res.cantons and "(%s)" % res.cantons or ""
        )


@register.simple_tag
def render_resources(cell, editable=False, public=False, link='direct'):
    if isinstance(cell, dict):
        return ""

    all_resources = cell.get_resources(public_site=public)
    content = ''
    for typ, ress in all_resources.items():
        if typ in ('grps', 'can', 'epro') or not ress:
            continue
        content += _render_single_resources(typ, ress, editable, public, link=link)
    content += _render_grp_resources(all_resources.get('grps'), editable, public)
    if 'epro' in all_resources:
        content += _render_single_resources('epro', all_resources['epro'], editable, public, link=link)
    if content:
        content = '<div class="resources">%s</div>' % content
    return mark_safe(content)


@cache
def edit_str():
    return '''
    <span class="edit_widgets" data-resid="%(link_id)s">
        <a class="resource_%(is_grp)sedit %(aclass)s" href="%(edit_url)s" title="Modifier cette resource" target="_blank"><img src="{edit_img}" alt="Édition"></a>
        <a class="resource_copy" href="%(copy_url)s" title="Copier cette ressource"><img src="{copy_img}" alt="Copier"></a>
        <a class="resource_cut" href="%(cut_url)s" title="Couper cette ressource"><img src="{cut_img}" alt="Couper"></a>
        <a class="resource_delete" href="%(del_url)s" title="Supprimer cette ressource"><img src="{delete_img}" alt="Suppression"></a>
    </span>'''.format(
        edit_img=static('img/edit.gif'), copy_img=static('img/copy.png'),
        cut_img=static('img/cut.png'), delete_img=static('img/delete.gif')
    )


def _render_single_resources(res_type, res_list, editable, public, link='direct'):
    """
    Render all resources except groups of resources and cantonal resources.
    `link` can be:
      * 'direct': the url is directly to the real resource URL
      * 'notice': the url points to the resource description
    """
    main_str = '%(text)s <a target="_blank" href="%(url)s">%(logo)s</a> %(cant)s'
    content = ''
    for res_link in res_list:
        res = res_link.resource
        # Format individual resource and append to result string
        cantons = "(%s)" % res.cantons if res.cantons else ""
        logo_path, logo_title = res.get_logo()
        img = '<img src="%s" title="%s" class="inline res_logo">' % (logo_path, logo_title) if logo_path else ''
        res_str = main_str % {
            'url': res.get_url() if link == 'direct' else res.get_absolute_url(),
            'text': "Pistes pour l’évaluation :" if res.type_r == 'epro' else res.title,
            'logo': img,
            'cant': cantons
        }
        if editable is True:
            res_str += edit_str() % {
              'is_grp': '',
              'edit_url': reverse('resource_edit_full', args=[res.id]),
              'del_url': reverse('resource_unlinkcell'),
              'copy_url': reverse('resource_copy'),
              'cut_url': reverse('resource_cut'),
              'link_id': res_link.pk,
              'aclass': '',
            }
        res_str = '<div class="%s">%s</div>' % ('validating' if res.status != 'validee' else '', res_str)
        content += res_str
    if content:
        content = '<div class="resource_%s">%s</div>' % (res_type, content)
    return content


def _render_grp_resources(resource_grps, editable, public):
    """
    Render all grouped resources.
    """
    content = ''
    base_host = settings.ALLOWED_HOSTS[0]
    logo_src = static('resources/public/images/logo_m.png')
    logo_src_open = logo_src.replace('logo_m.png', 'logo_m_open.png')
    insert_in_li = False

    if public and len(resource_grps): #  > 1
        insert_in_li = True

    # Formatting of resource groups
    for grp_name, grp_list in resource_grps.items():
        """
        Format this way:
        <div class="resources">
          <div class="resource_group">
            <img src="...resources/public/images/logo_m.png" class="inline">
            <a target="_blank" href="%(url)s">Title of resource group</a>
            [ if editable ] <span class="edit_widgets">...</span> [ endif ]
          </div>
          <div class="resource_group">...</div>
          ...
        </div>
        """
        grp_list_str = ''
        for grp_link in grp_list:
            grp = grp_link.catalog
            grp_str = '%(icon)s %(prefix)s <a target="_blank" class="link_rsrc" href="%(url)s">%(text)s</a>' % {
                'icon': '<img src="%s" class="inline">' % logo_src if not insert_in_li else '',
                'url': 'https://%s%s' % (base_host, grp.url),
                'prefix': '[%s] ' % grp.groupment if not insert_in_li and grp.groupment else '',
                'text': grp.title}
            if editable is True:
                grp_str += edit_str() % {
                  'is_grp': 'grp_',
                  'edit_url':  reverse('resource_grp_edit', args=[grp.id]),
                  'del_url': reverse('resource_unlinkcell'),
                  'copy_url': reverse('resource_copy'),
                  'cut_url': reverse('resource_cut'),
                  'link_id': grp_link.pk,
                  'aclass': '',
                }
            grp_list_str += '%s<div class="resource_group %s" data-grpid="%s">%s</div>%s' % (
                '<li>' if insert_in_li else '',
                grp.grp_status, grp.id, grp_str,
                '</li>' if insert_in_li else ''
            )

        if insert_in_li:
            grp_list_str = ('%(grp_name)s<img src="%(logo)s" data-altsrc="%(logo_alt)s" class="toggle-rsrc" '
                            'title="Cliquez pour afficher les ressources disponibles"><br clear="right">'
                            '<ul class="rsrc per-hidden">%(content)s</ul>') % {
                                'logo': logo_src, 'logo_alt': logo_src_open,
                                'content': grp_list_str,
                                'grp_name': '%s : ' % grp_name if grp_name != 'none' else ''}
        content += '<div class="resource_grp" data-orderurl="%s">%s</div>' % (
            reverse('resource_grp_ordering') if editable else '', grp_list_str)
    return content

@register.simple_tag
def render_cant_resources(cell, editable, public):
    """
    Formatting of cantonal resources.
    """
    if isinstance(cell, dict):
        return ""
    content = ''
    resource_cant = cell.get_resources(public_site=public).get('can')
    if not resource_cant:
        return content
    res_l = []
    res_l_val = []

    for res_link in resource_cant:
        # Format individual resources and put them in res_l list
        res = res_link.resource
        img_paths = res.get_cant_logos()
        res_str = "<a target='_blank' href='%(url)s' title='%(title)s'>%(text)s</a>" % {
            'url': res.get_url(),
            'title': res.title,
            'text': " ".join(["<img src='%s'/>" % img_path for img_path in img_paths]),
        }
        if editable is True:
          res_str += edit_str() % {
              'is_grp': '',
              'aclass': 'pop_editor',
              'edit_url':  reverse('resource_edit_urlonly', args=[res.id]),
              'del_url': reverse('resource_unlinkcell'),
              'copy_url': reverse('resource_copy'),
              'cut_url': reverse('resource_cut'),
              'link_id': res_link.pk,
          }
        if res.status != 'validee':
            res_l_val.append(res_str)
        else:
            res_l.append(res_str)

    if len(res_l):
        content += "<div class='resources-cant'>Précisions cantonales :%s %s</div>\n" % (
            '<br>' if len(res_l) > 1 else '', " ".join(res_l))
    if len(res_l_val):
        content += "<div class='validating resources-cant'>Précisions cantonales (en cours de validation) : %s</div>\n" % " ".join(res_l_val)
    if content:
        content = '<div class="resources">%s</div>' % content

    return mark_safe(content)

@register.filter
def is_checkbox(field):
    return isinstance(field.field.widget, CheckboxInput)

@register.filter
def custom_label(bfield):
    if 'bsn_required' in bfield.field.widget.attrs.get('class', ''):
        tag = bfield.label_tag(attrs={'class': 'bsn_required'})
    elif bfield.field.required:
        tag = bfield.label_tag(attrs={'class': 'required'})
    else:
        tag = bfield.label_tag()
    if is_checkbox(bfield):
        tag = mark_safe(tag.replace(':', ''))
    return tag


@register.filter
def strip_colon(label):
    return mark_safe(label.replace(':', ''))


@register.filter
def qrcode(res):
    path = res.qrcode
    if path:
        return mark_safe('<img src="%s" width="120">' % os.path.join(settings.MEDIA_URL, res.qrcode))
    else:
        return ''

@register.filter
def type_doc_icon(value):
    if value:
        html = ''
        for val in value.split(', '):
            if val in TYPE_DOC_MAPPING:
                html += '<span class="mmicon" title="%s">%c</span>' % (value, TYPE_DOC_MAPPING[val])
            else:
                html += val
        return mark_safe(html)
    return value

@register.filter
def type_doc_bold(widget):
    if widget['value'] in TYPE_DOC_MAIN:
        return format_html('<b>{}</b>', widget['label'])
    return widget['label']

@register.filter
def lom_urlencode(lom_id):
    # Probably a bug on the bsn, the '#' has to be double-encoded to work
    return urlquote(urlquote(lom_id))

@register.filter
def resource_data_groups(res, exclude=''):
    excludes = exclude.split(',')
    for group_title, fields in res.GROUPS:
        values = []
        for field_name in fields:
            if field_name in excludes:
                continue
            elif field_name == 'emedia' and not res.emedia:
                continue
            elif field_name == 'relations':
                for rel in res.relations.all():
                    values.append((field_name, rel.get_label(), rel.as_html()))
            else:
                val = field_value(res, field_name)
                if val != '':
                    field = res._meta.get_field(field_name)
                    values.append((field_name, field.verbose_name, val))
        if group_title == "Plan d'études":
            values.append(('resourceperlink_set', '', res.grouped_per_links()))
        if values:
            yield group_title, values

def field_value(res, field_name):
    value = res.get_value(field_name)
    if field_name == 'thumb' and res.thumb:
        return mark_safe('<img src="%s">' % res.thumb.url)
    elif field_name in ('description', 'source'):
        value = mark_safe(value)
    elif str(value).startswith('http'):
        value = mark_safe('<a href="%s" target="_blank">%s</a>' % (value, value))
    return value

@register.inclusion_tag('ressource/tab-navigation.html')
def tab_navigation(groups, current_group):
    """Template tag to display bottom tab navigation in resource edition form."""
    group_names = [g[0] for g in groups]
    current_idx = group_names.index(current_group)
    context = {}
    if current_idx > 0:
        context['prev'] = group_names[current_idx - 1]
    if current_idx < (len(group_names) - 1):
        context['next'] = group_names[current_idx + 1]
    return context


@register.filter
def hierarch(per_link_struct):
    """Display ResourcePerLink in a hierarchical structure.
       per_link_struct is the result of resource.grouped_per_links().
    """
    val_templ = '<tr class="per"><td colspan="2">{}</td></tr>'
    result = ''
    for cycle, domains in per_link_struct.items():
        snip = '<div class="cycle">Cycle %d</div>' % cycle
        for domain, disciplines in domains.items():
            for disc, objectifs in disciplines.items():
                snip += '<div class="domaine color%s">%s : %s</div>' % (domain.abrev, domain.name, disc)
                for objectif, contenus in objectifs.items():
                    snip += '<div class="objectif color%s">%s</div>' % (domain.abrev, str(objectif))
                    conts = "".join(
                        '<li class="%s">%s</li>' % (cont.cellule.get_css_class(domain.abrev), cont.contenu)
                        for cont in contenus
                    )
                    if conts:
                        snip += '<ul class="contenu">%s</ul>' % conts
                    result += val_templ.format(snip)
                    snip = ''

    return mark_safe(result)


@register.filter
def objectif_or_groupment(cat):
    return str(cat.objectif) if cat.objectif else (cat.groupment or 'Sans objectif')


@register.filter
def auto_exposant(text):
    return mark_safe(auto_exposant_utils(escape(text)))
