from collections import Counter
from datetime import date, datetime, timedelta
from io import BytesIO
from pathlib import Path
from urllib.parse import quote, unquote
import itertools
import locale
import mimetypes
import os
import re
import tablib
import zipfile

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin as PermissionRequiredMixinDjango
from django.contrib.auth.views import redirect_to_login
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import BadRequest, ObjectDoesNotExist, PermissionDenied
from django.core.files.storage import storages
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator as DjangoPaginator
from django.db import IntegrityError, transaction
from django.db.models import Count, F, OuterRef, Prefetch, Q, Subquery
from django.forms.models import BaseInlineFormSet, inlineformset_factory, modelformset_factory
from django.http import (
    FileResponse, Http404, HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect, JsonResponse
)
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.dateparse import parse_datetime
from django.utils.functional import cached_property
from django.utils.html import escape
from django.utils.http import url_has_allowed_host_and_scheme
from django.utils.safestring import SafeText
from django.views.generic import DeleteView, DetailView, FormView, TemplateView, UpdateView, View

from pper.models import Cellule, Domain
from shortener.models import ShortLink
from .forms import (
    ResourceCheckForm, ResourceSearchForm, ResourceEditForm, ResourceGroupEditForm,
    ResourceSuggestionForm, ResourceGroupLinkForm, ResourcePerLinkFormIntern,
    SubThemeForm
)
from .models import (
    MediaURL, Resource, ResourceCheck, ResourceClick, ResourceGroup, ResourceGroupLink,
    ResourcePerLink, ResourceRelation, SubTheme,
)
from .ontologies import RELATION_TYPES_HELP
from .utils import XLSX_MIMETYPE


class PermissionRequiredMixin(PermissionRequiredMixinDjango):
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            raise PermissionDenied(self.get_permission_denied_message())
        return super().handle_no_permission()


class Paginator(DjangoPaginator):
    """
    Custom paginator to:
    - allow retrieving all remaining results
    - allow retrieving all results until current page
    """
    def page(self, number, from0=False, and_remaining=False):
        if from0:
            number = self.validate_number(number)
            top = number * self.per_page
            return self._get_page(self.object_list[0:top], number, self)
        elif and_remaining:
            number = self.validate_number(number)
            bottom = (number - 1) * self.per_page
            return self._get_page(self.object_list[bottom:self.count], number, self)
        else:
            return super().page(number)


# ***** Public Views ******

class RenderingMixin:
    pdf_title = "Recherche de ressources du Plan d’études romand"

    def render_to_pdf(self, resources, form=None, title=None):
        from .pdf_gen import ResourceSearchPDF

        if self.request.user.is_anonymous:
            return redirect_to_login(self.request.get_full_path())
        doc = ResourceSearchPDF(
            resources, search_criteria=form.readable_criteria() if form is not None else None
        )
        tampon = BytesIO()
        doc.produce(tampon, title=title or self.pdf_title)
        tampon.seek(0)
        return FileResponse(tampon, as_attachment=True, filename='PER-Ressources_%s.pdf' % date.today())

    def render_to_xls(self, resources):
        field_order_priority = [
            'id', 'title', 'url', 'description', 'keywords', 'status', 'status_comment',
            'created', 'resourcesubmitter',
        ]

        def get_verbose(field):
            if field.one_to_many or field.many_to_many:
                return field.remote_field.model._meta.verbose_name_plural
            elif field.one_to_one:
                return field.remote_field.model._meta.verbose_name
            return field.verbose_name

        def field_order(field):
            try:
                return field_order_priority.index(field.name)
            except ValueError:
                return 50

        def get_value(res, field):
            if field.one_to_many or field.many_to_many:
                accessor = field.get_accessor_name() if hasattr(field, 'get_accessor_name') else field.name
                return '\n'.join([str(obj) for obj in getattr(res, accessor).all()])
            return res.get_value(field.name, request=self.request)

        excluded_fields = {
            'cell', 'position', 'fts', 'resourcegrouplink', 'resourceperindex',
            'checked_links',
        }
        exported_fields = sorted([
            f for f in Resource._meta.get_fields() if f.name not in excluded_fields
        ], key=field_order)
        data = tablib.Dataset()
        data.headers = [get_verbose(f) for f in exported_fields]
        for res in resources.select_related('resourcesubmitter').prefetch_related(
            Prefetch('relations', queryset=ResourceRelation.objects.select_related('other')),
            'contribs', 'files', 'resourcegroup_set', 'resourceperlink_set'
        ):
            data.append([get_value(res, f) for f in exported_fields])
        response = HttpResponse(data.xlsx, content_type=XLSX_MIMETYPE)
        response['Content-Disposition'] = 'attachment; filename=bd_per_resources_%s.xlsx' % (
            date.today(),)
        return response


class ResourceHomeView(RenderingMixin, TemplateView):
    title = "Ressources numériques"
    can_search = True
    empty_message = ''
    emedia = False
    temp = False

    @property
    def is_naked(self):
        return self.request.GET.get('output', '') == 'naked'

    def get_template_names(self):
        return ['ressource/results_only.html'] if self.is_naked else ['ressource/home.html']

    def paginate(self, form, resources):
        no_page = self.request.GET.get('p', self.request.GET.get('page', 1))
        page_size = form.cleaned_data.get('size', 10) or 10
        include_all = self.request.GET.get('remain') == 'true'
        try:
            page = Paginator(resources, page_size).page(
                no_page, from0=not self.is_naked, and_remaining=include_all
            )
        except (EmptyPage, PageNotAnInteger):
            page = []
        return page

    def get(self, request, *args, **kwargs):
        resources = []

        if self.emedia:
            filtre = Q(emedia=True)
        elif self.temp:
            filtre = Q(description__contains='<a')
        else:
            filtre = None

        result_page = []
        if request.GET:
            form = ResourceSearchForm(request.user, data=request.GET)
            if form.is_valid():
                xls_output = request.GET.get('output', '') == 'xlsx'
                resources = form.search(filtre=filtre, prefetch=False if xls_output else True)
                if request.GET.get('output', '') == 'pdf':
                    title = request.GET.get('title')
                    return self.render_to_pdf(resources, form, title=escape(title) if title else None)
                elif xls_output:
                    return self.render_to_xls(resources)
                result_page = self.paginate(form, resources)
                self.empty_message = "Désolé, aucune ressource ne correspond à votre recherche."
        elif filtre:
            form = ResourceSearchForm(request.user, data={})
            form.is_valid()
            resources = form.search(filtre=filtre)
            result_page = self.paginate(form, resources)
            self.empty_message = "Aucune ressource à afficher."
        else:
            form = ResourceSearchForm(request.user)

        context = self.get_context_data(**kwargs)
        context.update({
            'domains': Domain.objects.all(),
            'form': form,
            'qs': re.sub(r'&p=\d+', '', request.META['QUERY_STRING']),
            'resources': result_page,
        })
        return self.render_to_response(context)


class ResourceSelectedView(RenderingMixin, TemplateView):
    title = "Ma sélection de ressources numériques"
    pdf_title = title
    template_name = 'ressource/resource_selected.html'
    empty_message = "Aucune ressource à afficher."
    can_search = False

    def get(self, request, *args, **kwargs):
        selected_pks = unquote(self.request.COOKIES.get('resource_selection', '')).split(',')
        if selected_pks == ['']:
            self.resources = Resource.objects.none()
        else:
            self.resources = Resource.objects.filter(pk__in=selected_pks)
        if request.GET.get('output', '') == 'pdf':
            title = escape(request.GET.get('title', self.pdf_title))
            return self.render_to_pdf(self.resources, title=title)
        elif request.GET.get('output', '') == 'xlsx':
            return self.render_to_xls(self.resources)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        referer = self.request.META.get('HTTP_REFERER')
        if referer and reverse('resource_home') + '?' in referer:
            search_link = referer
        else:
            search_link = reverse('resource_home')
        context.update({
            'breadcrumb' : [('Recherche', search_link),],
            'resources': self.resources,
        })
        return context


class ResourceAccessMixin:
    model = Resource

    def get_object(self, **kwargs):
        try:
            obj = super().get_object(**kwargs)
        except AttributeError:
            obj = get_object_or_404(self.model, pk=self.kwargs.get('pk', self.kwargs.get('id')))
        if not obj.can_view(self.request.user):
            raise PermissionDenied(
                "Désolé, vous n'avez pas les permissions pour accéder à cette ressource."
            )
        return obj


class ResourceDisplay(ResourceAccessMixin, DetailView):
    # Template overriden from urls.py for full view
    template_name = 'ressource/resource.html'
    version = 'v1'

    def get_template_names(self):
        if self.version == 'appr':
            return ['ressource/resource_appr.html']
        return super().get_template_names()

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        if self.version != 'appr':
            try:
                nums = list(self.object.resourcegrouplink_set.exclude(number='').values_list('number', flat=True))
                if len(nums) == 1:
                    context['rn_tag'] = nums[0]
            except AttributeError:
                pass
        context.update({
            'metadata': self.object.metadata(subset='appr' if self.version == 'appr' else 'standard'),
            'show_refs': self.version != 'appr' and self.object.show_refs,
            'vdata': self.object.media_data(self.request),
        })
        try:
            sh = ShortLink.get_for_object(self.object)
            context['short_url'] = sh.short_url()
            buf = BytesIO()
            sh.qr_image(format='svg').save(buf)
            context['short_svg'] = buf.getvalue().decode()
        except ShortLink.DoesNotExist:
            pass
        if self.object.has_images:
            context['images'] = self.object.get_images
        return context


class ResourcePDFView(ResourceAccessMixin, View):
    def get(self, request, *args, **kwargs):
        from .pdf_gen import ResourcePDF

        resource = self.get_object()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=RessourcePER-%d.pdf' % resource.pk

        doc = ResourcePDF(resource)
        doc.produce(response)
        return response


class ResourceDownloadView(ResourceAccessMixin, View):
    """zip resource files and serve result."""
    def get(self, request, *args, **kwargs):
        resource = self.get_object()
        zip_content = BytesIO()
        filename = 'ressource_%d.zip' % resource.pk
        try:
            with zipfile.ZipFile(zip_content, mode='w', compression=zipfile.ZIP_DEFLATED) as filezip:
                for rfile in itertools.chain(resource.files.all(), resource.get_images):
                    filezip.write(rfile.rfile.path, arcname=os.path.basename(rfile.rfile.path))
        except FileNotFoundError:
            raise Http404

        response = HttpResponse(zip_content.getvalue(), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename
        return response


class ResourceCreateShortlinkView(PermissionRequiredMixin, ResourceAccessMixin, View):
    permission_required = 'ressource.change_resource'

    def post(self, request, *args, **kwargs):
        res = self.get_object()
        sh = ShortLink.get_for_object(res, create=True)
        messages.success(request, "Un lien court a bien été créé pour cette ressource")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', res.get_absolute_url()))


class ResourceShortQRDownloadView(ResourceAccessMixin, View):
    def get(self, request, *args, **kwargs):
        res = self.get_object()
        try:
            sh = ShortLink.get_for_object(res)
        except ShortLink.DoesNotExist:
            raise Http404
        buf = BytesIO()
        sh.qr_image(format='svg').save(buf)
        buf.seek(0)
        #svg_source = buf.getvalue().decode()
        # Insert short link text under the QR code
        # Deactivated
        #svg_source = svg_source.replace('height="33mm"', 'height="37mm"')
        #svg_source = svg_source.replace('viewBox="0 0 33 33"', 'viewBox="0 0 33 37"')
        #svg_source = svg_source.replace(
        #    '</svg>',
        #    '<text x="2.8" y="33.8" font-family="sans-serif" font-size="3.6px" textLength="27">%s</text></svg>' % sh.short_url()
        #)
        return FileResponse(buf, content_type='image/svg+xml', filename=f'{sh.key}.svg')


class ResourceGroupDisplay(TemplateView):
    template_name = 'ressource/resource_grp.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        group = get_object_or_404(ResourceGroup.objects.prefetch_related('tags'), pk=self.kwargs['pk'])
        if self.request.user.is_anonymous and group.grp_status == 'avalider':
            raise PermissionDenied("Accès autorisé uniquement pour les utilisateurs connectés.")
        domaine = group.domaine()
        resources = [{
            'rlink': rlink,
            'resource': rlink.resource,
            'vdata': rlink.resource.media_data(self.request),
        } for rlink in group.ordered_resources(self.request.user)]
        context.update({
            'object': group,
            'tags': [t.tag for t in group.tags.all()],
            'subthemes_grouped': group.subthemes.all().ordered(),
            'domain': domaine or '',
            'domain_style': domaine.abrev if domaine else 'GEN',
            'disciplines': group.disciplines(),
            'objectif': group.objectif,
            'resource_data': resources,
        })
        return context


class ResourceGroupPDFView(View):
    def get(self, request, *args, **kwargs):
        from .pdf_gen import ResourceGroupPDF

        group = get_object_or_404(ResourceGroup, pk=kwargs['group_id'])
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=PER-Ressources-%d_%s.pdf' % (group.id, date.today())

        doc = ResourceGroupPDF(group, request.user, request.GET.get('flt'))
        doc.produce(response)
        return response


class ResourceEditBaseView(FormView):
    """
    Common base for resource edition, both for staff edition and public suggestion.
    """
    model = Resource
    op = None

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.m2ms = {}
        return kwargs

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        next_url = self.request.POST.get('next', self.request.META.get('HTTP_REFERER', ''))
        res = context['form'].instance
        if not '#' in next_url and res.pk:
            next_url += f'#res{res.pk}'
        context.update({
            'relations_help': RELATION_TYPES_HELP,
            'next': next_url,
        })
        if res.pk:
            url_obj = MediaURL.detect(res)
            if url_obj:
                try:
                    context['video_path'] = url_obj.video_path()
                except AttributeError:
                    pass
        return context


class ResourceSuggestionView(ResourceEditBaseView):
    form_class = ResourceSuggestionForm
    template_name = 'ressource/resource_edit.html'
    op = 'suggestion'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_intro'] = not context['form'].is_bound and not self.request.GET.get('from')
        return context

    def form_valid(self, form):
        from .pdf_gen import ResourcePDF

        resource = form.save()
        if 'resourcerelation_set' in self.m2ms:
            # Save relations, even if not in the suggestion form
            for rel in self.m2ms['resourcerelation_set']:
                rel.resource = resource
                rel.save()
        messages.success(self.request, """
            Nous vous remercions du temps que vous avez pris pour décrire une nouvelle ressource.<br>
            Vous recevrez une confirmation par courriel avec les données que vous avez introduites."""
        )
        form.send_email_confirmation()
        return HttpResponseRedirect(reverse('resource_home'))


class ResourceFileView(View):
    """Protect access to resource files needing authentication."""

    # When a resource was deleted, adding the {old_id: new_id} mapping allow
    # the old link to still be valid.
    FILE_COMPAT_MAPPING = {
        4172: 4680,  # Sur le chemin de l’école
    }

    def get(self, request, *args, **kwargs):
        if self.kwargs['pk'] in self.FILE_COMPAT_MAPPING:
            return HttpResponsePermanentRedirect(
                reverse('download', kwargs={
                    'pk': self.FILE_COMPAT_MAPPING[self.kwargs['pk']],
                    'path': self.kwargs['path']
                })
            )

        resource = get_object_or_404(Resource, pk=self.kwargs['pk'])
        if self.kwargs['path'].lower().endswith(('_w220.jpg', '_w220.jpeg')):
            # Allow thumbnails without protection
            return self.serve_file(resource)
        if not resource.can_view_file(request.user):
            if self.request.user.is_authenticated:
                raise PermissionDenied("Vous n'avez pas la permission d'accéder à cette ressource")
            return redirect_to_login(request.get_full_path())
        return self.serve_file(resource)

    def serve_file(self, resource):
        filename = self.kwargs['path']
        final_path = f"ressources/{resource.pk}/{filename}"
        fs_path = f"{settings.MEDIA_ROOT}/{final_path}"
        if not storages["upload"].exists(final_path):
            raise Http404("Sorry, this file does not exist.")

        if storages["default"].exists(final_path):
            # Continue with filesystem storage
            response = HttpResponse()
            disposition = 'inline' if filename.endswith(('pdf', 'jpg')) else 'attachment'
            try:
                filename.encode('ascii')
                file_expr = f'filename="{filename}"'
            except UnicodeEncodeError:
                file_expr = f"filename*=utf-8''{quote(filename)}"
            response['Content-Disposition'] = f'{disposition}; {file_expr}'
            response['Content-Type'] = mimetypes.guess_type(fs_path)[0]
            response['X-Sendfile'] = fs_path.encode('utf-8')
            return response
        else:
            # Else it should be on S3, redirect to s3 temp url.
            return HttpResponseRedirect(s3_storage.url(s3_path))


# ****** Protected editing views *******

class ResourceEditView(PermissionRequiredMixin, ResourceEditBaseView):
    form_class = ResourceEditForm
    template_name = 'ressource/resource_edit.html'
    permission_required = 'ressource.change_resource'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        resource_id = self.kwargs.get('resource_id', self.kwargs.get('pk', None))
        if self.op == 'edit':
            # Edit existing resource
            kwargs['instance'] = get_object_or_404(self.model, pk=resource_id)
        elif self.op == 'new':
            # New resource (possibly attached to a resource group)
            if self.request.user.has_perm('ressource.add_resource'):
                kwargs['initial']['status'] = 'validee'
            if 'add_in_group' in self.request.GET:
                kwargs['initial']['add_in_group'] = self.request.GET['add_in_group']
        else:
            raise Exception("self.op with unknown value %r" % self.op)
        if 'new' in self.op:
            if self.request.user.has_perm('ressource.add_resource'):
                kwargs['initial']['type_r'] = 'rno'
            else:
                kwargs['initial']['type_r'] = 'med'

        if self.request.POST:
            kwargs['data'] = self.request.POST
        if self.request.FILES:
            kwargs['files'] = self.request.FILES
        return kwargs

    def form_valid(self, form):
        instance = form.save()
        if self.op in ['new', 'new_for_cell']:
            msg = "La ressource « %s » a bien été créée" % instance.title
        else:
            msg = "La ressource « %s » a bien été modifiée" % instance.title
        messages.success(self.request, msg)
        redirect_to = self.request.POST.get('next')
        if form.cleaned_data['add_in_group']:
            # Création d'une nouvelle ressource dans un catalogue
            group = ResourceGroup.objects.get(pk=form.cleaned_data['add_in_group'])
            group.add_resource(instance)
            redirect_to = reverse('resource_grp_edit', args=[group.pk])
        if not redirect_to and self.op == 'new':
            redirect_to = instance.get_absolute_url()
        if (
            not redirect_to or
            not url_has_allowed_host_and_scheme(redirect_to, allowed_hosts={self.request.get_host()})
        ):
            redirect_to = instance.get_absolute_url()
        return HttpResponseRedirect(redirect_to)


class ResourceEditUrlOnlyView(PermissionRequiredMixin, UpdateView):
    """Essentiellement pour l'édition des précisions cantonales."""
    model = Resource
    permission_required = 'ressource.change_resource'
    fields = ['url']
    template_name = 'ressource/resource_edit_urlonly.html'

    def form_valid(self, form):
        form.save()
        return JsonResponse({
            'refreshPage': True,
        })


class ResourceGroupListMixin:
    def get_catalogs(self, user):
        catalogs = ResourceGroup.all_by_user(user)
        if self.request.GET.get('updated_from'):
            try:
                upd_date = parse_datetime(self.request.GET['updated_from'])
            except ValueError:
                raise BadRequest("updated_from format is not correct")
            if upd_date is None:
                raise BadRequest("updated_from format is not correct")
            if timezone.is_naive(upd_date):
                upd_date = timezone.make_aware(upd_date)
            catalogs = catalogs.filter(updated__gte=upd_date)
        catalogs = catalogs.annotate(num_resources=Count('resources')).prefetch_related('resourceperlink_set')
        catalogs = list(catalogs)
        no_domain = Domain(name='Autres ressources pour l’enseignant', abrev='GEN')
        for cat in catalogs:
            cat.domaine = cat.domaine() or no_domain
        locale.setlocale(locale.LC_ALL, '')
        catalogs.sort()
        return catalogs


class ResourceGroupHome(ResourceGroupListMixin, TemplateView):
    template_name = 'ressource/resource_grp_list.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'catalogs': self.get_catalogs(self.request.user),
            'editor': self.request.user.has_perm('ressource.change_resourcegroup'),
        })
        return context


class ResourceGroupEditView(PermissionRequiredMixin, FormView):
    form_class = ResourceGroupEditForm
    model = ResourceGroup
    template_name = 'ressource/resource_grp_edit.html'
    permission_required = 'ressource.change_resourcegroup'

    class CustomFormSet(BaseInlineFormSet):
        """As some subforms might have been ajax-deleted, don't choke on absent form."""
        @cached_property
        def forms(self):
            forms = []
            for i in range(self.total_form_count()):
                try:
                    forms.append(self._construct_form(i))
                except MultiValueDictKeyError:
                    forms.append(self.empty_form)
            return forms

    def subforms_class(self):
        return inlineformset_factory(
            ResourceGroup, ResourceGroupLink, form=ResourceGroupLinkForm,
            formset=self.CustomFormSet, extra=0, can_delete=False
        )

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        if not 'subforms' in context:
            ResourceFormSet = self.subforms_class()
            context['subforms'] = ResourceFormSet(instance=context['form'].instance)
        return context

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        if 'group_id' in self.kwargs:
            form_kwargs['instance'] = get_object_or_404(self.model, pk=self.kwargs.get('group_id'))
        return form_kwargs

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.form_class)
        if form.instance.pk:
            subforms = self.get_form(self.subforms_class())
        else:
            subforms = None
        form_valid = True
        if subforms:
            if subforms.is_valid():
                subforms.save()
            else:
                form_valid = False
        form_valid = form_valid and form.is_valid()
        if form_valid:
            instance = form.save()
            messages.success(
                self.request, "Le catalogue de ressources « %s » a bien été modifié" % instance.title
            )
            return HttpResponseRedirect(instance.get_absolute_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, subforms=subforms))


class ResourceGroupChangeOrderingView(PermissionRequiredMixin, View):
    """
    This view should reveive POST data with a list of ResourceGroup ids in the
    desired ordering.
    """
    permission_required = 'ressource.change_resourcegroup'

    def post(self, request, *args, **kwargs):
        ids = [int(i) for i in request.POST.getlist('grp_ids[]')]
        groups = sorted(ResourceGroup.objects.in_bulk(ids).values(), key=lambda o: ids.index(o.pk))
        for index, group in enumerate(groups):
            if group.weight != index:
                group.weight = index
                group.save()
        return HttpResponse('')


class SubThemesEdit(PermissionRequiredMixin, FormView):
    template_name = 'ressource/subthemes_edit.html'
    permission_required = 'ressource.change_resourcegroup'

    def dispatch(self, request, *args, **kwargs):
        self.group = get_object_or_404(ResourceGroup, pk=kwargs['group_id'])
        return super().dispatch(request, *args, **kwargs)

    def get_form_class(self):
        return modelformset_factory(SubTheme, form=SubThemeForm, extra=3, can_delete=True)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['queryset'] = self.group.subthemes.all()
        return kwargs

    def get_initial(self):
        return [{'group': self.group.pk}] * 3

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'group': self.group,
        })
        return context

    def get_success_url(self):
        return reverse('resource_grp_edit', args=[self.group.pk])

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class ResourceLinkToCellView(PermissionRequiredMixin, FormView):
    """Link a resource to a PER cell."""
    permission_required = 'pper.change_specification'
    template_name = 'link_resource.html'
    form_class = ResourcePerLinkFormIntern

    def form_valid(self, form):
        cell = get_object_or_404(Cellule, pk=self.kwargs['cell_id'])
        if form.cleaned_data['resource_pk']:
            res = get_object_or_404(Resource, pk=form.cleaned_data['resource_pk'])
            cat = None
        if form.cleaned_data.get('catalog_pk'):
            cat = get_object_or_404(ResourceGroup, pk=form.cleaned_data['catalog_pk'])
            res = None
        ResourcePerLink.objects.create(
            resource=res,
            catalog=cat,
            content_type=ContentType.objects.get_for_model(cell),
            object_id=cell.pk,
        )
        return JsonResponse({
            'result': 'OK',
            'cell_id': cell.pk,
            'cell_url': reverse('course_cell_content', args=['spec', cell.get_super().pk, cell.pk]),
        })


class ResourceLinkToGroupView(PermissionRequiredMixin, View):
    """
    Add a resource to a group, from ResourceGroup edition form.
    """
    permission_required = 'ressource.change_resourcegroup'

    def post(self, request, *args, **kwargs):
        grp = ResourceGroup.objects.get(pk=kwargs['group_id'])
        if 'res_pk' in request.POST and request.POST['res_pk']:
            pk = int(request.POST['res_pk'])
            res = Resource.objects.get(pk=pk)
            try:
                with transaction.atomic():
                    grp.add_resource(res)
            except IntegrityError:
                return HttpResponse("Cette ressource est déjà contenue dans ce groupe")
        return HttpResponse('')


class ResourceUnlinkFromGroupView(PermissionRequiredMixin, View):
    """
    Remove a resource from a group, from ResourceGroup edition form.
    """
    permission_required = 'ressource.change_resourcegroup'

    def post(self, request, *args, **kwargs):
        lnk = get_object_or_404(ResourceGroupLink, group__id=kwargs['group_id'], resource__id=kwargs['resource_id'])
        res = lnk.resource
        lnk.group.save()
        lnk.delete()
        res.index()
        return HttpResponse('')


class ResourceUnlinkAllFromGroupView(PermissionRequiredMixin, View):
    """
    Remove all resources from a group, from ResourceGroup edition form.
    """
    permission_required = 'ressource.change_resourcegroup'

    def post(self, request, *args, **kwargs):
        grp = get_object_or_404(ResourceGroup, pk=kwargs['group_id'])
        for lnk in grp.resourcegrouplink_set.all():
            res = lnk.resource
            lnk.delete()
            res.index()
        return HttpResponse('')


class ResourceGroupSearch(View):
    def get(self, request, *args, **kwargs):
        results = ResourceGroup.objects.filter(title__icontains=request.GET['term']
                                 ).values('pk', 'title')
        response = {
            'total': len(results),
            'results': [{'label': res['title'], 'pk': res['pk']} for res in results],
        }
        return JsonResponse(response)


class ResourceSearch(TemplateView):
    """
    A simple search view to search resources (local) from catalog edition
    to be able to insert an existing resource in a catalog.
    """
    source = 'local'
    template_name = 'ressource/resource_search_details.html'

    def get(self, request, *args, **kwargs):
        if int(self.kwargs['group_id']) > 0:
            group = get_object_or_404(ResourceGroup, pk=self.kwargs['group_id'])
        else:
            group = None
        results = Resource.objects.filter(
            Q(title__icontains=request.GET['term']) | Q(url__icontains=request.GET['term'])
        )
        if group:
            results = results.exclude(pk__in=group.resources.all())
        if request.GET.get('type_doc'):
            results = results.filter(type_doc=[request.GET.get('type_doc')])
        response = {
            'total': results.count(),
            'results': [
                {'label': str(res), 'pk': res.pk, 'description': res.description}
                for res in results
            ],
        }
        if kwargs['frmt'] == 'json':
            # We are returning data for the drop-down autocomplete list
            return JsonResponse(response)
        else:
            # We are returning data to populate search details template
            response.update({
                'term': request.GET['term'],
                'source': self.source,
                'grp': group.pk if group is not None else '',
                'submit_class': 'submit_%s' % self.source,
            })
            return self.render_to_response(response)


class ResourceUnlinkCellView(View):
    """Remove the link between a resource/catalog and a PER cell."""
    def post(self, request, *args, **kwargs):
        res_link = get_object_or_404(ResourcePerLink, pk=request.POST.get('obj_id'))
        response = {
            'cell_id': res_link.object_id,
            'reload_url': reverse('cell_content', args=[res_link.object_id])
        }
        res_link.delete()
        return JsonResponse(response)


class ResourceGroupDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'ressource.delete_resourcegroup'
    model = ResourceGroup
    pk_url_kwarg = 'group_id'
    success_url = reverse_lazy('resource_grp_home')

    def form_valid(self, form):
        msg = "Le catalogue «%s» a bien été supprimé." % self.get_object().title
        response = super().form_valid(form)
        messages.success(self.request, msg)
        return response


class ResourceDeleteView(PermissionRequiredMixin, View):
    permission_required = 'ressource.delete_resource'

    def post(self, request, *args, **kwargs):
        resource = get_object_or_404(Resource, pk=self.kwargs['resource_id'])
        success_url = self.get_success_url()
        success_message = "La ressource « %s » a bien été supprimée." % resource.title
        resource.delete()
        messages.success(self.request, success_message)
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        redirect_to = self.request.POST.get('next')
        if (
            not redirect_to or
            not url_has_allowed_host_and_scheme(redirect_to, allowed_hosts={self.request.get_host()})
        ):
            redirect_to = reverse('resource_home')
        return redirect_to


class CopyCutBaseView(View):
    def post(self, request, *args, **kwargs):
        obj_id = int(request.POST.get('obj_id'))
        obj = get_object_or_404(self.model, pk=obj_id)
        request.session['clipboard'] = '%s-%s-%s' % (obj._meta.model_name, obj.pk, self.op)
        return HttpResponse()

class ResourceCopyView(CopyCutBaseView):
    model = ResourcePerLink
    op = 'cop'

class ResourceCutView(CopyCutBaseView):
    model = ResourcePerLink
    op = 'cut'


class ResourcePasteView(View):
    def post(self, request, *args, **kwargs):
        try:
            model_name, pk, op = request.session['clipboard'].split('-')
        except KeyError:
            return JsonResponse({'status': 'error', 'message': "Le presse-papiers est vide"})
        try:
            obj = ContentType.objects.get_by_natural_key('ressource', model_name).model_class().objects.get(pk=pk)
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'message': "La ressource à coller n'existe pas (plus?)"})
        old_cell_id = obj.object_id
        try:
            new_cell = Cellule.objects.get(pk=self.kwargs['cell_id'])
        except Cellule.DoesNotExist:
            return JsonResponse({'status': 'error', 'message': "La cellule de destination n'existe pas (plus?)"})
        if op == 'cut':
            # Move the resource/group
            obj.object_id = new_cell.pk
            obj.save()
        elif op == 'cop':
            # Duplicate the resource link
            obj.pk = None
            obj.object_id = new_cell.pk
            obj.save()
        else:
            return JsonResponse({'status': 'error', 'message': "L'opération demandée n'existe pas."})
        if obj.resource_id:
            obj.resource.index()
        del request.session['clipboard']
        return JsonResponse({
            'status': 'OK',
            'old_cell_id': old_cell_id,
            'old_reload_url': reverse('cell_content', args=[old_cell_id]),
            'new_cell_id': new_cell.pk,
            'new_reload_url': reverse('cell_content', args=[new_cell.pk]),
        })


def resource_export(request):
    data = tablib.Dataset()
    exported_fields = ['title', 'url', 'status', 'cantons', 'type_r', 'description']
    data.headers = [Resource.verbose_field_name(field_name)
                    for field_name in (exported_fields + Resource.METADATA_FIELD_NAMES)]

    def safe_to_str(val):
        """Openpyxl doesn't support SafeText type"""
        return (val + '') if isinstance(val, SafeText) else val

    for res in Resource.objects.all():
        data.append([safe_to_str(getattr(res, field_name)) for field_name in exported_fields] +
                    [safe_to_str(d[1]) for d in res.metadata(subset='full', even_empty=True)])

    response = HttpResponse(data.xlsx, content_type=XLSX_MIMETYPE)
    response['Content-Disposition'] = 'attachment; filename=bd_per_resources_%s.xlsx' % (
        date.today(),)
    return response


class AudioVideoView(ResourceAccessMixin, TemplateView):
    player = None

    def dispatch(self, request, *args, **kwargs):
        # Only authorized when dynamically inserted
        if not is_ajax(request):
            return HttpResponseRedirect(reverse('resource_display', args=[kwargs['pk']]))
        response = super().dispatch(request, *args, **kwargs)
        try:
            response['Access-Control-Allow-Origin'] = response.context_data['vdata']['base']
        except (AttributeError, KeyError):
            # With the OPTIONS method, response has no context_data.
            pass
        return response

    def get_template_names(self):
        return ['ressource/video-player-block.html'] if is_ajax(self.request) else ['ressource/video-player.html']

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        resource = self.get_object()
        context.update({
            'resource': resource,
            'vdata': resource.media_data(self.request),
        })
        if not context['vdata']:
            raise Http404('No video data for this resource')
        return context


class GalleryView(ResourceAccessMixin, TemplateView):
    template_name = 'ressource/gallery-block.html'

    def dispatch(self, request, *args, **kwargs):
        # Only authorized when dynamically inserted
        if not is_ajax(request):
            return HttpResponseRedirect(reverse('resource_display', kwargs=kwargs))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        resource = self.get_object()
        context.update({
            'resource': resource,
            'need_auth': resource.need_auth and not self.request.user.is_authenticated,
            'images': resource.get_images,
         })
        return context


class ResourceQualityListView(PermissionRequiredMixin, TemplateView):
    permission_required = 'ressource.change_resource'
    template_name = 'ressource/qualite.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)

        latest_check_date = Subquery(ResourceCheck.objects.filter(
            resource_id=OuterRef("id"),
        ).order_by("-when").values('when')[:1])

        six_months_ago = timezone.now() - timedelta(days=180)
        res_to_check = Resource.objects.annotate(latestcheck=latest_check_date).exclude(url='').filter(status='validee').filter(
            Q(latestcheck__isnull=True) | Q(latestcheck__lt=six_months_ago)
        ).order_by(F('latestcheck').asc(nulls_first=True), 'updated')
        no_page = self.request.GET.get('p', self.request.GET.get('page', 1))

        latest_check_status = Subquery(ResourceCheck.objects.filter(
            resource_id=OuterRef("id"),
        ).order_by("-when").values('status')[:1])
        res_to_corr = Resource.objects.annotate(lateststatus=latest_check_status).exclude(
            lateststatus='ok'
        )

        context.update({
            'res_to_check': Paginator(res_to_check, 15).page(no_page),
            'res_to_corr': Paginator(res_to_corr, 15).page(no_page),
        })
        return context


class ResourceQualityView(PermissionRequiredMixin, FormView):
    permission_required = 'ressource.change_resource'
    template_name = 'ressource/qualite_edit.html'
    form_class = ResourceCheckForm

    def get_context_data(self, **context):
        resource = get_object_or_404(Resource, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**context),
            'resource': resource,
            'historic': resource.checks.order_by('-when'),
        }

    def form_valid(self, form):
        resource = get_object_or_404(Resource.objects.only('broken'), pk=self.kwargs['pk'])
        form.instance.user = self.request.user
        form.instance.resource_id = resource.pk
        form.instance.when = timezone.now()
        form.save()
        if form.instance.status == 'ko' and not resource.broken:
            resource.broken = True
            resource.save()
        elif form.instance.status != 'ko' and resource.broken:
            resource.broken = False
            resource.save()
        return HttpResponseRedirect(reverse('resource_quality_list'))


class PingResourceAccessView(View):
    def post(self, *args, **kwargs):
        try:
            pk = int(self.request.POST.get('pk'))
        except ValueError:
            # Simply ignore
            raise Http404
        res = get_object_or_404(Resource, pk=pk)
        ResourceClick.objects.create(resource=res)
        return HttpResponse('')


class StatsHomeView(PermissionRequiredMixin, TemplateView):
    permission_required = 'ressource.change_resource'
    template_name = 'ressource/stats_year.html'

    def get_context_data(self, **context):
        return {
            **super().get_context_data(**context),
            'annees': list(range(2018, date.today().year + 1)),
        }


class StatsYearView(PermissionRequiredMixin, View):
    """
    A form accepting a file which is parsed for catalogs/resources URLs and
    which then adds some metadata in additional columns.
    """
    permission_required = 'ressource.change_resource'
    path_model = '/var/lib/awstats/awstats{:0>2}{}.bdper-ressources.txt'
    #path_model = './awstats/var/lib/awstats/awstats{:0>2}{}.bdper-ressources.txt'

    types = {
        'emedia': {
            'marker': 'DOWNLOADS',
            'title': 'Téléchargements e-media',
            'headers': ['ID de ressource', 'URL', 'Titre', 'Cinéma?', 'SDM?', 'Nb de téléch.'],
            #/uploads/ressources/3595/kit_stereotypes.pdf
            'match': r"/uploads/ressources/(\d+)/.*",
        },
        'all': {
            'marker': 'SIDER',
            'title': 'Catalogues BD-PER',
            'headers': ['ID', 'URL', 'Titre', 'Domaine', 'Objectif', 'Téléchargements'],
            #/ressources/groupe/212/
            'match': r"/ressources/groupe/(\d+)/.?",
        },
        'clicks': {
            'title': 'Clics sur liens des ressources',
            'headers': ['ID de ressource', 'URL', 'Titre', 'Clics'],
        },
    }

    def get(self, request, *args, **kwargs):
        year = kwargs['year']
        typ = kwargs['typ']
        if year not in list(range(2018, date.today().year + 1)):
            raise Http404(f"L’année {year} n’est pas prise en charge.")
        if typ not in self.types:
            raise Http404(f"Le type ne peut être que: {', '.join(self.types.keys())}")

        data = tablib.Dataset(title=self.types[typ]['title'])
        data.headers = self.types[typ]['headers']

        if typ == 'clicks':
            self.collect_clicks_data(data)
        else:
            self.collect_awstats_data(data)

        response = HttpResponse(data.xlsx, content_type=XLSX_MIMETYPE)
        response['Content-Disposition'] = f"attachment; filename=ressources_{typ}_stats_{year}.xlsx"
        return response

    def collect_awstats_data(self, data):
        counter = Counter()
        typ = self.kwargs['typ']
        year = self.kwargs['year']
        for month in range(1, 13):
            for line in self.get_month_data(year, month, typ):
                counter[line[0]] += int(line[1])

        data_extr_func = self.emedia_data if typ == 'emedia' else self.all_data
        for key, count in counter.most_common():
            line_data = data_extr_func(key, count)
            if line_data:
                data.append(line_data)

    def collect_clicks_data(self, data):
        year = self.kwargs['year']
        query = ResourceClick.objects.filter(stamp__year=year).values('resource', 'resource__title'
            ).annotate(count=Count('pk')).order_by('-count')
        for line in query:
            data.append([
                line['resource'],
                self.request.build_absolute_uri(reverse('resource_display', args=[line['resource']])),
                line['resource__title'],
                line['count'],
            ])

    def get_month_data(self, year, month, typ):
        """
        Generator returning a list for all lines in the specified stats section.
        [page, view_count, mean_size, in, out]
        """
        stat_path = Path(self.path_model.format(month, year))
        if not stat_path.exists():
            print("Unable to read awstats file %s" % stat_path)
            return
        # Read lines between BEGIN_{marker} and END_{marker}
        marker = self.types[typ]['marker']
        matcher = self.types[typ]['match']
        with stat_path.open(mode='r') as fh:
            in_section = False
            for line in fh:
                if line.rstrip('\n').startswith(f'BEGIN_{marker} '):
                    in_section = True
                    continue
                if line.rstrip('\n') == f'END_{marker}':
                    break
                if in_section:
                    items = line.split()
                    if matcher:
                        m = re.match(matcher, items[0])
                        if not m:
                            continue
                        item_id = int(m.groups()[0])
                        yield [item_id] + items[1:]
                    else:
                        yield items

    def all_data(self, cat_id, cnt):
        try:
            cat = ResourceGroup.objects.get(pk=cat_id)
        except ResourceGroup.DoesNotExist:
            return None
        return [
            cat_id,
            self.request.build_absolute_uri(cat.get_absolute_url()),
            cat.title, cat.domaine(),
            cat.objectif.code if cat.objectif else '',
            cnt
        ]

    def emedia_data(self, res_id, cnt):
        try:
            res = Resource.objects.values('title', 'cinema', 'sdm').get(pk=res_id, emedia=True)
        except Resource.DoesNotExist:
            return None
        return [
            res_id, self.request.build_absolute_uri(reverse('resource_display', args=[res_id])),
            res['title'],
            res['cinema'] and 'x' or '',
            res['sdm'] and 'x' or '',
            cnt
        ]


def is_ajax(request):
    """Older Django implementation, jQuery dependent"""
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'
