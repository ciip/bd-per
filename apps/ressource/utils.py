import os
import re
import uuid
from collections import OrderedDict
from io import BytesIO
from pathlib import Path

import requests

from wand.color import Color
from wand.image import Image

from django.conf import settings
from django.db import transaction
from django.utils import timezone

XLSX_MIMETYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


class ResourceMixin:
    """
    Mixin to get resources for an object (currently pper.Cellule).
    Fill a ._resources cache.
    """
    # FIXME: Should use lru_cache decorator in Django 1.7
    res_order = ('type_r',)
    grp_order = ('weight', 'title')

    def get_resources(self, public_site=False):
        """
        Return cell resources grouped by type:
            {'<type>': [resourcelink,...], ...,
             'grps': {'groupment': [grp, ...], ...}
            }
        """
        if hasattr(self, '_resources'):
            # Count that call for an instance will not mix public_site True/False
            return self._resources

        if self.__class__.__name__ == 'Cellule':
            from ressource.models import ResourceGroup, ResourcePerLink
            spec = self.get_super()
            if not hasattr(spec, '_resource_cache'):
                spec._resource_cache = []
                spec._resourcegrp_cache =  []
                query = ResourcePerLink.from_cell_ids(spec.cellule_set.values_list('id', flat=True))
                # Cache all spec resources at spec level to save queries
                for link in query:
                    if link.catalog_id:
                        spec._resourcegrp_cache.append(link)
                    else:
                        spec._resource_cache.append(link)
            res_links = [lk for lk in spec._resource_cache if lk.object_id == self.pk]
            resource_groups = [lk for lk in spec._resourcegrp_cache if lk.object_id == self.pk]
        else:
            res_links = []
            # This is most probably broken now that for cell we have group links instead of groups
            # However the mer app is currently unused, so...
            resource_groups = self.resource_groups.order_by(*self.grp_order)

        res_dict = OrderedDict()
        for link in res_links:
            if link.resource.status == 'validee' or not public_site:
                res_dict.setdefault(link.resource.type_r, []).append(link)
        res_dict['grps'] = OrderedDict()
        for link in resource_groups:
            if link.catalog.grp_status != 'avalider' or not public_site:
                res_dict['grps'].setdefault(link.catalog.groupment or 'none', []).append(link)
        self._resources = res_dict
        return res_dict


THUMB_WIDTH = 440
THUMB_RATE = 2 / 3  # will give a height of 293 if width=440

def generate_thumbnail_for_pdf(file_field, storage):
    """
    Create a thumbnail for `file_field` and return the path (relative to storage root)
    to a generated png thumbnail for `file_field`.
    """
    out_name = os.path.join('resource_thumbs', f'{str(uuid.uuid4())}.png')
    with file_field.open('rb') as fh:
        with Image(file=fh) as im:
            # im.sequence[0] is the first PDF page
            thumb = Image(image=im.sequence[0]).convert('png')
    thumb.background_color = Color('white')
    thumb.alpha_channel = 'remove'
    thumb.crop(width=thumb.width, height=int(thumb.width * THUMB_RATE))
    thumb.resize(THUMB_WIDTH, int(THUMB_WIDTH * THUMB_RATE))
    buff = BytesIO()
    thumb.save(file=buff)
    thumb.close()
    buff.seek(0)
    storage.save(out_name, buff)
    return out_name


def generate_thumbnail_for_img(img_field, storage, width=None):
    width = width or THUMB_WIDTH
    out_name = os.path.join("resource_thumbs", f"{str(uuid.uuid4())}{Path(img_field.name).suffix.lower()}")
    with img_field.open("rb") as fh:
        with Image(file=fh) as img:
            with img.clone() as thumb:
                thumb.thumbnail(width, width)
                buff = BytesIO()
                thumb.save(file=buff)
                thumb.close()
    buff.seek(0)
    storage.save(out_name, buff)
    return out_name


def get_infomaniak_token(request, url):
    # Sample URL = 'https://player.vod2.infomaniak.com/share/1jhvl2uqezsd3'
    assert request.user.is_authenticated
    if settings.VOD_INFOMANIAK_TOKEN == 'xxxxx':
        return 'token-test'
    channel_id = 2124
    parts = url.split('/')
    share_id = parts[-1]
    token_url = f'https://api.vod2.infomaniak.com/api/pub/v1/channel/{channel_id}/share/{share_id}/token'
    resp = requests.post(token_url, json={}, headers={'Authorization': f'Bearer {settings.VOD_INFOMANIAK_TOKEN}'})
    resp_json = resp.json()
    if resp_json['result'] == 'error':
        return None
    return resp_json['data']


def get_infomaniak_videopath(url):
    channel_id = 2124
    parts = url.split('/')
    share_id = parts[-1]
    token_url = f'https://api.vod2.infomaniak.com/api/pub/v1/channel/{channel_id}/share/{share_id}'
    resp = requests.get(token_url, headers={'Authorization': f'Bearer {settings.VOD_INFOMANIAK_TOKEN}'})
    media_id = resp.json()['data']['target_id']
    token_url = f'https://api.vod2.infomaniak.com/api/pub/v1/channel/{channel_id}/media/{media_id}'
    resp = requests.get(token_url, headers={'Authorization': f'Bearer {settings.VOD_INFOMANIAK_TOKEN}'})
    data = resp.json()['data']
    return f"{data['folder']['path']}/{data['name']}"


def transform_resource_to_gallery(resource):
    if not any(f.is_image for f in resource.files.all()):
        return None  # Not a gallery

    with transaction.atomic():
        for im_file in sorted(resource.files.all(), key=lambda f: f.title):
            # Create a Resource for this ResourceFile
            new_res = resource.__class__.objects.create(
                title=im_file.title, updated=timezone.now(), status=resource.status,
                need_auth=resource.need_auth, validated=resource.validated,
                type_doc=['image'],
            )
            im_file.resource = new_res
            im_file.save()
            # Link the Resource to the gallery ressource
            resource.relations.model.objects.create(resource=resource, rtype='is_part_of', other=new_res)
    return resource


def auto_exposant(text):
    text = re.sub(r'(\d+)(er|re|e)', r'\1<sup>\2</sup>', text.replace(' Ier ', ' I<sup>er</sup> '))
    return re.sub(r'([XVI]+)e(\s|-|$)', r'\1<sup>e</sup>\2', text)
