import tablib
from datetime import date

from django.contrib import admin
from django.contrib.postgres.aggregates import StringAgg
from django.db.models import Count, Q
from django.forms import ModelForm
from django.http import HttpResponse

from .models import (
    Imagier, Imagier3, Resource, ResourceClick, ResourceContrib, ResourceFile,
    ResourceGroup, ResourceGroupTag, ResourcePerLink, ResourceRelation,
    ResourceSubmitter, SubTheme,
)
from .ontologies import TYPE_DOC_CHOICES, TYPE_DOC_MAIN, TYPE_RESOURCE
from .utils import XLSX_MIMETYPE


@admin.register(Imagier)
class ImagierAdmin(admin.ModelAdmin):
    list_display = ['nom', 'numero', 'caract', 'categorie']
    raw_id_fields = ['resource']
    search_fields = ['nom']


@admin.register(Imagier3)
class Imagier3Admin(admin.ModelAdmin):
    list_display = ['mot', 'no_imagier', 'type_img', 'phonetique']
    raw_id_fields = ['resource']
    search_fields = ['mot']


@admin.register(ResourceRelation)
class ResourceRelationAdmin(admin.ModelAdmin):
    raw_id_fields = ['resource', 'other']
    search_fields = ['description', 'url']


@admin.register(ResourcePerLink)
class ResourcePerLinkAdmin(admin.ModelAdmin):
    list_display = ['resource_or_catalog', 'content_type', 'object_id']
    raw_id_fields = ['catalog', 'resource']
    search_fields = ['resource__pk', 'catalog__pk', 'resource__title', 'catalog__title']

    def resource_or_catalog(self, obj):
        if obj.resource_id:
            return f"Ressource: {str(obj.resource)}"
        elif obj.catalog_id:
            return f"Catalogue: {str(obj.catalog)}"
        return "Non défini ?"


class ResourceRelationInline(admin.TabularInline):
    model = ResourceRelation
    fk_name = 'resource'
    raw_id_fields = ['other']
    extra = 1


class InCatalogListFilter(admin.SimpleListFilter):
    title = "dans un catalogue"
    parameter_name = "incat"

    def lookups(self, request, model_admin):
        return [
            ("1", "dans un catalogue"),
            ("0", "hors catalogues"),
        ]

    def queryset(self, request, queryset):
        if self.value() == "1":
            return queryset.annotate(num_cats=Count('resourcegrouplink')).filter(num_cats__gt=0)
        if self.value() == "0":
            return queryset.annotate(num_cats=Count('resourcegrouplink')).filter(num_cats=0)


class GalleryImgListFilter(admin.SimpleListFilter):
    title = "image de gallerie"
    parameter_name = "ingal"

    def lookups(self, request, model_admin):
        return [
            ("gal", "Galeries"),
            ("img", "Image de galerie"),
            ("0", "Ni galerie, ni image de galerie"),
        ]

    def queryset(self, request, queryset):
        if self.value() == "gal":
            return Resource.objects.galleries()
        if self.value() == "img":
            return queryset.annotate(
                count_ispart=Count('other_relations', filter=Q(other_relations__rtype='is_part_of'))
            ).filter(type_doc__contains=['image'], count_ispart__gt=0)
        if self.value() == "0":
            return queryset.annotate(
                count_ispart=Count(
                    'other_relations', filter=Q(other_relations__rtype='is_part_of')
                ),
                num_images=Count(
                    'relations', filter=Q(relations__other__type_doc__contains=['image'])
                )
            ).filter(Q(num_images=0) & (Q(count_ispart=0) | ~Q(type_doc__contains=['image'])))


@admin.register(Resource)
class ResourceAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'type_r', 'cantons']
    list_filter = [
        'type_r', 'partner', 'emedia', InCatalogListFilter, GalleryImgListFilter,
    ]
    search_fields = ['title', 'uuid', 'url', 'description', 'source']
    exclude = ['fts']
    actions = ['export_all']
    inlines = [ResourceRelationInline]

    @admin.action(description="Exporter les ressources sélectionnées")
    def export_all(self, request, queryset):
        data = tablib.Dataset()
        data.headers = [f.name for f in queryset.model._meta.fields]
        for line in queryset.values_list():
            data.append(line)
        response = HttpResponse(data.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; bdper_ressources_%s.xls' % (
            date.today())
        return response


class ResourceInlineForm(ModelForm):
    class Meta:
        model = ResourceGroup.resources.through
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.group_id:
            self.fields['subthemes'].queryset = SubTheme.objects.filter(group=self.instance.group)


class ResourceInline(admin.TabularInline):
    model = ResourceGroup.resources.through
    raw_id_fields = ('resource',)
    extra = 1
    form = ResourceInlineForm


@admin.register(ResourceGroup)
class ResourceGroupAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'objectif', 'grp_status')
    ordering = ('title',)
    search_fields = ('title',)
    list_filter = ['grp_status']
    inlines = [ResourceInline]
    actions = ['export_all']

    @admin.action(description="Exporter les catalogues sélectionnés")
    def export_all(self, request, queryset):
        typ_filters = {
            f'typer_{typ_code}': Count('resources', filter=Q(resources__type_r=typ_code))
            for typ_code, _ in TYPE_RESOURCE
        }
        doc_filters = {
            f'typed_{doc_code}': Count('resources', filter=Q(resources__type_doc__contains=[doc_code]))
            for doc_code in TYPE_DOC_MAIN
        }

        queryset = queryset.annotate(
            notices=Count('resourcegrouplink'),
            per_link=Count('resourceperlink'),
            sources=StringAgg('resources__source', delimiter=";", distinct=True),
            **typ_filters,
            **doc_filters,
        ).prefetch_related('subthemes')
        data = tablib.Dataset()
        #Relations [Nombre]	Plan d'études [Renseigné = 1 ; Non renseigné = 0]

        data.headers = [
            'Identifiant', 'Date création', 'Date dernière modif', 'Titre', 'Sous-titre',
            'Sous-titre secondaire', 'Regroupement', 'Description', 'Domaine', 'Objectif',
            'Degré scolaire', 'Lien MER', 'Statut de validation', 'Sous-thème', 'Notices',
            'Source',
        ] + [
            f'Type ressource: {typ_name}' for _, typ_name in TYPE_RESOURCE
        ] + [
            f'Type doc: {dict(TYPE_DOC_CHOICES)[typ_name]}' for typ_name in TYPE_DOC_MAIN
        ] + ['Plan d’études']
        for cat in queryset:
            data.append([
                cat.pk, cat.created.date(), cat.updated.date(), cat.title, cat.subtitle,
                cat.subtitle2, cat.groupment, cat.description, cat.domain, cat.objectif,
                cat.get_levels_display(), cat.mer_url, cat.get_grp_status_display(),
                len(cat.subthemes.all()) and 'oui' or 'non', cat.notices,
                cat.sources,
            ] + [
                getattr(cat, f'typer_{typ_code}') for typ_code, _ in TYPE_RESOURCE
            ] + [
                getattr(cat, f'typed_{typ_code}') for typ_code in TYPE_DOC_MAIN
            ] + [bool(cat.per_link)])
        response = HttpResponse(data.xlsx, content_type=XLSX_MIMETYPE)
        response['Content-Disposition'] = f"attachment; bdper_catalogues_{date.today().strftime('%Y%m%d')}.xlsx"
        return response


@admin.register(ResourceSubmitter)
class ResourceSubmitterAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'resource')
    raw_id_fields = ('resource',)


@admin.register(ResourceFile)
class ResourceFileAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'title')
    raw_id_fields = ('resource',)


@admin.register(ResourceClick)
class ResourceClickAdmin(admin.ModelAdmin):
    list_display = ('resource_id', 'stamp')
    raw_id_fields = ('resource',)


admin.site.register(ResourceContrib)
admin.site.register(ResourceGroupTag)
admin.site.register(SubTheme)
