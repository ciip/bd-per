# Remplacer strong dans les titres
# ********************************
from pper.models import Contenu
conts = Contenu.objects.filter(cellule__type_c__lt=0, texte__contains="<strong>")
for cont in conts:
    new_txt = cont.texte.replace("<strong>", "")
    new_txt = new_txt.replace("</strong>", "")
    used_in_str  = ", ".join([str(obj) for obj in cont.usage()])
    print(u"Remplacer (uid %d, %s):\n%s\n par\n%s" % (cont.uid, used_in_str, cont.texte, new_txt))
    res = raw_input(u"Taper 'o' pour accepter ('q' pour quitter): ")
    if res == u"o" or res == u"tout":
        cont.texte = new_txt
        cont.save()
    elif res == "q":
        break

# Marquer les textes généraux avec les lexiques
# *********************************************
from pper.models import Domain, Cellule, Lexique

domain = Domain.objects.get(id=??)
dom_tabl_ids = [tg.id for tg in domain.com_gen.all()]
tabl_cells = Cellule.objects.filter(tableau__id__in=dom_tabl_ids)
terms = Lexique.objects.filter(domain=domain)
Lexique.mark(terms, tabl_cells, confirm=True)

# Exporter tout le contenu texte dans un fichier
# *********************************************
from pper.models import Contenu
from pper.filters import FilterChain, TxtFilter
f = open("./per_content.txt", "w")
filters = FilterChain([TxtFilter()])
for cont in Contenu.objects.all():
    txt = cont.render(filters, {})
    f.write(txt.encode('utf-8') + "\n")
f.close()
