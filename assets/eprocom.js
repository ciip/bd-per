import htmx from 'htmx.org/dist/htmx.js';
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import mediumZoom from 'medium-zoom';

htmx.onLoad(function(target) {
    if (target.querySelectorAll('textarea').length > 0) {
        if (tinymce.activeEditor) tinymce.activeEditor.destroy();
        tinyMCE.init({
            selector: "textarea",
            theme: "silver",
            menubar: false,
            plugins: 'link code paste lists table image',
            paste_as_text: true,
            target_list: [
                {title: 'None', value: ''}
            ],
            image_list: '/eprocom/image_list/',
            image_prepend_url : '/static/img',
            relative_urls : false,
            remove_script_host : true,
            document_base_url : 'https://bd-per.plandetudes.ch/',
            toolbar: 'h3 bold italic superscript bullist numlist | alignleft aligncenter alignjustify alignright | link unlink | table tabledelete | image | undo redo code',
            entity_encoding: "raw",
            height: 400
        });
        target.querySelector('button[type=submit]').addEventListener('click', (ev) => {
            tinymce.triggerSave();
        });
    }
});

htmx.on("htmx:beforeRequest", function(evt) {
    /* Implement collapse for item <li>s. */
    const detail = evt.detail.target;
    if (detail && detail.classList.contains('item-detail') && detail.textContent.length > 0) {
        evt.preventDefault();
        detail.innerHTML = '';
    }
});

document.addEventListener("DOMContentLoaded", () => {
    document.querySelectorAll('.menuheader').forEach(item => {
        item.addEventListener('click', (ev) => {
            let submenu = ev.currentTarget.closest('li').querySelector('.submenu');
            if (submenu) submenu.classList.toggle("open");
        });
    });
    // Find active menu item
    document.querySelectorAll('.leftmenu a').forEach(link => {
        if (link.getAttribute('href') == location.pathname) {
            link.parentNode.classList.add('active');
        }
    });
    // Ensure variante item target is visible
    document.querySelectorAll('.variante-link').forEach(item => {
        item.addEventListener('click', (ev) => {
            const target = document.querySelector(ev.target.hash);
            if (target.offsetHeight == 0) {
                target.closest('details').open = true;
            }
        });
    });
    // Show button confirmation messages, if any
    document.querySelectorAll('button').forEach(btn => {
        if (btn.dataset.confirm) {
            btn.addEventListener('click', (ev) => {
                if (!confirm(ev.target.dataset.confirm)) {
                    ev.preventDefault();
                    return false;
                }
            });
        }
    });
    // Initialize tooltips
    tippy('.tooltip');

    // Initialize zoomable images
    mediumZoom('div.item-detail img');

    // Highlight hash target
    const hashMark = window.location.hash;
    if (hashMark.length) {
        const hashTarget = document.querySelector(hashMark);
        if (!hashTarget) return;
        const title = hashTarget.querySelector('.item-title') || hashTarget.querySelector('.material-title') || hashTarget.querySelector('summary');
        if (title) { title.classList.add('highlight'); }
        const details = hashTarget.querySelector('details.first-level');
        const onFrancais = document.querySelectorAll('.domain-L').length > 0;
        if (details && !onFrancais) details.open = true;
        if (title && title.scrollIntoView) { title.scrollIntoView(); }
        // If hash-linking to a closed details, open it
        if (hashTarget.tagName == 'DETAILS' && !hashTarget.open) hashTarget.open = true;
    }
});
