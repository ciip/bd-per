const path = require('path');

module.exports = {
  entry: {
    main: './assets/main.js',
    eprocom: './assets/eprocom.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'assets/dist'),
    publicPath: '/static/',  // Match Django STATIC_URL
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      }
    ]
  }
};
