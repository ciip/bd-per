import getpass
from fabric import task
from invoke import Context, Exit

MAIN_HOST = 'bdper.plandetudes.ch'
pyexec_path = '/var/www/virtualenvs/bdper/bin/python'


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname='bdper'):
    """ Dump a remote database and load it locally """
    local = Context()

    def exist_local_db(db):
        res = local.run('psql --list', hide='stdout')
        return db in res.stdout.split()

    tmp_path = f'/tmp/{dbname}.dump'

    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo(f'pg_dump --no-owner --no-privileges {dbname} -Fc > {tmp_path}', user='postgres')
    conn.get(tmp_path, None)

    if exist_local_db(dbname):
        rep = input(f'A local database named "{dbname}" already exists. Overwrite? (y/n)')
        if rep == 'y':
            local.run(f"dropdb {dbname}")
        else:
            raise Exit("Database not copied")

    local.run(f'createdb {dbname}')
    local.run(f'pg_restore --no-owner --no-privileges -d {dbname} {dbname}.dump')
    # Clean files
    rep = input('Delete downloaded SQL file? (y/n)')
    if rep == 'y':
        local.run(f'rm {dbname}.dump')
    else:
        print(f"The database can be found in {tmp_path}")


def set_server_maintenance(conn, switch, project="bd-per"):
    """ Pass the server into or out of maintenance mode
        switch is "on" or "off" """

    with conn.cd(f'/var/www/{project}'):
        if switch == "on":
            conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" apps/common/wsgi.py')
        elif switch == "off":
            conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" apps/common/wsgi.py')
        else:
            raise Exit("The parameter should be either on or off")
        conn.run('touch apps/common/wsgi.py')


@task(hosts=[MAIN_HOST])
def deploy(conn, project="bd-per"):
    with conn.cd(f'/var/www/{project}'):
        #conn.run('git stash && git pull && git stash pop')
        conn.run('git pull')
        conn.run('npm install')
        conn.run('npm run build')
        set_server_maintenance(conn, 'on', project)
        conn.run('%s manage.py migrate' % pyexec_path)
        conn.run('%s manage.py collectstatic --noinput' % pyexec_path)
        conn.run('touch apps/common/wsgi.py')
        set_server_maintenance(conn, 'off', project)


@task(hosts=[MAIN_HOST])
def clone_to_testdb(conn):
    db_from = 'bdper'
    db_to = 'bdper_test'
    tmp_path = f'/tmp/{db_from}.dump'
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo(f'pg_dump {db_from} -Fc > {tmp_path}', user='postgres')
    conn.sudo(f'dropdb {db_to}', user='postgres')
    # recreate
    conn.sudo(f'psql -c "CREATE DATABASE {db_to} OWNER=bdper;"', user='postgres')
    # load dump
    conn.sudo(f'pg_restore -d {db_to} {tmp_path}', user='postgres')
