import argparse
import csv
import os
import re
import sys
from collections import Counter
from pathlib import Path

os.environ['DJANGO_SETTINGS_MODULE'] = 'common.settings'
project_dir = Path(__file__).resolve().parent.parent
sys.path.insert(0, str(project_dir / 'apps'))

path_model = '/var/lib/awstats/awstats{:0>2}{}.bdper-ressources.txt'


def get_resources():
    from ressource.models import Resource
    return {res.pk: res for res in Resource.objects.filter(emedia=True)}


def get_month_data(year, month, resources):
    """
    Return a dict of stats in the form:
        {'<res_id>': <num_download>, …}
    """
    stat_path = Path(path_model.format(month, year))
    if not stat_path.exists():
        print("Unable to read awstats file %s" % stat_path)
    # Read lines between BEGIN_DOWNLOADS and END_DOWNLOADS
    with stat_path.open(mode='r') as fh:
        dl_data = []
        in_section = False
        for line in fh:
            if line.startswith('BEGIN_DOWNLOADS'):
                in_section = True
                continue
            if line.endswith('END_DOWNLOADS'):
                break
            if in_section:
                dl_data.append(line)

    month_data = {}
    for line in dl_data:
        #/uploads/ressources/3595/kit_stereotypes.pdf 1236 124 2215441480
        m = re.match(r"/uploads/ressources/(\d+)/[^ ]+ (\d+) .*", line)
        if not m:
            #print("Unable to match line %s" % line)
            continue
        res_id = int(m.groups()[0])
        if res_id in resources:
            dl_num = int(m.groups()[1])
            month_data[res_id] = dl_num
    return month_data


def as_bool(val):
    return 'x' if val is True else ''


def main(year, month):
    import django
    django.setup()

    months = [str(m) for m in range(1, 13)] if month == 'all' else [month]
    resources = get_resources()
    count = Counter()
    for month in months:
        count.update(get_month_data(year, month, resources))
    stat_path = 'stats{}-{}.csv'.format(year, month)
    with open(stat_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['ID de ressource', 'Titre', 'Cinéma?', 'SDM?', 'Nb de téléch.'])
        for res_id, dl_num in count.items():
            writer.writerow([
                res_id, resources[res_id].title,
                as_bool(resources[res_id].cinema), as_bool(resources[res_id].sdm),
                dl_num
            ])
    print("Stats written to %s" % stat_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('month', help='Month as 2019-11 (or 2019-all)')
    args = parser.parse_args()
    if not '-' in args.month:
        print("Missing '-' in month")
        sys.exit(1)
    year, month = args.month.split('-')
    main(year, month)
