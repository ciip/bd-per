#!/usr/bin/env python
import os, sys

project_dir = os.path.dirname(os.path.abspath(__file__))
apps_dir = os.path.join(project_dir, "apps")
sys.path.insert(0, apps_dir)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "common.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
